Synaptix.js accepts several environment variables

#### Synaptix Legacy
- `USE_GRAPHQL_RELAY` (**0**|1) : Force GraphQL endpoint to fully comply to [Relay]()
- `USE_LEGACY_SYNAPTIX_FORMAT` (**0**|1) : Force AMQP publishers/consumers to comply with legacy Synaptix (v1) format.

#### Network layer

- `RABBITMQ_LOG_LEVEL` (**NONE**|DEBUG) : Print AMQP publishers/consumers messages.
- `RABBITMQ_RPC_TIMEOUT` (5000) : AMQP RPC timeout.

#### Indexation
- `INDEX_DISABLED` (**0**|1): Disable index. Force data to be fetched in graphstores (Triplestore or Property graph)

!!! warning
    Breaking change in version `4.4.0-beta.2`. This variable was called `DISABLE_INDEX` in previous versions.
    
- `USE_INDEX_VERSION` (**V6**|V2|LEGACY): Choose index AMQP publisher version to comply with index used in Synaptix (LEGACY is V2).


#### SSO

- `OAUTH_REGISTRATION_DISABLED` (**0**|1) : Is SSO account registration disabled ? 
- `OAUTH_REALM` : SSO Realmn  name
- `OAUTH_AUTH_URL` : SSO authentication endpoint.
- `OAUTH_TOKEN_URL` :  SSO token validation endpoint.
- `OAUTH_LOGOUT_URL` : SSO logout endpoint.
- `OAUTH_REALM_CLIENT_ID` : SSO client ID.
- `OAUTH_REALM_CLIENT_SECRET` : SSO client secret.
- `OAUTH_ADMIN_USERNAME` : SSO admin username
- `OAUTH_ADMIN_PASSWORD` : SSO admin password
- `OAUTH_ADMIN_TOKEN_URL` : SSO admin Token endpoint
- `OAUTH_ADMIN_API_URL` :  SSO admin API endpoint

### CORS

- `CORS_DISABLED` : Disable Cross-Origin Request Sharing.

### Access Control Layer

- `ADMIN_USER_GROUP_ID` : mnx:UserGroup URI that corresponds to Administrators group.