## Overview

Synaptix.js tries to improve and simplify data manipulation by mostly automatizing things.

### Graph mutations

#### Creating an object

Creating an object required a related Mutation definition and Mutation resolvers described in [datamodel section](./datamodel/graphql_schema.md#graphQL-mutations)

To simplify this boring process, Synaptix.js exposes several [helpers](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/graphql/schema/types/generators.js) to generate these objects.

- `#!js generateCreateMutationDefinitionForType` for generating a mutation definition given a type name.
- `#!js generateCreateMutationResolverForType` for generating a mutation resolver given a type name.

To continue our [Book example definitions](./datamodel/graphql_schema.md/#graphql-types),  both would be used like so :

```js
import {
  generateCreateMutationDefinitionForType,
  generateCreateMutationResolverForType
} from "@mnemotix/synaptix.js";

export let CreateBookMutation = generateCreateMutationDefinitionForType('Book');         
export let CreateBookMutationResolvers = generateCreateMutationResolverForType('Book'); 
```

Adding theses two objects to your [DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) instance automatically adds the possibity to create `Book` instances in your datastore with the following mutation format.

```graphql
mutation {
  createBook(
    input: {                        # (1)
      objectInput: {                # (2)
        # Book input properties       
      }
    }
  ) {
    createdEdge {                   # (3)
      node { 
        __typename  # Book              
        # Book properties            
      }
    }
  }
}
```

- (1) To remain compliant with Relay, Synaptix.js demands an `input` root parameter.
- (2) To remain generic accross every mutations;  Synaptix.js demands an `objectInput` property that aims to be the related type input. In our [Book example](./datamodel/graphql_schema.md/#graphql-types) a `BookInput`
- (3) To remain compliant with Connections definition and optionaly Relay, a create mutation returns an edge instead of  directly the created object.

##### Deep object input parsing

!!! note
    Note that Synatix.js shipped a deep `objectInput` parsing. That is to say in this example :
    
    ```graphql
    mutation {
      createBook(
        input: {                       
          objectInput: {             
            title: "My book title"       # (1)
            pageCount: 130               # (2)
            authorInput: {               # (3)
              id: "person/12345"
            }
            chapterInputs: [{            # (4)
              title: "Chapter 1"
            }, {
              title: "Chapter 2"
            }], 
            editorInput: {              #  (5)
              inheritedTypename: "Organization"
              id: "org/45678"
            }
          }
        }
      ) {
        createdEdge {                  
          node { 
            # Book properties            
          }
        }
      }
    }
    ```
    
    - (1) Creating a i18n label
    - (2) Creating a literal
    - (3) Linking to existing object
    - (4) Creating and linking linked objects
    - (5) This handle the case of input that refers to interfaces. In this example, `editorInput` refers to `AgentInterface` that can be inherited in `Person` or `Organization` type. If `inheritedTypename` is not provided, Synaptix.js will throw a `NOT_INSTANTIABLE_OBJECT_INPUT` Error.

#### Updating an object

Updating an object required a related Mutation definition and Mutation resolvers described in [datamodel section](./datamodel/graphql_schema.md#graphQL-mutations)

To simplify this boring process, Synaptix.js exposes several [helpers](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/graphql/schema/types/generators.js) to generate these objects.

- `#!js generateUpdateMutationDefinitionForType` for generating a mutation definition given a type name.
- `#!js generateUpdateMutationResolverForType` for generating a mutation resolver given a type name.

To continue our [Book example definitions](./datamodel/graphql_schema.md/#graphql-types),  both would be used like so :

```js
import {
  generateUpdateMutationDefinitionForType,
  generateUpdateMutationResolverForType
} from "@mnemotix/synaptix.js";

export let UpdateBookMutation = generateUpdateMutationDefinitionForType('Book');         
export let UpdateBookMutationResolvers = generateUpdateMutationResolverForType('Book'); 
```

Adding theses two objects to your [DataModel](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/Datamodel.js) instance automatically adds the possibity to update `Book` instances in your datastore with the following mutation format.

```graphql
mutation {
  updateBook(
    input: {                        # (1)
      objectId: "id_to_update"      # (2)
      objectInput: {                # (3)
        # Book input properties        
      }
    }
  ) {
    updatedObject {                 # (4)
      __typename  # Book        
      # Book properties
    }
  }
}
```

- (1) To remain compliant with Relay, Synaptix.js demands an `input` root parameter.
- (2), (3) To remain generic accross every mutations,  Synaptix.js demands a tuple `objectId`, `objectInput` property that aims to be the related type input. In our [Book example](./datamodel/graphql_schema.md/#graphql-types) a `BookInput`
- (3) To remain generic accross every mutations, update mutation returns a `updatedObject` property as root in payload.

##### deep-object-input-deletion

!!! note "Note 1"
    Note that Synatix.js shipped a deep `objectInput` parsing. See previous [chapter note](./manipulating_data.md#deep-object-input-parsing) for more information.
        
!!! note "Note 2"
    To go deeper with preceding note, `objectInput` can handle data properties deletion and single link deletion.
    
     - To remove a literal or a localized label, you just need to pass "null" as value of desired key. For example to remove a person firstName, you can pass :
    
    ```graphql
    objectInput: {
      firstName: null
    }
    ```
    
     - To remove a 1:1 relationship link you just need to pass `null` as value of desired link input OR `null` as `id` value. For example to remove a book author (supposed that book author's link is tagged as 1:1, you can pass :
    
    ```graphql
    # Pass an input as null value.
    objectInput: {
      authorInput: null
    }
    
    # Pass an id input as null value.
    objectInput: {
      authorInput: {
        id: null
      }
    }
    ```
    
      
   
#### Removing an object

As GraphQL needs only an ID to remove an object, Synaptix.js ships a simple mutation called `#!graphql removeObject`

```graphql
type RemoveObjectPayload {
  deletedId: ID
}

type RemoveObjectInput {
  objectId: ID!
  permanent: Boolean
}

extend type Mutation {
  removeObject(input: RemoveObjectInput!): RemoveObjectPayload
}
```

!!! note
    Note the `permanent` argument input in `RemoveObjectInput`. This is helpful in the "Don't remove anything, just disable it" pattern. But to respect privacy and RGPD reglementation, we need a way to force data to be permanenlty deleted.
    
#### Removing a link between two objects

As GraphQL needs only two IDs to remove a link between two objects, Synaptix.js ships a simple mutation called `#!graphql RemoveEdge`

```graphql
type RemoveEdgePayload {
  deletedId: ID
}

input RemoveEdgeInput {
  objectId: ID!
  targetId: ID!
}

extend type Mutation{
  removeEdge(input: RemoveEdgeInput!): RemoveEdgePayload
}
```

### Graph middlewares

Synaptix.js implements the mechanism of Graph manipulation middleware through a [GraphMiddleware](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodules/middlewares/graph/GraphMiddleware.js) abstract class.

That class exposes 3 methods that are called after before every graph action that enable to manipulate object parameters.

- `#!js {object} objectInput` That is the raw representation of data properties of manipulating object.
- `#!js {Link[]} [links]` That is the list of [Link](https://gitlab.com/mnemotix/synaptix.js/blob/mastersrc/server/datamodel/toolkit/definitions/LinkDefinition.js) of manipulating object.

```js
export class GraphMiddleware {
  /** @type {SynaptixDatastoreSession} */
  _datastoreSession;

  /**
   *
   * @param {SynaptixDatastoreSession} datastoreSession
   */
  attachSession(datastoreSession){
    this._datastoreSession = datastoreSession;
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]} [links]
   * @returns {NodeCrudMiddlewareProcessedParameters}
   */
  processNodeCreationParameters({modelDefinition, objectInput, links}){
    return {objectInput, links};
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]} [links] -  List of links
   * @returns {NodeCrudMiddlewareProcessedParameters}
   */
  processNodeUpdateParameters({modelDefinition, objectInput, links}){
    return {objectInput, links};
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]} [links] -  List of links
   * @returns {NodeCrudMiddlewareProcessedParameters}
   */
  processNodeDeletionParameters({modelDefinition, objectInput, links}){
    return {objectInput, links};
  }
}
```

!!! note
    Synaptix.js implements to ready to use graph middlewares.

    - [LegacyGraphMiddleware](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodules/middlewares/graph/LegacyGraphMiddleware.js) that is a way to update `creationDate`, `lastUpdate`, `_enabled` data properties.
    - [MnxActionGraphMiddleware](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodules/middlewares/graph/MnxActionGraphMiddleware.js) that is a way to implement the `MnxAction` data model to track object activity according to [PROV-O](https://www.w3.org/TR/prov-o/) ontology.
    - [MnxAclGraphMiddleware](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodules/middlewares/graph/MnxAclGraphMiddleware.js) that is a way to implement the `MnxAcl` data model to securize objects according to [S4AC](http://ns.inria.fr/s4ac/) ontology.
      

To include a graph middleware in the application, simply declare it in your [SynaptixDatastoreRdfAdapter](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datastores/SynaptixDatastoreRdfAdapter.js) instantiation.

```js
import {
  SynaptixDatastoreRdfAdapter,
  MnxActionGraphMiddleware,
  MnxAclGraphMiddleware
} from '@mnemotix/synaptix.js';

import {dataModel} from './dataModel';
import {networkLayer} from './networkLayer';
import {ssoApiClient} from './ssoApiClient';
import {MyGraphMiddleware} from 'MyGraphMiddleware';

const datastoreAdapter = new SynaptixDatastoreRdfAdapter({
  networkLayer,
  modelDefinitionsRegister: dataModel.generateModelDefinitionsRegister(),
  ssoApiClient,
  graphMiddlewares: [
    new MnxActionGraphMiddleware(),
    new MnxAclGraphMiddleware(),
    new MyGraphMiddleware()
  ]
});
```

### Form input validation

To complete...