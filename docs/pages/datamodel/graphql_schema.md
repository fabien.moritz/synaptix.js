## Mapping RDF type in GraphQL

### Overview

After having described RDF type in related [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js) class, we can start to map it in GraphQL related types.


### GraphQL types

#### Definitions 

GraphQL type are heredoc string formatted in GraphQL language. Synaptix.js provides convenient helpers to speedup description :

Let's start with an example. Take a Book, Author and Chapter types.

```js
import {
  connectionArgs,
  generateConnectionForType,
  EntityInterfaceProperties,
  EntityInterfaceInput,
  filteringArgs
} from "@mnemotix/synaptix.js";

export let BookType = `
""" This is a book type """ 
type Book implements EntityInterface {               # (1)
  ${EntityInterfaceProperties}                       # (2)

  """ Title """                                      # (3)
  title: String
  
  """ pageCount """
  pageCount: Int 

  """ Related author """
  author: Author
  
  """ Related chapters """
  chapters: ChapterConnection
}
  
${generateConnectionForType("Book")}                 # (4)

""" This is a book input type """   
input BookInput {
  ${BookDefinition.generateGraphQLInputProperties()} # (5)
}

extend type Query{
  """ Search for books """
  books(                                             # (6)
    ${connectionArgs} 
    ${filteringArgs}
  ): BookConnection  

  """ Get a book """
  book(id:ID): Book
}
`;
```

In this example let's take a looks to the specific points :

- (1) Because BookDefinition declares [EntityDefinition](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/mnx-common/definitions/EntityDefinition.js) as parent definition, Book type implements EntityInterface automatically added by Synaptix.js 
- (2) Following (1) and following GraphQL standard Book type must redeclare EntityInterface properties exported in `EntityInterfaceProperties`
- (3) Declare the Book type properties normally
- (4) Synaptix.js provides `generateConnectionForType("[GraphQLType]")` helper to generate graphQL type related connection types. (BookConnection and BookEdge)
- (5) Synaptix.js provides `ModelDefinitionAbstract::generateGraphQLInputProperties()` helper to generate graphQL input properties related to a definition AND parent definitions (since version `4.5.8`). Note that only links declaring a `relatedInputName` are took into account.
- (6) Extends root Query to search books and get specific book. Synaptix.js provided `connectionArgs` and `filteringArgs` to add respectively pagination arguments and search filtering/sorting arguments.

#### Resolvers

These preceding GraphQL definitions must be provided with resolvers in a formt defined in [graphql-tools](https://github.com/apollographql/graphql-tools) package.

```js
import {
  generateTypeResolvers,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver,
  generateConnectionResolverFor
} from "@mnemotix/synaptix.js";

export let BookResolvers = {
  Book:{
    ...generateTypeResolvers("Book"),                         // (1)
    pageCount: (book) => book.pageCount,                      // (2)
    title: getLocalizedLabelResolver                          // (3)
      .bind(this, BookDefinition.getLabel('title'))               
  },
  Query:{
    book: getObjectResolver.bind(this, PersonDefinition),     // (4)
    books: getObjectsResolver.bind(this, PersonDefinition)    // (5)
  },
  ...generateConnectionResolverFor("Person")                  // (6)
};
```

- (1) Synaptix.js provides `generateTypeResolvers` helper to generate basic stuff like `id` resolver.
- (2) Non i18n data property are available in the `book` object (instance of [Model](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/toolkit/models/Model.js)). According to `graphql-tools` documentation
- (3) Synaptix.js provides `getLocalizedLabelResolver(LabelDefinition, object)` helper to retrieve i18n data property.
- (4) Synaptix.js provides `getObjectResolver(ModelDefinitionAbstract, {id})` helper to retrieve an instance given a model definition and an `id`
- (4) Synaptix.js provides `getObjectsResolver(ModelDefinitionAbstract)` helper to search instances given a model definition


### GraphQL mutations

Mutations work like Types in the way they are defined. Just declare it like so :

```js
export let MyMutation = `
"""MyMutation payload"""
type MyMutationPayload {
  ...
}

"""MyMutation input"""
input MyMutationInput {
  ...
}

extend type Mutation{
  myMutation(input: MyMutationInput!): MyMutationPayload
}
`;
```

And it's resolver :

```js
export let MyMutationResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {object} input - Mutation input arguments
     * @param {SynaptixDatastoreSession} synaptixSession - Instance of SynaptixSession
     */
    MyMutation: async (_, {input}, synaptixSession) => {
      // Apply your mutation here.
    },
  },
};
```

### GraphQL suscriptions

Subscriptions work mostly like Types and Mutations but need some configuration before being used.

First take a look at this documentation [graphql-subscriptions](https://github.com/apollographql/graphql-subscriptions). 

The way to use it in Synaptix.js will come soon...

