Synaptix.js is basically a framework to manipulate RDF-styled data. It provides an object-oriented modelisation format and related logics that will be used as a middleware between a GraphQL server and a RDF Triple Store.

This modelisation is used to expose model objects described in [MNX model documentation](https://mnemotix.gitlab.io/mnx-models/)  ![MNX generic model](images/mnx-model.jpg) that is divided into several fragments :

 - **MnxCommon**: Provided a simple `mnx:Entity` that is basically an extension of `prov:Entity`.
 - **MnxContribution** : An model to describe the ways for a person to contribute in the graph. This is an extension of [PROV-O](https://www.w3.org/TR/prov-o/)
 - **MnxGeo** :  A model to describe geographic stuffs. This is linked to [Geonames](http://www.geonames.org/ontology/documentation.html).
 - **MnxTime** : A model to describe temporal stuffs.
 - **MnxAcl** : A model to describe security layer based on [S4AC](http://ns.inria.fr/s4ac/v2/s4ac_v2.html).
 - **MnxAgent** : A model to describe a social network based on [FOAF](http://xmlns.com/foaf/spec/)
 - **MnxProject** : An ontology to describe temporal project management.
 - **MnxSkos** : An extension of [SKOS](https://www.w3.org/TR/2008/WD-skos-reference-20080829/skos.html).
 
The way to use them is described in following sections.

    
    
    
