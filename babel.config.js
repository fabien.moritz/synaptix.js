module.exports =  function(api) {
  const presets = [];

  if(api.env('test')){
    presets.push(["@babel/env", {
      targets: {
        node: 'current',
      }
    }]);
  } else {
    api.cache(false);
    presets.push(["@babel/env", {
      targets: {
        node: 'current'
      },
      modules: process.env.ES6_MODULES ? false : "cjs",
    }]);
  }

  const plugins = [
    ["@babel/transform-named-capturing-groups-regex"],
    ["@babel/proposal-decorators", { "legacy": true }],
    ["@babel/proposal-class-properties", { "loose" : true }],
    ["@babel/proposal-object-rest-spread"],
    ["@babel/proposal-optional-chaining"]
  ];

  let ignore = [];
  if (process.env.NODE_ENV !== 'test') {
    ignore = ignore.concat([
      /__tests__/,
      /__integrations__/,
      /__mocks__/,
      /__jsonSchemas__/
    ]);
  }


  return {
    presets,
    plugins,
    ignore,
    sourceMaps: "inline"
  };
};

