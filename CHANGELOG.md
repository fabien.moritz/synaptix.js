## [4.5.16] - 4 february 2020

### FIXED

- SSO user creation/login handle lowercase usernames. (Thanks to Hervé)

## [4.5.15] - 10 january 2020

### ADDED

- Added `getObjectsCount` resolver.

### FIXED

- Enforced isAuthenticatedRule by ensuring that logged user has not been unregistered or disabled between the next token session refresh.
- Updated mnx-project datamodel.

## [4.5.14] - 13 december 2019

### FIXED

- Fix call getLinkedObjectFor with fragments (Thanks to Hervé)

## [4.5.13] - 12 december 2019

### ADDED

- Add `FragmentDefinition` mechanism to require missing extra properties in case of InlineFragment GraphQL query. (thanks to Hervé)
- Add Sorting mechanism in `GraphControllerService::getNodes()` request.
- Add `PropertStep` in `LinkPath` to use in `PropertyDefintionAbstract` (`LiteralDefinion`, `LabelDefinition`) that enable to create indirect data properties.

### IMPROVED

- Lighten `GraphControllerService` class by extracting SPARQL helpers in dedicated files.

## [4.5.12] - 3 december 2019

### ADDED

- Add `LinkPath` construction mechanism to improve `GraphControllerService::getNodes()` request.
- Add `SynaptixDatastoreSession::getObjectsFilteredByLinkPath()` and `SynaptixDatastoreSession::getObjectFilteredByLinkPath()` to get object filtered by `LinkPath`

### CHANGED

- Improve and debug pagination mechanism in `GraphControllerService`

## REMOVED

- Removed the `graphql-relay` dependency.

## DEPRECATED

- Direct use of `connectionFromArray` is deprecated in resolvers (but retro-compatibity ensured). Use `SynaptixDatastoreSession::wrapObjectsIntoGraphQLConnection()` instead.

## [4.5.11] - 29 november 2019

### ADDED

- Add missing application `GraphControllerService::applyRequestNodeFiltersMiddlewares()` when requesting a single node by id.
- Add a `createdObject` field in create mutation payload generator.

## [4.5.10] - 28 november 2019

### ADDED

- Added a `GraphMiddleware::processNodeRequestFilters()` to add SPARQL filters while requesting nodes. 
- Added a `MnxActionGraphMiddleware::processNodeRequestFilters()` to ensure that not `mnx:Deletion` action instance is linked to requested nodes.
- Implemented `GraphControllerService::applyRequestNodeFiltersMiddlewares()` to add request filters when
 - Requesting a list of nodes (or just count them)
 - Requesting is a node exists (It might exists in the store but have a deletion flag such as `mnx:Deletion` action)
 - Requestion if two nodes are linked together (Targetted link of one of step link might exists in the store but have a deletion flag such as `mnx:Deletion` action)

### FIXED

- Fixed `isCreatorRule` rule to take object field query into account.
- Fixed unhandled exception handling in `replaceFieldValueIfRuleFailsMiddleware`
- Fixed `getLinkedObjectsCountResolver` and `SynaptixDatastoreRdfSession::getLinkedObjectsCountFor()` that counted wrong.



## [4.5.9] - 24 november 2019

### ADDED

- Added a `isRequired` property to `LiteralDefinition` and `LabelDefinition`. This improves SPARQL queries by removing the "OPTIONAL" clause in "WHERE" request part.
- Added `isCreatorRule` shield rule and `isLoggedUserObjectCreator` global helper to check that a logged user is the creator of an object (ie: mnx:Entity instance).

### FIXED

- Upgrade `graphql-shield` version that fixes the issue that ignore rules provided from external librairies. [@see fix here](https://github.com/maticzav/graphql-shield/commit/9a08e3e#diff-e1495d267619047a7cca5cfe8f692729L12). 
- Fixed missing `await` call for getting logged UserAccount ID in `MnxActionGraphMiddleware`. `mnx:Action` now have a working `prov:wasAttributedTo` object property.

## [4.5.8] - 19 november 2019

### BREAKING

- `ModelDefinitionAbstract::generateGraphQLInputProperties()` now generates parent definitions input if `excludeInheritedInputProperties` parameter is not truthy. This can break thing if in your project code you implements things like :

```js
import {
  EntityInterfaceInput,
} from "@mnemotix/synaptix.js";

export let BookInput = `
input BookInput {
  ${EntityInterfaceInput}                              
  ${BookDefinition.generateGraphQLInputProperties()}
}
`;
```

Will generate errors during schema compilation like `Field "BookInput.id" can only be defined once.`.

Two solutions here : 

- Either removing `${EntityInterfaceInput}` line.
- Or modifiying `${BookDefinition.generateGraphQLInputProperties({excludeInheritedInputProperties: true})}` line.

### ADDED

- Added mechanism in `SynaptixDatastoreRdfSession::_extractLinksFromObjectInput()` to handle non instantiale model definition related input. @see [Deep object input parsing documentation](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#deep-object-input-parsing)
- Added GraphQL mutation generators (create/update) based on `ModelDefinitionAbstract`, more robust than thosed based on type and impliying type resolution based on instance ID. 
- Added a `nodesTypeFormatter` method to `SynaptixDatastoreRdfSession` and `GraphControllerService` that enables to tweak  **globally** the way to tranform a node type into a URI prefix.
- Added a `getNodeTypeFormatter` method to `ModelDefinitionAbstract` that enables to tweak **locally** the way to tranform a node type into a URI prefix.
- Added `description` parameter to create/update mutation generators to tweak mutation documentation.

   
### FIXED

- Fixed typo in SSOControllerService::unregisterUserAccount() method.

## [4.5.7] - 18 november 2019

### Added 

- Added a `isInGroup(userGroupId)` property to `mnx:UserAccount` graphQL type to fastly interrogate datastore about user group permissions.

### Changed

- `ADMIN_USER_GROUP_URI` has been renamed to `ADMIN_USER_GROUP_ID` to be more generic.

## [4.5.6] - NOT FONCTIONNAL.
 
## [4.5.5] - 15 november 2019

### Added 

- Added UserAccount/UserGroup update mutations
- Added generic RemoveEntity/RemoveEntity mutations
- Added possibility to remove a 1:1 relationship link in `GraphControllerService::updateNode()` by introduction of `isDeletion` property in `Link`. @see ["Updating an object" section notes](https://mnemotix.gitlab.io/synaptix.js/manipulating_data/#updating-an-object)
- Added DisableUserAccount/EnableUserAccount mutations to block/unblock accounts
- In `MnxAgentGraphSyncSSOMiddleware` UserAccount instance URI now match with userId URIs.
- Added `isAdminRule`, `askSparqlRule` and `isInUserGroupRule` shield rules to protect GraphQL resolvers. @see ["GraphQL shield middlewares" section](https://mnemotix.gitlab.io/synaptix.js/securizing_data/#graphql-shield-middlewares)

### Fixed

- Fixed bug on SSOControllerService::logout service. If logout fails, cookie must be cleared anyway.
- Fixed bug on GraphControllerService::getNode() with a `linkFilter` having a `isReversed` property AND a linkDefinition with a rdfReversedObjectProperty.
- Fixed weird bug with synaptix-graph-controller JSON-LD result where sometime, dataProperty prefix is not stripped. It seems to appear if data property type is a boolean.

## [4.5.4] - 8 november 2019

### ADDED

- Added `DataModel::setModelDefinitionForLoggedUserPerson()`. Usefull and needed if mnx:Person is specialized in a project with extra properties to access these extra properties in graphql `query{ me {...} }`.

### FIXED

- `GraphControllerService::updateNode()` now properly removes literals and labels defined as `null` or `""` in `objectInput` parameters

## [4.5.3] - 7 november 2019

### ADDED

- Added SSO login `I18nError` instances in case of :
    - Account disabled (ACCOUNT_DISABLED)
    - Account not validated (ACCOUNT_NOT_VALIDATED) 
- Added possiblity to just disable a SSO account instead of removing it thanks to `permanent` parameter.
- Added possibility to store a `userGroupIds` attribute in SSO that is parsed by `MnxAgentGraphSyncSSOMiddleware`. If set, this array of URIs are automatically linked to freshly created `UserAccount` instance. 

### FIXED

- Fixed not optimized Babel compilation. Sourcemaps are added and useless plugins have been dumped.
- Fixed REST SSO. Reset password and add possibility to pass redirectURI in query (thanks to Hervé)


## [4.5.2] - 5 november 2019

### CHANGED

- In SSO mutation and Express routes. Password reset is now handled by two mechanisms. 
  - If user is connected, if can reset it's password by passing a triple (old password, new password, new password confirmation). This is handled by `resetUserAccountPassword` mutation or `/auth/reset-password` express route.
  - Otherwise, a password reset can be requested by passing username. This is handled by `resetUserAccountPasswordByMail` mutation or `/auth/reset-password-by-mail` express route.

## [4.5.1] - 4 november 2019

### ADDED

- Added `discardTypeAssertion` parameter on `SynaptixDatastoreRdfSession::getLinkedObjectFor()` and `GraphControllerService::getNodes()` methods. This this usefull when URI reference are stored in triple store but belongs to another triple store (Ex: Geonames)
- Added `personInput` argument in `registerUserAccount` mutation. This is usefull to record person related information (and constrained) during registration.
- Added `unregisterUserAccount` SSO mutation.

- WIP : Automatic GraphQL documentation generation. (undocumented yet.)

### CHANGED

- In `MnxActionGraphMiddleware` Creation/Deletion/Update actions are mutualised for every transaction.
- In `MnxActionGraphMiddleware` logged userAccount URI is used as `prov:wasAttributedTo` object.
- In `launchApplication` utility, `ssoMiddlewares` is passed as argument ion generateGraphQLEndpoint to provide it in DatastoreAdapter et DatastoreSession (thanks to Hervé)

### FIXED

- In SSO Rest middeware, email input on registering takes username value if not provided. (thanks to Hervé)
- Fix logged user session and account cache in `SynaptixDatastoreSession` (thanks to Hervé)
- Add default values for objectInput / links in extractLinksFromObjectInput, to avoid errors on existing calls (thanks to Hervé)
- Fixed a bug on `GraphControllerService::getNodes()`. `linkFilters` where wrongly parsed.

## [4.5.0] - 25 october 2019

After a big an effort on documentation and (not enough I know... 😥😓😭) unit testing enforcing. 4.5.0 is out !

## [4.5.0-beta.14] - 25 october 2019

### CHANGED

- Removing buggy addUserCommand from index.
- Big documentation improvement.

## [4.5.0-beta.13] - 24 october 2019

### BREAKING

- `[...]GraphCrudMiddleware` middlewares has been renamed to `[...]GraphMiddleware`

### ADDED

- Added `SynaptixDatastoreRdfAdapter` that returns `SynaptixDatastoreRdfSession`

### CHANGED

- Update documentation.

## [4.5.0-beta.13] - 23 october 2019

### FIXED

- Move `ora` dependency from devDependencies to dependencies

## [4.5.0-beta.11] - 22 october 2019

### BREAKING

- Removed `graphql` from dependencies. Add it in devDependencies and peerDependencies.

### CHANGED

- Update documentation.
- Enhance input parsing during object creation/update adding nested links based on LinkDefinition::relatedInputName property.


## [4.5.0-beta.10] - 18 october 2019

### CHANGED

- Homogenized GraphQL errors in `formatGraphQLError` helper. Unit tested.
- Improve `GraphControllerService::updateNode()` to add deletion triple on `1:1` links. This is ensure that edge is removed. 
  This work needs to be improved for link tagged as `1:1` **AND** `cascading removed` because at this stage, zombie triples will remain in the graph.

### ADDED
- Add unit tests on GraphSynchronisationSSOMiddleware
- Added `nickName` property to `mnx:Person` used in registration.
- Created FormValidationError to handle form input errors
- Addes GraphQL error responses mocks to test UI
- Added a `GraphQLErrorResponses` in testing index.


### FIXED

- Fixed bug on `SynaptixDatastoreRdfSession::_extractLinksFromObjectInput()` when links array is not empty.

## [4.5.0-beta.9] - 15 october 2019

### CHANGED

- Add an array of `LabelFilter` to `SynaptixDatastoreRdfSession::getObjects()` method to filter by localized labels.
- Add a filter raw parser in `SynaptixDatastoreSession` to convert an array of ":" separated filters into `(Link|Label|Literal)Filter`
- Improve GraphQLError by addind a `statusCode` itself added in `I18Error`.
- Homogenized GraphQL errors in `formatGraphQLError` helper. Unit tested.
- Improve `GraphControllerService::updateNode()` to add deletion triple on `1:1` links. This is ensure that edge is removed. 
  This work needs to be improved for link tagged as `1:1` **AND** `cascading removed` because at this stage, zombie triples will remain in the graph.

### ADDED
- Add unit tests on GraphSynchronisationSSOMiddleware
- Added `nickName` property to `mnx:Person` used in registration.
- Created FormValidationError to handle form input errors
- Addes GraphQL error responses mocks to test UI
- Added a `GraphQLErrorResponses` in testing index.


### FIXED

- Fixed bad stack generation in `I18nError` contructor.
- Fixed bug on `SynaptixDatastoreRdfSession::_extractLinksFromObjectInput()` when links array is not empty.

## [4.5.0-beta.8] - 15 october 2019

### FIXED

- Removing SSOApiClient cache. Not working as SSOApiClient is created at application initialization and not during session.

## [4.5.0-beta.7] - 14 october 2019

### CHANGED

- Improve SSOApiClient to cache admin session instead of recreating it at every API call.
- Improve GraphControllerService to force manual URI in object creation (and linked object creation) instead of using random URI generation.
- Improve GraphSynchronizationSSOMiddleware to force manual person/userAccount URI if they are already present in SSO user attributes.

## [4.5.0-beta.6] - 11 october 2019

### BREAKING CHANGE

- `ModelAbstract` has been renamed to `Model` 
- `ModelDefinitionAbstract::getGraphQLType()` now returns by default `ModelDefinitionAbstract::getNodeType()`

### CHANGED

- Normalizing SSOUser attributes.
- Add logged user convenient methods in SynaptixDatastoreSession (to retrieve related mnx:Person and mnx:UserAccount
- `ModelDefinitionAbstract::getModelClass()` is now optional for herited classes. By default `Model` is returned.
- Updated MnxContribution data model to comply with MNX generic ontology
  - Renamed `mnx:Contribution` to `mnx:OnlineContribution`
  - Added `mnx:Comment`
 
### BUGS
 
- Fixed bug on GraphSynchronizationSSOMiddleware
 
## [4.5.0-beta.5] - 10 october 2019 

### BREAKING CHANGE

- `ObjectProperties`, `ObjectInput`, `ObjectInterface`, `ObjectDefinitions` used in MnxCommon datamodel are renamed to `Entity*` to match with new MNX generic model.

### CHANGED

- Updated MnxAgent data model according to ontology.
  - Removed mnx:UserGroupMembership
- Updated MnxProject data model according to ontology.
  - Removed mnx:Team
- Updated MnxAcl datamodel to comply with MNX generic model.
  - Removed mnx:UpdatePriviledge, mnx:DeletePriviledge,  mnx:CreatePriviledge
  - Added mnx:WritePriviledge
- Updated MnxContribution datamodel to comply with MNX generic model.
  - Added PROV-O ontology concepts in MnxCommon and MnxContribution datamodels.
- Added a `isHerited` flag on `(Link|Literal|Label)Definition`.
- Improved Owl generator to take `isHerited` flag into account. 

## [4.5.0-beta.4] - 8 october 2019

### BREAKING CHANGE

- `SynaptixDatastoreAdapter::getSession` now requires object parameters (inline until now).

Huge renaming :

- `RdfSyncService` has been renamed to `GraphControllerService`
- `RdfSyncPublisher` has been renamed to `GraphControllerPublisher`
- `IndexSync*Service` has been renamed to `IndexController*Service`
- `IndexSyncPublisher` has been renamed to `IndexControllerPublisher`


# ADDED: 
- Refactored SSO services in a `SSOServiceController` used by generateSSOEndpoint.
- New SSO mutations available, use them by calling `DataModel::addSSOMutations()`
- Refactored `generateSSOEnpoint` utility. Reusable logic related to cookie has been extracted and moved in `SSOUser`.
- Login/logout is possible with Login/Logout mutation. Cookies are followed just like express related routes.
- `I18nError` and `SynaptixError` are new error to throw containing both i18nkey for internationaling error display.
- GraphQL endpoints now display errors with `I18nError::i18nKey` in place of `Error::message` in the `message` property and add an extra `extra_infos` property in development environment.
- GraphQL endpoints now display well Synaptix error trace in development environment.

## [4.5.0-beta.3]  - 25 september 2019

### ADDED

- `initMiddlewares` parameter function is added to `launchApplication` utility. It's role is to add express application middleware just after having created it.

## [4.5.0-beta.2]  - 25 september 2019

### BREAKING CHANGE

- `AbstractPublisher` has been renamed to `DataModulePublisher`
- `AbstractConsumer` has been renamed to `DataModuleConsumer`

### ADDED

- `DataModulePublisher::publish()` method accept a `expectingResponse` argument to wait or not to wait a response from the AMQP bus. 

## [4.5.0-beta.1]  - 24 september 2019

### BREAKING CHANGE
- `ObjectDefaultProperties` used in GraphQL types has been renamed to `ObjectInterfaceProperties`
- GraphQL date properties are now parsed into **ISO8601 string** (no more Float timestamp)
- Default ontologie has been updated and renamed `MnxOntologies`. Use `MnxOntologiesLegacy` to retrieve old version.
- `generateInterfaceResolverMap` function has been renamed to `generateInterfaceResolvers`
- `generateBaseResolverMap` function has been renamed to `generateTypeResolvers`
- `SSOSyncClient` as been renamed to `SSOApiClient`
- `authenticationMiddleware` is now a function renamed `authenticate` that returns the middleware and now accepts `acceptAnonymousRequest, disableAuthRedirection` parameters

### DEPRECATED
- The property `readOnly` on a `LinkDefinition` is deprecated. Use non instantiable property of a model definition instead.

## [4.4.0-beta.12] - 4 july 2019
### ADDED
- Add `color` property to Concept.

## [4.4.0-beta.11] - 4 july 2019
### ADDED
- Add `altLabel` property to Concept to select the first localized altLabel related to locale.

## [4.4.0-beta.10] - 21 june 2019
### ADDED
- Added possibility to skip prefixes in Ids. By default, all global ids returned in API are sent without prefix.

## [4.4.0-beta.9] - 20 june 2019
### FIXED
- Simplying Geonames datamodel.

## [4.4.0-beta.8] - 14 june 2019
### ADDED
- Added variable `CORS_DISABLED` to disable CORS in ExpressJS middlwares.

## [4.4.0-beta.7] - 6 june 2019
### ADDED
- Added a `isSearchable` clause in `LabelDefinition` and `LinkDefinition` handled by `GraphControllerService::getNodes()` method.

### FIXED
- Fixed a bug on `GraphControllerService::getNodes()` that returned `[ undefined ]` when triplestore returns `{}` 

## [4.4.0-beta.6] - 3 june 2019
### DEPRECATION
- `paginationArgs` GraphQL helper is deprecated in favour of `filteringArgs`



## [4.4.0-beta.5] - 22 may 2019
### BREAKING CHANGE
- Model definition inheritance changed. Every ModelDefinition must inherit from [ModelDefinitionAbstract](src/server/datamodel/toolkit/definitions/ModelDefinitionAbstract.js) and register its parents throw the method `ModelDefinitionAbstract::getParentDefinitions()`

## [4.4.0-beta.4] - 22 may 2019
### FIXED
- Removed useless Exceptions
- Fixed GraphControllerService::getRdfTypeForURI() and allowing it to return a list of types.
- Fixed SynaptixDatastoreRdfSession::getModelDefinitionForURI() and returns the more specific ModelDefinition given an URI.

## [4.4.0-beta.3] - 21 may 2019
### BREAKING CHANGE
- ID is automatically added in [ObjectDefaultProperties](src/server/datamodel/ontologies_V1/schema/types/ObjectInterface.graphql.js). It implies to remove `id: ID!` from GraphQL types calling this helper.

### ADD
- Introduce new helper [generateInterfaceResolverMap](src/server/datamodel/toolkit/graphql/helpers/resolverHelpers.js) that generated GraphQL `interface` resolver map that resolves basic properties and the GraphQL object type. See https://www.apollographql.com/docs/graphql-tools/resolvers#unions-and-interfaces.


## [4.4.0-beta.2] - 14 may 2019
### BREAKING CHANGE
- Environment variable `DISABLE_INDEX` is renamed as `INDEX_DISABLED`

### ADD
- Environment variable `OAUTH_REGISTRATION_DISABLED` to disabled account registration when `launchApplication` utility is used.


