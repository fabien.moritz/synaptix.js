/**
 * This module provides examples of JSON payloads (in string format) used by the SSO endpoints (as they are generated
 * by synaptix.js using `generateSSOEndpoint()`). For each endpoint, it provides an example of a valid JSON input (request payload),
 * examples of successful response and error responses.
 *
 * The synaptix.js library garantees that the provided messages are compliant with its current implementation, and uses them in
 * its own unit tests.
 */

export const SSOEndpointMessageExamples = {
  'POST /auth/register': {
    request: JSON.stringify({
      username: 'test@domain.com',
      firstName: 'John',
      lastName: 'Doe',
      password: 'pass'
    }),
    successResponse: JSON.stringify({
      id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
      email: 'test@domain.com',
      firstName: 'John',
      lastName: 'Doe',
      attributes: {}
    }),
    errorResponses: {
      badRequest: JSON.stringify({
        errors: {
          username: 'Username is empty'
        }
      }),
      unexpectedError: JSON.stringify({
        error: 'We got an unexpected issue'
      })
    }
  },
  'POST /auth/login': {
    request: JSON.stringify({
      username: 'test@domain.com',
      password: 'pass'
    }),
    successResponse: JSON.stringify({
      ticket: {
        access_token: 'hashaccesstoken',
        refresh_token: 'hashrefreshtoken',
        expires: 1554978396
      }
    }),
    errorResponses: {
      badRequest: JSON.stringify({
        errors: {
          username: 'Username is empty'
        }
      }),
      badCredentials: JSON.stringify({
        error: 'Invalid user credentials'
      })
    }
  },
  'POST /auth/reset-password': {
    request: JSON.stringify({
      username: 'test@domain.com'
    }),
    errorResponses: {
      userDoesntExist: JSON.stringify({
        errors: {
          username: 'User not found'
        }
      }),
      unexpectedError: JSON.stringify({
        error: 'We got an unexpected issue'
      })
    }
  }
};

