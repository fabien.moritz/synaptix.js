/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Converts a GraphQL query (tree syntax calling gql`...`) into a GraphQLResolveInfo object available
 * in fourth position resolver parameters.
 *
 * @param {object} query GraphQL tree object
 * @return {GraphQLResolveInfo}
 */
export function gqlQueryToResolveInfo(query){
  const operation = query.definitions
    .find(({ kind }) => kind === 'OperationDefinition');
  const fragments = query.definitions
    .filter(({ kind }) => kind === 'FragmentDefinition')
    .reduce((result, current) => ({
      ...result,
      [current.name.value]: current,
    }), {});

  return {
    fieldNodes: operation.selectionSet.selections,
    fragments,
  };
}