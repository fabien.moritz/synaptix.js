/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import BazDefinitionMock from "./BazDefinitionMock";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import NotInstantiableDefinitionMock from "./NotInstantiableDefinitionMock";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class BarDefinitionMock extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [NotInstantiableDefinitionMock];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:Bar';
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasBaz',
        rdfReversedObjectProperty: "mnx:hasBaz",
        relatedModelDefinition: BazDefinitionMock,
        isPlural: true,
        isCascadingRemoved: true,
        relatedInputName: "bazInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'barLabel1',
        rdfDataProperty: "mnx:barLabel1",
        inputFormOptions: 'type: "textarea"'
      }),
      new LabelDefinition({
        labelName: 'barLabel2',
        rdfDataProperty: "mnx:barLabel2",
        inputFormOptions: 'type: "textarea"'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'barLiteral1',
        rdfDataProperty: 'mnx:barLiteral1',
        isSearchable: true,
        isRequired: true
      }),
      new LiteralDefinition({
        literalName: 'barLiteral2',
        rdfDataProperty: 'mnx:barLiteral2',
        isSearchable: true
      }),
      new LiteralDefinition({
        literalName: 'barLiteral3',
        rdfDataProperty: 'mnx:barLiteral3',
      })
    ];
  }
};