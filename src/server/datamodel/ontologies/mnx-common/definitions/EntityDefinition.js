/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import ExternalLinkDefinition from "../../mnx-contribution/definitions/ExternalLinkDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import Entity from '../models/Entity';
import ActionDefinition from "../../mnx-contribution/definitions/ActionDefinition";
import AccessPolicyDefinition from "../../mnx-acl/definitions/AccessPolicyDefinition";
import CreationDefinition from "../../mnx-contribution/definitions/CreationDefinition";
import DeletionDefinition from "../../mnx-contribution/definitions/DeletionDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import {LinkPath} from "../../../toolkit/utils/LinkPath";

export default class EntityDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "prov": "http://www.w3.org/ns/prov#"
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Entity";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["prov:Entity"];
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Entity;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasExternalLink',
        pathInIndex: 'externalLinks',
        rdfObjectProperty: "mnx:hasExternalLink",
        relatedModelDefinition: ExternalLinkDefinition,
        isPlural: true,
        relatedInputName: "externalLinkInputs"
      }),
      new LinkDefinition({
        linkName: 'hasAction',
        pathInIndex: 'actions',
        rdfReversedObjectProperty: "prov:wasGeneratedBy",
        relatedModelDefinition: ActionDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'hasCreationAction',
        rdfReversedObjectProperty: "prov:wasGeneratedBy",
        relatedModelDefinition: CreationDefinition,
      }),
      new LinkDefinition({
        linkName: 'hasDeletionAction',
        rdfReversedObjectProperty: "prov:wasGeneratedBy",
        relatedModelDefinition: DeletionDefinition,
      }),
      new LinkDefinition({
        linkName: 'hasAccessPolicy',
        pathInIndex: 'accessPolicies',
        rdfObjectProperty: "mnx:hasAccessPolicy",
        relatedModelDefinition: AccessPolicyDefinition,
        isPlural: true,
        isCascadingRemoved: true,
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'createdAt',
        linkPath: new LinkPath()
          .step({linkDefinition: EntityDefinition.getLink("hasCreationAction")})
          .property({
            propertyDefinition: CreationDefinition.getLiteral("startedAtTime"),
            rdfDataPropertyAlias: 'mnx:createdAt'
          }),
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#dateTimeStamp'
      }),
    ];
  }
};