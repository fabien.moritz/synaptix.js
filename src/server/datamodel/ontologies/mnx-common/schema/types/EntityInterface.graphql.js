/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateInterfaceResolvers,
  getLinkedObjectResolver,
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import EntityDefinition from "../../definitions/EntityDefinition";
import {filteringArgs, connectionArgs} from "../../../../toolkit/graphql/schema/types/generators";
import {LinkPath} from "../../../../..";
import ActionDefinition from "../../../mnx-contribution/definitions/ActionDefinition";
import PersonDefinition from "../../../mnx-agent/definitions/PersonDefinition";
import UserAccountDefinition from "../../../mnx-agent/definitions/UserAccountDefinition";

/**
 * This is the default properties shared between every GraphQL type.
 *
 * @type {string}
 */
export let EntityInterfaceProperties = `
  """ Identifier """
  id: ID! 
   
  """ URI """
  uri: String
  
  """ Is the user allowed to update this entity """
  canUpdate: Boolean 
  
  actions(${connectionArgs}, ${filteringArgs}): ActionInterfaceConnection 
  
  """ The access policies """
  accessPolicies: AccessPolicyConnection
  
  """ Created at time """
  createdAt: String
  
  """ User account which creates object"""
  creatorUserAccount: UserAccount
  
  """ Person who created object """
  creatorPerson: Person
`;

/**
 * This is the default input shared between every GraphQL type.
 *
 * @type {string}
 */
export let EntityInterfaceInput = `
  """ The ID """
  id: ID @formInput(type:"hidden")
`;

/**
 * This is the object interface implemented by every GraphQL type.
 *
 * @type {string}
 */
export let EntityInterface = `
""" An interface for all entities (mnx:Entity instances) """
interface EntityInterface {
  ${EntityInterfaceProperties}
}

""" An input for all entities (mnx:Entity instances) """
input EntityInput{
  ${EntityDefinition.generateGraphQLInputProperties()}
}

extend type Query {
  """Fetches an entity given its global ID"""
  entity(id: ID!): EntityInterface
}
`;

/**
 * This is resolver map linked to the interface {EntityInterface}
 * @type {object}
 */
export let EntityInterfaceResolvers = {
  EntityInterface: {
    ...generateInterfaceResolvers("EntityInterface"),
    /**
     *
     * This property can be used in different ways.
     *
     * - With a global behaviour by overriding `SynaptixDatastoreSession::isLoggedUserAllowedToUpdateObject()` method in your own session. By default returns true.
     * - With a local behaviour by using `replaceFieldValueIfRuleFailsMiddleware` middleware.
     *
     * @see
     *
     * To override this value, you need to use selectivelly
     * @param {Model} object
     * @param __
     * @param {SynaptixDatastoreSession} synaptixSession
     * @return {*}
     */
    canUpdate: (object, __, synaptixSession) => {
      return synaptixSession.isLoggedUserAllowedToUpdateObject({
        object,
        modelDefinition: synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(object.type),
      })
    },
    actions: getLinkedObjectResolver.bind(this, EntityDefinition.getLink("hasAction")),
    accessPolicies: getLinkedObjectResolver.bind(this, EntityDefinition.getLink("hasAccessPolicy")),
    createdAt: async (object) => object.createdAt,
    /**
     * @param {Model} object
     * @param __
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    creatorUserAccount: async (object, __, synaptixSession) => synaptixSession.getObjectFilteredByLinkPath({
      modelDefinition: UserAccountDefinition,
      linkPath: new LinkPath()
        .step({linkDefinition: UserAccountDefinition.getLink("hasCreationAction")})
        .step({linkDefinition: ActionDefinition.getLink("hasEntity"), targetId: object.id})
    }),
    /**
     * @param {Model} object
     * @param __
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    creatorPerson: async (object, __, synaptixSession) => synaptixSession.getObjectFilteredByLinkPath({
      modelDefinition: PersonDefinition,
      linkPath: new LinkPath()
        .step({linkDefinition: PersonDefinition.getLink("hasUserAccount")})
        .step({linkDefinition: UserAccountDefinition.getLink("hasCreatedAction")})
        .step({
          linkDefinition: ActionDefinition.getLink("hasEntity"),
          targetId: object.id,
          typeAssertionDiscarded: true
        }),

    })
  },
  Query: {
    /**
     * @param {object} parent
     * @param {string} globalId
     * @param {SynaptixDatastoreSession} synaptixSession
     * @return {Model}
     */
    entity: async (parent, {id: globalId}, synaptixSession) => {
      let {id, type} = synaptixSession.parseGlobalId(globalId);

      return synaptixSession.getObject(
        synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type),
        id,
        synaptixSession.getContext().getLang()
      );
    }
  }
};
