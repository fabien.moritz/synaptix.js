/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import ReadPriviledge from "../models/ReadPriviledge";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import AccessPriviledgeDefinition from "./AccessPriviledgeDefinition";

export default class ReadPriviledgeDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "s4ac": "http://ns.inria.fr/s4ac/v2/"
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["s4ac:Read"];
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AccessPriviledgeDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ReadPriviledge"
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return ReadPriviledge;
  }
};
