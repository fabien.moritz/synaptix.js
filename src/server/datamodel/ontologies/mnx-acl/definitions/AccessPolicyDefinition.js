/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import AccessPolicy from "../models/AccessPolicy";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import AccessPolicyTargetDefinition from "./AccessPolicyTargetDefinition";
import AccessPriviledgeDefinition from "./AccessPriviledgeDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class AccessPolicyDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "s4ac": "http://ns.inria.fr/s4ac/v2/"
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["s4ac:AccessPolicy"];
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:AccessPolicy"
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return AccessPolicy;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasAccessPolicyTarget',
        pathInIndex: 'accessPolicyTargets',
        rdfReversedObjectProperty: "s4ac:hasAccessPolicy",
        relatedModelDefinition: AccessPolicyTargetDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'hasAccessPriviledge',
        pathInIndex: 'accessPriviledges',
        rdfObjectProperty: "s4ac:hasAccessPriviledge",
        relatedModelDefinition: AccessPriviledgeDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'appliesTo',
        rdfDataProperty: 's4ac:appliesTo',
        rdfDataType: "http://www.w3.org/2001/XMLSchema#anyURI"
      })
    ]
  }
};
