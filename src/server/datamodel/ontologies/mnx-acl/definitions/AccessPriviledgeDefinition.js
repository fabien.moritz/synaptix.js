/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import AccessPriviledge from "../models/AccessPriviledge";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class AccessPriviledgeDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "s4ac": "http://ns.inria.fr/s4ac/v2/"
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["s4ac:AccessPriviledge"];
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:AccessPriviledge"
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return AccessPriviledge;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasAccessPolicy',
        pathInIndex: 'accessPolicies',
        rdfReversedObjectProperty: "s4ac:hasAccessPrivilege",
        relatedModelDefinition: AccessPriviledgeDefinition,
        isPlural: true,
        isCascadingRemoved: true,
      })
    ];
  }
};
