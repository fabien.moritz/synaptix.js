/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  generateInterfaceResolvers,
  getLinkedObjectsResolver,
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

import AccessPriviledgeDefinition from "../../definitions/AccessPriviledgeDefinition";

export let AccessPriviledgeInterfaceProperties = `
  ${EntityInterfaceProperties}

  """ Access policy """
  accessPolicy(${connectionArgs}, ${filteringArgs}): AccessPolicy
`;

export let AccessPriviledgeInterfaceInput = `
  ${EntityInterfaceInput}
`;

export let AccessPriviledgeInterface = `
""" An AccessPriviledge interface """
interface AccessPriviledgeInterface {
  ${AccessPriviledgeInterfaceProperties}
}

${generateConnectionForType("AccessPriviledgeInterface")}
`;


export let AccessPriviledgeResolvers = {
  AccessPriviledgeInterface: {
    ...generateInterfaceResolvers("AccessPriviledge"),
    accessPolicy: getLinkedObjectsResolver.bind(this, AccessPriviledgeDefinition.getLink('hasAccessPolicy')),
  },
  ...generateConnectionResolverFor("AccessPriviledgeInterface"),
};