/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateTypeResolvers,
  getLocalizedLabelResolver,
  getLinkedObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import AccessPolicyDefinition from "../../definitions/AccessPolicyDefinition";
import {EntityInterfaceInput, EntityInterfaceProperties} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let AccessPolicyType = `
""" An access policy """
type AccessPolicy implements EntityInterface{
   ${EntityInterfaceProperties}
  
  """ Applies to URI """
  appliesTo: String
  
  """ Related access priviledges """
  accessPriviledges(${filteringArgs}, ${connectionArgs}): AccessPriviledgeInterfaceConnection
  
  """ Related access policy target """
  accessPolicyTarget: AccessPolicyTargetInterface
}

${generateConnectionForType("AccessPolicy")}

input AccessPolicyInput {
  ${EntityInterfaceInput}
  
  """ Applies to URI """
  appliesTo: String
}
`;

export let AccessPolicyResolvers = {
  AccessPolicy: {
    ...generateTypeResolvers("AccessPolicy"),
    appliesTo: (object) => object.appliesTo,
    accessPolicyTarget: getLocalizedLabelResolver.bind(this, AccessPolicyDefinition.getLabel('hasAccessPolicyTarget')),
    accessPriviledges: getLinkedObjectsResolver.bind(this, AccessPolicyDefinition.getLink('hasAccessPriviledge'))
  },
  ...generateConnectionResolverFor("AccessPolicy")
};
