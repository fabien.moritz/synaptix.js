/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import DefaultIndexMatcher from "../../../toolkit/matchers/DefaultIndexMatcher";

export default class MemoIndexMatcher extends DefaultIndexMatcher {
  /**
   * @inheritDoc
   */
  getSortingMapping() {
    return {};
  }

  /**
   * @inheritDoc
   */
  getFulltextQueryFragment(query) {
    return {
      "query_string": {
        "query": `${query}*`
      }
    };
  }

  /**
   * @inheritDoc
   */
  getFilterByKeyValue(key, value) {
    switch (key) {
      case "event":
        return {
          "nested": {
            "path": "event",
            "query": this.getTermFilter("event.id", value)
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  /**
   * @inheritDoc
   */
  getAggregations() {


    return {};
  }
}