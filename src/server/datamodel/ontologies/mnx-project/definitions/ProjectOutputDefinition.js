/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import ProjectOutput from "../models/ProjectOutput";
import ProjectDefinition from "./ProjectDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import ResourceDefinition from "./ResourceDefinition";
import AgentDefinition from "../../mnx-agent/definitions/AgentDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import {LinkPath} from "../../../toolkit/utils/LinkPath";

export default class ProjectOutputDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ProjectOutput";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return ProjectOutput;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasProject',
        pathInIndex: 'hasProject',
        rdfReversedObjectProperty: "mnx:hasProjectOutput",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true,
        relatedInputName: 'projectInput'
      }),
      new LinkDefinition({
        linkName: 'hasPicture',
        rdfObjectProperty: "mnx:hasPicture",
        relatedModelDefinition: ResourceDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        relatedInputName: 'pictureInputs'
      }),
      new LinkDefinition({
        linkName: 'hasMainPicture',
        rdfObjectProperty: "mnx:hasMainPicture",
        relatedModelDefinition: ResourceDefinition,
        isCascadingUpdated: true,
        relatedInputName: 'mainPictureInput'
      }),
      new LinkDefinition({
        linkName: 'hasRepresentation',
        rdfObjectProperty: "mnx:hasRepresentation",
        relatedModelDefinition: ProjectOutputDefinition,
        isCascadingUpdated: true,
        isPlural: true,
        relatedInputName: 'reprensentationInputs'
      }),
      new LinkDefinition({
        linkName: 'hasAuthor',
        rdfObjectProperty: "mnx:hasAuthor",
        relatedModelDefinition: AgentDefinition,
        isCascadingUpdated: true,
        isPlural: true,
        relatedInputName: 'authorInputs'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'title',
        pathInIndex: 'titles',
        rdfDataProperty: 'mnx:title',
        isSearchable: true,
        isRequired: true
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInIndex: 'descriptions',
        rdfDataProperty: 'mnx:description'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInIndex: 'shortDescriptions',
        rdfDataProperty: 'mnx:shortDescription'
      }),
      new LabelDefinition({
        labelName: 'projectTitle',
        linkPath: new LinkPath()
          .step({linkDefinition: ProjectOutputDefinition.getLink("hasProject")})
          .property({
            propertyDefinition: ProjectDefinition.getLabel("title"),
            rdfDataPropertyAlias: 'mnx:projectTitle'
          })
      })
    ];
  }
};