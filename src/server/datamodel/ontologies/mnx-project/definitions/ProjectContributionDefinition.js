/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import ProjectDefinition from "./ProjectDefinition";
import InvolvementDefinition from "./InvolvementDefinition";
import AttachmentDefinition from "./AttachmentDefinition";
import ProjectContribution from "../models/ProjectContribution";
import ProjectContributionIndexMatcher from "../matchers/ProjectContributionIndexMatcher";
import DurationDefinition from "../../mnx-time/definitions/DurationDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class ProjectContributionDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [DurationDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ProjectContribution";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'projectContribution';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return ProjectContribution;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ProjectContributionIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'hasProject',
        pathInIndex: 'project',
        rdfObjectProperty: "mnx:hasProject",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true,
        relatedInputName: 'projectInput'
      }),
      new LinkDefinition({
        linkName: 'hasRepliesTo',
        rdfObjectProperty: "mnx:hasRepliesTo",
        relatedModelDefinition: ProjectContributionDefinition,
        isCascadingUpdated: true,
        relatedInputName: 'repliesToInput'

      }),
      new LinkDefinition({
        linkName: 'hasAttachment',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'projectContribution',
          useIndexMatcherOf: AttachmentDefinition
        }),
        rdfReversedObjectProperty: "mnx:hasProjectContribution",
        relatedModelDefinition: AttachmentDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: 'attachmentInputs'
      }),
      new LinkDefinition({
        linkName: 'hasInvolvement',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'projectContribution',
          useIndexMatcherOf: InvolvementDefinition
        }),
        rdfReversedObjectProperty: "mnx:hasProjectContribution",
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: 'involvementInputs'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInIndex: 'titles',
        rdfDataProperty: 'mnx:title'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInIndex: 'descriptions',
        rdfDataProperty: 'mnx:description'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInIndex: 'shortDescriptions',
        rdfDataProperty: 'mnx:shortDescription'
      }),
      new LabelDefinition({
        labelName: 'memo',
        pathInIndex: 'memo',
        rdfDataProperty: 'mnx:memo'
      })
    ];
  }
};