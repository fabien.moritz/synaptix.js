/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import Involvement from "../models/Involvement";
import InvolvementIndexMatcher from "../matchers/InvolvementIndexMatcher";
import AgentDefinition from "../../mnx-agent/definitions/AgentDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import ProjectContributionDefinition from "./ProjectContributionDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import ProjectDefinition from "./ProjectDefinition";

export default class InvolvementDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Involvement";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'involvement';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Involvement;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return InvolvementIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'hasAgent',
        pathInIndex: 'hasAgent',
        rdfObjectProperty: "mnx:hasAgent",
        relatedModelDefinition: AgentDefinition,
        isCascadingUpdated: true,
        relatedInputName: "agentInput"
      }),
      new LinkDefinition({
        linkName: 'hasProjectContribution',
        pathInIndex: 'hasProjectContribution',
        rdfObjectProperty: "mnx:hasProjectContribution",
        relatedModelDefinition: ProjectContributionDefinition,
        isCascadingUpdated: true,
        relatedInputName: "projectContributionInput"
      }),
      new LinkDefinition({
        linkName: 'hasProject',
        rdfObjectProperty: "mnx:hasProject",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true,
        relatedInputName: "projectInput"
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'role',
        pathInIndex: 'roles',
        rdfDataProperty: "mnx:role"
      })
    ];
  }
};