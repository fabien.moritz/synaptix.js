/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import Resource from "../models/Resource";
import ResourceIndexMatcher from "../matchers/ResourceIndexMatcher";
import AttachmentDefinition from "../../../ontologies-legacy/definitions/projects/AttachmentDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class ResourceDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Resource";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Resource;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'resource';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ResourceIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasAttachment',
        pathInIndex: 'attachments',
        rdfReversedObjectProperty: "mnx:hasAttachedResource",
        relatedModelDefinition: AttachmentDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        relatedInputName: 'attachmentInputs'
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'description',
        pathInIndex: 'descriptions',
        rdfDataProperty: 'mnx:description'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'publicUrl',
        rdfDataProperty: 'mnx:publicUrl',
        isRequired: true
      })
    ];
  }
};