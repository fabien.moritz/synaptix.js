/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import ProjectContributionDefinition from "./ProjectContributionDefinition";
import Attachment from "../models/Attachment";
import AttachmentIndexMatcher from "../matchers/AttachmentIndexMatcher";
import ResourceDefinition from "./ResourceDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class AttachmentDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Attachment";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'attachment';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Attachment;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return AttachmentIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasResource',
        pathInIndex: 'hasResource',
        rdfObjectProperty: "mnx:hasResource",
        relatedModelDefinition: ResourceDefinition,
        isCascadingUpdated: true,
        relatedInputName: "resourceInput"
      }),
      new LinkDefinition({
        linkName: 'hasProjectContribution',
        pathInIndex: 'hasProjectContribution',
        rdfObjectProperty: "mnx:hasProjectContribution",
        relatedModelDefinition: ProjectContributionDefinition,
        isCascadingUpdated: true,
        relatedInputName: "projectContributionInput"
      })
    ];
  }
};