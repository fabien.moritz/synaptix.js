/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import ProjectContributionDefinition from "./ProjectContributionDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import ProjectIndexMatcher from "../matchers/ProjectIndexMatcher";
import Project from "../models/Project";
import ProjectOutputDefinition from "./ProjectOutputDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import PersonDefinition from "../../../ontologies-legacy/definitions/foaf/PersonDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import UserGroupDefinition from "../../mnx-agent/definitions/UserGroupDefinition";

export default class ProjectDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Project";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'project';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Project;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ProjectIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasAuthor',
        pathInIndex: 'author',
        rdfReversedObjectProperty: "mnx:hasAuthor",
        relatedModelDefinition: PersonDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        relatedInputName: 'authorInputs'
      }),
      new LinkDefinition({
        linkName: 'hasParentProject',
        pathInIndex: 'hasParentProject',
        rdfObjectProperty: "mnx:hasParentProject",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true,
        relatedInputName: 'parentProjectInput'
      }),
      new LinkDefinition({
        linkName: 'hasSubProject',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'parentProject',
          useIndexMatcherOf: ProjectContributionDefinition
        }),
        rdfReversedObjectProperty: "mnx:hasParentProject",
        relatedModelDefinition: ProjectDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        relatedInputName: 'subProjectInputs'
      }),
      new LinkDefinition({
        linkName: 'hasProjectContribution',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'project',
          useIndexMatcherOf: ProjectContributionDefinition
        }),
        rdfReversedObjectProperty: "mnx:hasProject",
        relatedModelDefinition: ProjectContributionDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: 'projectContributionInputs'
      }),
      new LinkDefinition({
        linkName: 'hasProjectOutput',
        rdfObjectProperty: "mnx:hasProjectOutput",
        relatedModelDefinition: ProjectOutputDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: 'projectOutputInputs'
      }),
      new LinkDefinition({
        linkName: 'hasTeam',
        rdfObjectProperty: "mnx:hasTeam",
        relatedModelDefinition: UserGroupDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        relatedInputName: 'teamInput'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInIndex: 'titles',
        rdfDataProperty: 'mnx:title',
        isSearchable: true,
        isRequired: true
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInIndex: 'descriptions',
        rdfDataProperty: 'mnx:description'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInIndex: 'shortDescriptions',
        rdfDataProperty: "mnx:shortDescription"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      }),
      new LiteralDefinition({
        literalName: 'image',
        rdfDataProperty: "mnx:image"
      })
    ];
  }
};