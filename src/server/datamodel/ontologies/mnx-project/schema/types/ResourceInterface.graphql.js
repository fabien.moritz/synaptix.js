/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateInterfaceResolvers,
  getLinkedObjectsResolver,
  getObjectResolver,
  getObjectsCountResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {EntityInterfaceProperties} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import ResourceDefinition from "../../definitions/ResourceDefinition";

export let ResourceInterfaceProperties = `
  ${EntityInterfaceProperties}
  
  attachments(${connectionArgs}, ${filteringArgs}): AttachmentConnection
`;

export let ResourceInterface = `
""" A resource interface"""
interface ResourceInterface {
  ${ResourceInterfaceProperties}
}

${generateConnectionForType("ResourceInterface")}

""" A resource input """
input ResourceInput {
  ${ResourceDefinition.generateGraphQLInputProperties()}
}

extend type Query{
  """ Search for resources """
  resources(${connectionArgs}, ${filteringArgs}): ResourceInterfaceConnection

  """ Count for resources """
  resourcesCount(${filteringArgs}): Int
  
  """ Get an project contribution """
  resource(id: ID!): ResourceInterface
}
`;

export let ResourceResolvers = {
  ResourceInterface: {
    ...generateInterfaceResolvers("ResourceInterface"),
    attachments: getLinkedObjectsResolver.bind(this, ResourceDefinition.getLink('hasAttachment'))
  },
  Query: {
    resources: getObjectsResolver.bind(this, ResourceDefinition),
    resourcesCount: getObjectsCountResolver.bind(this, ResourceDefinition),
    resource: getObjectResolver.bind(this, ResourceDefinition)
  },
  ...generateConnectionResolverFor("ResourceInterface")
};