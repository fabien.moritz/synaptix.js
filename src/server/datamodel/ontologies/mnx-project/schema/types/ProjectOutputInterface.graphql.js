/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateInterfaceResolvers, getLinkedObjectResolver, getLinkedObjectsResolver, getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import ProjectOutputInterfaceDefinition from "../../definitions/ProjectOutputDefinition";
import {EntityInterfaceProperties} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let ProjectOutputProperties = `
  ${EntityInterfaceProperties}

  """ Title """
  title: String
  
  """ Description """
  description: String

  """ Short description """
  shortDescription: String

  """ Related project """
  project: Project
  
  """ Related project title """
  projectTitle: String
  
  """ Related pictures """
  pictures: FileConnection
  
  """ Related main picture """
  mainPicture: File
  
  """ Related author """
  author: AgentInterface
  
  """ Related representations """
  representations(${connectionArgs}, ${filteringArgs}): ProjectOutputInterfaceConnection
`;

export let ProjectOutputInterfaceType = `
""" A project output interface"""
interface ProjectOutputInterface {
  ${ProjectOutputProperties}
}

${generateConnectionForType("ProjectOutputInterface")}

""" A projectOutput input """
input ProjectOutputInput {
  ${ProjectOutputInterfaceDefinition.generateGraphQLInputProperties()}
}

extend type Query{
  """ Search for project outputs """
  projectOutputs(${connectionArgs}, ${filteringArgs}): ProjectOutputInterfaceConnection

  """ Get an project output """
  projectOutput(id: ID!): ProjectOutputInterface
}
`;

export let ProjectOutputResolvers = {
  ProjectOutputInterface: {
    title: getLocalizedLabelResolver.bind(this, ProjectOutputInterfaceDefinition.getLabel("title")),
    description: getLocalizedLabelResolver.bind(this, ProjectOutputInterfaceDefinition.getLabel("description")),
    shortDescription: getLocalizedLabelResolver.bind(this, ProjectOutputInterfaceDefinition.getLabel("shortDescription")),
    project: getLinkedObjectResolver.bind(this, ProjectOutputInterfaceDefinition.getLink("hasProject")),
    projectTitle: getLocalizedLabelResolver.bind(this, ProjectOutputInterfaceDefinition.getLabel("projectTitle")),
    pictures: getLinkedObjectsResolver.bind(this, ProjectOutputInterfaceDefinition.getLink("hasPicture")),
    mainPicture: getLinkedObjectResolver.bind(this, ProjectOutputInterfaceDefinition.getLink("hasMainPicture")),
    author: getLinkedObjectResolver.bind(this, ProjectOutputInterfaceDefinition.getLink("hasAuthor")),
    representations: getLinkedObjectsResolver.bind(this, ProjectOutputInterfaceDefinition.getLink("hasRepresentation")),
    ...generateInterfaceResolvers("ProjectOutputInterface"),
  },
  Query: {
    projectOutputs: getObjectsResolver.bind(this, ProjectOutputInterfaceDefinition),
    projectOutput: getObjectResolver.bind(this, ProjectOutputInterfaceDefinition)
  },
  ...generateConnectionResolverFor("ProjectOutputInterface")
};