/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateConnectionForType,
  generateConnectionResolverFor,
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import AttachmentDefinition from "../../definitions/AttachmentDefinition";

export let AttachmentType = `
""" Reification object to link a resource with a project contribution """
type Attachment implements EntityInterface {
  ${EntityInterfaceProperties}

  """ Resource """
  resource: ResourceInterface
  
  """ ProjectContribution """
  projectContribution: ProjectContribution
}

input AttachmentInput {
  ${AttachmentDefinition.generateGraphQLInputProperties()}
}

${generateConnectionForType("Attachment")}

extend type Query{
  """ Get attachment """
  attachment(id:ID): Attachment
}
`;


export let AttachmentResolvers = {
  Attachment: {
    resource: getLinkedObjectResolver.bind(this, AttachmentDefinition.getLink('hasResource')),
    projectContribution: getLinkedObjectResolver.bind(this, AttachmentDefinition.getLink('hasProjectContribution')),
    ...generateTypeResolvers("Attachment"),
  },
  Query: {
    attachment: getObjectResolver.bind(this, AttachmentDefinition)
  },
  ...generateConnectionResolverFor("Attachment")
};