/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateTypeResolvers,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor,
} from "../../../../toolkit/graphql/schema/types/generators";
import {EntityInterfaceInput} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import FileDefinition from "../../definitions/FileDefinition";
import {ResourceInterfaceProperties} from "./ResourceInterface.graphql";

export let FileType = `
""" Reification object to link a resource with a project contribution """
type File implements EntityInterface & ResourceInterface {
  ${ResourceInterfaceProperties}
  
  """ File name """
  filename: String
  
  """ File Size """
  size: Float
  
  """ File MIME """
  mime: String
}

""" A file input """
input FileInput {
  ${FileDefinition.generateGraphQLInputProperties()}
}

${generateConnectionForType("File")}

extend type Query{
  """ Search for projects """
  files(${connectionArgs}, ${filteringArgs}): FileConnection
  
  """ Get file """
  file(id:ID): File
}
`;


export let FileResolvers = {
  File: {
    filename: (object) => object.filename,
    mime: (object) => object.mime,
    size: (object) => object.size,
    ...generateTypeResolvers("File"),
  },
  Query: {
    files: async (parent, args, synaptixSession) => getObjectsResolver(FileDefinition, parent, args, synaptixSession),
    file: getObjectResolver.bind(this, FileDefinition)
  },
  ...generateConnectionResolverFor("File")
};