/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  DurationInterfaceInput,
  DurationInterfaceProperties
} from "../../../mnx-time/schema/types/DurationInterface.graphql";
import ProjectContributionDefinition from "../../definitions/ProjectContributionDefinition";
import ProjectContribution from "../../models/ProjectContribution";

export let ProjectContributionType = `
""" A project contribution """
type ProjectContribution implements EntityInterface & DurationInterface{
  ${DurationInterfaceProperties}

  """ Title for current language """
  title: String

  """ Description """
  description: String

  """ Short description"""
  shortDescription: String
  
  """ Memo """
  memo: String
  
  """ Project """
  project: Project
  
  """ Involvements """
  involvements(${connectionArgs}, ${filteringArgs}): InvolvementConnection
  
  """ Involvements count"""
  involvementsCount: Int
  
  """ Attachments """
  attachments(${connectionArgs}, ${filteringArgs}): AttachmentConnection
  
  """ Attachments count """
  attachmentsCount: Int 
}

${generateConnectionForType("ProjectContribution")}

input ProjectContributionInput {
  ${ProjectContributionDefinition.generateGraphQLInputProperties()}
}

extend type Query{
  """ Search for project contributions """
  projectContributions(${connectionArgs}, ${filteringArgs}): ProjectContributionConnection

  """ Get an project contribution """
  projectContribution(id: ID!): ProjectContribution
}
`;

export let ProjectContributionResolvers = {
  ProjectContribution: {
    ...generateTypeResolvers("ProjectContribution"),
    title: getLocalizedLabelResolver.bind(this, ProjectContributionDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, ProjectContributionDefinition.getLabel('description')),
    shortDescription: getLocalizedLabelResolver.bind(this, ProjectContributionDefinition.getLabel('shortDescription')),
    memo: getLocalizedLabelResolver.bind(this, ProjectContributionDefinition.getLabel('memo')),
    project: getLinkedObjectResolver.bind(this, ProjectContributionDefinition.getLink('hasProject')),
    involvements: getLinkedObjectsResolver.bind(this, ProjectContributionDefinition.getLink('hasInvolvement')),
    involvementsCount: getLinkedObjectsCountResolver.bind(this, ProjectContributionDefinition.getLink('hasInvolvement')),
    attachments: getLinkedObjectsResolver.bind(this, ProjectContributionDefinition.getLink('hasAttachment')),
    attachmentsCount: getLinkedObjectsCountResolver.bind(this, ProjectContributionDefinition.getLink('hasAttachment')),
  },
  Query: {
    projectContributions: async (parent, args, synaptixSession) => getObjectsResolver(ProjectContributionDefinition, parent, args, synaptixSession),
    projectContribution: getObjectResolver.bind(this, ProjectContributionDefinition)
  },
  ...generateConnectionResolverFor("ProjectContribution")
};