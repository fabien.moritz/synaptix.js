/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver, getObjectsCountResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import ProjectDefinition from "../../definitions/ProjectDefinition";

export let ProjectType = `
""" A project """
type Project implements EntityInterface {
  ${EntityInterfaceProperties}
   
  """ Title for current language """
  title: String

  """ Description of the project """
  description: String

  """ Short description"""
  shortDescription: String
  
  """ Color """
  color: String
  
  """ Representative image URI """
  image: String

  """ Team """
  team: UserGroup
  
  """ Parent project """
  parentProject: Project
    
  """ Sub projects """
  subProjects(${connectionArgs}, ${filteringArgs}): ProjectConnection
  
   """ Sub projects count """
  subProjectsCount: Int
  
  """ Get project contributions """
  projectContributions(${connectionArgs}, ${filteringArgs}): ProjectContributionConnection
  
  """ Get project contributions count """
  projectContributionsCount: Int
  
  """ Oldest project contribution start date """
  startDate: String
  
  """ Newest project contribution end date """
  endDate: String
}

${generateConnectionForType("Project")}

input ProjectInput {
  ${ProjectDefinition.generateGraphQLInputProperties()}
}

extend type Query{
  """ Search for projects """
  projects(${connectionArgs}, ${filteringArgs}): ProjectConnection
  
   """ Search for projects count """
  projectsCount(${filteringArgs}): Int
  
  """ Get a project """
  project(id: ID!): Project
}
`;

export let ProjectResolvers = {
  Project: {
    ...generateTypeResolvers("Project"),
    title: getLocalizedLabelResolver.bind(this, ProjectDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, ProjectDefinition.getLabel('description')),
    shortDescription: getLocalizedLabelResolver.bind(this, ProjectDefinition.getLabel('shortDescription')),
    color: (object) => object.color,
    image: (object) => object.image,
    team: getLinkedObjectResolver.bind(this, ProjectDefinition.getLink('hasTeam')),
    parentProject: getLinkedObjectResolver.bind(this, ProjectDefinition.getLink('hasParentProject')),
    subProjects: getLinkedObjectsResolver.bind(this, ProjectDefinition.getLink('hasSubProject')),
    subProjectsCount: getLinkedObjectsCountResolver.bind(this, ProjectDefinition.getLink('hasSubProject')),
    projectContributions: getLinkedObjectsResolver.bind(this, ProjectDefinition.getLink('hasProjectContribution')),
    projectContributionsCount: getLinkedObjectsCountResolver.bind(this, ProjectDefinition.getLink('hasProjectContribution')),
    startDate: async (object, args, synaptixSession) => {
      let projectContributions = await synaptixSession.getLinkedObjectsFor(object, ProjectDefinition.getLink('hasProjectContribution'), {
        first: 1,
        sortBy: 'startDate',
        sortDirection: 'asc'
      });

      if (projectContributions.length > 0) {
        return projectContributions[0].startDate;
      }
    },
    endDate: async (object, args, synaptixSession) => {
      let projectContributions = await synaptixSession.getLinkedObjectsFor(object, ProjectDefinition.getLink('hasProjectContribution'), {
        first: 1,
        sortings: [{
          sortBy: 'startDate',
          sortDirection: 'desc'
        }, {
          sortBy: 'endDate',
          sortDirection: 'desc'
        }]
      });

      if (projectContributions.length > 0) {
        return projectContributions[0].startDate;
      }
    }
  },
  Query: {
    projects: getObjectsResolver.bind(this, ProjectDefinition),
    projectsCount: getObjectsCountResolver.bind(this, ProjectDefinition),
    project: getObjectResolver.bind(this, ProjectDefinition)
  },
  ...generateConnectionResolverFor("Project")
};