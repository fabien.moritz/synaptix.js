/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import InvolvementDefinition from "../../definitions/InvolvementDefinition";
import AgentDefinition from "../../../mnx-agent/definitions/AgentDefinition";

export let InvolvementType = `
""" Reification object to link a resource with an object """
type Involvement implements EntityInterface {
  ${EntityInterfaceProperties}
   
  """ Role """
  role: String
  
  """ Actor """
  agent: AgentInterface
  
  """ Project contribution """
  projectContribution: ProjectContribution
}

input InvolvementInput {
  ${InvolvementDefinition.generateGraphQLInputProperties()}
}

${generateConnectionForType("Involvement")}

extend type Query{
  """ Get involvement """
  involvement(id:ID): Involvement
}

extend interface AgentInterface{
  """ Affilitations """
  involvements(${connectionArgs}, ${filteringArgs}): InvolvementConnection
  
  """ Affilitations count """
  involvementsCount: Int
}

extend type Person{
  """ Affilitations """
  involvements(${connectionArgs}, ${filteringArgs}): InvolvementConnection
  
  """ Affilitations count """
  involvementsCount: Int
}

extend type Organization{
  """ Affilitations """
  involvements(${connectionArgs}, ${filteringArgs}): InvolvementConnection
  
  """ Affilitations count """
  involvementsCount: Int
}
`;

export let InvolvementResolvers = {
  Involvement: {
    role: getLocalizedLabelResolver.bind(this, InvolvementDefinition.getLabel('role')),
    agent: getLinkedObjectResolver.bind(this, InvolvementDefinition.getLink('hasAgent')),
    projectContribution: getLinkedObjectResolver.bind(this, InvolvementDefinition.getLink('hasProjectContribution')),
    ...generateTypeResolvers("Involvement"),
  },
  Query: {
    involvement: getObjectResolver.bind(this, InvolvementDefinition)
  },
  AgentInterface: {
    involvements: getLinkedObjectsResolver.bind(this, AgentDefinition.getLink('hasInvolvement')),
    involvementsCount: getLinkedObjectsCountResolver.bind(this, AgentDefinition.getLink('hasInvolvement')),
  },
  ...generateConnectionResolverFor("Involvement")
};