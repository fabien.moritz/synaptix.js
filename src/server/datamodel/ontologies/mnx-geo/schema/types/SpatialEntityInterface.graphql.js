/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import SpatialEntityDefinition from '../../definitions/SpatialEntityDefinition';

import {EntityInterfaceProperties} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {
  filteringArgs,
  connectionArgs,
  generateConnectionResolverFor,
  generateConnectionForType
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  generateInterfaceResolvers,
  getLinkedObjectResolver,
  getObjectsResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

export let SpatialEntityInterfaceProperties = `
  ${EntityInterfaceProperties}
  
  """ Has geometry """
  hasGeometry: GeometryInterface
`;

export let SpatialEntityInterface = `
interface SpatialEntityInterface {
  ${SpatialEntityInterfaceProperties}
}
  
${generateConnectionForType("SpatialEntityInterface")}
  
extend type Query{
  """ Search for SpatialEntity """
  spatialEntities(${connectionArgs}, ${filteringArgs}): SpatialEntityInterfaceConnection
  
  """ Get SpatialEntity """
  spatialEntity(id:ID!): SpatialEntityInterface
}
`;

export let SpatialEntityResolvers = {
  SpatialEntityInterface: {
    ...generateInterfaceResolvers("SpatialEntityInterface"),
    hasGeometry: getLinkedObjectResolver.bind(this, SpatialEntityDefinition.getLink('hasGeometry')),
  },
  Query: {
    spatialEntities: getObjectsResolver.bind(this, SpatialEntityDefinition),
    spatialEntity: getObjectResolver.bind(this, SpatialEntityDefinition)
  },
  ...generateConnectionResolverFor("SpatialEntityInterface")
};

