/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  filteringArgs,
  connectionArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  generateInterfaceResolvers,
  generateTypeResolvers,
  getObjectsResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

import {SpatialEntityInterfaceProperties} from "./SpatialEntityInterface.graphql";
import LocationDefinition from "../../definitions/LocationDefinition";
import {EntityInterfaceInput} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let LocationInterfaceProperties = `
  """ Geonames feature"""
  geonamesFeature: String

  ${SpatialEntityInterfaceProperties}
`;

export let LocationType = `
interface LocationInterface{
  ${LocationInterfaceProperties}
}

type Location implements EntityInterface & LocationInterface & SpatialEntityInterface {
  ${LocationInterfaceProperties}
}
  
${generateConnectionForType("Location")}

input LocationInput {
  ${EntityInterfaceInput}

  """ Geonames feature"""
  geonamesFeature: String
}

extend type Query{
  """ Search for locations """
  locations(${connectionArgs}, ${filteringArgs}): LocationConnection
  
  """ Get Location """
  location(id:ID!): Location
}
`;

export let LocationResolvers = {
  LocationInterface: {
    ...generateInterfaceResolvers("LocationInterface"),
    geonamesFeature: (object) => object.geonamesFeature,
  },
  Location: {
    ...generateTypeResolvers("Location"),
  },
  Query: {
    locations: getObjectsResolver.bind(this, LocationDefinition),
    location: getObjectResolver.bind(this, LocationDefinition)
  },
  ...generateConnectionResolverFor("Location")
};

