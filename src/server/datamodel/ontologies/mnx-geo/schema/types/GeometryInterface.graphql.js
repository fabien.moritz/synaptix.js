/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import GeometryDefinition from '../../definitions/GeometryDefinition';
import {
  filteringArgs,
  connectionArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

import {
  generateInterfaceResolvers,
  getObjectsResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

import {EntityInterfaceProperties} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let GeometryInterfaceProperties = `
  ${EntityInterfaceProperties}
  
  """ WKT notation """
  asWKT: String
`;

export let GeometryInterface = `
interface GeometryInterface {
  ${GeometryInterfaceProperties}
}
  
${generateConnectionForType("GeometryInterface")}
  
extend type Query{
  """ Search for geometries """
  geometries(${connectionArgs}, ${filteringArgs}): GeometryInterfaceConnection
  
  """ Get Geometry """
  geometry(id:ID!): GeometryInterface
}
`;

export let GeometryResolvers = {
  GeometryInterface: {
    ...generateInterfaceResolvers("GeometryInterface"),
    asWKT: (object) => object.asWKT,
  },
  Query: {
    geometries: getObjectsResolver.bind(this, GeometryDefinition),
    geometry: getObjectResolver.bind(this, GeometryDefinition)
  },
  ...generateConnectionResolverFor("GeometryInterface")
};

