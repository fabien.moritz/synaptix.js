/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import SpatialEntity from "../models/SpatialEntity";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import GeometryDefinition from "./GeometryDefinition";
import MaterialEntityDefinition from "../../mnx-project/definitions/MaterialEntityDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class SpatialEntityDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [MaterialEntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:SpatialEntity";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return SpatialEntity;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasGeometry',
        pathInIndex: 'geometry',
        rdfObjectProperty: 'mnx:hasGeometry',
        relatedModelDefinition: GeometryDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
      })
    ]
  }
};

