/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import DefaultIndexMatcher from "../../../toolkit/matchers/DefaultIndexMatcher";

export default class AffiliationIndexMatcher extends DefaultIndexMatcher {
  /**
   * @inheritDoc
   */
  getSortingMapping() {
    return {
      organization: 'organization.title',
      person: 'affiliate.name',
      creator: 'creator.fullName'
    };
  }

  /**
   * @inheritDoc
   */
  getFulltextQueryFragment(query) {
    return {
      "function_score": {
        "query": {
          "bool": {
            "should": [
              {
                "query_string": {
                  "query": `${query}*`
                }
              }
            ]
          }
        },
        "min_score": 0.2
      }
    };
  }

  /**
   * @inheritDoc
   */
  getFilterByKeyValue(key, value) {
    switch (key) {
      case "Organization":
        return {
          "nested": {
            "path": "organization",
            "query": this.getTermFilter("organization.id", value)
          }
        };
      case "person":
        return {
          "nested": {
            "path": "affiliate",
            "query": this.getTermFilter("affiliate.id", value)
          }
        };
      case "creator":
        return {
          "nested": {
            "path": "creator",
            "query": this.getTermFilter("creator.id", value)
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  /**
   * @inheritDoc
   */
  getAggregations() {
    let size = 100;

    return {
      "Organizations": {
        "nested": {
          "path": "organization"
        },
        "aggs": {
          "nesting": {
            "terms": {
              "field": "organization.id",
              "size": size
            }
          }
        }
      },
      "persons": {
        "nested": {
          "path": "affiliate"
        },
        "aggs": {
          "nesting": {
            "terms": {
              "field": "affiliate.id",
              "size": size
            }
          }
        }
      },
      "creators": {
        "nested": {
          "path": "creator"
        },
        "aggs": {
          "nesting": {
            "terms": {
              "field": "creator.id",
              "size": size
            }
          }
        }
      }
    };
  }
}