/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import PartnershipDefinition from "../../definitions/PartnershipDefinition";

export let PartnershipType = `
""" Reification object to link a organization with another organization """
type Partnership implements EntityInterface {
   ${EntityInterfaceProperties}
   
  """ Start date """
  startDate: String

  """ End date """
  endDate: String

  """ Role for current language """
  role: String
  
  """ Contractant organization """
  contractant: Organization
  
  """ Contractor organization """
  contractor: Organization
}

${generateConnectionForType("Partnership")}

input PartnershipInput {
  ${EntityInterfaceInput}

  """ Start date """
  startDate: String @formInput(type:"date")

  """ End date """
  endDate: String @formInput(type:"date")

  """ Role in the Organization """
  role: String
}

extend type Query{
  """ Get partnership """
  partnership(id:ID): Partnership
}

`;

export let PartnershipResolvers = {
  Partnership: {
    ...generateTypeResolvers("Partnership"),
    startDate: (object) => object.startDate,
    endDate: (object) => object.endDate,
    role: getLocalizedLabelResolver.bind(this, PartnershipDefinition.getLabel('role')),
    contractant: getLinkedObjectResolver.bind(this, PartnershipDefinition.getLink('hasContractant')),
    contractor: getLinkedObjectResolver.bind(this, PartnershipDefinition.getLink('hasContractor'))
  },
  Query: {
    partnership: async (root, {me, ...args}, synaptixSession) => getObjectResolver(PartnershipDefinition, root, args, synaptixSession)
  },
  ...generateConnectionResolverFor("Partnership")
};
