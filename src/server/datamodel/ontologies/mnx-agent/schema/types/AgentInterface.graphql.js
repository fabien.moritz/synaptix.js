/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {
  generateInterfaceResolvers,
  getLinkedObjectsResolver,
  getObjectResolver, getObjectsCountResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

import OrganizationDefinition from "../../definitions/OrganizationDefinition";
import PersonDefinition from "../../definitions/PersonDefinition";
import AgentDefinition from "../../definitions/AgentDefinition";
import Agent from "../../models/Agent";

export let AgentInterfaceProperties = `
  ${EntityInterfaceProperties}

  """ Display name """
  displayName: String
  
  """ Avatar """
  avatar: String

  """ Main email """
  mainEmail: EmailAccount

  """ Secondary emails """
  emails(${connectionArgs}, ${filteringArgs}): EmailAccountConnection

  """ Phones """
  phones(${connectionArgs}, ${filteringArgs}): PhoneConnection

  """ Addresses """
  addresses(${connectionArgs}, ${filteringArgs}): AddressConnection

  """ External links """
  externalLinks(${connectionArgs}, ${filteringArgs}): ExternalLinkConnection
`;

export let AgentInterfaceInput = `
  ${AgentDefinition.generateGraphQLInputProperties()}
`;

export let AgentInterface = `
""" An Agent interface """
interface AgentInterface {
  ${AgentInterfaceProperties}
}

${generateConnectionForType("AgentInterface")}

""" An agent input """
input AgentInput {
  ${AgentDefinition.generateGraphQLInputProperties()}
}

enum AgentEnum {
  Person
  Organization
}

extend type Query{
  """ Search for agents """
  agents(${connectionArgs}, ${filteringArgs}, filterOn: AgentEnum): AgentInterfaceConnection
  
  """ Count for agents """
  agentsCount(${filteringArgs}, filterOn: AgentEnum): Int
  
  """ Get agent """
  agent(id:ID! type:AgentEnum): AgentInterface
}
`;


export let AgentResolvers = {
  AgentInterface: {
    ...generateInterfaceResolvers("Agent"),
    avatar: (object) => object.avatar,
    emails: getLinkedObjectsResolver.bind(this, AgentDefinition.getLink('hasEmailAccount')),
    phones: getLinkedObjectsResolver.bind(this, AgentDefinition.getLink('hasPhone')),
    addresses: getLinkedObjectsResolver.bind(this, AgentDefinition.getLink('hasAddress')),
    externalLinks: getLinkedObjectsResolver.bind(this, AgentDefinition.getLink('hasExternalLink'))
  },
  Query: {
    agents: async (parent, {filterOn, ...args} = {}, synaptixSession, info) => {
      let modelDefinition = AgentDefinition;
      if (filterOn){
        modelDefinition = filterOn === "Organization" ? OrganizationDefinition : PersonDefinition;
      }
      return getObjectsResolver(modelDefinition, parent, args, synaptixSession, info);
    },
    agentsCount: async (parent, {filterOn, ...args} = {}, synaptixSession) => {
      let modelDefinition = AgentDefinition;
      if (filterOn){
        modelDefinition = filterOn === "Organization" ? OrganizationDefinition : PersonDefinition;
      }
      return getObjectsCountResolver(modelDefinition, parent, args, synaptixSession);
    },
    agent: async (parent, {id, type}, synaptixSession, info) => {
      return getObjectResolver(type === "Organization" ? OrganizationDefinition : PersonDefinition, parent, {id}, synaptixSession, info)
    }
  },
  ...generateConnectionResolverFor("AgentInterface"),
};