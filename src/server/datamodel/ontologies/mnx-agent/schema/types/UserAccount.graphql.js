/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {
  generateTypeResolvers, getLinkedObjectResolver, getLinkedObjectsResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs, filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import UserAccountDefinition from "../../definitions/UserAccountDefinition";
import {isAdminRule} from "../middlewares/aclValidation/rules/isAdminRule";

export let UserAccountType = `
type UserAccount implements EntityInterface & AccessPolicyTargetInterface{
  ${EntityInterfaceProperties}
  
  """ Account name """
  accountName: String

  """ User id """
  userId: String
  
  """ Username """
  username: String

  """ Person """
  person: Person
  
  """ Related user groups """
  userGroups(${filteringArgs} ${connectionArgs}): UserGroupConnection
  
  """ Check if userAccount belongs to given userGroup id"""
  isInGroup(userGroupId: ID!): Boolean
  
   """ Is user unregistered in SSO ? """
  isUnregistered: Boolean
  
  """ Is user disabled in SSO ? """
  isDisabled: Boolean
  
  """ Is user verified in SSO ?"""
  isVerified: Boolean
}

${generateConnectionForType("UserAccount")}

input UserAccountInput {
  ${UserAccountDefinition.generateGraphQLInputProperties()}
}


extend type Query{
  """ Get user account """
  userAccount(me: Boolean id:ID): UserAccount
  
  """ Search for user accounts """
  userAccounts(${connectionArgs}, ${filteringArgs}): UserAccountConnection
}
`;

export let UserAccountResolvers = {
  UserAccount: {
    ...generateTypeResolvers("UserAccount"),
    accountName: (object) => object.accountName,
    userId: (object) => object.userId,
    username: (object) => object.username,
    isVerified: (object) => !!object.isVerified,
    isUnregistered:  (object) => !!object.isUnregistered,
    isDisabled:  (object) => !!object.isDisabled,
    isInGroup: (object, {userGroupId}, synaptixSession) => synaptixSession.isObjectLinkedToTargetId({
      object,
      modelDefinition: UserAccountDefinition,
      linkDefinitions: [UserAccountDefinition.getLink("hasUserGroup")],
      targetId: synaptixSession.extractIdFromGlobalId(userGroupId)
    }),
    person: getLinkedObjectResolver.bind(this, UserAccountDefinition.getLink("hasPerson")),
    userGroups: getLinkedObjectsResolver.bind(this, UserAccountDefinition.getLink("hasUserGroup"))
  },
  Query: {
    userAccount: getObjectResolver.bind(this, UserAccountDefinition),
    userAccounts: getObjectsResolver.bind(this, UserAccountDefinition),
  },
  ...generateConnectionResolverFor("UserAccount")
};


export let UserAccountShieldRules = {
  Query: {
    userAccount: isAdminRule(),
    userAccounts: isAdminRule()
  }
};