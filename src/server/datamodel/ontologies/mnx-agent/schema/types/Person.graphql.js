/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getMeResolver,
  getObjectResolver,
  getObjectsCountResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {AgentInterfaceProperties} from "./AgentInterface.graphql";
import PersonDefinition from "../../definitions/PersonDefinition";


export let PersonType = `
type Person implements EntityInterface & AgentInterface {
  ${AgentInterfaceProperties}
  
  """ Full name """
  fullName: String

  """ First name """
  firstName: String

  """ Last name """
  lastName: String

  """ Nick name """
  nickName: String
  
  """ Maiden name """
  maidenName: String

  """ Gender """
  gender: String

  """ Bio for current language """
  bio: String

  """ Short Bio for current language """
  shortBio: String

  """ Birthday """
  bday: Float
  
  """ User account """
  userAccount: UserAccount
  
  """ Affilitations """
  affiliations(${connectionArgs}, ${filteringArgs}): AffiliationConnection
  
  """ Affilitations count """
  affiliationsCount: Int
}

input PersonInput {
  ${PersonDefinition.generateGraphQLInputProperties()}
}

${generateConnectionForType("Person")}

extend type Query{
  """ Get myself object """
  me: Person
  
  """ Get person """
  person(me: Boolean id:ID): Person
  
  """ Search for persons """
  persons(${connectionArgs}, ${filteringArgs}): PersonConnection
  
  """ Count for persons """
  personsCount(${filteringArgs}): Int
}
`;


export let PersonResolvers = {
  Person: {
    ...generateTypeResolvers("Person"),
    // This property is helpful for the UI
    displayName: (object) => {
      if (object.firstName || object.lastName) {
        return `${object.firstName || ''} ${object.lastName || ''}`.trim();
      }

      if (object.nickName) {
        return object.nickName;
      }

      return "anonymous";
    },
    fullName: (object) => `${object.firstName || ''} ${object.lastName || ''}`,
    firstName: (object) => object.firstName,
    lastName: (object) => object.lastName,
    nickName: (object) => object.nickName,
    maidenName: (object) => object.maidenName,
    gender: (object) => object.gender,
    bday: (object) => object.bday,
    bio: getLocalizedLabelResolver.bind(this, PersonDefinition.getLabel('bio')),
    shortBio: getLocalizedLabelResolver.bind(this, PersonDefinition.getLabel('shortBio')),
    mainEmail: async (object, args, synaptixSession) => {
      let emails = await synaptixSession.getLinkedObjectsFor(object, PersonDefinition.getLink('hasEmailAccount'), args);

      if (emails.length > 0) {
        return emails[0];
      }
    },
    userAccount: getLinkedObjectResolver.bind(this, PersonDefinition.getLink('hasUserAccount')),
    affiliations: getLinkedObjectsResolver.bind(this, PersonDefinition.getLink('hasAffiliation')),
    affiliationsCount: getLinkedObjectsCountResolver.bind(this, PersonDefinition.getLink('hasAffiliation')),
  },
  Query: {
    me: getMeResolver,
    person: async (root, {me, ...args}, synaptixSession, info) => {
      return me ? synaptixSession.getMe() : getObjectResolver(PersonDefinition, root, args, synaptixSession, info);
    },
    persons: getObjectsResolver.bind(this, PersonDefinition),
    personsCount: getObjectsCountResolver.bind(this, PersonDefinition),
  },
  ...generateConnectionResolverFor("Person")
};