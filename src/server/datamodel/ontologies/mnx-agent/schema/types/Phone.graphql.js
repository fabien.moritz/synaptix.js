/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {generateTypeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let PhoneType = `
type Phone implements EntityInterface {
  ${EntityInterfaceProperties}
  
  """ Phone label """
  label: String

  """ Phone number """
  number: String
}

${generateConnectionForType("Phone")}

input PhoneInput {
  ${EntityInterfaceInput}
  
  """ Label """
  label: String @formInput(enumValues: ["Pro", "Perso", "Mobile"])
  
  """ Number """
  number: String
}`;

export let PhoneResolvers = {
  Phone: {
    ...generateTypeResolvers("Phone"),
    label: (object) => object.label,
    number: (object) => object.number,
  },
  ...generateConnectionResolverFor("Phone")
};