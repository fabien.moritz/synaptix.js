/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateInterfaceResolvers,
  getLinkedObjectResolver,
  getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import GroupDefinition from "../../definitions/GroupDefinition";
import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let GroupInterfaceProperties = `
  ${EntityInterfaceProperties}
  
  """ Group name """
  name: String
  
  """ Memberships """
  memberships: MembershipConnection
`;

export let GroupInterfaceInput = `
  ${EntityInterfaceInput}
  
  """ Group name """
  name: String
`;


export let GroupInterfaceType = `
""" Reification object to link a person with an Organization """
interface GroupInterface{
   ${GroupInterfaceProperties}
}

${generateConnectionForType("GroupInterface")}
`;

export let GroupInterfaceResolvers = {
  GroupInterface: {
    ...generateInterfaceResolvers("GroupInterface"),
    name: getLocalizedLabelResolver.bind(this, GroupDefinition.getLabel('name')),
    memberships: getLinkedObjectResolver.bind(this, GroupDefinition.getLink('hasMembership'))
  },
  Query: {
    group: async (root, {me, ...args}, synaptixSession) => getObjectResolver(GroupDefinition, root, args, synaptixSession)
  },
  ...generateConnectionResolverFor("GroupInterface")
};
