/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import AffiliationDefinition from "../../definitions/AffiliationDefinition";
import {
  DurationInterfaceInput,
  DurationInterfaceProperties
} from "../../../mnx-time/schema/types/DurationInterface.graphql";

export let AffiliationType = `
""" Reification object to link a person with an Organization """
type Affiliation implements EntityInterface & DurationInterface{
   ${DurationInterfaceProperties}
   
  """ Role for current language """
  role: String
  
  """ Related person """
  person: Person
  
  """ Related organization """
  organization: Organization
}

${generateConnectionForType("Affiliation")}

input AffiliationInput {
  ${AffiliationDefinition.generateGraphQLInputProperties()}
}

extend type Query{
  """ Get affiliation """
  affiliation(id:ID): Affiliation
  
  """ Search for affiliations """
  affiliations(${connectionArgs}, ${filteringArgs}): AffiliationConnection
}

`;

export let AffiliationResolvers = {
  Affiliation: {
    ...generateTypeResolvers("Affiliation"),
    role: getLocalizedLabelResolver.bind(this, AffiliationDefinition.getLabel('role')),
    person: getLinkedObjectResolver.bind(this, AffiliationDefinition.getLink('hasPerson')),
    organization: getLinkedObjectResolver.bind(this, AffiliationDefinition.getLink('hasOrg'))
  },
  Query: {
    affiliations: async (parent, args, synaptixSession) => getObjectsResolver(AffiliationDefinition, parent, args, synaptixSession),
    affiliation: getObjectResolver.bind(this, AffiliationDefinition)
  },
  ...generateConnectionResolverFor("Affiliation")
};
