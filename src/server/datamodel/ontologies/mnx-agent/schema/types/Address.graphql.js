/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateTypeResolvers,
  getLocalizedLabelResolver,
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateConnectionForType,
  generateConnectionResolverFor,
} from "../../../../toolkit/graphql/schema/types/generators";
import AddressDefinition from '../../definitions/AddressDefinition';
import {LocationInterfaceProperties} from "../../../mnx-geo/schema/types/Location.graphql";
import {EntityInterfaceInput} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let AddressType = `
type Address implements EntityInterface & LocationInterface & SpatialEntityInterface {
  ${LocationInterfaceProperties}
    
  """ City"""
  city: String
  
  """ Country"""
  country: String
  
  """ Postal code"""
  postalCode: String
  
  """ Street 1"""
  street1: String
  
  """ Street 2"""
  street2: String
}
  
${generateConnectionForType("Address")}
  
input AddressInput {
  ${EntityInterfaceInput}

  """ City"""
  city: String
  
  """ Country"""
  country: String
  
  """ Postal code"""
  postalCode: String
  
  """ Street 1"""
  street1: String
  
  """ Street 2"""
  street2: String
}
`;

export let AddressResolvers = {
  Address: {
    ...generateTypeResolvers("Address"),
    street1: (object) => object.street1,
    street2: (object) => object.street2,
    postalCode: (object) => object.postalCode,
    city: getLocalizedLabelResolver.bind(this, AddressDefinition.getLabel('city')),
    country: getLocalizedLabelResolver.bind(this, AddressDefinition.getLabel('country')),
  },
  ...generateConnectionResolverFor("Address")
};

