/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateInterfaceResolvers,
  generateTypeResolvers,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsCountResolver,
  getObjectsResolver,
  getTypeResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {AgentInterfaceProperties} from "./AgentInterface.graphql";
import OrganizationDefinition from "../../definitions/OrganizationDefinition";

export let OrganizationInterfaceProperties = `
    ${AgentInterfaceProperties}
  
    """ Name """
    name: String

    """ Short name """
    shortName: String

    """ Description """
    description: String
    
    
    """ Short description """
    shortDescription: String
    
    """ Affilitations """
    affiliations(${connectionArgs}, ${filteringArgs}): AffiliationConnection
    
    """ Affilitations count """
    affiliationsCount: Int
`;

export let OrganizationInterfaceInput = `
  ${OrganizationDefinition.generateGraphQLInputProperties()}
`;


export let OrganizationType = `
interface OrganizationInterface {
  ${OrganizationInterfaceProperties}
}

type Organization implements EntityInterface & AgentInterface & OrganizationInterface{
  ${OrganizationInterfaceProperties}
}

input OrganizationInput {
  ${OrganizationInterfaceInput}
}

${generateConnectionForType("Organization")}

extend type Query{
  """ Get organization """
  organization(id:ID): Organization
  
  """ Search for organizations """
  organizations(${connectionArgs}, ${filteringArgs}): OrganizationConnection
  
  """ Count for organizations """
  organizationsCount(${filteringArgs}): Int
}

`;


export let OrganizationResolvers = {
  OrganizationInterface: {
    ...generateInterfaceResolvers("Organization"),
    __resolveType: getTypeResolver.bind(this),
    displayName: getLocalizedLabelResolver.bind(this, OrganizationDefinition.getLabel('name')),
    name: getLocalizedLabelResolver.bind(this, OrganizationDefinition.getLabel('name')),
    shortName: getLocalizedLabelResolver.bind(this, OrganizationDefinition.getLabel('shortName')),
    avatar: (object) => object.avatar,
    description: getLocalizedLabelResolver.bind(this, OrganizationDefinition.getLabel('description')),
    shortDescription: getLocalizedLabelResolver.bind(this, OrganizationDefinition.getLabel('shortDescription')),
    affiliations: getLinkedObjectsResolver.bind(this, OrganizationDefinition.getLink('hasAffiliation')),
    affiliationsCount: getLinkedObjectsCountResolver.bind(this, OrganizationDefinition.getLink('hasAffiliation')),
  },
  Organization: {
    ...generateTypeResolvers("Organization")
  },
  Query: {
    organization: getObjectResolver.bind(this, OrganizationDefinition),
    organizations: getObjectsResolver.bind(this, OrganizationDefinition),
    organizationsCount: getObjectsCountResolver.bind(this, OrganizationDefinition)
  },
  ...generateConnectionResolverFor("Organization")
};
