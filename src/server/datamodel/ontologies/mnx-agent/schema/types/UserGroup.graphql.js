/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {
  generateTypeResolvers, getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs, filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import UserGroupDefinition from "../../definitions/UserGroupDefinition";
import {isAdminRule} from "../middlewares/aclValidation/rules/isAdminRule";

export let UserGroupType = `
type UserGroup implements EntityInterface & AccessPolicyTargetInterface {
  ${EntityInterfaceProperties}
  
  """ Human readable label """
  label: String
}

${generateConnectionForType("UserGroup")}

input UserGroupInput {
  ${UserGroupDefinition.generateGraphQLInputProperties()}
}

extend type Query{
  """ Get user group """
  userGroup(me: Boolean id:ID): UserGroup
  
  """ Search for user groups """
  userGroups(${connectionArgs}, ${filteringArgs}): UserGroupConnection
}
`;

export let UserGroupResolvers = {
  UserGroup: {
    label: getLocalizedLabelResolver.bind(this, UserGroupDefinition.getLabel("label")),
    ...generateTypeResolvers("UserGroup"),
  },
  Query: {
    userGroup: getObjectResolver.bind(this, UserGroupDefinition),
    userGroups: getObjectsResolver.bind(this, UserGroupDefinition),
  },
  ...generateConnectionResolverFor("UserGroup")
};

export let UserGroupShieldRules = {
  Query: {
    userGroup: isAdminRule(),
    userGroups: isAdminRule()
  }
};