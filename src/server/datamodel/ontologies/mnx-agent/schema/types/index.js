/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {mergeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {AgentInterface, AgentResolvers} from "./AgentInterface.graphql";
import {PersonResolvers, PersonType} from "./Person.graphql";
import {OrganizationInterfaceProperties, OrganizationInterfaceInput, OrganizationResolvers, OrganizationType} from "./Organization.graphql";
import {PhoneResolvers, PhoneType} from "./Phone.graphql";
import {EmailAccountResolvers, EmailAccountType} from "./EmailAccount.graphql";
import {AffiliationResolvers, AffiliationType} from "./Affiliation.graphql";
import {PartnershipResolvers, PartnershipType} from "./Partnership.graphql";
import {UserGroupResolvers, UserGroupShieldRules, UserGroupType} from "./UserGroup.graphql";
import {AddressResolvers, AddressType} from "./Address.graphql";
import {UserAccountResolvers, UserAccountShieldRules, UserAccountType} from "./UserAccount.graphql";
import {mergeRules} from "../../../../toolkit/graphql/schema/middlewares/aclValidation/rules";

export let MnxAgentInterfaceProperties = {
  OrganizationInterfaceProperties
};

export let MnxAgentInterfaceInput = {
  OrganizationInterfaceInput
};

export let MnxAgentTypes = [
  AddressType,
  AffiliationType,
  AgentInterface,
  EmailAccountType,
  OrganizationType,
  PartnershipType,
  PersonType,
  PhoneType,
  UserAccountType,
  UserGroupType,
];

export let MnxAgentTypesResolvers = mergeResolvers(
  AddressResolvers,
  AffiliationResolvers,
  AgentResolvers,
  EmailAccountResolvers,
  OrganizationResolvers,
  PartnershipResolvers,
  PersonResolvers,
  PhoneResolvers,
  UserAccountResolvers,
  UserGroupResolvers,
);

export let MnxAgentShieldsRules = mergeRules(
  UserAccountShieldRules,
  UserGroupShieldRules
);