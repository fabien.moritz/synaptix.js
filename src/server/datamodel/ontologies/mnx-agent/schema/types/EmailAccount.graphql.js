/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  EntityInterfaceInput,
  EntityInterfaceProperties
} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {generateTypeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let EmailAccountType = `
type EmailAccount implements EntityInterface {
  ${EntityInterfaceProperties}
  
  """ Account name """
  accountName: String

  """ Email """
  email: String

  """ Is email verified """
  isVerified: Boolean

  """ Is email the main one """
  isMainEmail: Boolean
}

${generateConnectionForType("EmailAccount")}


input EmailAccountInput {
  ${EntityInterfaceInput}

  """ Label """
  accountName: String @formInput(enumValues: ["Main", "Pro", "Perso", "Other"])

  """ Email """
  email: String
}

`;

export let EmailAccountResolvers = {
  EmailAccount: {
    ...generateTypeResolvers("EmailAccount"),
    accountName: (object) => object.accountName,
    email: (object) => object.email,
    isVerified: (object) => object.isVerified || false,
    isMainEmail: (object) => object.isMainEmail || false
  },
  ...generateConnectionResolverFor("EmailAccount")
};