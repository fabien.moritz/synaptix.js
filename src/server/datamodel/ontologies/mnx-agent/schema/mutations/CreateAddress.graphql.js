/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import AddressDefinition from "../../definitions/AddressDefinition";
import {
  generateCreateMutationDefinitionForType,
  generateCreateMutationResolverForType,
} from "../../../../toolkit/graphql/schema/mutations/generators";
import {createObjectInConnectionResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import PointDefinition from "../../../mnx-geo/definitions/PointDefinition";

export let CreateAddress = generateCreateMutationDefinitionForType('Address', `
    agentId: ID, geoCoords: [Float]
`);

export let CreateAddressResolvers = generateCreateMutationResolverForType('Address',
  (_, {input: {objectInput, agentId, geoCoords}}, synaptixSession) => {
    let links = [];

    if (agentId) {
      links.push(
        AddressDefinition.getLink("hasAgent").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(agentId))
      );
    }

    if (geoCoords) {
      let [lat, long] = geoCoords;

      links.push(
        AddressDefinition.getLink("hasGeometry").generateLinkFromTargetProps({
          targetObjectInput: {lat, long},
          targetModelDefinition: PointDefinition
        })
      );
    }

    return createObjectInConnectionResolver(AddressDefinition, links, _, {input: {objectInput}}, synaptixSession);
  });