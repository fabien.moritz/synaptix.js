/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {createObjectInConnectionResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateCreateMutationDefinitionForType,
  generateCreateMutationResolverForType
} from "../../../../toolkit/graphql/schema/mutations/generators";
import PartnershipDefinition from "../../definitions/PartnershipDefinition";

export let CreatePartnership = generateCreateMutationDefinitionForType('Partnership', `
  contractorId: ID!
  contractantId: ID!
`);

export let CreatePartnershipResolvers = generateCreateMutationResolverForType('Partnership',
  (_, {input: {objectInput, contractorId, contractantId}}, synaptixSession) => createObjectInConnectionResolver(
    PartnershipDefinition,
    [
      PartnershipDefinition.getLink("hasContractor").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(contractorId)),
      PartnershipDefinition.getLink("hasContractant").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(contractantId))
    ],
    _, {input: {objectInput}}, synaptixSession)
);