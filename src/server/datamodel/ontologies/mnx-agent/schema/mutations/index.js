/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {mergeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {UpdateOrganization, UpdateOrganizationResolvers} from "./UpdateOrganization.graphql";
import {UpdatePerson, UpdatePersonResolvers} from "./UpdatePerson.graphql";
import {UpdatePhone, UpdatePhoneResolvers} from "./UpdatePhone.graphql";
import {UpdateEmailAccount, UpdateEmailAccountResolvers} from "./UpdateEmailAccount.graphql";
import {UpdateAffiliation, UpdateAffiliationResolvers} from "./UpdateAffiliation.graphql";
import {CreatePerson, CreatePersonResolvers} from "./CreatePerson.graphql";
import {CreateOrganization, CreateOrganizationResolvers} from "./CreateOrganization.graphql";
import {CreateEmailAccount, CreateEmailAccountResolvers} from "./CreateEmailAccount.graphql";
import {CreatePhone, CreatePhoneResolvers} from "./CreatePhone.graphql";
import {CreateAffiliation, CreateAffiliationResolvers} from "./CreateAffiliation.graphql";
import {UpdateAddress, UpdateAddressResolvers} from "./UpdateAddress.graphql";
import {CreateAddress, CreateAddressResolvers} from "./CreateAddress.graphql";
import {CreatePartnership, CreatePartnershipResolvers} from "./CreatePartnership.graphql";
import {UpdatePartnership, UpdatePartnershipResolvers} from "./UpdatePartnership.graphql";
import {CreateUserGroup, CreateUserGroupResolvers} from "./CreateUserGroup.graphql";
import {UpdateUserGroup, UpdateUserGroupResolvers, UpdateUserGroupShieldRules} from "./UpdateUserGroup.graphql";
import {UpdateUserAccount, UpdateUserAccountResolvers, UpdateUserAccountShieldRules} from "./UpdateUserAccount.graphql";
import {mergeRules} from "../../../../toolkit/graphql/schema/middlewares/aclValidation/rules";

export let MnxAgentMutations = [
  CreateAddress,
  CreateAffiliation,
  CreateEmailAccount,
  CreateOrganization,
  CreatePartnership,
  CreatePerson,
  CreatePhone,
  CreateUserGroup,
  UpdateAddress,
  UpdateAffiliation,
  UpdateEmailAccount,
  UpdateOrganization,
  UpdatePartnership,
  UpdatePerson,
  UpdatePhone,
  UpdateUserGroup,
  UpdateUserAccount
];

export let MnxAgentMutationsResolvers = mergeResolvers(
  CreateAddressResolvers,
  CreateAffiliationResolvers,
  CreateEmailAccountResolvers,
  CreateOrganizationResolvers,
  CreatePartnershipResolvers,
  CreatePersonResolvers,
  CreatePhoneResolvers,
  CreateUserGroupResolvers,
  UpdateAddressResolvers,
  UpdateAffiliationResolvers,
  UpdateEmailAccountResolvers,
  UpdateOrganizationResolvers,
  UpdatePartnershipResolvers,
  UpdatePersonResolvers,
  UpdatePhoneResolvers,
  UpdateUserAccountResolvers,
  UpdateUserGroupResolvers,
);

export let MnxAgentMutationsShieldRules = mergeRules(
  UpdateUserAccountShieldRules,
  UpdateUserGroupShieldRules
);