/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import EmailAccountDefinition from "./EmailAccountDefinition";
import PhoneDefinition from "./PhoneDefinition";
import AddressDefinition from "./AddressDefinition";
import Agent from "../models/Agent";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import InvolvementDefinition from "../../mnx-project/definitions/InvolvementDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class AgentDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static isInstantiable(){
    return false;
  }

  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "foaf": "http://xmlns.com/foaf/0.1/"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Agent"
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Agent;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasEmailAccount',
        pathInIndex: 'emails',
        rdfObjectProperty: "foaf:account",
        relatedModelDefinition: EmailAccountDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        rdfPrefixesMapping: {
          "foaf": "http://xmlns.com/foaf/0.1/"
        },
        relatedInputName: "emailAccountInputs"
      }),
      new LinkDefinition({
        linkName: 'hasPhone',
        pathInIndex: 'phones',
        rdfObjectProperty: "mnx:hasPhone",
        relatedModelDefinition: PhoneDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        relatedInputName: "phoneInputs"
      }),
      new LinkDefinition({
        linkName: 'hasAddress',
        pathInIndex: 'addresses',
        rdfObjectProperty: "mnx:hasAddress",
        relatedModelDefinition: AddressDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        relatedInputName: "addressInputs"
      }),
      new LinkDefinition({
        linkName: 'hasInvolvement',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'agent',
          useIndexMatcherOf: InvolvementDefinition
        }),
        rdfReversedObjectProperty: "mnx:hasAgent",
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'avatar',
        rdfDataProperty: "mnx:avatar",
        inputFormOptions: 'type: "image"'
      })
    ];
  }
};
