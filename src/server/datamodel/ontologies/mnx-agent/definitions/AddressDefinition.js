/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import Address from "../models/Address";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import LocationDefinition from "../../mnx-geo/definitions/LocationDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import AgentDefinition from "./AgentDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class AddressDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [LocationDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Address";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Address;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasAgent',
        rdfReversedObjectProperty: "mnx:hasAddress",
        relatedModelDefinition: AgentDefinition,
        isCascadingUpdated: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'city',
        pathInIndex: 'cities',
        rdfDataProperty: 'mnx:city'
      }),
      new LabelDefinition({
        labelName: 'country',
        pathInIndex: 'countries',
        rdfDataProperty: 'mnx:country'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'label',
        rdfDataProperty: "rdfs:label"
      }),
      new LiteralDefinition({
        literalName: 'street1',
        rdfDataProperty: "mnx:street1"
      }),
      new LiteralDefinition({
        literalName: 'street2',
        rdfDataProperty: "mnx:street2"
      }),
      new LiteralDefinition({
        literalName: 'postalCode',
        rdfDataProperty: 'mnx:postalCode'
      })
    ];
  }
};