/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import UserGroup from "../models/UserGroup";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import AccessPolicyTargetDefinition from "../../mnx-acl/definitions/AccessPolicyTargetDefinition";
import UserAccountDefinition from "./UserAccountDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";

export default class UserGroupDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "sioc": "http://rdfs.org/sioc/ns#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AccessPolicyTargetDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:UserGroup";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["sioc:UserGroup"];
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return UserGroup;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasUserAccount',
        pathInIndex: 'hasUserAccount',
        rdfObjectProperty: "sioc:has_member",
        relatedModelDefinition: UserAccountDefinition,
        isPlural: true,
        relatedInputName: "userAccountInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'label',
        pathInIndex: 'labels',
        rdfDataProperty: "rdfs:label"
      }),
    ]
  }
};

