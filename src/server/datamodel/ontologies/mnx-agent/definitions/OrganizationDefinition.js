/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import AffiliationDefinition from "./AffiliationDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import AgentDefinition from "./AgentDefinition";
import OrganizationIndexMatcher from "../matchers/OrganizationIndexMatcher";
import Organization from "../models/Organization";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class OrganizationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AgentDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:Organization';
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["foaf:Organization"];
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'org';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Organization;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return OrganizationIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasAffiliation',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'organization',
          useIndexMatcherOf: AffiliationDefinition
        }),
        relatedModelDefinition: AffiliationDefinition,
        rdfReversedObjectProperty: "mnx:hasOrg",
        isPlural: true,
        isCascadingRemoved: true,
        relatedInputName: "affiliationInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'name',
        pathInIndex: 'names',
        rdfDataProperty: "mnx:name"
      }),
      new LabelDefinition({
        labelName: 'shortName',
        pathInIndex: 'shortNames',
        rdfDataProperty: "mnx:shortName"
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInIndex: 'descriptions',
        rdfDataProperty: "dc:description",
        inputFormOptions: 'type: "textarea"'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInIndex: 'shortDescriptions',
        rdfDataProperty: "mnx:shortDescription",
        inputFormOptions: 'type: "textarea"'
      }),
      new LabelDefinition({
        labelName: 'other',
        pathInIndex: 'others',
        rdfDataProperty: "mnx:other",
        inputFormOptions: 'type: "textarea"'
      })
    ];
  }
};