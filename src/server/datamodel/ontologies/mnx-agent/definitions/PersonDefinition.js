/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import Person from "../models/Person";
import PersonIndexMatcher from "../matchers/PersonIndexMatcher";
import UserAccountDefinition from "./UserAccountDefinition";
import AffiliationDefinition from "./AffiliationDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import AgentDefinition from "./AgentDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class PersonDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AgentDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'mnx:Person';
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["foaf:Person"];
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'person';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Person;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return PersonIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasUserAccount',
        pathInIndex: 'userAccount',
        rdfObjectProperty: "foaf:account",
        relatedModelDefinition: UserAccountDefinition,
        isPlural: false,
        isCascadingRemoved: true,
        rdfPrefixesMapping: {
          "foaf": "http://xmlns.com/foaf/0.1/"
        },
        relatedInputName: "userAccountInput"
      }),
      new LinkDefinition({
        linkName: 'hasAffiliation',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'person',
          useIndexMatcherOf: AffiliationDefinition
        }),
        rdfReversedObjectProperty: "mnx:hasPerson",
        relatedModelDefinition: AffiliationDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        relatedInputName: "affiliationInputs"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'bio',
        pathInIndex: 'bios',
        rdfDataProperty: "mnx:bio",
        inputFormOptions: 'type: "textarea"'
      }),
      new LabelDefinition({
        labelName: 'shortBio',
        pathInIndex: 'shortBios',
        rdfDataProperty: "mnx:shortBio",
        inputFormOptions: 'type: "textarea"'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'firstName',
        rdfDataProperty: 'foaf:firstName',
        isSearchable: true,
      }),
      new LiteralDefinition({
        literalName: 'lastName',
        rdfDataProperty: 'foaf:lastName',
        isSearchable: true
      }),
      new LiteralDefinition({
        literalName: 'nickName',
        rdfDataProperty: 'foaf:nick',
        isSearchable: true
      }),
      new LiteralDefinition({
        literalName: 'maidenName',
        rdfDataProperty: "mnx:maidenName",
        inputFormOptions: 'showIfPropName: "gender" showIfPropEnumValue: "female"'
      }),
      new LiteralDefinition({
        literalName: 'gender',
        rdfDataProperty: 'foaf:gender',
        inputFormOptions: 'enumValues: ["male", "female"]'
      }),
      new LiteralDefinition({
        literalName: 'birthday',
        rdfDataProperty: "foaf:birthday",
        inputFormOptions: 'type: "date"'
      })
    ];
  }
};