/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import UserAccount from "../models/UserAccount";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import AccessPolicyTargetDefinition from "../../mnx-acl/definitions/AccessPolicyTargetDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import UserGroupDefinition from "./UserGroupDefinition";
import PersonDefinition from "./PersonDefinition";
import CreationDefinition from "../../mnx-contribution/definitions/CreationDefinition";

export default class UserAccountDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "sioc": "http://rdfs.org/sioc/ns#",
      "prov": "http://www.w3.org/ns/prov#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [AccessPolicyTargetDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:UserAccount";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["sioc:User", "prov:Agent"];
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return UserAccount;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasUserGroup',
        pathInIndex: 'hasUserGroup',
        rdfReversedObjectProperty: "sioc:has_member",
        relatedModelDefinition: UserGroupDefinition,
        isPlural: true,
        relatedInputName: "userGroupInputs"
      }),
      new LinkDefinition({
        linkName: 'hasPerson',
        pathInIndex: 'hasPerson',
        rdfReversedObjectProperty: "foaf:account",
        relatedModelDefinition: PersonDefinition,
        isPlural: false,
        relatedInputName: "personInput"
      }),
      new LinkDefinition({
        linkName: 'hasCreatedAction',
        rdfReversedObjectProperty: "prov:wasAttributedTo",
        relatedModelDefinition: CreationDefinition,
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'userId',
        rdfDataProperty: "mnx:userId",
        isSearchable: true,
        excludeFromInput: true
      }),
      new LiteralDefinition({
        literalName: 'username',
        rdfDataProperty: "mnx:username",
        isSearchable: true,
        excludeFromInput: true
      }),
      new LiteralDefinition({
        literalName: 'isDisabled',
        rdfDataProperty: "mnx:isUserDisabled",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
      new LiteralDefinition({
        literalName: 'isUnregistered',
        rdfDataProperty: "mnx:isUserUnregistered",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
    ];
  }
};

