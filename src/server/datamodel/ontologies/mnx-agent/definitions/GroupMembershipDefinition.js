/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import PersonDefinition from "./PersonDefinition";
import GroupDefinition from "./GroupDefinition";
import GroupMembership from "../models/GroupMembership";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class GroupMembershipDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:GroupMembership";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return GroupMembership;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'hasPerson',
        pathInIndex: 'person',
        rdfObjectProperty: "mnx:hasPerson",
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'hasGroup',
        pathInIndex: 'group',
        rdfObjectProperty: "mnx:hasGroup",
        relatedModelDefinition: GroupDefinition,
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'roles',
        pathInIndex: 'roles',
        rdfDataProperty: "mnx:role"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'startDate',
        rdfDataProperty: "mnx:startDate",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      }),
      new LiteralDefinition({
        literalName: 'endDate',
        rdfDataProperty: "mnx:endDate",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      })
    ];
  }
};
