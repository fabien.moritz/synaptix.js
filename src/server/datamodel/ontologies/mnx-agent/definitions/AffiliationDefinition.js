/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import PersonDefinition from "./PersonDefinition";
import OrganizationDefinition from "./OrganizationDefinition";
import AffiliationIndexMatcher from "../matchers/AffiliationIndexMatcher";
import Affiliation from "../models/Affiliation";
import DurationDefinition from "../../mnx-time/definitions/DurationDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class AffiliationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [DurationDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Affiliation";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'affiliation';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Affiliation;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return AffiliationIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasPerson',
        pathInIndex: 'person',
        rdfObjectProperty: "mnx:hasPerson",
        relatedModelDefinition: PersonDefinition,
        isCascadingUpdated: true,
        relatedInputName: "personInput"
      }),
      new LinkDefinition({
        linkName: 'hasOrg',
        pathInIndex: 'org',
        rdfObjectProperty: "mnx:hasOrg",
        relatedModelDefinition: OrganizationDefinition,
        isCascadingUpdated: true,
        relatedInputName: "orgInput"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'role',
        pathInIndex: 'roles',
        rdfDataProperty: "mnx:role"
      })
    ];
  }
};