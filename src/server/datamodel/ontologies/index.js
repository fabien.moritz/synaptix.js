/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {DataModel} from "../DataModel";
import {
  MnxAgentInterfaceInput,
  MnxAgentInterfaceProperties,
  MnxAgentTypes,
  MnxAgentTypesResolvers
} from "./mnx-agent/schema/types";
import {MnxAgentMutations, MnxAgentMutationsResolvers} from "./mnx-agent/schema/mutations";
import {MnxAgentDefinitions} from "./mnx-agent/definitions";
import {MnxGeoTypes, MnxGeoTypesResolvers} from "./mnx-geo/schema/types";
import {MnxGeoMutations, MnxGeoMutationsResolvers} from "./mnx-geo/schema/mutations";
import {MnxGeoDefinitions} from "./mnx-geo/definitions";
import {MnxTimeTypes, MnxTimeTypesResolvers} from "./mnx-time/schema/types";
import {MnxTimeDefinitions} from "./mnx-time/definitions";
import {MnxProjectInterfaceProperties, MnxProjectTypes, MnxProjectTypesResolvers} from "./mnx-project/schema/types";
import {MnxProjectMutations, MnxProjectMutationsResolvers} from "./mnx-project/schema/mutations";
import {MnxProjectDefinitions} from "./mnx-project/definitions";
import {
  MnxContributionInterfaceInputs,
  MnxContributionInterfaceProperties,
  MnxContributionTypes,
  MnxContributionTypesResolvers
} from "./mnx-contribution/schema/types";
import {MnxContributionDefinitions} from "./mnx-contribution/definitions";
import {MnxContributionMutations, MnxContributionMutationsResolvers} from "./mnx-contribution/schema/mutations";
import {MnxSkosTypes, MnxSkosTypesResolvers} from "./mnx-skos/schema/types";
import {MnxSkosMutations, MnxSkosMutationsResolvers} from "./mnx-skos/schema/mutations";
import {MnxSkosDefinitions} from "./mnx-skos/definitions";
import {MnxCommonTypes, MnxCommonTypesResolvers} from "./mnx-common/schema/types";
import {MnxCommonDefinitions} from "./mnx-common/definitions";
import {mnxCommonDataModel} from "./mnx-common";
import {mnxAgentDataModel} from "./mnx-agent";
import {mnxGeoDataModel} from "./mnx-geo";
import {mnxTimeDataModel} from "./mnx-time";
import {mnxProjectDataModel} from "./mnx-project";
import {mnxContributionDataModel} from "./mnx-contribution";
import {mnxSkosDataModel} from "./mnx-skos";
import {MnxAclTypes, MnxAclTypesResolvers} from "./mnx-acl/schema/types";
import {MnxAclMutations, MnxAclMutationsResolvers} from "./mnx-acl/schema/mutations";
import {MnxAclDefinitions} from "./mnx-acl/definitions";
import {mnxAclDataModel} from "./mnx-acl";

export let MnxOntologies = {
  mnxCommon: {
    name: "mnx-common",
    schema: {
      Types: MnxCommonTypes,
    },
    resolvers: {
      Types: MnxCommonTypesResolvers,
    },
    ModelDefinitions: MnxCommonDefinitions,
  },
  mnxAgent: {
    name: "mnx-agent",
    schema: {
      Interface: {
        Properties: MnxAgentInterfaceProperties,
        Input: MnxAgentInterfaceInput
      },
      Types: MnxAgentTypes,
      Mutations: MnxAgentMutations
    },
    resolvers: {
      Types: MnxAgentTypesResolvers,
      Mutations: MnxAgentMutationsResolvers
    },
    ModelDefinitions: MnxAgentDefinitions,
  },
  mnxGeo: {
    name: "mnx-geo",
    schema: {
      Types: MnxGeoTypes,
      Mutations: MnxGeoMutations
    },
    resolvers: {
      Types: MnxGeoTypesResolvers,
      Mutations: MnxGeoMutationsResolvers
    },
    ModelDefinitions: MnxGeoDefinitions,
  },
  mnxTime: {
    name: "mnx-time",
    schema: {
      Types: MnxTimeTypes,
    },
    resolvers: {
      Types: MnxTimeTypesResolvers,
    },
    ModelDefinitions: MnxTimeDefinitions,
  },
  mnxProject: {
    name: "mnx-project",
    schema: {
      Types: MnxProjectTypes,
      Mutations: MnxProjectMutations,
      Properties: MnxProjectInterfaceProperties
    },
    resolvers: {
      Types: MnxProjectTypesResolvers,
      Mutations: MnxProjectMutationsResolvers
    },
    ModelDefinitions: MnxProjectDefinitions
  },
  mnxContribution: {
    name: "mnx-contribution",
    schema: {
      Interface: {
        Properties: MnxContributionInterfaceProperties,
        Inputs: MnxContributionInterfaceInputs
      },
      Types: MnxContributionTypes,
      Mutations: MnxContributionMutations
    },
    resolvers: {
      Types: MnxContributionTypesResolvers,
      Mutations: MnxContributionMutationsResolvers
    },
    ModelDefinitions: MnxContributionDefinitions
  },
  mnxSkos: {
    name: "mnx-skos",
    schema: {
      Types: MnxSkosTypes,
      Mutations: MnxSkosMutations
    },
    resolvers: {
      Types: MnxSkosTypesResolvers,
      Mutations: MnxSkosMutationsResolvers
    },
    ModelDefinitions: MnxSkosDefinitions
  },
  mnxAcl: {
    name: "mnx-acl",
    schema: {
      Types: MnxAclTypes,
      Mutations: MnxAclMutations
    },
    resolvers: {
      Types: MnxAclTypesResolvers,
      Mutations: MnxAclMutationsResolvers
    },
    ModelDefinitions: MnxAclDefinitions
  },
};

export let MnxDatamodels = {
  mnxCommonDataModel,
  mnxGeoDataModel,
  mnxTimeDataModel,
  mnxContributionDataModel,
  mnxAgentDataModel,
  mnxProjectDataModel,
  mnxSkosDataModel,
  mnxAclDataModel,
  // Bundled
  mnxCoreDataModel: (new DataModel()).mergeWithDataModels([mnxCommonDataModel, mnxGeoDataModel, mnxTimeDataModel, mnxContributionDataModel, mnxSkosDataModel, mnxAclDataModel])
};