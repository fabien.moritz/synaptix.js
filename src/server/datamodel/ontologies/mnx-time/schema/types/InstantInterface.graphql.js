/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {generateInterfaceResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {TemporalEntityInterfaceProperties} from "./TemporalEntityInterface.graphql";
import {EntityInterfaceInput} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let InstantInterfaceProperties = `
   ${TemporalEntityInterfaceProperties} 
  
  """ Occurs"""
  occursAt: Float
`;

export let InstantInterfaceInput = `
  ${EntityInterfaceInput}

  """ Occurs"""
  occursAt: String
`;

export let InstantInterface = `
interface InstantInterface{
  ${InstantInterfaceProperties}
}
  
${generateConnectionForType("InstantInterface")}
`;


export let InstantResolvers = {
  InstantInterface: {
    ...generateInterfaceResolvers("InstantInterface"),
    occursAt: (object) => object.occursAt,
  },
  ...generateConnectionResolverFor("InstantInterface")
};

