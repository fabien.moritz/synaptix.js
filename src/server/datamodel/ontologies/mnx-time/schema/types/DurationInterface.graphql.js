/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {generateInterfaceResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {generateConnectionForType, generateConnectionResolverFor } from "../../../../toolkit/graphql/schema/types/generators";
import {TemporalEntityInterfaceProperties} from "./TemporalEntityInterface.graphql";
import {EntityInterfaceInput} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let DurationInterfaceProperties = `
  ${TemporalEntityInterfaceProperties} 
  
  """ End date"""
  endDate: String
  
  """ Start date"""
  startDate: String
`;

export let DurationInterfaceInput = `
  ${EntityInterfaceInput}

  """ End date"""
  endDate: String
  
  """ Start date"""
  startDate: String
`;

export let DurationInterface = `
interface DurationInterface{
  ${DurationInterfaceProperties}
}
  
${generateConnectionForType("DurationInterface")}
`;

export let DurationResolvers = {
  DurationInterface: {
    ...generateInterfaceResolvers("DurationInterface"),
    endDate: (object) => object.endDate,
    startDate: (object) => object.startDate,
  },
  ...generateConnectionResolverFor("DurationInterface")
};

