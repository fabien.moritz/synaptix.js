/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import Tagging from "../models/Tagging";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import OnlineContributionDefinition from "./OnlineContributionDefinition";
import TaggingIndexMatcher from "../matchers/TaggingIndexMatcher";
import ConceptDefinition from "../../mnx-skos/definitions/ConceptDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class TaggingDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [OnlineContributionDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Tagging";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Tagging;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'tagging';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return TaggingIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasConcept',
        rdfObjectProperty: "mnx:hasConcept",
        relatedModelDefinition: ConceptDefinition
      }),
      new LinkDefinition({
        linkName: 'hasEntity',
        rdfObjectProperty: "mnx:hasEntity",
        relatedModelDefinition: EntityDefinition
      })
    ];
  }
};