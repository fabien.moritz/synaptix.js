/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import ExternalLink from "../models/ExternalLink";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class ExternalLinkDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:ExternalLink";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return ExternalLink;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasEntity',
        rdfReversedObjectProperty: "mnx:hasExternalLink",
        relatedModelDefinition: EntityDefinition,
        isPlural: true
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'label',
        pathInIndex: 'labels',
        rdfDataProperty: "rdfs:label"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'link',
        rdfDataProperty: "mnx:link"
      })
    ];
  }
};