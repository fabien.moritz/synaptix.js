/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import OnlineContribution from "../models/OnlineContribution";
import InstantDefinition from "../../mnx-time/definitions/InstantDefinition";

import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import CommentDefinition from "./CommentDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";

export default class OnlineContributionDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "sioc": "http://rdfs.org/sioc/ns#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [InstantDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:OnlineContribution";
  }

  /**
   * @inheritDoc
   */
  static getRdfSameAsTypes() {
    return ["sioc:Item"];
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return OnlineContribution;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'hasComment',
        rdfObjectProperty: "sioc:hasReply",
        relatedModelDefinition: CommentDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: "content",
        rdfDataProperty: "sioc:content",
        pathInIndex: "contents"
      })
    ];
  }
};