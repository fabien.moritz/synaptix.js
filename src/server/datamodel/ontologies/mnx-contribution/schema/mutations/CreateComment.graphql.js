/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import CommentDefinition from "../../definitions/CommentDefinition";
import {
  generateCreateMutationDefinitionForType,
  generateCreateMutationResolverForType,
} from "../../../../toolkit/graphql/schema/mutations/generators";
import {createObjectInConnectionResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";

export let CreateComment = generateCreateMutationDefinitionForType('Comment', `
    onlineContributionId: ID!
`);

export let CreateCommentResolvers = generateCreateMutationResolverForType('Comment',
  (_, {input: {objectInput, onlineContributionId}}, synaptixSession) => {
    return createObjectInConnectionResolver(CommentDefinition, [
      CommentDefinition.getLink("hasOnlineContribution").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(onlineContributionId)),
    ], _, {input: {objectInput}}, synaptixSession);
  });