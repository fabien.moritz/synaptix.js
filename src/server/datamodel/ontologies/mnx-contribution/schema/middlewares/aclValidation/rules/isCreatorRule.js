/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {rule} from 'graphql-shield';
import {I18nError} from "../../../../../../..";
import CreationDefinition from "../../../../definitions/CreationDefinition";
import ActionDefinition from "../../../../definitions/ActionDefinition";
import EntityDefinition from "../../../../../mnx-common/definitions/EntityDefinition";

/**
 * This helper checks that a logged user is the object creator.
 *
 * Caution : to work, model must follow MnxContributionDataModel that is to say that an object must have the following triples.
 *
 *  ```
 *  :objectId a mnx:Entity
 *  ?creationId prov:wasGeneratedBy :objectId.
 *  ?creationId a mnx:Creation.
 *  ?creationId prov:wasAttributedTo :userAccountId.
 *  ```
 *
 * @param {Model} object
 * @param {SynaptixDatastoreSession} synaptixSession
 */

export let isLoggedUserObjectCreator = async ({objectId, synaptixSession}) => {
  let userAccount = await synaptixSession.getLoggedUserAccount();

  return !userAccount || !(await synaptixSession.isObjectLinkedToTargetId({
    object: {id : objectId},
    modelDefinition: EntityDefinition,
    linkDefinitions: [
      EntityDefinition.getLink("hasCreationAction"),
      CreationDefinition.getLink("hasUserAccount")
    ],
    targetId: userAccount.id
  }));
};

/**
 * This rule can be applied to :
 *  - an object field query
 *  - an object query
 *  - an object update/deletion mutation
 *  - a mnx:Entity instance link deletion mutation
 * @return {Rule}
 */
export let isCreatorRule = () => rule()(
  /**
   * @param object
   * @param args
   * @param {SynaptixDatastoreSession} synaptixSession
   * @param {object} info
   * @return {*}
   */
  async (object, args, synaptixSession, info) => {
    let objectId;

    // This is the "object field query" case.
    if (object?.id) {
      objectId = synaptixSession.extractIdFromGlobalId(object?.id);
    // This is the "object query" case.
    } else if (args.id){
      objectId = synaptixSession.extractIdFromGlobalId(args.id);
    // This is the "object update mutation" case.
    } else if (args.input?.objectId){
      objectId = synaptixSession.extractIdFromGlobalId(args.input.objectId);
    } else if (args.input?.sourceEntityId){
      objectId = synaptixSession.extractIdFromGlobalId(args.input.sourceEntityId);
    } else {
      throw new I18nError(`
\`isCreatorRule\` 🛡 rule can't be used with this resolver. "args" must have one of these forms: 

- \`{id : "object id"}\` this is the "object query" case.
- \`{input : {objectId: "object id"}} \` this is the "object update/deletion mutation" case.
- \`{input : {sourceEntityId: "source entity id"}} \` this is the "a mnx:Entity instance link deletion mutation" case.

Here is ${JSON.stringify(args)} provided.
`);
    }

    if (await isLoggedUserObjectCreator({objectId, synaptixSession})){
      return new I18nError(`Not allowed !  (Blocked by \`isCreatorRule\` 🛡 rule)`, "USER_NOT_ALLOWED", 401);
    }

    return true;
  });