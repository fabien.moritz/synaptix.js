/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ModelDefinitionsRegister from "../../../../../../../toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../../../../../../../network/amqp/NetworkLayerAMQP";
import {PubSub} from "graphql-subscriptions";
import GraphQLContext from "../../../../../../../toolkit/graphql/GraphQLContext";
import SynaptixDatastoreRdfSession from "../../../../../../../../datastores/SynaptixDatastoreRdfSession";
import AgentDefinition from "../../../../../../mnx-agent/definitions/AgentDefinition";
import PersonDefinition from "../../../../../../mnx-agent/definitions/PersonDefinition";
import OrganizationDefinition from "../../../../../../mnx-agent/definitions/OrganizationDefinition";
import {isCreatorRule, isLoggedUserObjectCreator} from "../isCreatorRule";
import SSOUser from "../../../../../../../../datamodules/drivers/sso/models/SSOUser";
import Model from "../../../../../../../toolkit/models/Model";
import {I18nError} from "../../../../../../../..";

jest.mock("nanoid/generate");

let modelDefinitionsRegister = new ModelDefinitionsRegister([AgentDefinition, PersonDefinition, OrganizationDefinition]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();

/**
 * @param {GraphQLContext} context
 * @return {{synaptixSession: *}}
 */
let initSession = (context, ) => {
  let synaptixSession =  new SynaptixDatastoreRdfSession({
    modelDefinitionsRegister,
    networkLayer,
    context,
    pubSubEngine,
    schemaNamespaceMapping: {
      "mnx": "http://ns.mnemotix.com/onto/"
    },
    nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
    nodesPrefix: "test"
  });

  let userAccountId = "test:useraccount/1234";

  jest.spyOn(synaptixSession, "getLoggedUserAccount").mockImplementation(() => {
    return context.isAnonymous() ? null : new Model(userAccountId, userAccountId);
  });

  let askSpy = jest.spyOn(synaptixSession.getGraphControllerService().getGraphControllerPublisher(), 'ask');
  askSpy.mockImplementation(() => true);

  return {synaptixSession, askSpy, userAccountId};

};

describe("Test isInUserGroupRule", () => {
  it("test isLoggedUserObjectCreator method on anomymous session", async () => {
    let {synaptixSession, askSpy} = initSession(new GraphQLContext({
      anonymous: true, lang: 'fr'
    }));

    await isLoggedUserObjectCreator({
      objectId: "test:an-object/1345",
      synaptixSession
    });

    expect(askSpy).not.toBeCalled();
  });

  it("test isLoggedUserObjectCreator method", async () => {
    let {synaptixSession, askSpy} = initSession(new GraphQLContext({
      user: new SSOUser({
        user: {
          id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
          username: 'test@domain.com',
          firstName: 'John',
          lastName: 'Doe'
        }
      }),
      lang: 'fr'
    }));

    await isLoggedUserObjectCreator({
      objectId: "test:an-object/1345",
      synaptixSession
    });

    expect(askSpy).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
ASK WHERE {
 ?hasCreationAction rdf:type mnx:Creation.
 ?hasCreationAction prov:wasGeneratedBy <http://ns.mnemotix.com/instances/an-object/1345>.
 ?hasCreationAction prov:wasAttributedTo <http://ns.mnemotix.com/instances/useraccount/1234>.
}`
    });
  });

  it("should throw an error if objectId not findable", async () => {
    let rule = isCreatorRule();

    let {synaptixSession} = initSession(new GraphQLContext({
      anonymous: true, lang: 'fr'
    }));

    await expect(rule.resolve(undefined, {qs: "dsf"}, synaptixSession)).rejects.toThrow();
  });

  it('should block an anonymous user', async () => {

    let rule = isCreatorRule();

    let {synaptixSession} = initSession(new GraphQLContext({
      anonymous: true, lang: 'fr'
    }));

    let exception = await rule.resolve(undefined, {id: "test:an-object/1345"}, synaptixSession);

    expect(exception).toBeInstanceOf(I18nError);
    expect(exception.i18nKey).toBe("USER_NOT_ALLOWED");
  });

  it('should ask graph on user', async () => {

    let rule = isCreatorRule();

    let {synaptixSession, askSpy} = initSession(new GraphQLContext({
      user: new SSOUser({
        user: {
          id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
          username: 'test@domain.com',
          firstName: 'John',
          lastName: 'Doe'
        }
      }),
      lang: 'fr'
    }));

    let result = await rule.resolve({}, {id: "test:an-object/1345"}, synaptixSession, {}, {});

    expect(askSpy).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
ASK WHERE {
 ?hasCreationAction rdf:type mnx:Creation.
 ?hasCreationAction prov:wasGeneratedBy <http://ns.mnemotix.com/instances/an-object/1345>.
 ?hasCreationAction prov:wasAttributedTo <http://ns.mnemotix.com/instances/useraccount/1234>.
}`
    });
    expect(result).toBe(true);

    askSpy.mockImplementation(() => false);

    let exception = await rule.resolve({}, {id: "test:an-object/1345"}, synaptixSession, {}, {});

    expect(exception).toBeInstanceOf(I18nError);
    expect(exception.i18nKey).toBe("USER_NOT_ALLOWED")
  });
});