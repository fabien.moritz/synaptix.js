/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {OnlineContributionInterfaceInput, OnlineContributionInterfaceProperties} from "./OnlineContributionInterface.graphql";
import CommentDefinition from "../../definitions/CommentDefinition";

export let CommentType = `
""" A comment action """
type Comment implements EntityInterface & OnlineContributionInterface & InstantInterface{
  ${OnlineContributionInterfaceProperties}
  
  """ Related online contribution """
  onlineContribution: OnlineContributionInterface
}

${generateConnectionForType("Comment")}

""" A comment input"""
input CommentInput{
  ${OnlineContributionInterfaceInput}
}
`;

export let CommentResolvers = {
  Comment: {
    ...generateTypeResolvers("Comment"),
    onlineContribution: getLinkedObjectResolver.bind(this, CommentDefinition.getLink("hasOnlineContribution")),
  },
  ...generateConnectionResolverFor("Comment")
};