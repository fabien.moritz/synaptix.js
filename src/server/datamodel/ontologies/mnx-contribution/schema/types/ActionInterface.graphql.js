/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import {generateInterfaceResolvers, getLinkedObjectResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import ActionDefinition from "../../definitions/ActionDefinition";
import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {EntityInterfaceInput, EntityInterfaceProperties} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let ActionInterfaceProperties = `
  ${EntityInterfaceProperties}
  
  """ Started at time """
  startedAtTime: String
  
  """ Started at time """
  endedAtTime: String
  
  """ Action user creator """
  userAccount: UserAccount
`;

export let ActionInterfaceInput = `
  ${ActionDefinition.generateGraphQLInputProperties()}
`;

export let ActionInterface = `
""" An interface for all actions """
interface ActionInterface {
  ${ActionInterfaceProperties}
}

${generateConnectionForType("ActionInterface")}
`;

export let ActionInterfaceResolvers = {
  ActionInterface: {
    ...generateInterfaceResolvers("ActionInterface"),
    startedAtTime: (object) => object.startedAtTime,
    endedAtTime: (object) => object.endedAtTime,
    userAccount: getLinkedObjectResolver.bind(this, ActionDefinition.getLink("hasUserAccount"))
  },
  ...generateConnectionResolverFor("ActionInterface")
};
