/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */


import {ActionInterface, ActionInterfaceResolvers} from "./ActionInterface.graphql";
import {CreationResolvers, CreationType} from "./Creation.graphql";
import {DeletionResolvers, DeletionType} from "./Deletion.graphql";
import {ExternalLinkResolvers, ExternalLinkType} from "./ExternalLink.graphql";
import {UpdateResolvers, UpdateType} from "./Update";
import {mergeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {TaggingResolvers, TaggingType} from "./Tagging.graphql";
import {
  OnlineContributionInterface,
  OnlineContributionInterfaceProperties,
  OnlineContributionInterfaceResolvers,
  OnlineContributionInterfaceInput
} from "./OnlineContributionInterface.graphql";
import {CommentResolvers, CommentType} from "./Comment.graphql";


export let MnxContributionInterfaceProperties = {
  OnlineContributionInterfaceProperties
};

export let MnxContributionInterfaceInputs = {
  OnlineContributionInterfaceInput
};

export let MnxContributionTypes = [
  ActionInterface,
  OnlineContributionInterface,
  CreationType,
  DeletionType,
  ExternalLinkType,
  UpdateType,
  TaggingType,
  CommentType
];

export let MnxContributionTypesResolvers = mergeResolvers(
  ActionInterfaceResolvers,
  OnlineContributionInterfaceResolvers,
  CreationResolvers,
  DeletionResolvers,
  ExternalLinkResolvers,
  UpdateResolvers,
  TaggingResolvers,
  CommentResolvers
);