/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import {
  generateInterfaceResolvers,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers"
import {
  InstantInterfaceInput,
  InstantInterfaceProperties
} from "../../../mnx-time/schema/types/InstantInterface.graphql";
import OnlineContributionDefinition from "../../definitions/OnlineContributionDefinition";

export let OnlineContributionInterfaceProperties = `
  ${InstantInterfaceProperties}
  
  """ Online contribution readable content """
  content: String
  
  """ Comments """
  comments: CommentConnection
`;

export let OnlineContributionInterfaceInput = `
  ${InstantInterfaceInput}
  
   """ Online contribution readable content """
  content: String
`;

export let OnlineContributionInterface = `
""" An interface for all Online contributions """
interface OnlineContributionInterface {
  ${OnlineContributionInterfaceProperties}
}
`;

export let OnlineContributionInterfaceResolvers = {
  OnlineContributionInterface: {
    ...generateInterfaceResolvers("OnlineContributionInterface"),
    comments: getLinkedObjectsResolver.bind(this, OnlineContributionDefinition.getLink("hasComment")),
    content: getLocalizedLabelResolver.bind(this, OnlineContributionDefinition.getLabel("content"))
  }
};
