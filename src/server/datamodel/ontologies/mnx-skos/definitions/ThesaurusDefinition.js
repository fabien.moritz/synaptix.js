/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import Thesaurus from "../models/Thesaurus";
import ThesaurusIndexMatcher from "../matchers/ThesaurusIndexMatcher";
import ConceptDefinition from "./ConceptDefinition";
import SchemeDefinition from "./SchemeDefinition";
import CollectionDefinition from "./CollectionDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";

export default class ThesaurusDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Thesaurus";
  }


  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Thesaurus';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'thesaurus';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Thesaurus;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ThesaurusIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'concepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'thesaurus',
          useIndexMatcherOf: ConceptDefinition
        }),
        rdfReversedObjectProperty: "mnx:conceptOf",
        relatedModelDefinition: ConceptDefinition,
        rdfObjectProperty: "skos:concept",
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'schemes',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'thesaurus',
          useIndexMatcherOf: SchemeDefinition
        }),
        rdfObjectProperty: "mnx:schemeOf",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'draftScheme',
        rdfReversedObjectProperty: "mnx:draftSchemeOf",
        relatedModelDefinition: SchemeDefinition,
      }),
      new LinkDefinition({
        linkName: 'collections',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'thesaurus',
          useIndexMatcherOf: CollectionDefinition
        }),
        rdfReversedObjectProperty: "mnx:collectionOf",
        relatedModelDefinition: CollectionDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInIndex: 'titles',
        rdfDataProperty: "mnx:title"
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInIndex: 'descriptions',
        rdfDataProperty: "mnx:description"
      })
    ];
  }
};