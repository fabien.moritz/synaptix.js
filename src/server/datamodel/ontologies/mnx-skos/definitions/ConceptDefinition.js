/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ConceptIndexMatcher from "../matchers/ConceptIndexMatcher";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import Concept from "../models/Concept";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import LinkDefinition, {
  ChildOfIndexDefinition,
  UseIndexMatcherOfDefinition
} from "../../../toolkit/definitions/LinkDefinition";
import ThesaurusDefinition from "./ThesaurusDefinition";
import SchemeDefinition from "./SchemeDefinition";
import CollectionDefinition from "./CollectionDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import TaggingDefinition from "../../mnx-contribution/definitions/TaggingDefinition";
import EntityDefinition from "../../mnx-common/definitions/EntityDefinition";

export default class ConceptDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "skos": "http://www.w3.org/2004/02/skos/core#"
    };
  }

  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [EntityDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'skos:Concept';
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Concept';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'concept';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Concept;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ConceptIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'thesaurus',
        pathInIndex: new ChildOfIndexDefinition(ThesaurusDefinition),
        rdfObjectProperty: "mnx:conceptOf",
        relatedModelDefinition: ThesaurusDefinition
      }),
      new LinkDefinition({
        linkName: 'inScheme',
        pathInIndex: 'inSchemes',
        rdfObjectProperty: "skos:inScheme",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'topInSchemes',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'topInScheme',
          useIndexMatcherOf: ConceptDefinition
        }),
        rdfReversedObjectProperty: "skos:hasTopConcept",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'collections',
        pathInIndex: 'collections',
        rdfObjectProperty: "skos:member",
        relatedModelDefinition: CollectionDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'broaders',
        // pathInIndex: new UseIndexMatcherOfDefinition({
        //   filterName: 'broaders',
        //   useIndexMatcherOf: ConceptDefinition
        // }),
        pathInIndex: "broaders",
        rdfObjectProperty: "skos:broader",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'related',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'related',
          useIndexMatcherOf: ConceptDefinition
        }),
        rdfObjectProperty: "skos:related",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'narrowers',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'narrowers',
          useIndexMatcherOf: ConceptDefinition
        }),
        //rdfObjectProperty: "skos:narrower",
        rdfReversedObjectProperty: "skos:broader",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'closeMatchConcepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'closeMatches',
          useIndexMatcherOf: ConceptDefinition
        }),
        rdfObjectProperty: "skos:closeMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'exactMatchConcepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'exactMatches',
          useIndexMatcherOf: ConceptDefinition
        }),
        rdfObjectProperty: "skos:exactMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'taggings',
        pathInIndex: 'taggings',
        rdfReversedObjectProperty: "mnx:subject",
        relatedModelDefinition: TaggingDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'prefLabel',
        pathInIndex: 'prefLabels',
        rdfDataProperty: "skos:prefLabel",
        isSearchable: true,
        isRequired: true
      }),
      new LabelDefinition({
        labelName: 'altLabel',
        pathInIndex: 'altLabels',
        rdfDataProperty: "skos:altLabel",
        isSearchable: true
      }),
      new LabelDefinition({
        labelName: 'hiddenLabel',
        pathInIndex: 'altLabels',
        rdfDataProperty: "skos:hiddenLabel"
      }),
      new LabelDefinition({
        labelName: 'scopeNote',
        pathInIndex: 'scopeNotes',
        rdfDataProperty: "skos:scopeNote"
      }),
      new LabelDefinition({
        labelName: 'definition',
        pathInIndex: 'definitions',
        rdfDataProperty: "skos:definition"
      }),
      new LabelDefinition({
        labelName: 'changeNote',
        pathInIndex: 'changeNotes',
        rdfDataProperty: "skos:changeNote"
      }),
      new LabelDefinition({
        labelName: 'notation',
        pathInIndex: 'notations',
        rdfDataProperty: "skos:notations"
      }),
      new LabelDefinition({
        labelName: 'historyNote',
        pathInIndex: 'historyNotes',
        rdfDataProperty: "skos:historyNote"
      }),
      new LabelDefinition({
        labelName: 'example',
        pathInIndex: 'examples',
        rdfDataProperty: "skos:example"
      }),
      new LabelDefinition({
        labelName: 'editorialNote',
        pathInIndex: 'editorialNotes',
        rdfDataProperty: "skos:editorialNote"
      }),
      new LabelDefinition({
        labelName: 'note',
        pathInIndex: 'notes',
        rdfDataProperty: "skos:note"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'isDraft',
        rdfDataProperty: "mnx:isDraft"
      }),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      })
    ];
  }
};