/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {generateInterfaceResolvers, getObjectResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {EntityInterfaceProperties} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let SkosElementInterfaceProperties = `
  ${EntityInterfaceProperties}
  
  """ All childrens """
  childrenSKOSElements(${connectionArgs}, ${filteringArgs}): SKOSElementInterfaceConnection
  
  """ Count all childrens """
  childrenSKOSElementsCount(${filteringArgs}): Int
`;

export let SkosElementInterface = `
""" A skos element """
interface SKOSElementInterface {
  ${SkosElementInterfaceProperties}
}

${generateConnectionForType("SKOSElementInterface")}

extend type Query{
  skosElement(id: ID!): SKOSElementInterface
}
`;

export let SkosElementResolvers = {
  SKOSElementInterface: {
    ...generateInterfaceResolvers("SKOSElementInterface")
  },
  Query: {
    /**
     * @param {string} _
     * @param {string} id
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    skosElement: (_, {id}, synaptixSession) => {
      let modelDefinition = synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(synaptixSession.extractTypeFromGlobalId(id));
      return getObjectResolver(modelDefinition, _, {id}, synaptixSession);
    }
  },
  ...generateConnectionResolverFor("SKOSElementInterface")
};