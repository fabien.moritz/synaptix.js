/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import CollectionDefinition from "../../definitions/CollectionDefinition";
import {
  generateTypeResolvers,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {SkosElementInterfaceProperties} from "./SkosElementInterface.graphql";
import {EntityInterfaceInput} from "../../../mnx-common/schema/types/EntityInterface.graphql";

export let CollectionType = `
""" A skos:collection in a thesaurus """
type Collection implements EntityInterface & SKOSElementInterface {
  ${SkosElementInterfaceProperties}
   
  """ Name of the collection """
  title: String

  """ Description of collection """
  description: String

  """ Color of collection """
  color: String
  
  """ Member concepts of the collection"""
  concepts(${connectionArgs}, ${filteringArgs}): ConceptConnection
}

${generateConnectionForType("Collection")}

input CollectionInput {
  ${EntityInterfaceInput}

  """ Name of the collection """
  title: String

  """ Description of collection """
  description: String

  """ Color of collection """
  color: String
}

extend type Query{
  """ Get a collection """
  collection(id: ID!): Collection
}
`;

export let CollectionResolvers = {
  Collection: {
    ...generateTypeResolvers("Collection"),
    color: (object) => object.color,
    title: getLocalizedLabelResolver.bind(this, CollectionDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, CollectionDefinition.getLabel('description')),
    concepts: getLinkedObjectsResolver.bind(this, CollectionDefinition.getLink('concepts')),
    childrenSKOSElementsCount: getLinkedObjectsCountResolver.bind(this, CollectionDefinition.getLink('concepts')),
    childrenSKOSElements: getLinkedObjectsResolver.bind(this, CollectionDefinition.getLink('concepts')),
  },
  Query: {
    collection: getObjectResolver.bind(this, CollectionDefinition)
  },
  ...generateConnectionResolverFor("Collection")
};
