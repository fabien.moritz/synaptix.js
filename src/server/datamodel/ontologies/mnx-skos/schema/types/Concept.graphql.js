/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import ConceptDefinition from "../../definitions/ConceptDefinition";
import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver, getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {SkosElementInterfaceProperties} from "./SkosElementInterface.graphql";
import {EntityInterfaceInput} from "../../../mnx-common/schema/types/EntityInterface.graphql";
import {generateGraphQLFiltersDocumentation} from "../../../../toolkit/definitions/helpers";

export let ConceptType = `
""" A skos:concept in a thesaurus """
type Concept implements EntityInterface & SKOSElementInterface {
  ${SkosElementInterfaceProperties}
 
  """ skos:prefLabel value for current language """
  prefLabel: String

  """ skos:altLabel value for current language """
  altLabel: String
  
  """ skos:definition value """
  definition: String

  """ skos:scopeNote value """
  scopeNote: String
  
  """ skos:example value """
  example: String
  
  """ skos:historyNote value """
  historyNote: String
  
  """ skos:editorialNote value """
  editorialNote: String
  
  """ skos:changeNote value """
  changeNote: String
          
  """ Member of skos:schemes """
  schemes(${connectionArgs}, ${filteringArgs}): SchemeConnection

  """ Member of skos:collections """
  collections(${connectionArgs}, ${filteringArgs}): CollectionConnection
  
  """ Color of the concept """
  color: String
  
  """ Is this a draft concept """
  isDraft: Boolean
  
  """ The broaders skos:concept """
  broaderConcepts(${connectionArgs}, ${filteringArgs}): ConceptConnection

  """ The broaders skos:concept count"""
  broaderConceptsCount(${filteringArgs}): Int
  
  """ The related skos:concept """
  relatedConcepts(${connectionArgs}, ${filteringArgs}): ConceptConnection

  """ The related skos:concept count"""
  relatedConceptsCount(${filteringArgs}): Int
  
  """ The narrowers skos:concept """
  narrowerConcepts(${connectionArgs}, ${filteringArgs}): ConceptConnection

  """ The narrowers skos:concept count"""
  narrowerConceptsCount(${connectionArgs}, ${filteringArgs}): Int
  
  """ The descendant skos:concept """
  descendantConcepts(${connectionArgs}, ${filteringArgs}): ConceptConnection
  
  """ The descendants skos:concept count"""
  descendantConceptsCount(${connectionArgs}, ${filteringArgs}): Int
  
  """ The "close matched" skos:concepts"""
  closeMatchConcepts(${connectionArgs}, ${filteringArgs}): ConceptConnection
  
  """ The "close matched" related skos:concept count"""
  closeMatchConceptsCount(${connectionArgs}, ${filteringArgs}): Int

   """ The "exact matched" related skos:concepts """
  exactMatchConcepts(${connectionArgs}, ${filteringArgs}): ConceptConnection
  
  """ The "exact matched" skos:concept count"""
  exactMatchConceptsCount(${connectionArgs}, ${filteringArgs}): Int
  
  """ The related vocabulary """
  thesaurus(${connectionArgs}, ${filteringArgs}): Thesaurus

  """ Related fagging count"""
  taggingCount(${filteringArgs}): Int
}

${generateConnectionForType("Concept")}

input ConceptInput {
  ${EntityInterfaceInput}
  
  """ skos:prefLabel value """
  prefLabel: String
  
  """ skos:definition value """
  definition: String

  """ skos:scopeNote value """
  scopeNote: String
  
  """ skos:example value """
  example: String
  
  """ skos:historyNote value """
  historyNote: String
  
  """ skos:editorialNote value """
  editorialNote: String
  
  """ skos:changeNote value """
  changeNote: String
  
  """ Color of the concept """
  color: String
  
  """ Is this a draft concept """
  isDraft: Boolean
}

extend type Query{
  """ Get a concept """
  concept(id: ID!): Concept
  
  """ ## Search for concepts 
    ${generateGraphQLFiltersDocumentation(ConceptDefinition)}
  """
  concepts(${connectionArgs}, ${filteringArgs}): ConceptConnection
}
`;

export let ConceptResolvers = {
  Concept: {
    ...generateTypeResolvers("Concept"),
    prefLabel: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('prefLabel')),
    altLabel: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('altLabel')),
    definition: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('definition')),
    scopeNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('scopeNote')),
    example: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('example')),
    historyNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('historyNote')),
    editorialNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('editorialNote')),
    changeNote: getLocalizedLabelResolver.bind(this, ConceptDefinition.getLabel('changeNote')),
    isDraft: (object) => !!object.isDraft,
    color: (object) => object.color,
    schemes: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('inScheme')),
    collections: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('collections')),
    broaderConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('broaders')),
    broaderConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('broaders')),
    relatedConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('related')),
    relatedConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('related')),
    narrowerConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    narrowerConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    closeMatchConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('closeMatchConcepts')),
    closeMatchConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('closeMatchConcepts')),
    exactMatchConceptsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('exactMatchConcepts')),
    exactMatchConcepts: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('exactMatchConcepts')),

    /**
     * @param {Model} object
     * @param {DataQueryArguments} args
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    descendantConceptsCount: async (object, args, synaptixSession) => {
      await synaptixSession.getLinkedObjectsCountFor({object, linkDefinition: ConceptDefinition.getLink('narrowers'), args})
    },
    childrenSKOSElementsCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    childrenSKOSElements: getLinkedObjectsResolver.bind(this, ConceptDefinition.getLink('narrowers')),
    thesaurus: getLinkedObjectResolver.bind(this, ConceptDefinition.getLink('thesaurus')),
    taggingCount: getLinkedObjectsCountResolver.bind(this, ConceptDefinition.getLink('taggings')),
  },
  Query: {
    concept: getObjectResolver.bind(this, ConceptDefinition),
    concepts: getObjectsResolver.bind(this, ConceptDefinition)
  },
  ...generateConnectionResolverFor("Concept")
};
