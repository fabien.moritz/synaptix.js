/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import ModelDefinitionsRegister from "./toolkit/definitions/ModelDefinitionsRegister";
import {generateSchema} from "./toolkit/graphql/schema/utilities/generateSchema";
import {QueryResolvers, QueryType} from "./toolkit/graphql/schema/types/Query.graphql";
import {MutationResolvers, MutationType} from "./toolkit/graphql/schema/mutations/Mutation.graphql";
import {mergeResolvers} from "./toolkit/graphql/helpers/resolverHelpers";
import {ConnectionTypes} from "./toolkit/graphql/schema/types/ConnectionDefinitions.graphql";
import {logError} from "../utilities/logger";
import {SSOMutationResolvers, SSOMutations, SSOShieldRules} from "./toolkit/graphql/schema/mutations/sso";
import {applyMiddleware, IMiddleware} from "graphql-middleware";
import {
  formValidationMiddleware
} from "./toolkit/graphql/schema/middlewares/formValidation/formValidationMiddleware";
import {aclValidationMiddleware} from "./toolkit/graphql/schema/middlewares/aclValidation/aclValidationMiddleware";
import {mergeRules} from "./toolkit/graphql/schema/middlewares/aclValidation/rules/mergeRules";

/**
 * A class to gather and build stuffs to describe a datamodel
 */
export class DataModel {
  /**
   * @param {boolean} [omitRootQueryType=false]
   * @param {boolean} [omitRootMutationType=false]
   * @param {boolean} [omitConnectionTypes=false]
   * @param {array} [typeDefs]
   * @param {object[]} [resolvers]
   * @param {ModelDefinitionAbstract[]} [modelDefinitions]
   * @param {IMiddleware[]} middlewares
   * @param {IRules} [shieldRules]
   */
  constructor({typeDefs, resolvers, modelDefinitions, middlewares, omitRootQueryType, omitRootMutationType, omitConnectionTypes, shieldRules} = {}) {
    this.typeDefs = typeDefs || [];
    this.resolvers = resolvers || {};
    this.shieldRules = shieldRules || {};
    this.middlewares = middlewares || [];

    if (typeof modelDefinitions === "object" && !Array.isArray(modelDefinitions)) {
      modelDefinitions = Object.values(modelDefinitions);
    }

    this.modelDefinitions = modelDefinitions || [];
  }

  /**
   * Use this method to add schema, root Query, optionaly root mutation to the GraphQL schema.
   *
   * @param {boolean} [omitRootMutationType=false]
   * @param {boolean} [addRootSubscriptionType=false]
   * @return {DataModel}
   */
  addDefaultSchemaTypeDefs({omitRootMutationType, addRootSubscriptionType} = {}) {
    this.typeDefs.push(ConnectionTypes);

    this.typeDefs.push(QueryType);
    this.resolvers = mergeResolvers(this.resolvers, QueryResolvers);

    if (!omitRootMutationType) {
      this.typeDefs.push(MutationType);
      this.resolvers = mergeResolvers(this.resolvers, MutationResolvers);
    }

    this.typeDefs.push(`schema{
  query: Query 
  ${!omitRootMutationType ? "mutation: Mutation" : ""}
  ${addRootSubscriptionType ? "subscription: Subscription" : ""} 
}`);
    return this;
  }

  /**
   * Use this method to add SSO mutations. This is a friendly way of use GraphQL to login/logout/register...
   */
  addSSOMutations() {
    this.addFormValidationMiddlewares();
    this.addTypeDefs(SSOMutations);
    this.addResolvers(SSOMutationResolvers);
    this.addShieldRules(SSOShieldRules);
    return this;
  }

  /**
   * Use this method to add Yup middlewares.
   */
  addFormValidationMiddlewares() {
    this.addMiddlewares([formValidationMiddleware()]);
  }

  /**
   * @param {array} typeDefs
   * @return {DataModel}
   */
  addTypeDefs(typeDefs) {
    for (let typeDef of typeDefs) {
      if (this.typeDefs.indexOf(typeDef) === -1) {
        this.typeDefs.push(typeDef);
      }
    }
    return this;
  }

  /**
   * @param {object} resolvers
   * @return {DataModel}
   */
  addResolvers(resolvers) {
    this.resolvers = mergeResolvers(this.resolvers, resolvers);
    return this;
  }

  /**
   * @param {IRules} shieldRules
   * @return {DataModel}
   */
  addShieldRules(shieldRules) {
    this.shieldRules = mergeRules(this.shieldRules, shieldRules);
    return this;
  }

  /**
   * @param {ModelDefinitionAbstract[]} modelDefinitions
   * @return {DataModel}
   */
  addModelDefinitions(modelDefinitions) {
    this.modelDefinitions = [].concat(this.modelDefinitions, modelDefinitions);
    return this;
  }

  /**
   * @param {Array.<IMiddleware>} middlewares
   * @return {DataModel}
   */
  addMiddlewares(middlewares) {
    this.middlewares = [].concat(this.middlewares, middlewares);
    return this;
  }

  /**
   * Returns ModelDefinition for logged user related person
   * @param {typeof ModelDefinitionAbstract } modelDefinitionForLoggedUserPerson
   * @return {DataModel}
   */
  setModelDefinitionForLoggedUserPerson(modelDefinitionForLoggedUserPerson){
    this._modelDefinitionForLoggedUserPerson = modelDefinitionForLoggedUserPerson;
    return this;
  }

  /**
   * @return {array} modelDefinitions
   */
  getTypeDefs() {
    return this.typeDefs;
  }

  /**
   * @return {array} modelDefinitions
   */
  getResolvers() {
    return this.resolvers;
  }

  /**
   * @return {ModelDefinitionAbstract[]} modelDefinitions
   */
  getModelDefinitions() {
    return this.modelDefinitions;
  }

  /**
   * @return {IRules}
   */
  getShieldRules() {
    return this.shieldRules;
  }

  /**
   * Generate a graphQL executable schema.
   */
  generateExecutableSchema(options) {
    try {
      let schema = generateSchema({
        typeDefs: this.typeDefs,
        resolvers: this.resolvers,
        ...options
      });

      return applyMiddleware(schema, aclValidationMiddleware({shieldRules: this.shieldRules}), ...this.middlewares);

    } catch (e) {
      logError(`GraphQL schema is not valid.`);
      throw e;
    }
  }

  /**
   * Generate model definitions register
   * @return {ModelDefinitionsRegister}
   */
  generateModelDefinitionsRegister() {
    let modelDefinitionRegister = new ModelDefinitionsRegister(this.modelDefinitions);

    if (this._modelDefinitionForLoggedUserPerson){
      modelDefinitionRegister.setModelDefinitionForLoggedUserPerson(this._modelDefinitionForLoggedUserPerson);
    }

    return modelDefinitionRegister;
  }

  /**
   * Merge the dataModel with others.
   * @param {DataModel[]} datamodels
   * @return {DataModel}
   */
  mergeWithDataModels(datamodels) {
    for (let datamodel of datamodels) {
      this.addTypeDefs(datamodel.getTypeDefs());
      this.addResolvers(datamodel.getResolvers());
      this.addModelDefinitions(datamodel.getModelDefinitions());
      this.addShieldRules(datamodel.getShieldRules())
    }

    return this;
  }
}