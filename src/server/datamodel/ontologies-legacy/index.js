/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {FOAFModelDefinitions} from "./definitions/foaf";
import {FOAFResolvers, FOAFTypes} from "./schema/types/foaf";
import {FOAFMutations, FOAFMutationsResolvers} from "./schema/mutations/foaf";
import {GeoNamesResolvers, GeoNamesTypes} from "./schema/types/geonames";
import {GeonamesMutations, GeonamesMutationsResolvers} from "./schema/mutations/geonames";
import {GeonamesModelDefinitions} from "./definitions/geonames";
import {SKOSResolvers, SKOSTypes} from "./schema/types/skos";
import {SKOSModelDefinitions} from "./definitions/skos";
import {ResourcesResolvers, ResourcesTypes} from "./schema/types/resources";
import {ResourcesMutations, ResourcesMutationsResolvers} from "./schema/mutations/resources";
import {ResourcesModelDefinitions} from "./definitions/resources";
import {CommonResolvers, CommonTypes} from "./schema/types/common";
import {CommonMutations, CommonMutationsResolvers} from "./schema/mutations/common";
import {CommonModelDefinitions} from "./definitions/common";
import {ProjectsResolvers, ProjectsTypes} from "./schema/types/projects";
import {ProjectsMutations, ProjectsMutationsResolvers} from "./schema/mutations/projects";
import {ProjectsModelDefinitions} from "./definitions/projects";

export {ObjectDefaultProperties as ObjectDefaultGraphQLProperties} from "./schema/types/ObjectInterface.graphql";
export {QueryResolvers, QueryType} from "./schema/types/Query.graphql";

export let Ontologies = {
  common: {
    schema: {
      Types: CommonTypes,
      Mutations: CommonMutations
    },
    resolvers: {
      Types: CommonResolvers,
      Mutations: CommonMutationsResolvers
    },
    ModelDefinitions: CommonModelDefinitions
  },
  foaf: {
    schema: {
      Types: FOAFTypes,
      Mutations: FOAFMutations
    },
    resolvers: {
      Types: FOAFResolvers,
      Mutations: FOAFMutationsResolvers
    },
    ModelDefinitions: FOAFModelDefinitions,
  },
  geonames: {
    schema: {
      Types: GeoNamesTypes,
      Mutations: GeonamesMutations
    },
    resolvers: {
      Types: GeoNamesResolvers,
      Mutations: GeonamesMutationsResolvers
    },
    ModelDefinitions: GeonamesModelDefinitions
  },
  skos: {
    schema: {
      Types: SKOSTypes,
      // Mutations: SkosMutations
    },
    resolvers: {
      Types: SKOSResolvers,
      // Mutations:  SKOSMutationsResolvers
    },
    ModelDefinitions: SKOSModelDefinitions
  },
  resources: {
    schema: {
      Types: ResourcesTypes,
      Mutations: ResourcesMutations
    },
    resolvers: {
      Types: ResourcesResolvers,
      Mutations: ResourcesMutationsResolvers
    },
    ModelDefinitions: ResourcesModelDefinitions
  },
  projects: {
    schema: {
      Types: ProjectsTypes,
      Mutations: ProjectsMutations
    },
    resolvers: {
      Types: ProjectsResolvers,
      Mutations: ProjectsMutationsResolvers
    },
    ModelDefinitions: ProjectsModelDefinitions
  },
};