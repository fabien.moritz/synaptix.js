/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import Memo from "../../models/projects/Memo";
import MemoIndexMatcher from "../../matchers/projects/MemoIndexMatcher";
import EventDefinition from "./EventDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import PersonDefinition from "../foaf/PersonDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class MemoDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "dc": "http://purl.org/dc/elements/1.1/",
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Memo";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Memo';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'memo';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Memo;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return MemoIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'creator',
        pathInGraphstore: `out('CREATOR')`,
        rdfObjectProperty: "dc:creator",
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'event',
        pathInIndex: 'event',
        pathInGraphstore: `in('HAS_MEMO').hasLabel('${EventDefinition.getNodeType()}')`,
        rdfReversedObjectProperty: "mnx:memo",
        relatedModelDefinition: EventDefinition,
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'content',
        pathInGraphstore: `out('CONTENT')`,
        pathInIndex: 'contents',
        rdfDataProperty: "mnx:content",
        isHTML: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'content',
        rdfDataProperty: "mnx:content"
      })
    ];
  }
};