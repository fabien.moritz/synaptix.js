/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import Involvement from "../../models/projects/Involvement";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import ActorDefinition from "../foaf/ActorDefinition";
import ProjectDefinition from "./ProjectDefinition";
import EventDefinition from "./EventDefinition";
import PersonDefinition from "../foaf/PersonDefinition";
import InvolvementIndexMatcher from "../../matchers/projects/InvolvementIndexMatcher";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";
import OrganisationDefinition from "../foaf/OrganisationDefinition";

export default class InvolvementDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "dc": "http://purl.org/dc/elements/1.1/",
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Involvement";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Involvement';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'involvement';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Involvement;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return InvolvementIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'creator',
        pathInGraphstore: `out('CREATOR')`,
        rdfObjectProperty: "dc:creator",
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'actor',
        pathInIndex: 'agent',
        pathInGraphstore: `out('AGENT')`,
        rdfObjectProperty: "mnx:agent",
        relatedModelDefinition: ActorDefinition,
        isCascadingUpdated: true,
        readOnly: true
      }),
      new LinkDefinition({
        linkName: 'person',
        pathInIndex: 'agent',
        pathInGraphstore: `out('AGENT')`,
        rdfObjectProperty: "mnx:agent",
        relatedModelDefinition: PersonDefinition,
        isCascadingUpdated: true,
      }),
      new LinkDefinition({
        linkName: 'organisation',
        pathInIndex: 'agent',
        pathInGraphstore: `out('AGENT')`,
        rdfObjectProperty: "mnx:agent",
        relatedModelDefinition: OrganisationDefinition,
        isCascadingUpdated: true,
      }),
      new LinkDefinition({
        linkName: 'project',
        pathInIndex: 'project',
        pathInGraphstore: `out('PROJECT')`,
        rdfObjectProperty: "mnx:project",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'event',
        pathInIndex: 'event',
        pathInGraphstore: `out('EVENT')`,
        rdfObjectProperty: "mnx:event",
        relatedModelDefinition: EventDefinition,
        isCascadingUpdated: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'role',
        pathInGraphstore: `out('ROLE')`,
        pathInIndex: 'roles',
        rdfDataProperty: "mnx:role"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'startDate',
        rdfDataProperty: "mnx:startDate",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      }),
      new LiteralDefinition({
        literalName: 'endDate',
        rdfDataProperty: "mnx:endDate",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      })
    ];
  }
};