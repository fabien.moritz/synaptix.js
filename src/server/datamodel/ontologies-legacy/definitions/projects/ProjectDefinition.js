/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import Project from "../../models/projects/Project";
import ProjectIndexMatcher from "../../matchers/projects/ProjectIndexMatcher";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import EventDefinition from "./EventDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import PersonDefinition from "../foaf/PersonDefinition";
import InvolvementDefinition from "./InvolvementDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class ProjectDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "dc": "http://purl.org/dc/elements/1.1/",
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Project";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Project';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'project';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Project;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ProjectIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'creator',
        pathInGraphstore: `out('CREATOR')`,
        rdfObjectProperty: "dc:creator",
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'parentProject',
        pathInIndex: 'parentProject',
        pathInGraphstore: `out('PARENT_PROJECT')`,
        rdfObjectProperty: "mnx:parentProject",
        relatedModelDefinition: ProjectDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'subProjects',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'parentProject',
          useIndexMatcherOf: EventDefinition
        }),
        pathInGraphstore: `in('PARENT_PROJECT')`,
        rdfReversedObjectProperty: "mnx:parentProject",
        relatedModelDefinition: ProjectDefinition,
        isPlural: true,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'events',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'project',
          useIndexMatcherOf: EventDefinition
        }),
        pathInGraphstore: `in('OCCURS_IN').hasLabel('Event')`,
        rdfReversedObjectProperty: "mnx:occursIn",
        relatedModelDefinition: EventDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'involvements',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'project',
          useIndexMatcherOf: InvolvementDefinition
        }),
        pathInGraphstore: `in('PROJECT').hasLabel('${InvolvementDefinition.getNodeType()}')`,
        rdfReversedObjectProperty: "mnx:project",
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingUpdated: true,
        isCascadingRemoved: true
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles',
        rdfDataProperty: 'dc:title'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions',
        rdfDataProperty: 'dc:description'
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInGraphstore: `out('SHORT_DESCRIPTION')`,
        pathInIndex: 'shortDescriptions',
        rdfDataProperty: "mnx:shortDescription"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      }),
      new LiteralDefinition({
        literalName: 'image',
        rdfDataProperty: "mnx:image"
      })
    ];
  }
};