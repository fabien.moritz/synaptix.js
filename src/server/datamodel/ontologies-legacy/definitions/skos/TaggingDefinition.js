/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import Tagging from "../../models/skos/Tagging";
import {generateCreatorLink} from "../foaf/PersonDefinition";
import ConceptDefinition from "./ConceptDefinition";
import TaggingIndexMatcher from "../../matchers/skos/TaggingIndexMatcher";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class TaggingDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Tagging";
  }


  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Tagging';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'tagging';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Tagging;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return TaggingIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'object',
        pathInIndex: 'object',
        pathInGraphstore: `out('TAGGING_OBJECT')`,
        rdfObjectProperty: "mnx:object",
        relatedModelDefinition: ModelDefinitionAbstract,
      }),
      new LinkDefinition({
        linkName: 'concept',
        pathInIndex: 'subject',
        pathInGraphstore: `out('TAGGING_SUBJECT')`,
        rdfObjectProperty: "mnx:subject",
        relatedModelDefinition: ConceptDefinition,
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'weight',
        rdfDataProperty: "mnx:weight",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float"
      })
    ];
  }
};