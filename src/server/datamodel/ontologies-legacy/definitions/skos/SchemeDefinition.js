/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import Scheme from "../../models/skos/Scheme";
import SchemeIndexMatcher from "../../matchers/skos/SchemeIndexMatcher";
import ConceptDefinition from "./ConceptDefinition";
import ThesaurusDefinition from "./ThesaurusDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class SchemeDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "dc": "http://purl.org/dc/elements/1.1/",
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'skos:Scheme';
  }


  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Scheme';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'scheme';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Scheme;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return SchemeIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'concepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'scheme',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `in('IN_SCHEME')`,
        rdfReversedObjectProperty: "skos:inScheme",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true,
        isCascadingRemoved: true,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'topConcepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'topInScheme',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `out('HAS_TOP_CONCEPT')`,
        rdfObjectProperty: "skos:hasTopConcept",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'thesaurus',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'scheme',
          useIndexMatcherOf: ThesaurusDefinition
        }),
        pathInGraphstore: `out('SCHEME_OF')`,
        rdfObjectProperty: "mnx:schemeOf",
        relatedModelDefinition: ThesaurusDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles',
        rdfDataProperty: "dc:title"
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions',
        rdfDataProperty: "dc:description"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      })
    ];
  }
};