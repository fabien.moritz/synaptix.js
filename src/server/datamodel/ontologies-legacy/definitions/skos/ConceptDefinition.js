/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ConceptIndexMatcher from "../../matchers/skos/ConceptIndexMatcher";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import Concept from "../../models/skos/Concept";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import LinkDefinition, {
  ChildOfIndexDefinition,
  UseIndexMatcherOfDefinition
} from "../../../toolkit/definitions/LinkDefinition";
import ThesaurusDefinition from "./ThesaurusDefinition";
import SchemeDefinition from "./SchemeDefinition";
import LocalizedLabelDefinition from "../common/LocalizedLabelDefinition";
import TaggingDefinition from "./TaggingDefinition";
import CollectionDefinition from "./CollectionDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class ConceptDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'skos:Concept';
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Concept';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'concept';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Concept;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ConceptIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'thesaurus',
        pathInIndex: new ChildOfIndexDefinition(ThesaurusDefinition),
        pathInGraphstore: `out('CONCEPT_OF')`,
        rdfObjectProperty: "mnx:conceptOf",
        relatedModelDefinition: ThesaurusDefinition
      }),
      new LinkDefinition({
        linkName: 'schemes',
        pathInIndex: 'schemes',
        pathInGraphstore: `out('IN_SCHEME')`,
        rdfObjectProperty: "skos:inScheme",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'topInSchemes',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'topInScheme',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `in('HAS_TOP_CONCEPT')`,
        rdfReversedObjectProperty: "skos:hasTopConcept",
        relatedModelDefinition: SchemeDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'collections',
        pathInIndex: 'collections',
        pathInGraphstore: `in('MEMBER')`,
        rdfObjectProperty: "skos:member",
        relatedModelDefinition: CollectionDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'broaders',
        // pathInIndex: new UseIndexMatcherOfDefinition({
        //   filterName: 'broaders',
        //   useIndexMatcherOf: ConceptDefinition
        // }),
        pathInIndex: "broaders",
        pathInGraphstore: `out('BROADER').hasLabel('Concept')`,
        rdfObjectProperty: "skos:broader",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'related',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'related',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `both('RELATED').hasLabel('Concept')`,
        rdfObjectProperty: "skos:related",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'narrowers',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'narrowers',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `in('BROADER').hasLabel('Concept')`,
        //rdfObjectProperty: "skos:narrower",
        rdfReversedObjectProperty: "skos:broader",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'closeMatchConcepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'closeMatches',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `both('CLOSE_MATCH').hasLabel('Concept')`,
        rdfObjectProperty: "skos:closeMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'exactMatchConcepts',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'exactMatches',
          useIndexMatcherOf: ConceptDefinition
        }),
        pathInGraphstore: `both('EXACT_MATCH').hasLabel('Concept')`,
        rdfObjectProperty: "skos:exactMatch",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'altLabels',
        pathInIndex: 'altLabels',
        pathInGraphstore: `out('ALT_LABEL')`,
        rdfObjectProperty: "skos:altLabel",
        relatedModelDefinition: LocalizedLabelDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'prefLabels',
        pathInIndex: 'prefLabels',
        pathInGraphstore: `out('PREF_LABEL')`,
        rdfObjectProperty: "skos:prefLabel",
        relatedModelDefinition: LocalizedLabelDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'hiddenLabels',
        pathInIndex: 'hiddenLabels',
        pathInGraphstore: `out('HIDDEN_LABEL')`,
        rdfObjectProperty: "skos:hiddenLabel",
        relatedModelDefinition: LocalizedLabelDefinition,
        isPlural: true
      }),
      new LinkDefinition({
        linkName: 'taggings',
        pathInIndex: 'taggings',
        pathInGraphstore: `in('TAGGING_SUBJECT')`,
        rdfReversedObjectProperty: "mnx:subject",
        relatedModelDefinition: TaggingDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'prefLabel',
        pathInGraphstore: `out('PREF_LABEL')`,
        pathInIndex: 'prefLabels',
        rdfDataProperty: "skos:prefLabel"
      }),
      new LabelDefinition({
        labelName: 'altLabel',
        pathInGraphstore: `out('ALT_LABEL')`,
        pathInIndex: 'altLabels',
        rdfDataProperty: "skos:altLabel"
      }),
      new LabelDefinition({
        labelName: 'hiddenLabel',
        pathInGraphstore: `out('HIDDEN_LABEL')`,
        pathInIndex: 'altLabels',
        rdfDataProperty: "skos:hiddenLabel"
      }),
      new LabelDefinition({
        labelName: 'scopeNote',
        pathInGraphstore: `out('SCOPE_NOTE')`,
        pathInIndex: 'scopeNotes',
        rdfDataProperty: "skos:scopeNote"
      }),
      new LabelDefinition({
        labelName: 'definition',
        pathInGraphstore: `out('DEFINITION')`,
        pathInIndex: 'definitions',
        rdfDataProperty: "skos:definition"
      }),
      new LabelDefinition({
        labelName: 'changeNote',
        pathInGraphstore: `out('CHANGE_NOTE')`,
        pathInIndex: 'changeNotes',
        rdfDataProperty: "skos:changeNote"
      }),
      new LabelDefinition({
        labelName: 'notation',
        pathInGraphstore: `out('NOTATION')`,
        pathInIndex: 'notations',
        rdfDataProperty: "skos:notations"
      }),
      new LabelDefinition({
        labelName: 'historyNote',
        pathInGraphstore: `out('HISTORY_NOTE')`,
        pathInIndex: 'historyNotes',
        rdfDataProperty: "skos:historyNote"
      }),
      new LabelDefinition({
        labelName: 'example',
        pathInGraphstore: `out('EXAMPLE')`,
        pathInIndex: 'examples',
        rdfDataProperty: "skos:example"
      }),
      new LabelDefinition({
        labelName: 'editorialNote',
        pathInGraphstore: `out('EDITORIAL_NOTE')`,
        pathInIndex: 'editorialNotes',
        rdfDataProperty: "skos:editorialNote"
      }),
      new LabelDefinition({
        labelName: 'note',
        pathInGraphstore: `out('NOTE')`,
        pathInIndex: 'notes',
        rdfDataProperty: "skos:note"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'isDraft',
        rdfDataProperty: "mnx:isDraft"
      }),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      })
    ];
  }
};