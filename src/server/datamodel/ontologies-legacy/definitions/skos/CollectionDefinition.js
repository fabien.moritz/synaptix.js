/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import LinkDefinition, {ChildOfIndexDefinition} from "../../../toolkit/definitions/LinkDefinition";
import ThesaurusDefinition from "./ThesaurusDefinition";
import Collection from "../../models/skos/Collection";
import CollectionIndexMatcher from "../../matchers/skos/CollectionIndexMatcher";
import ConceptDefinition from "./ConceptDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class CollectionDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'skos:Collection';
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Collection';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'collection';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Collection;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return CollectionIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'thesaurus',
        pathInIndex: new ChildOfIndexDefinition(ThesaurusDefinition),
        pathInGraphstore: `out('COLLECTION_OF')`,
        rdfObjectProperty: "mnx:collectionOf",
        relatedModelDefinition: ThesaurusDefinition
      }),
      new LinkDefinition({
        linkName: 'concepts',
        pathInIndex: 'concepts',
        pathInGraphstore: `out('MEMBER')`,
        rdfReversedObjectProperty: "skos:member",
        relatedModelDefinition: ConceptDefinition,
        isPlural: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'color',
        rdfDataProperty: "mnx:color"
      })
    ];
  }
};