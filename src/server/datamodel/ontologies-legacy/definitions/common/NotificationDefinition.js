/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import PersonDefinition, {generateCreatorLink} from "../foaf/PersonDefinition";
import Notification from "../../models/common/Notification";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";

export default class NotificationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Notification";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Notification';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Notification;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'target',
        pathInIndex: 'notificationTarget',
        pathInGraphstore: `out('NOTIFICATION_TARGET')`,
        rdfObjectProperty: "mnx:notificationTarget",
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'subject',
        pathInIndex: 'notificationSubject',
        pathInGraphstore: `out('NOTIFICATION_OBJECT')`,
        rdfObjectProperty: "mnx:notificationSubject",
        relatedModelDefinition: ModelDefinitionAbstract,
      })
    ];
  }
};