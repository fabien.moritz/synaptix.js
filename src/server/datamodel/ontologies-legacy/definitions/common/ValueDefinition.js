/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import {generateCreatorLink} from "../foaf/PersonDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import Value from "../../models/common/Value";

export default class ValueDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Value";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Value';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Value;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'object',
        pathInGraphstore: `in('HAS_VALUE')`,
        relatedModelDefinition: ModelDefinitionAbstract,
        rdfReversedObjectProperty: "mnx:value",
        isCascadingUpdated: true
      })
    ];
  }
};

export let generateValueLink = () => new LinkDefinition({
  linkName: 'values',
  pathInIndex: '_values',
  pathInGraphstore: `out('HAS_VALUE')`,
  rdfObjectProperty: "mnx:value",
  relatedModelDefinition: ValueDefinition,
  isCascadingRemoved: true
});