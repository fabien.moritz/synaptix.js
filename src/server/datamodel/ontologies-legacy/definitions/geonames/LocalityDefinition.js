/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import Locality from "../../models/geonames/Locality";
import {generateCreatorLink} from "../foaf/PersonDefinition";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import ActorDefinition from "../foaf/ActorDefinition";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class LocalityDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Locality";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Locality';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Locality;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'object',
        pathInGraphstore: `in('HAS_ADDRESS')`,
        rdfReversedObjectProperty: "mnx:address",
        relatedModelDefinition: ActorDefinition,
        isCascadingUpdated: true
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'street1',
        rdfDataProperty: "mnx:street1"
      }),
      new LiteralDefinition({
        literalName: 'street2',
        rdfDataProperty: "mnx:street2"
      }),
      new LiteralDefinition({
        literalName: 'postCode',
        rdfDataProperty: 'gn:postalCode'
      }),
      new LiteralDefinition({
        literalName: 'city',
        rdfDataProperty: "mnx:cityName"
      }),
      new LiteralDefinition({
        literalName: 'countryName',
        rdfDataProperty: "mnx:countryName"
      }),
      new LiteralDefinition({
        literalName: 'longitude',
        rdfDataProperty: 'geo:long'
      }),
      new LiteralDefinition({
        literalName: 'latitude',
        rdfDataProperty: 'geo:lat'
      }),
    ];
  }
};