/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import Point from "../../models/geonames/Point";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class PointDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "geo": "http://www.w3.org/2003/01/geo/wgs84_pos#"
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "geo:Point";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Point';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Point;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'alt',
        rdfDataProperty: "geo:alt",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      }),
      new LiteralDefinition({
        literalName: 'longitude',
        rdfDataProperty: 'geo:long',
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      }),
      new LiteralDefinition({
        literalName: 'latitude',
        rdfDataProperty: 'geo:lat',
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      })
    ];
  }
};