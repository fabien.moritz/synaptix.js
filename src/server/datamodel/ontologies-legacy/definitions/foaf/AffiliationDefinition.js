/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import PersonDefinition, {generateCreatorLink} from "./PersonDefinition";
import Affiliation from "../../models/foaf/Affiliation";
import OrganisationDefinition from "./OrganisationDefinition";
import AffiliationIndexMatcher from "../../matchers/foaf/AffiliationIndexMatcher";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class AffiliationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Affiliation";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Affiliation';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'affiliation';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Affiliation;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return AffiliationIndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      generateCreatorLink(),
      new LinkDefinition({
        linkName: 'person',
        pathInIndex: 'affiliate',
        pathInGraphstore: `out('AFFILIATE')`,
        rdfObjectProperty: "mnx:affiliate",
        relatedModelDefinition: PersonDefinition,
        isCascadingUpdated: true
      }),
      new LinkDefinition({
        linkName: 'organisation',
        pathInIndex: 'organization',
        pathInGraphstore: `out('ORGANIZATION')`,
        rdfObjectProperty: "mnx:organization",
        relatedModelDefinition: OrganisationDefinition,
        isCascadingUpdated: true,
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'role',
        pathInGraphstore: `out('ROLE')`,
        pathInIndex: 'roles',
        rdfDataProperty: "mnx:role"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'startDate',
        rdfDataProperty: "mnx:startDate",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      }),
      new LiteralDefinition({
        literalName: 'endDate',
        rdfDataProperty: "mnx:endDate",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float",
      })
    ];
  }
};