/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import Group from "../../models/foaf/Group";
import GroupIndexMatcher from "../../matchers/foaf/GroupIndexMatcher";
import MembershipDefinition from "./MembershipDefinition";
import ActorDefinition from "./ActorDefinition";

export default class GroupDefinition extends ActorDefinition {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'foaf:Group';
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Group';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'group';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Group;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return GroupIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'memberships',
        pathInIndex: 'memberships',
        pathInGraphstore: `in('GROUP').hasLabel('Membership')`,
        rdfObjectProperty: "mnx:organization",
        relatedModelDefinition: MembershipDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'name',
        pathInGraphstore: `out('NAMES')`,
        pathInIndex: 'names'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions'
      })
    ];
  }
};