/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LinkDefinition, {UseIndexMatcherOfDefinition} from "../../../toolkit/definitions/LinkDefinition";
import AffiliationDefinition from "./AffiliationDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import Organisation from "../../models/foaf/Organisation";
import ActorDefinition from "./ActorDefinition";
import OrganisationIndexMatcher from "../../matchers/foaf/OrganisationIndexMatcher";
import InvolvementDefinition from "../projects/InvolvementDefinition";
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";

export default class OrganisationDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getParentDefinitions() {
    return [ActorDefinition];
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return 'foaf:Organization';
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Org';
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'org';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Organisation;
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return OrganisationIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'affiliations',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'organisation',
          useIndexMatcherOf: AffiliationDefinition
        }),
        pathInGraphstore: `in('ORGANIZATION').hasLabel("Affiliation")`,
        relatedModelDefinition: AffiliationDefinition,
        rdfReversedObjectProperty: "mnx:organization",
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'involvements',
        pathInIndex: new UseIndexMatcherOfDefinition({
          filterName: 'organisation',
          useIndexMatcherOf: InvolvementDefinition
        }),
        pathInGraphstore: `in('AGENT').hasLabel('${InvolvementDefinition.getNodeType()}')`,
        rdfReversedObjectProperty: "mnx:agent",
        relatedModelDefinition: InvolvementDefinition,
        isPlural: true,
        isCascadingRemoved: true
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'name',
        pathInGraphstore: `out('NAME')`,
        pathInIndex: 'names',
        rdfDataProperty: "mnx:name"
      }),
      new LabelDefinition({
        labelName: 'shortName',
        pathInGraphstore: `out('SHORT_NAME')`,
        pathInIndex: 'shortNames',
        rdfDataProperty: "mnx:shortName"
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions',
        rdfDataProperty: "dc:description"
      }),
      new LabelDefinition({
        labelName: 'shortDescription',
        pathInGraphstore: `out('SHORT_DESCRIPTION')`,
        pathInIndex: 'shortDescriptions',
        rdfDataProperty: "mnx:shortDescription"
      }),
      new LabelDefinition({
        labelName: 'other',
        pathInGraphstore: `out('OTHER')`,
        pathInIndex: 'others',
        rdfDataProperty: "mnx:other"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals()
    ];
  }
};