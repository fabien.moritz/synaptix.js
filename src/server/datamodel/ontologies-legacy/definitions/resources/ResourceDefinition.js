/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import ModelDefinitionAbstract from "../../../toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition from "../../../toolkit/definitions/LinkDefinition";
import LabelDefinition from "../../../toolkit/definitions/LabelDefinition";
import Resource from "../../models/resources/Resource";
import AttachmentDefinition from "../projects/AttachmentDefinition";
import ExternalLinkDefinition from "../common/ExternalLinkDefinition";
import PersonDefinition from "../foaf/PersonDefinition";
import ResourceIndexMatcher from "../../matchers/resources/ResourceIndexMatcher";
import LiteralDefinition from "../../../toolkit/definitions/LiteralDefinition";

export default class ResourceDefinition extends ModelDefinitionAbstract {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
      ...super.getRdfPrefixesMapping(),
      "dc": "http://purl.org/dc/elements/1.1/",
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "mnx:Resource";
  }

  /**
   * @inheritDoc
   */
  static getNodeType() {
    return 'Resource';
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return Resource;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'resource';
  }

  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    return ResourceIndexMatcher
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      new LinkDefinition({
        linkName: 'creator',
        pathInIndex: 'object',
        pathInGraphstore: `out('OWNER')`,
        rdfObjectProperty: "mnx:owner",
        relatedModelDefinition: PersonDefinition,
      }),
      new LinkDefinition({
        linkName: 'externalLinks',
        pathInIndex: 'externalLinks',
        pathInGraphstore: `out('HAS_LINK')`,
        rdfObjectProperty: "mnx:externalLinks",
        relatedModelDefinition: ExternalLinkDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
      new LinkDefinition({
        linkName: 'attachments',
        pathInIndex: 'attachments',
        pathInGraphstore: `in('RESOURCE')`,
        rdfReversedObjectProperty: "mnx:resource",
        relatedModelDefinition: AttachmentDefinition,
        isPlural: true,
        isCascadingRemoved: true
      }),
    ];
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      new LabelDefinition({
        labelName: 'title',
        pathInGraphstore: `out('TITLE')`,
        pathInIndex: 'titles',
        rdfDataProperty: 'dc:title'
      }),
      new LabelDefinition({
        labelName: 'description',
        pathInGraphstore: `out('DESCRIPTION')`,
        pathInIndex: 'descriptions',
        rdfDataProperty: 'dc:description'
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'filename',
        rdfDataProperty: "mnx:filename"
      }),
      new LiteralDefinition({
        literalName: 'mime',
        rdfDataProperty: "mnx:mime"
      }),
      new LiteralDefinition({
        literalName: 'size',
        rdfDataProperty: "mnx:size",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#float"
      }),
      new LiteralDefinition({
        literalName: 'etag',
        rdfDataProperty: "mnx:etag"
      }),
      new LiteralDefinition({
        literalName: 'publicUrl',
        rdfDataProperty: "mnx:publicUrl"
      })
    ];
  }
};