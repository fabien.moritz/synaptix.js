/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 16/10/2017
 */

import DefaultIndexMatcher from '../../toolkit/matchers/DefaultIndexMatcher';
import PersonIndexMatcher from './foaf/PersonIndexMatcher';
import OrganisationIndexMatcher from './foaf/OrganisationIndexMatcher';

export {
  DefaultIndexMatcher, PersonIndexMatcher, OrganisationIndexMatcher
};
