/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 16/10/2017
 */

import DefaultIndexMatcher from "../../../toolkit/matchers/DefaultIndexMatcher";

export default class OrganisationIndexMatcher extends DefaultIndexMatcher {
  /**
   * @inheritDoc
   */
  getFulltextQueryFragment(query) {
    return [{
      "nested": {
        "path": "names",
        "query": {
          "multi_match": {
            "query": query,
            "type": "phrase_prefix",
            "fields": ["names.value^2"],
            "boost": 2
          }
        }
      }
    }, {
      "query_string": {
        "query": `${query}*`
      }
    }];
  }

  /**
   * @inheritDoc
   */
  getFulltextQuery(query) {
    return {
      "function_score": {
        "query": {
          "bool": {
            "should": this.getFulltextQueryFragment(query)
          }
        },
        "min_score": 0.6
      }
    };
  }

  /**
   * @inheritDoc
   */
  getFilterByKeyValue(key, value) {
    switch (key) {
      case "concepts":
        return {
          "nested": {
            "path": "taggings.subject",
            "query": this.getTermFilter("taggings.subject.id", value)
          }
        };
      default:
        return super.getFilterByKeyValue(key, value);
    }
  }

  /**
   * @inheritDoc
   */
  getAggregations() {
    let size = 1;

    return {
      "concepts": {
        "nested": {
          "path": "taggings.subject"
        },
        "aggs": {
          "nesting": {
            "terms": {
              "field": "taggings.subject.id",
              "size": size
            }
          }
        }
      },
      "affiliations": {
        "nested": {
          "path": "affiliates.affiliate"
        },
        "aggs": {
          "nesting": {
            "terms": {
              "field": "affiliates.affiliate.id",
              "size": size
            }
          }
        }
      }
    };
  }
}