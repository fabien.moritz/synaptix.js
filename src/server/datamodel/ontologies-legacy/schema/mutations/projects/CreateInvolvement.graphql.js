/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {generateCreateMutationDefinitionForType, generateCreateMutationResolverForType} from "../utils/generators";
import {createObjectInConnectionResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import InvolvementDefinition from "../../../definitions/projects/InvolvementDefinition";

export let CreateInvolvement = generateCreateMutationDefinitionForType('Involvement', `
  actorId: ID!
  eventId: ID
  projectId: ID,
  creatorId: ID
`);

export let CreateInvolvementResolvers = generateCreateMutationResolverForType('Involvement',
  async (_, {input: {objectInput, actorId, eventId, projectId, creatorId}}, synaptixSession) => {
    if (!eventId && !projectId) {
      throw new Error(`One of param 'eventId' or 'projectId' must be defined`);
    }

    if (creatorId) {
      creatorId = synaptixSession.extractIdFromGlobalId(creatorId)
    } else {
      creatorId = (await synaptixSession.getMe() || {}).id;
    }

    if (!creatorId) {
      throw new Error("A creatorId must be provided");
    }

    return createObjectInConnectionResolver(
      InvolvementDefinition,
      [
        InvolvementDefinition.getLink("creator").generateLinkFromTargetId(creatorId),
        InvolvementDefinition.getLink("actor").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(actorId)),
        eventId ?
          InvolvementDefinition.getLink("event").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(eventId)) :
          InvolvementDefinition.getLink("project").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(projectId))
      ],
      _, {input: {objectInput}}, synaptixSession);
  }
);
