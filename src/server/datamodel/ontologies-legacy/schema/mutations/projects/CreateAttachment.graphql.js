/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {generateCreateMutationDefinitionForType, generateCreateMutationResolverForType} from "../utils/generators";
import {createObjectInConnectionResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import AttachmentDefinition from "../../../definitions/projects/AttachmentDefinition";

export let CreateAttachment = generateCreateMutationDefinitionForType('Attachment', `
  resourceId: ID!
  eventId: ID!
  creatorId: ID
`);

export let CreateAttachmentResolvers = generateCreateMutationResolverForType('Attachment',
  async (_, {input: {objectInput, resourceId, eventId, creatorId}}, synaptixSession) => {

    if (creatorId) {
      creatorId = synaptixSession.extractIdFromGlobalId(creatorId)
    } else {
      creatorId = (await synaptixSession.getMeAsPerson())?.id;
    }
    if (!creatorId) {
      throw new Error("A creatorId must be provided");
    }

    return createObjectInConnectionResolver(
      AttachmentDefinition,
      [
        AttachmentDefinition.getLink("creator").generateLinkFromTargetId(creatorId),
        AttachmentDefinition.getLink("resource").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(resourceId)),
        AttachmentDefinition.getLink("event").generateLinkFromTargetId(synaptixSession.extractIdFromGlobalId(eventId))
      ],
      _, {input: {objectInput}}, synaptixSession)
  }
);
