/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {createObjectInConnectionResolver, updateObjectResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";

export const generateUpdateMutationDefinitionForType = (type) => `
input Update${type}Input {
  objectId: ID!
  objectInput: ${type}Input!
}

type Update${type}Payload {
  updatedObject: ${type}
}

extend type Mutation{
  update${type}(input: Update${type}Input!): Update${type}Payload
}
`;

export const generateUpdateMutationResolverForType = (type, resolver) => ({
  Mutation: {
    [`update${type}`]: resolver || updateObjectResolver.bind(this, null)
  }
});

/**
 * Generate "create" node mutation.
 *
 * @param {string} type mutation type name
 * @param {string} [extraInputArgs] extra inputs
 * @param {string} [edgeType = type] returned edge type
 * @return {string}
 */
export const generateCreateMutationDefinitionForType = (type, extraInputArgs, edgeType) => `
input Create${type}Input {
  objectInput: ${edgeType || type}Input
  ${extraInputArgs || ""}
}

type Create${type}Payload {
  createdEdge: ${edgeType || type}Edge
}

extend type Mutation{
  create${type}(input: Create${type}Input!): Create${type}Payload
}
`;

/**
 * Generate "create" node resolver.
 *
 * @param {string} type
 * @param {LinkDefinition[]|function} [linksOrResolver] Array of definition
 * @return {object}
 */
export const generateCreateMutationResolverForType = (type, linksOrResolver) => ({
  Mutation: {
    [`create${type}`]: typeof linksOrResolver === "function" ? linksOrResolver : createObjectInConnectionResolver.bind(this, type, linksOrResolver)
  }
});