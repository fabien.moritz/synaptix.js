/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {generateSchema, mergeResolvers} from "../../../index";

import {FOAFResolvers, FOAFTypes} from './types/foaf/index';
import {FOAFMutations, FOAFMutationsResolvers} from './mutations/foaf/index';
import {GeoNamesResolvers, GeoNamesTypes} from './types/geonames/index';
import {GeonamesMutations, GeonamesMutationsResolvers} from './mutations/geonames/index';
import {SKOSResolvers, SKOSTypes} from './types/skos/index';
import {QueryType} from "./types/Query.graphql";
import {MutationType} from "../../toolkit/graphql/schema/mutations/Mutation.graphql";
import {ResourcesResolvers, ResourcesTypes} from "./types/resources/index";
import {ResourcesMutations, ResourcesMutationsResolvers} from "./mutations/resources/index";
import {ProjectsResolvers, ProjectsTypes} from "./types/projects";
import {ProjectsMutations, ProjectsMutationsResolvers} from "./mutations/projects";

generateSchema({
  typeDefs: [].concat(QueryType, FOAFTypes, SKOSTypes, ResourcesTypes, MutationType, FOAFMutations, GeoNamesTypes, GeonamesMutations, ResourcesMutations, ProjectsTypes, ProjectsMutations),
  resolvers: mergeResolvers(FOAFMutationsResolvers, FOAFResolvers, SKOSResolvers, ResourcesResolvers, GeoNamesResolvers, GeonamesMutationsResolvers, ResourcesMutationsResolvers, ProjectsResolvers, ProjectsMutationsResolvers),
  printGraphQLSchemaPath: `${__dirname}/schema.graphql`,
  printJSONSchemaPath: `${__dirname}/schema.json`
});