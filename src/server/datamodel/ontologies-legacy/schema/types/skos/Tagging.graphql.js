/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import TaggingDefinition from "../../../definitions/skos/TaggingDefinition";
import {
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let TaggingType = `
""" A tagging object """
type Tagging implements ObjectInterface {
  """ Tagging certitude weight """
  weight: Float

  """ Tagging concept """
  concept: Concept

  ${ObjectDefaultProperties}
}

${generateConnectionForType("Tagging")}

extend type Query{
  """ Get tagging """
  tagging(id:ID): Tagging
}

`;

export let TaggingResolvers = {
  Tagging: {
    ...generateTypeResolvers("Tagging"),
    concept: getLinkedObjectResolver.bind(this, TaggingDefinition.getLink('concept')),
    weight: (object) => object.weight,
  },
  Query: {
    tagging: getObjectResolver.bind(this, TaggingDefinition)
  },
  ...generateConnectionResolverFor("Tagging")
};