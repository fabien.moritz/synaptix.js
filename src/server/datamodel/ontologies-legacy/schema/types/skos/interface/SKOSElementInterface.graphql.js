/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../../toolkit/graphql/schema/types/generators";
import {getObjectResolver} from "../../../../../toolkit/graphql/helpers/resolverHelpers";

export let SKOSElementInterface = `
""" A skos element """
interface SKOSElementInterface {
  id: ID!
  childrenSKOSElements(${connectionArgs}, ${filteringArgs}): SKOSElementInterfaceConnection
  childrenSKOSElementsCount(${filteringArgs}): Int
}

${generateConnectionForType("SKOSElementInterface")}

extend type Query{
  skosElement(id: ID!): SKOSElementInterface
}
`;

export let SkosElementResolvers = {
  SKOSElementInterface: {
    __resolveType(obj) {
      return obj.constructor.name;
    }
  },
  Query: {
    /**
     * @param {string} _
     * @param {id} id
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    skosElement: (_, {id}, synaptixSession) => {
      let modelDefinition = synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(synaptixSession.extractTypeFromGlobalId(id));
      return getObjectResolver(modelDefinition, _, {id}, synaptixSession);
    }
  },
  ...generateConnectionResolverFor("SKOSElementInterface")
};