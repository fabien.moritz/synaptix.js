/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ConceptResolvers, ConceptType} from "./Concept.graphql";
import {CollectionResolvers, CollectionType} from "./Collection.graphql";
import {SchemeResolvers, SchemeType} from "./Scheme.graphql";
import {ThesauthequeResolvers, ThesauthequeType} from "./Thesautheque.graphql";
import {SKOSElementInterface, SkosElementResolvers} from "./interface/SKOSElementInterface.graphql";
import {ObjectInterface, ObjectResolvers} from "../ObjectInterface.graphql";
import {ThesaurusResolvers, ThesaurusType} from "./Thesaurus.graphql";
import {LocalizedLabelType} from "../common/LocalizedLabel.graphql";
import {ConnectionTypes} from '../../../../toolkit/graphql/schema/types/ConnectionDefinitions.graphql';
import {mergeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {NodeInterface, NodeResolvers} from "../common/NodeInterface.graphql";
import {MatcherResolvers, MatcherType} from "../common/Matcher.graphql";
import {TaggingResolvers, TaggingType} from "./Tagging.graphql";


export let SKOSTypes = [
  NodeInterface,
  ObjectInterface,
  ConnectionTypes,
  ConceptType,
  CollectionType,
  SchemeType,
  ThesaurusType,
  ThesauthequeType,
  LocalizedLabelType,
  MatcherType,
  SKOSElementInterface,
  TaggingType
];

export let SKOSResolvers = mergeResolvers(
  NodeResolvers,
  ObjectResolvers,
  ConceptResolvers,
  CollectionResolvers,
  SchemeResolvers,
  ThesaurusResolvers,
  ThesauthequeResolvers,
  MatcherResolvers,
  SkosElementResolvers,
  TaggingResolvers
);