/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {
  generateTypeResolvers,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import ThesaurusDefinition from "../../../definitions/skos/ThesaurusDefinition";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let ThesaurusType = `
""" A thesaurus """
type Thesaurus implements ObjectInterface & SKOSElementInterface  {
  """ Name of the thesaurus """
  title: String

  """ Description of thesaurus """
  description: String

  """ Concepts of thesaurus """
  concepts(${connectionArgs}, ${filteringArgs}): ConceptConnection
  
  """ Concepts count of thesaurus """
  conceptsCount(${filteringArgs}): Int
  
  """ Collections of thesaurus """
  collections(${connectionArgs}, ${filteringArgs}): CollectionConnection
  
  """ Collections count of thesaurus """
  collectionsCount(${filteringArgs}): Int
  
  """ Schemes of thesaurus """
  schemes(${connectionArgs}, ${filteringArgs}): SchemeConnection
  
  """ Schemes count of thesaurus """
  schemesCount(${filteringArgs}): Int
  
  """ The children SKOSElements connection """
  childrenSKOSElements(${connectionArgs}, ${filteringArgs}): SKOSElementInterfaceConnection

  """ The children SKOSElements count"""
  childrenSKOSElementsCount(${filteringArgs}): Int
  
  ${ObjectDefaultProperties}
}

input ThesaurusInput {
  """ The ID """
  id: ID

  """ Name of the thesaurus """
  title: String

  """ Description of thesaurus """
  description: String
}

${generateConnectionForType("Thesaurus")}

extend type Query{
  """ Get a thesaurus """
  thesaurus(id: ID!): Thesaurus
}
`;

export let ThesaurusResolvers = {
  Thesaurus: {
    ...generateTypeResolvers("Thesaurus"),
    title: getLocalizedLabelResolver.bind(this, ThesaurusDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, ThesaurusDefinition.getLabel('description')),
    concepts: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('concepts')),
    conceptsCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('concepts')),
    collections: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('collections')),
    collectionsCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('collections')),
    schemes: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
    schemesCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
    childrenSKOSElements: getLinkedObjectsResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
    childrenSKOSElementsCount: getLinkedObjectsCountResolver.bind(this, ThesaurusDefinition.getLink('schemes')),
  },
  Query: {
    thesaurus: getObjectResolver.bind(this, ThesaurusDefinition)
  },
  ...generateConnectionResolverFor("Thesaurus")
};
