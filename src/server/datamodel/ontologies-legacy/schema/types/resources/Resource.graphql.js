/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import ResourceDefinition from "../../../definitions/resources/ResourceDefinition";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import get from 'lodash/get';

export let ResourceType = `
type Resource implements ObjectInterface {
  """ Title """
  title: String

  """ Description """
  description: String

  """ File name """
  filename: String

  """ Path """
  path: String

  """ Public URL """
  publicUrl: String

  """ Credits """
  credits: String

  """ MIME type """
  mime: String

  """ Size """
  size: Float

  """ Creator """
  creator: Person
  
  """ External links """
  externalLinks(${connectionArgs}, ${filteringArgs}): ExternalLinkConnection
  
  ${ObjectDefaultProperties}
}

input ResourceInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  """ Title """
  title: String

  """ Description """
  description: String  @formInput(type:"textarea")
  
  """ Credits """
  credits: String  @formInput(type:"textarea")
  
  """ MIME type """
  mime: String @formInput(disabled: true)

  """ Size """
  size: Float @formInput(disabled: true) 
  
  """ Public URL """
  publicUrl: String @formInput(disabled: true)
  
  """ File name """
  name: String @formInput(disabled: true)
  
  """ File path """
  path: String @formInput(disabled: true)
  
  """ Etag """
  etag: String @formInput(disabled: true)
}

${generateConnectionForType("Resource")}

extend type Query{
  """ Search for resources """
  resources(${connectionArgs}, ${filteringArgs}): ResourceConnection
  
  """ Get resource """
  resource(id:ID): Resource
}
`;


export let ResourceResolvers = {
  Resource: {
    ...generateTypeResolvers("Resource"),
    title: getLocalizedLabelResolver.bind(this, ResourceDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, ResourceDefinition.getLabel('description')),
    credits: (object) => object.credits,
    filename: (object) => get(object, "name", get(object, "ocdata.name")),
    path: (object) => get(object, "path", get(object, "ocdata.path")),
    publicUrl: (object) => get(object, "publicUrl", get(object, "ocdata.publicUrl")),
    mime: (object) => get(object, "mime", get(object, "ocdata.mime")),
    size: (object) => get(object, "size", get(object, "ocdata.size")),
    creator: getLinkedObjectResolver.bind(this, ResourceDefinition.getLink('creator')),
    externalLinks: getLinkedObjectsResolver.bind(this, ResourceDefinition.getLink('externalLinks')),
  },
  Query: {
    resources: getObjectsResolver.bind(this, ResourceDefinition),
    resource: getObjectResolver.bind(this, ResourceDefinition)
  },
  ...generateConnectionResolverFor("Resource")
};