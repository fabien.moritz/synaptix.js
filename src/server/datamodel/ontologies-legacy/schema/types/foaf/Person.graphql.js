/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  generateTypeResolvers,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getMeResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import PersonDefinition from "../../../definitions/foaf/PersonDefinition";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let PersonType = `
type Person implements ObjectInterface & ActorInterface {
  """ Display name """
  displayName: String

  """ Full name """
  fullName: String

  """ First name """
  firstName: String

  """ Last name """
  lastName: String

  """ Maiden name """
  maidenName: String

  """ Avatar URI """
  avatar: String

  """ Gender """
  gender: String

  """ Bio for current language """
  bio: String

  """ Short Bio for current language """
  shortBio: String

  """ Birthday """
  bday: Float

  """ Main email """
  mainEmail: EmailAccount

  """ Secondary emails """
  emails(${connectionArgs}, ${filteringArgs}): EmailAccountConnection

  """ Phones """
  phones(${connectionArgs}, ${filteringArgs}): PhoneConnection

  """ Addresses """
  addresses(${connectionArgs}, ${filteringArgs}): LocalityConnection

  """ External links """
  externalLinks(${connectionArgs}, ${filteringArgs}): ExternalLinkConnection
  
  """ Affilitations """
  affiliations(${connectionArgs}, ${filteringArgs}): AffiliationConnection
  
  """ Affilitations count """
  affiliationsCount: Int

  ${ObjectDefaultProperties}
}

input PersonInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  """ Avatar """
  avatar: String @formInput(type:"image")
  
  """ First name """
  firstName: String

  """ Last name """
  lastName: String

  """ Gender """
  gender: String @formInput(enumValues: ["male", "female"])

  """ Maiden name """
  maidenName: String @formInput(showIfPropName: "gender" showIfPropEnumValue: "female")
  
  """ Biography """
  bio: String @formInput(type:"textarea")

  """ Short biography """
  shortBio: String @formInput(type:"textarea")

  """ Birthday """
  bday: Float @formInput(type:"date")
}

${generateConnectionForType("Person")}

extend type Query{
  """ Get myself object """
  me: Person
  
  """ Get person """
  person(me: Boolean id:ID): Person
  
  """ Search for persons """
  persons(${connectionArgs}, ${filteringArgs}): PersonConnection
}
`;


export let PersonResolvers = {
  Person: {
    ...generateTypeResolvers("Person"),
    displayName: (object) => object.firstName ? `${object.firstName || ''} ${object.lastName || ''}` : object.fullName,
    fullName: (object) => object.fullName,
    firstName: (object) => object.firstName,
    lastName: (object) => object.lastName,
    maidenName: (object) => object.maidenName,
    avatar: (object) => object.avatar,
    gender: (object) => object.gender,
    bday: (object) => object.bday,
    bio: getLocalizedLabelResolver.bind(this, PersonDefinition.getLabel('bio')),
    shortBio: getLocalizedLabelResolver.bind(this, PersonDefinition.getLabel('shortBio')),
    mainEmail: async (object, args, synaptixSession) => {
      let emails = await synaptixSession.getLinkedObjectsFor(object, PersonDefinition.getLink('emails'), args);

      if (emails.length > 0) {
        return emails[0];
      }
    },
    emails: getLinkedObjectsResolver.bind(this, PersonDefinition.getLink('emails')),
    phones: getLinkedObjectsResolver.bind(this, PersonDefinition.getLink('phones')),
    addresses: getLinkedObjectsResolver.bind(this, PersonDefinition.getLink('addresses')),
    externalLinks: getLinkedObjectsResolver.bind(this, PersonDefinition.getLink('externalLinks')),
    affiliations: getLinkedObjectsResolver.bind(this, PersonDefinition.getLink('affiliations')),
    affiliationsCount: getLinkedObjectsCountResolver.bind(this, PersonDefinition.getLink('affiliations')),
  },
  Query: {
    me: getMeResolver,
    person: async (root, {me, ...args}, synaptixSession) => {
      return me ? synaptixSession.getMe() : getObjectResolver(PersonDefinition, root, args, synaptixSession);
    },
    persons: async (parent, args, synaptixSession) => getObjectsResolver(PersonDefinition, parent, args, synaptixSession),
  },
  ...generateConnectionResolverFor("Person")
};