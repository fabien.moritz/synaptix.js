/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import {getObjectResolver, getObjectsResolver} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import OrganisationDefinition from "../../../definitions/foaf/OrganisationDefinition";
import PersonDefinition from "../../../definitions/foaf/PersonDefinition";

export let ActorInterface = `
""" An interface for all actors objects """
interface ActorInterface {
  """ Display name """
  displayName: String
  
  """ Avatar """
  avatar: String


  """ Main email """
  mainEmail: EmailAccount

  """ Secondary emails """
  emails(${connectionArgs}, ${filteringArgs}): EmailAccountConnection

  """ Phones """
  phones(${connectionArgs}, ${filteringArgs}): PhoneConnection

  """ Addresses """
  addresses(${connectionArgs}, ${filteringArgs}): LocalityConnection

  """ External links """
  externalLinks(${connectionArgs}, ${filteringArgs}): ExternalLinkConnection
  
  """ Affilitations """
  affiliations(${connectionArgs}, ${filteringArgs}): AffiliationConnection
  
  """ Affilitations count """
  affiliationsCount: Int

  ${ObjectDefaultProperties}
}

${generateConnectionForType("ActorInterface")}

enum ActorEnum {
  Person
  Organisation
}

extend type Query{
  """ Search for actors """
  actors(${connectionArgs}, ${filteringArgs}, filterOn: ActorEnum): ActorInterfaceConnection
  
  """ Get actor """
  actor(id:ID! type:ActorEnum): ActorInterface
}
`;


export let ActorResolvers = {
  ActorInterface: {
    /**
     * @param obj
     * @return {*}
     * @private
     */
    __resolveType: (obj) => {
      return obj.getType();
    }
  },
  ...generateConnectionResolverFor("ActorInterface"),
  Query: {
    actors: async (parent, args, synaptixSession) => {
      return getObjectsResolver(
        args && args.filterOn === "Organisation" ? OrganisationDefinition : PersonDefinition,
        parent, args, synaptixSession
      );
    },
    actor: async (parent, {id, type}, synaptixSession) => {
      return getObjectResolver(type === "Organisation" ? OrganisationDefinition : PersonDefinition, parent, {id}, synaptixSession)
    }
  }
};