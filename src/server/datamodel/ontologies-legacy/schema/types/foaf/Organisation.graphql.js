/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  generateTypeResolvers,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import OrganisationDefinition from "../../../definitions/foaf/OrganisationDefinition";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let OrganisationType = `
type Organisation implements ObjectInterface & ActorInterface {
  """ Display name """
  displayName: String

  """ Name """
  name: String

  """ Short name """
  shortName: String

  """ Description """
  description: String
  
  
  """ Short description """
  shortDescription: String

  """ Avatar URI """
  avatar: String

  """ Main email """
  mainEmail: EmailAccount

  """ Secondary emails """
  emails(${connectionArgs}, ${filteringArgs}): EmailAccountConnection

  """ Phones """
  phones(${connectionArgs}, ${filteringArgs}): PhoneConnection

  """ Addresses """
  addresses(${connectionArgs}, ${filteringArgs}): LocalityConnection

  """ External links """
  externalLinks(${connectionArgs}, ${filteringArgs}): ExternalLinkConnection
  
  """ Affilitations """
  affiliations(${connectionArgs}, ${filteringArgs}): AffiliationConnection
  
  """ Affilitations count """
  affiliationsCount: Int

  ${ObjectDefaultProperties}
}

input OrganisationInput {
  id: ID @formInput(type:"hidden")
  
  """ Avatar URI """
  avatar: String @formInput(type:"image")
  
  """ Name """
  name: String

  """ Short name """
  shortName: String

  """ Description """
  description: String
  
  """ Short description """
  shortDescription: String @formInput(type:"textarea")
}

${generateConnectionForType("Organisation")}

extend type Query{
  """ Get organisation """
  organisation(id:ID): Organisation
  
  """ Search for organisations """
  organisations(${connectionArgs}, ${filteringArgs}): OrganisationConnection
}

`;

export let OrganisationResolvers = {
  Organisation: {
    ...generateTypeResolvers("Organisation"),
    displayName: getLocalizedLabelResolver.bind(this, OrganisationDefinition.getLabel('name')),
    name: getLocalizedLabelResolver.bind(this, OrganisationDefinition.getLabel('name')),
    shortName: getLocalizedLabelResolver.bind(this, OrganisationDefinition.getLabel('shortName')),
    avatar: (object) => object.avatar,
    description: getLocalizedLabelResolver.bind(this, OrganisationDefinition.getLabel('description')),
    shortDescription: getLocalizedLabelResolver.bind(this, OrganisationDefinition.getLabel('shortDescription')),
    emails: getLinkedObjectsResolver.bind(this, OrganisationDefinition.getLink('emails')),
    phones: getLinkedObjectsResolver.bind(this, OrganisationDefinition.getLink('phones')),
    addresses: getLinkedObjectsResolver.bind(this, OrganisationDefinition.getLink('addresses')),
    externalLinks: getLinkedObjectsResolver.bind(this, OrganisationDefinition.getLink('externalLinks')),
    affiliations: getLinkedObjectsResolver.bind(this, OrganisationDefinition.getLink('affiliations')),
    affiliationsCount: getLinkedObjectsCountResolver.bind(this, OrganisationDefinition.getLink('affiliations')),
  },
  Query: {
    organisation: async (root, {me, ...args}, synaptixSession) => getObjectResolver(OrganisationDefinition, root, args, synaptixSession),
    organisations: async (parent, args, synaptixSession) => getObjectsResolver(OrganisationDefinition, parent, args, synaptixSession),

  },
  ...generateConnectionResolverFor("Organisation")
};
