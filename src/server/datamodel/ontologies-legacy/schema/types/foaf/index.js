/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {mergeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {ActorInterface, ActorResolvers} from "./ActorInterface.graphql";
import {PersonResolvers, PersonType} from "./Person.graphql";
import {OrganisationResolvers, OrganisationType} from "./Organisation.graphql";
import {ObjectInterface, ObjectResolvers} from "../ObjectInterface.graphql";
import {LocalizedLabelType} from "../common/LocalizedLabel.graphql";
import {ConnectionTypes} from '../../../../toolkit/graphql/schema/types/ConnectionDefinitions.graphql';
import {NodeInterface, NodeResolvers} from "../common/NodeInterface.graphql";
import {PhoneResolvers, PhoneType} from "./Phone.graphql";
import {EmailAccountResolvers, EmailAccountType} from "./EmailAccount.graphql";
import {AffiliationResolvers, AffiliationType} from "./Affiliation.graphql";
import {GeoNamesResolvers, GeoNamesTypes} from "../geonames/index";
import {ExternalLinkResolvers, ExternalLinkType} from "../common/ExternalLink.graphql";
import {GeonamesMutations, GeonamesMutationsResolvers} from "../../mutations/geonames/index";

export let FOAFTypes = [
  NodeInterface,
  ObjectInterface,
  ConnectionTypes,
  ActorInterface,
  PersonType,
  OrganisationType,
  PhoneType,
  EmailAccountType,
  AffiliationType,
  LocalizedLabelType,
  ExternalLinkType,
  ...GeoNamesTypes,
  ...GeonamesMutations
];


export let FOAFResolvers = mergeResolvers(
  NodeResolvers,
  ObjectResolvers,
  ActorResolvers,
  PersonResolvers,
  OrganisationResolvers,
  PhoneResolvers,
  EmailAccountResolvers,
  AffiliationResolvers,
  ExternalLinkResolvers,
  GeoNamesResolvers,
  GeonamesMutationsResolvers,
);