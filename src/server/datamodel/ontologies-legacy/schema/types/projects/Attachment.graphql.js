/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getObjectResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import AttachmentDefinition from "../../../definitions/projects/AttachmentDefinition";
import ResourceDefinition from "../../../definitions/resources/ResourceDefinition";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let AttachmentType = `
""" Reification object to link a resource with an object """
type Attachment implements ObjectInterface {
  """ Creator """
  creator: Person
  
  """ Resource """
  resource: Resource
  
  """ Event """
  event: Event
  
  ${ObjectDefaultProperties}
}

input AttachmentInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
}

${generateConnectionForType("Attachment")}

extend type Query{
  """ Get attachment """
  attachment(id:ID): Attachment
}

extend type Resource{
  """ Affilitations """
  attachments(${connectionArgs}, ${filteringArgs}): AttachmentConnection
  
  """ Affilitations count """
  attachmentsCount: Int
}
`;


export let AttachmentResolvers = {
  Attachment: {
    creator: getLinkedObjectResolver.bind(this, AttachmentDefinition.getLink('creator')),
    resource: getLinkedObjectResolver.bind(this, AttachmentDefinition.getLink('resource')),
    event: getLinkedObjectResolver.bind(this, AttachmentDefinition.getLink('event')),
    ...generateTypeResolvers("Attachment"),
  },
  Query: {
    attachment: getObjectResolver.bind(this, AttachmentDefinition)
  },
  Resource: {
    attachments: getLinkedObjectsResolver.bind(this, ResourceDefinition.getLink('attachments')),
    attachmentsCount: getLinkedObjectsCountResolver.bind(this, ResourceDefinition.getLink('attachments')),
  },
  ...generateConnectionResolverFor("Attachment")
};