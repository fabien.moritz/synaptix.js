/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

import ProjectDefinition from "../../../definitions/projects/ProjectDefinition";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";

export let ProjectType = `
""" A project """
type Project implements ObjectInterface {
  """ Title for current language """
  title: String

  """ Description of the project """
  description: String

  """ Short description"""
  shortDescription: String
  
  """ Color """
  color: String
  
  """ Representative image URI """
  image: String

  """ Creator """
  creator: Person
  
  """ Parent project """
  parentProject: Project
    
  """ Sub projects """
  subProjects(${connectionArgs}, ${filteringArgs}): ProjectConnection
  
   """ Sub projects count """
  subProjectsCount: Int
  
  """ Get events """
  events(${connectionArgs}, ${filteringArgs}): EventConnection
  
  """ Get events count """
  eventsCount: Int
  
  """ Oldest events start date """
  startDate: Float
  
  """ Newest events end date """
  endDate: Float
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Project")}

input ProjectInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  """ Avatar """
  image: String @formInput(type:"image")
  
  """ Title"""
  title: String
  
  """ Description"""
  description: String

  """ Short description"""
  shortDescription: String
    
  """ Color """
  color: String @formInput(type:"color")
}

extend type Query{
  """ Search for projects """
  projects(${connectionArgs}, ${filteringArgs}): ProjectConnection
  
  """ Get a project """
  project(id: ID!): Project
}

#extend type Group{
#  """ Get a related project """
#  parentProject: Project
#}
`;

export let ProjectResolvers = {
  Project: {
    ...generateTypeResolvers("Project"),
    title: getLocalizedLabelResolver.bind(this, ProjectDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, ProjectDefinition.getLabel('description')),
    shortDescription: getLocalizedLabelResolver.bind(this, ProjectDefinition.getLabel('shortDescription')),
    color: (object) => object.color,
    image: (object) => object.image,
    creator: getLinkedObjectResolver.bind(this, ProjectDefinition.getLink('creator')),
    parentProject: getLinkedObjectResolver.bind(this, ProjectDefinition.getLink('parentProject')),
    subProjects: getLinkedObjectsResolver.bind(this, ProjectDefinition.getLink('subProjects')),
    subProjectsCount: getLinkedObjectsCountResolver.bind(this, ProjectDefinition.getLink('subProjects')),
    events: getLinkedObjectsResolver.bind(this, ProjectDefinition.getLink('events')),
    eventsCount: getLinkedObjectsCountResolver.bind(this, ProjectDefinition.getLink('events')),
    startDate: async (object, args, synaptixSession) => {
      let events = await synaptixSession.getLinkedObjectsFor(object, ProjectDefinition.getLink('events'), {
        first: 1,
        sortBy: 'startDate',
        sortDirection: 'asc'
      });

      if (events.length > 0) {
        return events[0].startDate;
      }
    },
    endDate: async (object, args, synaptixSession) => {
      let events = await synaptixSession.getLinkedObjectsFor(object, ProjectDefinition.getLink('events'), {
        first: 1,
        sortings: [{
          sortBy: 'startDate',
          sortDirection: 'desc'
        }, {
          sortBy: 'endDate',
          sortDirection: 'desc'
        }]
      });

      if (events.length > 0) {
        return events[0].startDate;
      }
    }
  },
  Query: {
    projects: async (parent, args, synaptixSession) => getObjectsResolver(ProjectDefinition, parent, args, synaptixSession),
    project: getObjectResolver.bind(this, ProjectDefinition)
  },
  ...generateConnectionResolverFor("Project")
};