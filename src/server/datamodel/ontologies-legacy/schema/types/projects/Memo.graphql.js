/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";

import MemoDefinition from "../../../definitions/projects/MemoDefinition";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor,
} from "../../../../toolkit/graphql/schema/types/generators";

export let MemoType = `
""" A memo """
type Memo implements ObjectInterface {
  """ The ID """
  id: ID! 

  """ Raw data """
  rawData: String

  """ Content """
  content: String
  
  """ Main memo ? """
  isMain: Boolean
  
  """ Related event """
  event: Event
  
  """ Creator """
  creator: Person
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Memo")}

input MemoInput {
  """ The ID """
  id: ID @formInput(type:"hidden")
  
    """ Raw data """
  rawData: String

  """ Content """
  content: String
  
  """ Main memo ? """
  isMain: Boolean
}

extend type Query{
  """ Search for memos """
  memos(${connectionArgs}, ${filteringArgs}): MemoConnection

  """ Get a memo """
  memo(id: ID!): Memo
}
`;

export let MemoResolvers = {
  Memo: {
    ...generateTypeResolvers("Memo"),
    content: getLocalizedLabelResolver.bind(this, MemoDefinition.getLabel('content')),
    rawData: (object) => object.rawData,
    creator: getLinkedObjectResolver.bind(this, MemoDefinition.getLink('creator')),
    event: getLinkedObjectResolver.bind(this, MemoDefinition.getLink('event')),
  },
  Query: {
    memos: async (parent, args, synaptixSession) => getObjectsResolver(MemoDefinition, parent, args, synaptixSession),
    memo: getObjectResolver.bind(this, MemoDefinition)
  },
  ...generateConnectionResolverFor("Memo")
};