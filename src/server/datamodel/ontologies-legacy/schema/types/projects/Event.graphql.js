/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {
  generateTypeResolvers,
  getLinkedObjectResolver,
  getLinkedObjectsCountResolver,
  getLinkedObjectsResolver,
  getLocalizedLabelResolver,
  getObjectResolver,
  getObjectsResolver
} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {ObjectDefaultProperties} from "../ObjectInterface.graphql";
import {
  connectionArgs,
  filteringArgs,
  generateConnectionForType,
  generateConnectionResolverFor
} from "../../../../toolkit/graphql/schema/types/generators";
import EventDefinition from "../../../definitions/projects/EventDefinition";

export let EventType = `
""" An eve,t """
type Event implements ObjectInterface {
  """ Title for current language """
  title: String

  """ Description """
  description: String

  """ Start date """
  startDate: Float
  
  """ End date """
  endDate: Float
  
  """ Short description"""
  shortDescription: String
  
  """ Creator """
  creator: Person
  
  """ Project """
  project: Project
  
  """ Involvements """
  involvements(${connectionArgs}, ${filteringArgs}): InvolvementConnection
  
  """ Involvements count"""
  involvementsCount: Int
  
  """ Attachments """
  attachments(${connectionArgs}, ${filteringArgs}): AttachmentConnection
  
  """ Attachments count """
  attachmentsCount: Int 
  
  """ Memos """
  memos(${connectionArgs}, ${filteringArgs}): MemoConnection
  
  """ Memos count """
  memosCount: Int 
  
  ${ObjectDefaultProperties}
}

${generateConnectionForType("Event")}

input EventInput {
  """ The ID """
  id: ID @formInput(type:"hidden")

  """ Start date """
  startDate: Float @formInput(type:"date")
  
  """ End date """
  endDate: Float @formInput(type:"date")
  
  """ Title"""
  title: String
  
  """ Description"""
  description: String

  """ Short description"""
  shortDescription: String
}

extend type Query{
  """ Search for events """
  events(${connectionArgs}, ${filteringArgs}): EventConnection

  """ Get an event """
  event(id: ID!): Event
}
`;

export let EventResolvers = {
  Event: {
    ...generateTypeResolvers("Event"),
    title: getLocalizedLabelResolver.bind(this, EventDefinition.getLabel('title')),
    description: getLocalizedLabelResolver.bind(this, EventDefinition.getLabel('description')),
    shortDescription: getLocalizedLabelResolver.bind(this, EventDefinition.getLabel('shortDescription')),
    startDate: (object) => object.startDate,
    endDate: (object) => object.endDate,
    creator: getLinkedObjectResolver.bind(this, EventDefinition.getLink('creator')),
    project: getLinkedObjectResolver.bind(this, EventDefinition.getLink('project')),
    involvements: getLinkedObjectsResolver.bind(this, EventDefinition.getLink('involvements')),
    involvementsCount: getLinkedObjectsCountResolver.bind(this, EventDefinition.getLink('involvements')),
    attachments: getLinkedObjectsResolver.bind(this, EventDefinition.getLink('attachments')),
    attachmentsCount: getLinkedObjectsCountResolver.bind(this, EventDefinition.getLink('attachments')),
    memos: getLinkedObjectsResolver.bind(this, EventDefinition.getLink('memos')),
    memosCount: getLinkedObjectsCountResolver.bind(this, EventDefinition.getLink('memos'))
  },
  Query: {
    events: async (parent, args, synaptixSession) => getObjectsResolver(EventDefinition, parent, args, synaptixSession),
    event: getObjectResolver.bind(this, EventDefinition)
  },
  ...generateConnectionResolverFor("Event")
};