/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

/**
 * This is the default properties shared between every GraphQL type.
 *
 * @type {string}
 */
export let ObjectDefaultProperties = `
  """ Identifier """
  id: ID! 
   
  """ URI """
  uri: String
  
  """ Creation date """
  creationDate: Float

  """ Last update date """
  lastUpdate: Float
  
  """ Is object disabled """
  isDisabled: Boolean
`;

/**
 * This is the default input shared between every GraphQL type.
 *
 * @type {string}
 */
export let ObjectDefaultInput = `
  """ The ID """
  id: ID @formInput(type:"hidden")
`;

/**
 * This is the object interface implemented by every GraphQL type.
 *
 * @type {string}
 */
export let ObjectInterface = `
""" An interface for all objects """
interface ObjectInterface {
  ${ObjectDefaultProperties}
}
`;

/**
 * This is resolver map linked to the interface {ObjectInterface}
 *
 * @type {object}
 */
export let ObjectResolvers = {
  ObjectInterface: {
    __resolveType(obj) {
      return obj.constructor.name;
    },
    uri: (object) => object.uri,
    creationDate: (object) => object.creationDate,
    lastUpdate: (object) => object.lastUpdate,
    isDisabled: (object) => object._enabled === false
  }
};
