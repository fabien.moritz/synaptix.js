/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {mergeResolvers} from "../../../../toolkit/graphql/helpers/resolverHelpers";
import {ObjectInterface, ObjectResolvers} from "../ObjectInterface.graphql";
import {LocalizedLabelResolvers, LocalizedLabelType} from "../common/LocalizedLabel.graphql";
import {ConnectionTypes} from '../../../../toolkit/graphql/schema/types/ConnectionDefinitions.graphql';
import {NodeInterface, NodeResolvers} from "../common/NodeInterface.graphql";
import {LocalityResolvers, LocalityType} from "./Locality.graphql";

export let GeoNamesTypes = [
  NodeInterface,
  ObjectInterface,
  ConnectionTypes,
  LocalityType,
  LocalizedLabelType
];


export let GeoNamesResolvers = mergeResolvers(
  NodeResolvers,
  ObjectResolvers,
  LocalityResolvers,
  LocalizedLabelResolvers,
);