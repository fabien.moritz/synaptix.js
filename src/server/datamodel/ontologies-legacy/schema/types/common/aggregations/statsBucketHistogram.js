import {GraphQLList, GraphQLObjectType} from 'graphql';

import {statsBucketType} from './statsBucket';

let statsBucketHistogramType = new GraphQLObjectType({
  name: 'AggregationBucketWithStatsList',
  fields: () => ({
    buckets: {
      type: new GraphQLList(statsBucketType),
      description: 'Count',
      resolve: (histogram) => histogram.buckets
    }
  })
});

export {statsBucketHistogramType};