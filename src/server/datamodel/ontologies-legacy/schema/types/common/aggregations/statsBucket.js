import {GraphQLFloat, GraphQLID, GraphQLObjectType, GraphQLString} from 'graphql';

import {statsType} from './stats';

let statsBucketType = new GraphQLObjectType({
  name: 'AggregationBucketWithStats',
  fields: () => ({
    id: {
      type: GraphQLID,
      description: 'Min',
      resolve: (bucket) => bucket.id || bucket.key
    },
    label: {
      type: GraphQLString,
      description: 'Max',
      resolve: (bucket) => bucket.key
    },
    count: {
      type: GraphQLFloat,
      description: 'Count',
      resolve: (bucket) => bucket.stats.doc_count
    },
    stats: {
      type: statsType,
      description: 'Count',
      resolve: (bucket) => bucket.stats.stat
    }
  })
});

export {statsBucketType};