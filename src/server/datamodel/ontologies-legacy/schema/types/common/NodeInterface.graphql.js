/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */


/**
 * This is an interface needed by Relay
 *
 * @see https://facebook.github.io/relay/graphql/objectidentification.htm
 *
 * @type {string}
 */
export let NodeInterface = `
""" An object with an ID """
interface Node {
  """ The id of the object. """
  id: ID!
}

extend type Query {
  """Fetches an object given its global ID"""
  node(id: ID!): Node
}
`;

export let NodeResolvers = {
  Node: {
    __resolveType(obj) {
      return obj.constructor.name;
    }
  },
  Query: {
    /**
     * @param {object} parent
     * @param {string} globalId
     * @param {SynaptixDatastoreSession} synaptixSession
     * @return {Model}
     */
    node: async (parent, {id: globalId}, synaptixSession) => {
      let {id, type} = synaptixSession.parseGlobalId(globalId);

      return synaptixSession.getObject(
        synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type),
        id,
        synaptixSession.getContext().getLang()
      );
    }
  }
};