/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export class Step{}

export class LinkStep extends Step{
  /**
   * @param {LinkDefinition} linkDefinition
   * @param {string} [targetId]
   * @param {boolean} [reversed]
   * @param {boolean} [typeAssertionDiscarded] - Disable the assertion of link related target type.
   */
  constructor({linkDefinition, reversed, targetId, typeAssertionDiscarded}){
    super();
    this._linkDefinition = linkDefinition;
    this._reversed = reversed;
    this._targetId = targetId;
    this._typeAssertionDiscarded = typeAssertionDiscarded;
  }

  getLinkDefinition() {
    return this._linkDefinition;
  }

  isReversed() {
    return this._reversed;
  }

  getTargetId() {
    return this._targetId;
  }

  isTypeAssertionDiscarded(){
    return this._typeAssertionDiscarded;
  }
}

export class PropertyStep extends Step{
  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {string} rdfDataPropertyAlias
   */
  constructor({propertyDefinition, rdfDataPropertyAlias}){
    super();
    this._propertyDefinition = propertyDefinition;
    this._rdfDataPropertyAlias = rdfDataPropertyAlias || propertyDefinition.getRdfDataProperty();
  }

  getPropertyDefinition() {
    return this._propertyDefinition;
  }

  getRdfDataPropertyAlias() {
    return this._rdfDataPropertyAlias;
  }
}

export class PropertyFilterStep extends Step{
  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {*} value
   */
  constructor({propertyDefinition, value}){
    super();
    this._propertyDefinition = propertyDefinition;
    this._value = value;
  }

  /**
   * @return {PropertyDefinitionAbstract}
   */
  getPropertyDefinition() {
    return this._propertyDefinition;
  }

  getValue() {
    return this._value;
  }

  /**
   * @return {boolean}
   */
  isLiteral(){
    return !!this._literalDefiniton;
  }
}

export class LinkPath{
  _steps = [];

  /**
   * @return {Step[]}
   */
  getSteps(){
    return this._steps;
  }

  /**
   * @param {LinkDefinition} linkDefinition
   * @param {string} [targetId]
   * @param {boolean} [reversed=false]
   * @param {boolean} [typeAssertionDiscarded] - Disable the assertion of link related target type.
   */
  step({linkDefinition, targetId, reversed, typeAssertionDiscarded}){
    this._steps.push(new LinkStep({linkDefinition, reversed, targetId, typeAssertionDiscarded}));
    return this;
  }

  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {string} rdfDataPropertyAlias
   */
  property({propertyDefinition, rdfDataPropertyAlias}){
    this._steps.push(new PropertyStep({propertyDefinition, rdfDataPropertyAlias}));
    return this;
  }

  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {*} value
   * @param {string} [lang]
   */
  filterOnProperty({propertyDefinition, value, lang}){
    this._steps.push(new PropertyFilterStep({propertyDefinition, value, lang}));
    return this;
  }

  /**
   * Reverse the path
   */
  reverse(){
    this._steps = this._steps.reverse();
    return this;
  }

  /**
   * @return {Step}
   */
  getLastStep(){
    return this._steps?.[this._steps.length - 1];
  }

  /**
   * @return {LinkStep}
   */
  getLastLinkStep(){
    return this._steps.reverse().find(step => step instanceof LinkStep);
  }

  /**
   * @return {PropertyStep}
   */
  getLastPropertyStep(){
    return this._steps.reverse().find(step => step instanceof PropertyStep);
  }
}