/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import LocalizedLabelDefinition from "../../ontologies-legacy/definitions/common/LocalizedLabelDefinition";
import HTMLContentDefinition from "../../ontologies-legacy/definitions/common/HTMLContentDefinition";
import {PropertyDefinitionAbstract} from "./PropertyDefinitionAbstract";

export default class LabelDefinition extends PropertyDefinitionAbstract{
  /**
   * @param {string} labelName
   * @param {string} [pathInGraphstore]
   * @param {string} [pathInIndex]
   * @param {string} [rdfDataProperty]
   * @param {bool} [isHTML]
   * @param {typeof ModelDefinitionAbstract} [relatedNodeDefinition]
   * @param {boolean} [isSearchable=false]
   * @param {string} [inputFormOptions] - Input form options to use in form directive. @see {FormInputDirectiveType} definition
   * @param {LinkPath} [linkPath]
   * @param {boolean} [isRequired] Is label required
   */
  constructor({labelName, pathInGraphstore, rdfDataProperty, pathInIndex, isHTML, relatedNodeDefinition, isSearchable, inputFormOptions, isRequired, linkPath}) {
    super({propertyName: labelName, pathInGraphstore, rdfDataProperty, pathInIndex, isHTML, relatedNodeDefinition, isSearchable, inputFormOptions, isRequired, linkPath});

    this._pathInGraphstore = pathInGraphstore;
    this._relatedNodeDefinition = relatedNodeDefinition || (isHTML ? HTMLContentDefinition : LocalizedLabelDefinition);
  }

  /**
   * Get label name
   */
  getLabelName() {
    return this.getPropertyName();
  }

  getNodeType() {
    return this._relatedNodeDefinition.getNodeType();
  }

  getModelClass() {
    return this._relatedNodeDefinition.getModelClass();
  }

  /**
   * Convert this literal into a SPARQL value.
   * @return {string}
   */
  toSparqlValue(value) {
    return `"${value}"`;
  }

  /**
   * Convert this literal into a SPARQL value.
   * @return {string}
   */
  toSparqlLocalizedValue(value, lang) {
    return `"${value}"@${lang}`;
  }
}
