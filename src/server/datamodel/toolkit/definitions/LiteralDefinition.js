/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import {PropertyDefinitionAbstract} from "./PropertyDefinitionAbstract";

/**
 */
export default class LiteralDefinition extends PropertyDefinitionAbstract{
  /**
   * @param {string} literalName
   * @param {string} [pathInIndex]
   * @param {string} [rdfDataProperty]
   * @param {string} [rdfLiteralType]
   * @param {boolean} [isSearchable=false]
   * @param {string} [inputFormOptions] Input form options to use in form directive. @see {FormInputDirectiveType} definition
   * @param {boolean} [excludeFromInput] Exclude this literal from input generation
   * @param {boolean} [isRequired] Is literal required
   * @param {LinkPath} [linkPath]
   */
  constructor({literalName, pathInIndex, rdfDataProperty, rdfDataType, isSearchable, inputFormOptions, excludeFromInput, isRequired, linkPath}) {
    super({propertyName: literalName, pathInIndex, rdfDataProperty, rdfDataType, isSearchable, inputFormOptions, excludeFromInput, isRequired, linkPath});

    // Set the datatype as "rdfs:Literal" y default.
    this._rdfDataType = rdfDataType || "rdfs:Literal";
  }

  /**
   * Get literal name
   */
  getLiteralName() {
    return this.getPropertyName();
  }

  /**
   * Convert this literal into a SPARQL variable.
   * @return {string}
   */
  toSparqlValue(value) {
    let type;

    if (this._rdfDataType === "rdfs:Literal") {
      type = '';
    } else {
      type = `^^${this._rdfDataType}`;
    }

    return `"${value}"${type}`;
  }
}