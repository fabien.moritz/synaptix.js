/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import DefaultIndexMatcher from '../matchers/DefaultIndexMatcher';

import IndexControllerService from '../../../datamodules/services/index/IndexControllerService';

/**
 * Get model definition link
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param linkName
 * @returns {LinkDefinition}
 */
export function getModelDefinitionLink(modelDefinition, linkName) {
  return modelDefinition.getLinks().find((linkDefinition) => linkName === linkDefinition.getLinkName());
}

/**
 * Get model definition link groovy directive.
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param linkName
 * @returns {string} Groovy directive
 */
export function getModelDefinitionLinkGraphstoreDirective(modelDefinition, linkName) {
  let linkDefinition = getModelDefinitionLink(modelDefinition, linkName);

  if (linkDefinition) {
    return linkDefinition.getPathInGraphstore();
  }
}

/**
 * Get model definition label edge definition.
 * @param {LabelDefinition} labelDefinition
 * @param justEdgeName
 * @returns {string, object}
 */
export function getLabelDefinitionEdge(labelDefinition, justEdgeName = true) {
  let groovy = labelDefinition.getPathInGraphstore();

  if (groovy) {
    return getLinkDefinitionFromGroovy(groovy, justEdgeName);
  }
}

/**
 * @param {string} groovy
 * @param {boolean} justEdgeName
 * @param {boolean} isReversed
 * @return {*}
 */
export function getLinkDefinitionFromGroovy(groovy, justEdgeName = false, isReversed = false) {
  let match = groovy.match(/(out|in|both)\('([\w_]+)'\)/);

  if (match && Object.keys(match).length > 2) {
    return justEdgeName ? match[2] : {
      direction: match[1] === "both" ? "out" : isReversed ? (match[1] === "out" ? "in" : "out") : match[1],
      edgeName: match[2].toLowerCase(),
    }
  }
}

/**
 * Return matcher pour object
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {IndexControllerService} indexService
 * @return {DefaultIndexMatcher}
 */
export function getModelDefinitionIndexMatcher(modelDefinition, indexService) {
  let IndexMatcher = modelDefinition.getIndexMatcher();

  if (IndexMatcher) {
    return new IndexMatcher({
      modelDefinition,
      indexService
    });
  }
}

/**
 * Return all links that have to be cascading removed
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @return {LinkDefinition[]}
 */
export function getModelDefinitionCascadingRemoveLinks(modelDefinition) {
  return modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingRemoved());
}

/**
 * Return all links that have to be cascading updated
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @return {LinkDefinition[]}
 */
export function getModelDefinitionCascadingUpdateLinks(modelDefinition) {
  return modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingUpdated());
}

/**
 * Reconstruct the inheritance hierarchy to gather all model definitions
 *
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @return {typeof ModelDefinitionAbstract[]}
 */
export function gatherModelDefinitionHierarchy(modelDefinition) {
  let hierarchyDefinitions = [];

  for (let parentDefition of modelDefinition.getParentDefinitions()) {
    hierarchyDefinitions = [].concat(hierarchyDefinitions, [parentDefition], gatherModelDefinitionHierarchy(parentDefition))
  }

  return hierarchyDefinitions;
}

/**
 * Type syntax converter from RDF to GraphQL
 *
 */
export const GRAPHQL_RDFTYPE_MAPPING = {
  'rdfs:Literal': 'String',
  'http://www.w3.org/2000/01/rdf-schema#Literal': 'String',
  'http://www.w3.org/2001/XMLSchema#string': 'String',
  'http://www.w3.org/2001/XMLSchema#anyURI': 'String',
  'http://www.w3.org/2001/XMLSchema#int': 'Int',
  'http://www.w3.org/2001/XMLSchema#integer': 'Int',
  'http://www.w3.org/2001/XMLSchema#float': 'Float',
  'http://www.w3.org/2001/XMLSchema#boolean': 'Boolean',
  'http://www.w3.org/2001/XMLSchema#dateTime': 'Float',
  'http://www.w3.org/2001/XMLSchema#dateTimeStamp': 'Float'
};

/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 */
export const generateGraphQLFiltersDocumentation = function(modelDefinition){
  let documentation = `
### Filters
    `;

  if(modelDefinition.getLiterals().length > 0){
    documentation += `
#### Literal filters
Literal filters accepts following filters keys :`;
    for(let literal of modelDefinition.getLiterals()){
      documentation += `
- ${literal.getLiteralName()}`;
    }
  }

  if(modelDefinition.getLabels().length > 0) {
    documentation += `
#### Label filters  
Label filters accepts following filters keys :`;
    for (let label of modelDefinition.getLabels()) {
      documentation += `
- ${label.getLabelName()}`;
    }
  }

  if(modelDefinition.getLinks().length > 0) {
    documentation += `
#### Link filters  
Link filters accepts following filters keys :`;
    for (let link of modelDefinition.getLinks()) {
      documentation += `
- ${link.getLinkName()}`;
    }
  }

  return documentation;
};