/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * A ModelDefinitionFragmentDefinition is a portion of ModelDefinitionAbstract.
 */
export class FragmentDefinition {
  /**
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {PropertyDefinitionAbstract[]} properties
   */
  constructor({modelDefinition, properties}) {
    this._modelDefinition = modelDefinition;
    this._properties = properties || [];
  }

  /**
   * @return {ModelDefinitionAbstract}
   */
  getModelDefinition(){
    return this._modelDefinition;
  }

  /**
   * @return {PropertyDefinitionAbstract[]}
   */
  getProperties() {
    return this._properties;
  }
}