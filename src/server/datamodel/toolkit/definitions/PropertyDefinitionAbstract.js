/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {PropertyStep} from "../../../datamodel/toolkit/utils/LinkPath";

export class PropertyDefinitionAbstract {
  /**
   * @param {string} propertyName
   * @param {string} [pathInIndex]
   * @param {string} [rdfDataProperty]
   * @param {string} [rdfLiteralType]
   * @param {LinkPath} [linkPath]
   * @param {boolean} [isSearchable=false]
   * @param {string} [inputFormOptions] Input form options to use in form directive. @see {FormInputDirectiveType} definition
   * @param {boolean} [excludeFromInput] Exclude this literal from input generation
   * @param {boolean} [isRequired] Is literal required
   */
  constructor({propertyName, pathInIndex, pathInGraphstore, rdfDataProperty, linkPath, rdfDataType, isSearchable, inputFormOptions, excludeFromInput, isRequired}) {
    if (!propertyName){
      throw new TypeError("A PropertyDefinition instance must have a propertyName defined.")
    }

    if (linkPath && !(linkPath.getLastStep() instanceof PropertyStep)){
      throw new TypeError("Last step of the property definion linkPath must be a PropertyStep.");
    }

    this._propertyName    = propertyName;
    this._rdfDataProperty = rdfDataProperty;
    this._linkPath = linkPath;
    this._rdfDataType = rdfDataType || "rdfs:Literal";
    this._inputFormOptions = inputFormOptions;
    this._excludeFromInput = excludeFromInput;
    this._pathInGraphstore = pathInGraphstore;
    this._pathInIndex = pathInIndex;

    this._isRequired = isRequired;
    this._isSearchable = isSearchable;
    this._isHerited = false;
  }

  /**
   * Returns the propery name
   * @return {string}
   */
  getPropertyName(){
    return this._propertyName;
  }

  /**
   * Returns the path to retrieve literal in index
   * @return {string|UseIndexMatcherOfDefinition|ChildOfIndexDefinition}
   */
  getPathInIndex() {
    return this._pathInIndex || this._propertyName;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   */
  getPathInGraphstore() {
    return this._pathInGraphstore;
  }

  /**
   * Returns the RDF data property predicate used to retrieve literal or the link path alias.
   * @return {string}
   */
  getRdfDataProperty() {
    return this._rdfDataProperty;
  }

  /**
   * Returns the RDF data property predicate alias used to retrieve literal.
   * @return {string}
   */
  getRdfAliasDataProperty(){
    if (this._linkPath){
      return this._linkPath.getLastPropertyStep().getRdfDataPropertyAlias()
    }
  }

  /**
   * Returns the RDF datatype.
   * @return {*}
   */
  getRdfDataType() {
    return this._rdfDataType;
  }

  /**
   * Returns the link path to access the data
   * @return {LinkPath}
   */
  getLinkPath() {
    return this._linkPath;
  }

  /**
   * Set literal as herited from parent definition
   */
  setIsHerited(){
    this._isHerited = true;
  }

  /**
   * Is literal herited from parent definition
   */
  isHerited(){
    return this._isHerited;
  }

  /**
   * Is this property excluded from input generation ?
   */
  isExcludedFromInput(){
    return this._excludeFromInput;
  }

  /**
   * Return input form options to use in form directive.
   * @see {FormInputDirectiveType} definition
   * @return {string}
   */
  getInputFormOptions() {
    return this._inputFormOptions;
  }

  /**
   * Is this literal flagged as searchable ?
   * @return {boolean}
   */
  isSearchable() {
    return this._isSearchable;
  }

  /**
   * Is literal required
   */
  isRequired(){
    return this._isRequired;
  }
  
  /**
   * Convert this literal into a SPARQL variable.
   * @param  {string} [suffix]
   * @return {string}
   */
  toSparqlVariable({suffix} = {}) {
    if (this._linkPath){
      return this._linkPath.getLastPropertyStep().getPropertyDefinition().toSparqlVariable()
    }

    return `?${this._propertyName}${suffix || ""}`;
  }

  /**
   * Convert this literal into a SPARQL variable.
   * @return {string}
   */
  toSparqlValue(value) {
    return `"${value}"`;
  }
}