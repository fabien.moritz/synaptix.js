/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * account: {
      paths: {
        inIndex: 'userAccount',
        inGraphStore: `out('HAS_ACCOUNT')`
      },
      ModelClass: Models.UserAccount,
      plural: false,
      nodeType: "UserAccount"
    },
 */
export default class LinkDefinition {

  /**
   * @param {string} linkName
   * @param {typeof ModelDefinitionAbstract} relatedModelDefinition
   * @param {string} [pathInGraphstore}
   * @param {string|UseIndexMatcherOfDefinition|ChildOfIndexDefinition} [pathInIndex]
   * @param {string} [rdfObjectProperty]
   * @param {string} [rdfReversedObjectProperty]
   * @param {boolean} [isPlural]
   * @param {boolean} [isCascadingRemoved]
   * @param {boolean} [isCascadingUpdated]
   * @param {string} [pathInIndexForCount]
   * @param {object} [rdfPrefixesMapping]
   * @param {boolean} [readOnly=false] - Use this property to forbit using this link in a node creation. Usefull for "interfaces" as AgentDefinition.
   * @param {string} [relatedInputName] - Use this property to map a GraphQL input property name to this link.
   */
  constructor({linkName, relatedModelDefinition, pathInGraphstore, pathInIndex, rdfObjectProperty, rdfReversedObjectProperty, rdfPrefixesMapping, isPlural, isCascadingRemoved, isCascadingUpdated, pathInIndexForCount, readOnly, relatedInputName}) {
    this._linkName = linkName;
    this._relatedModelDefinition = relatedModelDefinition;
    this._pathInGraphstore = pathInGraphstore;
    this._pathInIndex = pathInIndex;
    this._isPlural = isPlural;
    this._isCascadingUpdated = isCascadingUpdated;
    this._isCascadingRemoved = isCascadingRemoved;
    this._pathInIndexForCount = pathInIndexForCount;
    this._rdfObjectProperty = rdfObjectProperty;
    this._rdfReversedObjectProperty = rdfReversedObjectProperty;
    this._readOnly = readOnly;
    this._rdfPrefixesMapping = rdfPrefixesMapping;
    this._isHerited = false;
    this._relatedInputName = relatedInputName;
  }

  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  getRdfPrefixesMapping() {
    return this._rdfPrefixesMapping;
  }

  /**
   * Get link name
   */
  getLinkName() {
    if (!this._linkName) {
      throw new Error(`No linkName defined for ${this.constructor.name}`);
    }

    return this._linkName;
  }

  /**
   * Returns related model definition.
   * @returns typeof ModelDefinitionAbstract
   */
  getRelatedModelDefinition() {
    if (!this._relatedModelDefinition) {
      throw new Error(`No relatedModelDefinition defined for ${this.constructor.name}`);
    }

    return this._relatedModelDefinition;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   * @return {string}
   */
  getPathInGraphstore() {
    return this._pathInGraphstore;
  }

  /**
   * Returns the path to retrieve link in index
   * @return {string|UseIndexMatcherOfDefinition|ChildOfIndexDefinition}
   */
  getPathInIndex() {
    return this._pathInIndex;
  }

  /**
   * Returns the RDF object property predicate used to retrieve link.
   * @return {string}
   */
  getRdfObjectProperty() {
    return this._rdfObjectProperty;
  }

  /**
   * Returns the RDF object property predicate used to retrieve link.
   * @return {string}
   */
  getRdfReversedObjectProperty() {
    return this._rdfReversedObjectProperty;
  }

  /**
   * Returns the groovy code to retrieve link in graph store
   * @return {string}
   */
  getPathInIndexForCount() {
    return this._pathInIndexForCount;
  }

  /**
   * Is this link plural.
   * @returns {boolean}
   */
  isPlural() {
    return this._isPlural || false;
  }

  /**
   * Set literal as herited from parent definition
   */
  setIsHerited(){
    this._isHerited = true;
  }

  /**
   * Is literal herited from parent definition
   */
  isHerited(){
    return this._isHerited;
  }

  /**
   * Is the both connected nodes removed if one of them is.
   * @returns {boolean}
   */
  isCascadingRemoved() {
    return this._isCascadingRemoved || false;
  }

  /**
   * Is the both connected nodes updated if one of them is.
   * @returns {boolean}
   */
  isCascadingUpdated() {
    return this._isCascadingUpdated || false;
  }

  /**
   * Gives the GraphQL input property name tha is map to this link.
   *
   * Example :
   * type Person {
   *   hasUserAccount: UserAccount
   * }
   *
   * input PersonInput {
   *   userAccountInput: UserAccountInput
   * }
   *
   * "userAccountInput" may be the "relatedInputName" property of `Person ~> hasUserAccount ~> UserAccount` link definition.
   *
   * @return {string}
   */
  getRelatedInputName() {
    return this._relatedInputName;
  }

  /**
   * Generate link instance from target id
   * @param {string} targetId
   * @return {Link}
   */
  generateLinkFromTargetId(targetId, targetModelDefinition) {
    return new Link({
      linkDefinition: this,
      targetId,
      targetModelDefinition
    });
  }

  /**
   * Generate link instance from target id
   * @param {object} targetObjectInput - Target object input
   * @param {typeof ModelDefinitionAbstract} [targetModelDefinition]
   * @param {LinkDefinition[]} [targetNestedLinks]
   * @return {Link}
   */
  generateLinkFromTargetProps({targetObjectInput, targetModelDefinition, targetNestedLinks}) {
    return new Link({
      linkDefinition: this,
      targetObjectInput,
      targetModelDefinition,
      targetNestedLinks
    });
  }

  /**
   * Generte link deletion instance
   * @return {Link}
   */
  generateDeletionLink(){
    return new Link({
      linkDefinition: this,
      isDeletion: true
    })
  }

  /**
   * Is this link forbidded using this link in a node creation
   * @return {boolean}
   */
  isReadOnly() {
    return this._readOnly;
  }

  /**
   * Convert this link into a SPARQL variable.
   * @return {string}
   */
  toSparqlVariable() {
    return `?${this._linkName}`;
  }
}

export class Link {
  /**
   * @param {LinkDefinition} linkDefinition - The link definition
   * @param {string|null} [targetId] - The target node ID if it exists
   * @param {object|null} [targetObjectInput] - The target node input if it doesn't exists
   * @param {typeof ModelDefinitionAbstract} targetModelDefinition
   * @param {LinkDefinition[]} [targetNestedLinks]
   * @param {boolean} [isDeletion] - Tag this link as deletion. That informs the graph controller to remove related linkDefinition instances
   */
  constructor({linkDefinition, targetId, targetObjectInput, targetModelDefinition, targetNestedLinks, isDeletion}) {
    this._linkDefinition = linkDefinition;
    this._targetId = targetId;
    this._targetObjectInput = targetObjectInput;
    this._reversed = false;
    this._targetModelDefinition = targetModelDefinition || linkDefinition.getRelatedModelDefinition();
    this._targetNestedLinks = targetNestedLinks || [];
    this._isDeletion = isDeletion;
  }

  /**
   * @return {LinkDefinition}
   */
  getLinkDefinition() {
    return this._linkDefinition;
  }

  /**
   * @param {string} targetId
   */
  setTargetId(targetId) {
    this._targetId = targetId;
  }

  /**
   * @param {object} targetObjectInput
   */
  setTargetObjectInput(targetObjectInput) {
    this._targetObjectInput = targetObjectInput;
  }

  /**
   * @param {LinkDefinition[]} targetNestedLinks
   */
  setTargetNestedLinks(targetNestedLinks) {
    this._targetNestedLinks = targetNestedLinks;
  }

  /**
   * @return {string}
   */
  getTargetId() {
    return this._targetId;
  }

  /**
   * @return {object}
   */
  getTargetObjectInput() {
    return this._targetObjectInput;
  }

  /**
   * @return {typeof ModelDefinitionAbstract}
   */
  getTargetModelDefinition() {
    return this._targetModelDefinition;
  }

  /**
   * @return {LinkDefinition[]}
   */
  getTargetNestedLinks(){
    return this._targetNestedLinks;
  }

  isReversed() {
    return this._reversed;
  }

  /**
   * @return {boolean}
   */
  isDeletion() {
    return this._isDeletion;
  }

  /**
   * @deprecated Use `LinkDefinition::rdfReversedObjectProperty` property instead.
   *
   * @param {typeof ModelDefinitionAbstract} [targetModelDefinition]
   * @return {Link}
   */
  reverse({targetModelDefinition} = {}) {
    if (targetModelDefinition) {
      this._targetModelDefinition = targetModelDefinition;
    }

    this._reversed = true;

    return this;
  }
}

export class ChildOfIndexDefinition {
  constructor({parentIndex}) {
    this._parentIndex = parentIndex;
  }

  /**
   * @returns {typeof ModelDefinitionAbstract}
   */
  getParentIndex() {
    if (!this._parentIndex) {
      throw new Error(`No parentIndex defined for ${this.constructor.name}`);
    }

    return this._parentIndex;
  }
}

export class UseIndexMatcherOfDefinition {
  constructor({useIndexMatcherOf, filterName}) {
    this._filterName = filterName;
    this._useIndexMatcherOf = useIndexMatcherOf;
  }

  /**
   * @returns {typeof ModelDefinitionAbstract}
   */
  getUseIndexMatcherOf() {
    if (!this._useIndexMatcherOf) {
      throw new Error(`No useIndexMatcherOf defined for ${this.constructor.name} (filterName: ${this._filterName})`);
    }

    return this._useIndexMatcherOf;
  }

  getFilterName() {
    if (!this._filterName) {
      throw new Error(`No filterName defined for ${this.constructor.name}`);
    }

    return this._filterName;
  }
}
