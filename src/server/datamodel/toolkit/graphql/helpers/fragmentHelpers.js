/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {FragmentDefinition} from "../../definitions/FragmentDefinition";

/**
 * Introspect a GraphQL request resolver "info" parameter to find all extra properties contained in GraphQL Inline fragments.
 *
 * This method is usefull to query a GraphQL type defined by an interface.
 *
 * query {
 *   agents{      <= Refers to AgentInterface
 *     id
 *     displayName
 *     ...on Person{   <= Refers to Person type
 *       fullName
 *     }
 *     ...on Organization{  <= Refers to Orgnization type
 *       shortName
 *     }
 *   }
 * }
 *
 * This helper should returns an array of FragmentDefinition extra properties :
 *
 * [
 *    new FragmentDefinition({
 *      modelDefinition: PersonDefinition,
 *      properties: [ PersonDefinition.getProperty("fullName") ]
 *    }),
 *    new FragmentDefinition({
 *      modelDefinition: OrganizationDefinition,
 *      properties: [ PersonDefinition.getProperty("shortName") ]
 *    }),
 * ]
 *
 * @param {GraphQLResolveInfo} info Context of the request
 * @param {ModelDefinitionsRegister} modelDefinitionsRegister
 * @param {Boolean} [isConnection=false] Indicates if node queried is a connection or not
 * @returns {FragmentDefinition[]}
 */
export let getFragmentDefinitionsFromGraphQLResolveInfo = ({ info, isConnection, modelDefinitionsRegister }) => {
  if (!info) {
    return [];
  }

  // Get properties queried for this node
  let propertiesFromNode = info.fieldNodes?.[0]?.selectionSet?.selections || [];

  // If node queried is a connection, we get properties to edges -> node -> properties
  if (isConnection && Array.isArray(propertiesFromNode)){
    propertiesFromNode = propertiesFromNode[0]?.selectionSet?.selections?.[0]?.selectionSet?.selections || [];
  }

  // Get properties queried from fragments for this node if any
  let propertiesFromFragments = [].concat(...Object.values(info.fragments)
    .filter(fragment => fragment.kind === 'FragmentDefinition')
    .map(fragment => fragment.selectionSet?.selections)
  );

  let mergedPropertiesFromFragments = [...propertiesFromFragments, ...propertiesFromNode];

  // Concat properties for node and fragments, and look for inline fragments
  return mergedPropertiesFromFragments.reduce((fragmentDefinitions, prop) => {
    if (prop.selectionSet && prop.kind === 'InlineFragment'){
      let graphQLType =  prop.typeCondition.name.value;

      if (modelDefinitionsRegister.isModelDefinitionExistsForGraphQLType(graphQLType)){
        let modelDefinition = modelDefinitionsRegister.getModelDefinitionForGraphQLType(graphQLType);

        let properties = (prop.selectionSet?.selections || []).reduce((properties, selection) => {
          let property = modelDefinition.getProperty(selection.name?.value);

          if(property){
            properties.push(property);
          }

          return properties;
        }, []);

        if (properties.length > 0){
          fragmentDefinitions.push(new FragmentDefinition({
            modelDefinition,
            properties
          }));
        }
      }
    }

    return fragmentDefinitions;
  }, []);
};