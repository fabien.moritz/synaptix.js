/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  createObjectInConnectionResolver,
  updateObjectResolver
} from "../resolverHelpers";

import ModelDefinitionsRegister from "../../../definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../../../../network/amqp/NetworkLayerAMQP";
import {PubSub} from "graphql-subscriptions";
import SynaptixDatastoreRdfSession from "../../../../../datastores/SynaptixDatastoreRdfSession";
import GraphQLContext from "../../GraphQLContext";
import PersonDefinition from "../../../../ontologies/mnx-agent/definitions/PersonDefinition";

let modelDefinitionsRegister = new ModelDefinitionsRegister([PersonDefinition]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();

let synaptixSession = new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context: new GraphQLContext({anonymous: true, lang: "fr"}),
  pubSubEngine,
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
});

let createSpyFn = jest.spyOn(synaptixSession, "createObject").mockImplementation(() => ({}));
let updateSpyFn = jest.spyOn(synaptixSession, "updateObject").mockImplementation(() => ({}));


describe("Test graphQL resolvers helpers", () => {

  it('should call synaptix session create function based on type', async () => {
    await createObjectInConnectionResolver("Person", [], undefined, {input: {objectInput: { foo: "bar"}}}, synaptixSession);

    expect(createSpyFn).toBeCalledWith({
      graphQLType: "Person",
      lang: "fr",
      links: [],
      objectInput: { foo: "bar"}
    });
  });

  it('should call synaptix session create function based on modelDefinition', async () => {
    await createObjectInConnectionResolver(PersonDefinition, [], undefined, {input: {objectInput: { foo: "bar"}}}, synaptixSession);

    expect(createSpyFn).toBeCalledWith({
      graphQLType: "Person",
      lang: "fr",
      links: [],
      objectInput: { foo: "bar"}
    });
  });

  it('should call synaptix session update function based on modelDefinition', async () => {
    await updateObjectResolver(PersonDefinition, undefined, {input: {objectId: "person/1234", objectInput: { foo: "bar"}}}, synaptixSession);

    expect(updateSpyFn).toBeCalledWith({
      modelDefinition: PersonDefinition,
      objectId: "test:person/1234",
      updatingProps: { foo: "bar"},
      lang: "fr"
    });
  });

  it('should call synaptix session update function based on type', async () => {
    await updateObjectResolver("Person", undefined, {input: {objectId: "person/1234", objectInput: { foo: "bar"}}}, synaptixSession);

    expect(updateSpyFn).toBeCalledWith({
      modelDefinition: PersonDefinition,
      objectId: "test:person/1234",
      updatingProps: { foo: "bar"},
      lang: "fr"
    });
  });
});
