/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {getFragmentDefinitionsFromGraphQLResolveInfo} from "../fragmentHelpers";

import ModelDefinitionsRegister from "../../../definitions/ModelDefinitionsRegister";
import PersonDefinition from "../../../../ontologies/mnx-agent/definitions/PersonDefinition";
import OrganizationDefinition from "../../../../ontologies/mnx-agent/definitions/OrganizationDefinition";
import {gqlQueryToResolveInfo} from "../../../../../../testing/gqlQueryToResolveInfo";
import gql from "graphql-tag";
import {FragmentDefinition} from "../../../definitions/FragmentDefinition";

let modelDefinitionsRegister = new ModelDefinitionsRegister([PersonDefinition, OrganizationDefinition]);

describe("Test graphQL fragments helpers", () => {

  it('should extract fragments from object query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsFromGraphQLResolveInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          agent{
            id
            displayName
            ...on Person{
              firstName
            }
            ...on Organization{
              shortName
            }
          }
        }
      `),
      modelDefinitionsRegister
    });

    expect(fragmentDefinitions).toEqual([
      new FragmentDefinition({
        modelDefinition: PersonDefinition,
        properties: [PersonDefinition.getProperty("firstName")]
      }),
      new FragmentDefinition({
        modelDefinition: OrganizationDefinition,
        properties: [OrganizationDefinition.getProperty("shortName")]
      })
    ]);
  });

  it('should extract fragments from objects connection query', async () => {
    let fragmentDefinitions = getFragmentDefinitionsFromGraphQLResolveInfo({
      info: gqlQueryToResolveInfo(gql`
        query{
          agents{
            edges{
              node{
                id
                displayName
                ...on Person{
                  firstName
                }
                ...on Organization{
                  shortName
                }
              }
            }
          }
        }
      `),
      isConnection: true,
      modelDefinitionsRegister
    });

    expect(fragmentDefinitions).toEqual([
      new FragmentDefinition({
        modelDefinition: PersonDefinition,
        properties: [PersonDefinition.getProperty("firstName")]
      }),
      new FragmentDefinition({
        modelDefinition: OrganizationDefinition,
        properties: [OrganizationDefinition.getProperty("shortName")]
      })
    ]);
  });
});
