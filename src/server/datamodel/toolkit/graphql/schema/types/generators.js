export let connectionArgs = `after: String, first: Int, before: String, last: Int`;
export let filteringArgs = `qs: String, sortBy: String, isSortDescending: Boolean, filters: [String], sortings: [SortingInput]`;

/**
 * @param {string} filtersInput
 * @param {string} sortingsInput
 * @return {string}
 */
export let generateConnectionArgs = ({filtersInput, sortingsInput} = {}) => {
  if (!filtersInput){
    filtersInput = "[String]";
  }

  if (!sortingsInput){
    sortingsInput = "[SortingInput]";
  }

  return `${connectionArgs} qs: String filters: ${filtersInput} sortings: ${sortingsInput}`;
};

export let generateConnectionForType = (type) => `
""" A connection to a list of ${type}. See https://facebook.github.io/relay/graphql/connections.htm"""
type ${type}Connection {
  """ Information to paginate """
  pageInfo: PageInfo!

  """ A list of edges """
  edges: [${type}Edge]
}

""" An edge in a ${type} connection. """
type ${type}Edge {
  """ The item at the end of the edge """
  node: ${type}

  """ A cursor for use in pagination """
  cursor: String!
}
`;

export let generateConnectionResolverFor = (type) => ({
  [`${type}Connection`]: {
    edges: (object) => object.edges,
    pageInfo: (object) => object.pageInfo,
  },
  [`${type}Edge`]: {
    node: (object) => object.node,
    cursor: (object) => object.cursor,
  }
});

