/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {buildErrorObjectFromValidationError} from "./buildErrorObjectFromValidationError";

const defaultOptions = {
  shouldTransformArgs: true,
  yupOptions: {
    abortEarly: false,
  },
};

/**
 * Form validation middleware is a tweaked version of yupMiddleware.js.
r * @return {IMiddleware}
 */
export let formValidationMiddleware = (options = {}) => {
  const mergedOptions = Object.assign({}, defaultOptions, options);

  if (typeof options === 'function') {
    throw new TypeError('You have to call the formValidationMiddleware before adding it to the middlewares: formValidationMiddleware({...})');
  }

  return {
    async Mutation(resolve, root, args, context, info) {
      const mutationField = info.schema.getMutationType();
      // Form validation must only occurs on mutations
      if (!mutationField) {
        return;
      }
      const mutationDefinition = mutationField.getFields()[info.fieldName];
      const mutationValidationSchema = mutationDefinition.validationSchema;
      const mutationValidationOptions = mutationDefinition.validationOptions;

      if (mutationValidationSchema) {
        const finalOptions = Object.assign({}, mergedOptions, mutationValidationOptions);
        const schema = typeof mutationValidationSchema === 'function'
          ? mutationValidationSchema(root, args, context, info)
          : mutationValidationSchema;
        try {
          const values = await schema.validate(args, Object.assign({abortEarly: false}, finalOptions.yupOptions));
          return resolve(root, finalOptions.shouldTransformArgs ? values : args, context, info);
        } catch (error) {
          if (error.name === "ValidationError") {
            return buildErrorObjectFromValidationError(error, {
              root,
              args,
              context,
              info,
            });
          } else {
            throw error;
          }
        }
      }
      return resolve(root, args, context, info);
    },
  };
};