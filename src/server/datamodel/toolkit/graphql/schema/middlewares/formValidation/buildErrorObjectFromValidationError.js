
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import groupBy from "lodash/groupBy";
import {FormValidationError} from "./FormValidationError";

/**
 * @param error
 * @throws {FormValidationError}
 */
export function buildErrorObjectFromValidationError(error){
  let formInputErrors = [];

  if (error.inner?.length) {
    const errorsGrouped = groupBy(error.inner, 'path');

    formInputErrors = Object.keys(errorsGrouped).reduce(
      (acc, key) => [
        ...acc,
        {
          field: key,
          errors: errorsGrouped[key].map(fieldError => fieldError.message),
        },
      ],
      [],
    );
  }

  throw new FormValidationError(`${error.message} during form validation`, formInputErrors );
}