/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

export let RemoveEdgeType = `
"""Remove object mutation payload"""
type RemoveEdgePayload {
  deletedId: ID
}

"""Remove object mutation input"""
input RemoveEdgeInput {
  objectId: ID!
  targetId: ID!
}

extend type Mutation{
  removeEdge(input: RemoveEdgeInput!): RemoveEdgePayload
}
`;

export let RemoveEdgeResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {string} objectId
     * @param {boolean} permanent
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    removeEdge: async (_, {input: {objectId, targetId}}, synaptixSession) => {
      let modelDefinition = synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(synaptixSession.extractTypeFromGlobalId(objectId));
      let targetModelDefinition = synaptixSession.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(synaptixSession.extractTypeFromGlobalId(targetId));

      await synaptixSession.removeEdge(
        modelDefinition,
        synaptixSession.extractIdFromGlobalId(objectId),
        modelDefinition.getFirstLinkForTargetModelDefinition(targetModelDefinition),
        synaptixSession.extractIdFromGlobalId(targetId)
      );

      return {deletedId: objectId};
    },
  },
};