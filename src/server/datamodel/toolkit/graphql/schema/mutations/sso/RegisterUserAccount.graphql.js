/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {object, string} from "yup";
import {allow} from "graphql-shield";
import {I18nError} from "../../../../../../utilities/I18nError";
import {FormValidationError} from "../../middlewares/formValidation/FormValidationError";

export let RegisterUserAccountMutation = `
"""Register account mutation payload"""
type RegisterUserAccountPayload {
  success: Boolean
}

"""Register account mutation input"""
input RegisterUserAccountInput {
  username: String
  email: String
  firstName: String
  lastName: String
  nickName: String
  password: String
  isTemporaryPassword: Boolean
  personInput: PersonInput
}

extend type Mutation{
  """ 
  This mutation input parameters are inforced by Yup middleware and returns a FORM_VALIDATION_ERROR listing input error details.
  
  Possible i18n error keys are :
  
    - IS_REQUIRED
    - PASSWORD_TO_SHORT
    - EMAIL_MISFORMED
    - EMAIL_ALREADY_REGISTERED
  """
  registerUserAccount(input: RegisterUserAccountInput!): RegisterUserAccountPayload
}
`;

export let RegisterUserAccountMutationResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {boolean} userParams
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    registerUserAccount: {
      validationSchema: object().shape({
        input: object().shape({
          firstName: string().trim(),
          lastName: string().trim(),
          // If no firstName and lastName are filled, get this field required.
          nickName: string().when(["firstName", "lastName"], {
            is: (firstName, lastName) => !firstName || !lastName,
            then: string().trim().required("IS_REQUIRED"),
          }),
          password: string().trim().required("IS_REQUIRED").min(6, "PASSWORD_TOO_SHORT"),
          email: string().trim().required("IS_REQUIRED").email("EMAIL_MISFORMED"),
        })
      }),
      resolve: async (_, {input: userParams}, synaptixSession) => {
        try {
          await synaptixSession.getSSOControllerService().registerUserAccount(userParams);
        } catch (e) {
          // Simulate FormValidationError
          if(e instanceof I18nError && e.statusCode === 400 && e.i18nKey === "USER_ALREADY_EXISTS"){
            throw new FormValidationError(e.message, [{
              field: "input.email",
              errors: [
                "EMAIL_ALREADY_REGISTERED"
              ]
            }])
          }

          throw e;
        }
        return {success: true};
      }
    },
  },
};

export let RegisterUserAccountMutationShieldRules = {
  Mutation: {
    registerUserAccount: allow
  }
};