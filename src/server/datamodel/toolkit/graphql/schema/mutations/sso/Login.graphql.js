/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {object, string} from "yup";
import {allow} from "graphql-shield";

export let LoginMutation = `
"""Login mutation payload"""
type LoginPayload {
  """ When login suceeds, this paremeter is true and a session cookie is set.""" 
  success: Boolean
}

"""Login mutation input"""
input LoginInput {
  username: String
  password: String 
}

extend type Mutation{
  """ 
  This mutation input parameters are inforced by Yup middleware and returns a FORM_VALIDATION_ERROR listing input error details.
  
  Possible i18n error keys are :
  
    - IS_REQUIRED
    
  Other error keys issued by SSO are :
  
    - INVALID_CREDENTIALS
    - ACCOUNT_DISABLED
    - ACCOUNT_NOT_VALIDATED
  """
  login(input: LoginInput!): LoginPayload
}
`;

export let LoginMutationResolvers = {
  Mutation: {
    /**
     * @param {object} _
     * @param {string} username
     * @param {string} password
     * @param {SynaptixDatastoreSession} synaptixSession
     */
    login: {
      validationSchema: object().shape({
        input: object().shape({
          username: string().trim().required("IS_REQUIRED"),
          password: string().trim().required("IS_REQUIRED"),
        })
      }),
      resolve : async (_, {input: {username, password}}, synaptixSession) => {
        await synaptixSession.getSSOControllerService().login({username, password});
        return {success: true};
      }
    },
  },
};

export let LoginMutationShieldRules = {
  Mutation: {
    login: allow
  }
};