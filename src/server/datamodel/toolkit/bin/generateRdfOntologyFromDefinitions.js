/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {MnxOntologies} from "../../ontologies";
import yargs from "yargs";
import dayjs from "dayjs";
import ora from "ora";
import path from "path";
import fs from "fs";
import {gatherModelDefinitionHierarchy} from "../definitions/helpers";

let headerTemplate = ({prefixes, baseNamespace}) =>
`@prefix : <${baseNamespace}> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
${Object.entries(prefixes).map(([prefix, ns]) => `@prefix ${prefix}: <${ns}> .`).join("\n")}
@prefix dc: <http://purl.org/dc/elements/1.1/> .

@base <${baseNamespace}> .

<http://ns.mnemotix.com/ontologies/2019/8/generic-model> rdf:type owl:Ontology ;
owl:imports <http://www.w3.org/2001/XMLSchema#>, ${Object.values(prefixes).map(ns => `<${ns}>`).join(", ")};
  dc:creator "Mnemotix SCIC"@fr ;
  dc:created "${dayjs().toISOString()}"^^xsd:dateTimestamp;
  rdfs:comment "Mnémotix generic and cross-domain ontology gathering primary classes used in most of our projects"@en ;
  rdfs:label "Mnémotix Generic Model"@en , "Ontologie générique Mnémotix"@fr .

`;

let classTemplate = ({className, description, parentClasses, sameAsClasses, baseNamespace}) =>
`### ${baseNamespace}${className.replace("mnx:", "")}
${className} rdf:type owl:Class;
  ${sameAsClasses.length > 0 ? `rdfs:subClassOf ${sameAsClasses.map(sameAsClass => sameAsClass.indexOf("http://") !== -1 ?  `<${sameAsClass}>` :  sameAsClass ).join(", ")} ;` : ""} 
  ${parentClasses.length > 0 ? `rdfs:subClassOf ${parentClasses.join(", ")} ;` : ""}
  ${!!description ? `rdfs:comment "${description}"@en ;` : ""}
  rdfs:label "${className.replace("mnx:", "")}"@en .
`;

let objectPropertyTemplate = ({subjectClassName, objectClassName, propertyName, baseNamespace, isPlural}) => {
  let isMnxProperty = propertyName.includes("mnx:");
  let propertyURI = isMnxProperty ? `${baseNamespace}${propertyName.replace("mnx:", "")}` : propertyName;

  return `### ${propertyURI}
${propertyName} rdf:type owl:ObjectProperty ;
  ${!isPlural ? "owl:maxCardinality \"1\"^^xsd:int ;": ""}
  rdfs:domain ${subjectClassName} ;
  rdfs:range ${objectClassName} .
`
};

let dataPropertyTemplate = ({subjectClassName, literalValue, propertyName, baseNamespace}) => {
  let isMnxProperty = propertyName.includes("mnx:");
  let propertyURI = isMnxProperty ? `${baseNamespace}${propertyName.replace("mnx:", "")}` : propertyName;

  return `### ${propertyURI}
${propertyName} rdf:type owl:DatatypeProperty;
  rdfs:domain ${subjectClassName} ;
  rdfs:range ${literalValue ? literalValue.indexOf("http://") !== -1 ?  `<${literalValue}>` :  literalValue : "rdfs:Literal"} .
`;
};


export let generateRdfOntologyFromDefinitions = () => {
  let spinner = ora().start();
  spinner.spinner = "clock";

  let {outputDir, baseNamespace} = yargs
    .usage("yarn ontology:generate:owl [options] -o [Output dir]")
    .example("yarn ontology:generate:owl -o mnx-agent.ttl", "")
    .option('o', {
      alias: 'outputDir',
      //default: "/Users/mrogelja/Dev/mnx/Mnemotix/mnx-models/generic-model",
      default: path.resolve(__dirname, "../../../../../.generated"),
      describe: 'Output directory for generated files',
      nargs: 1
    })
    .option('n', {
      alias: 'baseNamespace',
      default: "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
      describe: 'Base RDF namespace',
      nargs: 1
    })
    .help('h')
    .alias('h', 'help')
    .epilog('Copyright Mnemotix 2019')
    .help()
    .argv;


  if (!fs.existsSync(outputDir)){
    fs.mkdirSync(outputDir);
  }

  for (let {name, bundled, ModelDefinitions} of Object.values(MnxOntologies)) {
    // Don't process bundled ontologies.
    if(bundled){
      return;
    }

    let prefixes = {};
    let owlClasses = [];
    let owlObjectProperties = [];
    let owlDataProperties = [];

    spinner.info(`Processing "${name}" model`);

    for(let modelDefinition of Object.values(ModelDefinitions)){
      prefixes = Object.assign(prefixes, modelDefinition.getRdfPrefixesMapping());
      let className = modelDefinition.getRdfType();

      owlClasses.push(classTemplate(({
        className,
        description: "",
        parentClasses: gatherModelDefinitionHierarchy(modelDefinition).map(ParentModelDefinition => ParentModelDefinition.getRdfType()),
        sameAsClasses: modelDefinition.getRdfSameAsTypes(),
        baseNamespace
      })));

      for(let link of modelDefinition.getLinks()){
        let relatedModelDefinition = link.getRelatedModelDefinition();
        let rdfObjectProperty = link.getRdfObjectProperty();
        let rdfReversedObjectProperty = link.getRdfReversedObjectProperty();
        let relatedModelDefinitionClassName = relatedModelDefinition.getRdfType();

        if (!link.isHerited()) {
          owlObjectProperties.push(objectPropertyTemplate({
            subjectClassName: !!rdfObjectProperty ? className : relatedModelDefinitionClassName,
            propertyName: (rdfObjectProperty || rdfReversedObjectProperty),
            objectClassName: !!rdfObjectProperty ? relatedModelDefinitionClassName : className,
            baseNamespace,
            isPlural: link.isPlural()
          }));
        }
      }

      for(let literal of modelDefinition.getLiterals()){
        if (!literal.isHerited()) {
          owlDataProperties.push(dataPropertyTemplate({
            subjectClassName: className,
            propertyName: literal.getRdfDataProperty(),
            literalValue: literal.getRdfDataType(),
            baseNamespace
          }))
        }
      }

      for(let label of modelDefinition.getLabels()){
        try {
          if (!label.isHerited()) {
            owlDataProperties.push(dataPropertyTemplate({
              subjectClassName: className,
              propertyName: label.getRdfDataProperty(),
              baseNamespace
            }))
          }
        } catch (e) {
          spinner.fail(`An error occured in the processing of label ${className} -> ${label.getLabelName()} : ${e}`)
        }
      }
    }

    let outputFile = `${outputDir}/${name}.ttl`;
    let outputOwl  = `${headerTemplate({prefixes, baseNamespace})}

#################################################################
#    Classes
#################################################################

${owlClasses.join("\n")}

#################################################################
#    Object Properties
#################################################################

${owlObjectProperties.join("\n")}

#################################################################
#    Data properties
#################################################################

${owlDataProperties.join("\n")}    
`;

    outputOwl = outputOwl.replace(/^(?: +\n)+(.*)/mg, "$1");
    outputOwl = outputOwl.replace(/mnx:/g, ":");

    fs.writeFileSync(outputFile, outputOwl);


    spinner.succeed(`File ${outputFile} generated with success.`);
  }

  process.exit(0);
};
