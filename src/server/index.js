/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

export {default as DataModulePublisher} from "./datamodules/drivers/DataModulePublisher";
export {default as DataModuleConsumer} from "./datamodules/drivers/DataModuleConsumer";

export {
  UriGenPublisher,
  DseControllerPublisher,
  GraphStoreConsumer,
  IndexControllerV2Publisher,
  IndexSyncV6Publisher,
  IndexControllerConsumer,
  SSOApiClient,
  OwncloudClient,
  MinioConsumer,
  createEdge, createNode, Edge, Node
} from "./datamodules/drivers";

export {
  DseControllerService,
  IndexControllerService,
  GraphControllerService
} from "./datamodules/services";

export {
  GraphMiddleware,
  LegacyGraphMiddleware,
  MnxAclGraphMiddleware,
  MnxActionGraphMiddleware
} from "./datamodules/middlewares/graph";

export {
  default as SSOMiddleware
} from "./datamodules/middlewares/sso/SSOMiddleware";

export {
  logDebug, logError, logInfo, logWarning, logException
} from "./utilities/logger";

export {
  formatGraphQLError,
  generateGraphQLEndpoint,
  generateSSOEndpoint,
  initEnvironment,
  launchApplication,
  ExpressApp
} from "./utilities";

export {
  DatastoreAdapterAbstract,
  DatastoreSessionAbstract,
  SynaptixDatastoreAdapter,
  SynaptixDatastoreSession,
  SynaptixDatastoreDseSession,
  SynaptixDatastoreRdfAdapter,
  SynaptixDatastoreRdfSession
} from "./datastores";


export {
  default as NetworkLayerAbstract
} from "./network/NetworkLayerAbstract";

export {
  default as NetworkLayerAMQP
} from "./network/amqp/NetworkLayerAMQP";

export {MnxOntologies, MnxDatamodels} from "./datamodel/ontologies";

export {
  GraphQLContext, GraphQLPaginationArgs, GraphQLConnectionArgs, GraphQLConnectionDefinitions
} from "./datamodel/toolkit/graphql";

export {
  createModelFromGraphstoreNode, createModelFromIndexSyncDocument
} from "./datamodel/toolkit/models";

export {
  generateSchema,
  mergeSchemas
} from "./datamodel/toolkit/graphql/schema/utilities/generateSchema"

export {
  mergeResolvers, getObjectsResolver, getObjectsCountResolver, getLocalizedLabelResolver, getLinkedObjectsResolver,
  getLinkedObjectResolver, getObjectResolver, generateTypeResolvers, createObjectInConnectionResolver,
  updateObjectResolver, getMeResolver, createEdgeResolver, getLinkedObjectsCountResolver, getTypeResolver,
  generateInterfaceResolvers
} from "./datamodel/toolkit/graphql/helpers/resolverHelpers"

export {
  ModelDefinitionsRegister,
  ModelDefinitionAbstract,
  UseIndexMatcherOfDefinition,
  LinkDefinition,
  LabelDefinition,
  LiteralDefinition
} from "./datamodel/toolkit/definitions";

export {LinkPath, LinkStep, PropertyStep, PropertyFilterStep} from "./datamodel/toolkit/utils/LinkPath";

export {
  generateCreateMutationDefinitionForType,
  generateCreateMutationDefinition,
  generateUpdateMutationDefinitionForType,
  generateUpdateMutationDefinition,
  generateCreateMutationResolverForType,
  generateCreateMutationResolver,
  generateUpdateMutationResolverForType,
  generateUpdateMutationResolver
} from "./datamodel/toolkit/graphql/schema/mutations/generators";

export {
  generateConnectionForType,
  generateConnectionResolverFor,
  filteringArgs,
  connectionArgs,
  generateConnectionArgs
} from "./datamodel/toolkit/graphql/schema/types/generators";

export {
  EntityInterfaceProperties, EntityInterfaceInput
} from "./datamodel/ontologies/mnx-common/schema/types/EntityInterface.graphql";

export {default as EntityDefinition} from "./datamodel/ontologies/mnx-common/definitions/EntityDefinition";


export {
  default as Organization
} from './datamodel/ontologies/mnx-agent/models/Organization';

export {Model} from "./datamodel/toolkit/models/Model";

export {DataModel} from "./datamodel/DataModel";

export {
  sendValidationErrorsJSON as sendValidationErrorsJSONExpressMiddleware
}from "./utilities/express-validator-utils/sendValidationErrorsJSON";
export {
  attachDatastoreSession as attachDatastoreSessionExpressMiddleware
}from "./utilities/express-middlewares/attachDatastoreSession";


export {I18nError} from "./utilities/I18nError";
export {FormValidationError} from "./datamodel/toolkit/graphql/schema/middlewares/formValidation/FormValidationError";

// Generic
export {askSparqlRule, mergeRules, isAuthenticatedRule} from "./datamodel/toolkit/graphql/schema/middlewares/aclValidation/rules";
export {isCreatorRule, isLoggedUserObjectCreator} from "./datamodel/ontologies/mnx-contribution/schema/middlewares/aclValidation/rules/isCreatorRule";
export {isInUserGroupRule, isAdminRule} from "./datamodel/ontologies/mnx-agent/schema/middlewares/aclValidation/rules";
export {replaceFieldValueIfRuleFailsMiddleware} from "./datamodel/toolkit/graphql/schema/middlewares/fieldReplacement/replaceFieldValueIfRuleFailsMiddleware";

/// RETROCOMPATIBILITY
export {connectionFromArray, cursorToOffset, offsetToCursor} from "./datamodel/toolkit/graphql/helpers/connectionHelpers";
export {Ontologies as MnxOntologiesLegacy, ObjectDefaultGraphQLProperties} from "./datamodel/ontologies-legacy";
export {
  filteringArgs as paginationArgs
} from "./datamodel/toolkit/graphql/schema/types/generators";
/*export {
  generateInterfaceResolvers as generateInterfaceResolverMap,
  generateTypeResolvers as generateBaseResolverMap
} from "./datamodel/toolkit/graphql/resolvers/helpers";*/
export {
  SSOApiClient as SSOSyncClient
} from "./datamodules/drivers";
export {Model as ModelAbstract} from "./datamodel/toolkit/models/Model";
export {
  default as SSOActionMiddleware
} from "./datamodules/middlewares/sso/SSOMiddleware";
