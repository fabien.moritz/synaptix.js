/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import GraphControllerService from "../datamodules/services/graph/GraphControllerService";
import {gatherModelDefinitionHierarchy} from "../datamodel/toolkit/definitions/helpers";
import NetworkLayerAMQP from "../network/amqp/NetworkLayerAMQP";
import SynaptixDatastoreSession from "./SynaptixDatastoreSession";
import {I18nError} from "..";
import env from 'env-var';

/**
 * This is the synaptix session designed to use RDF triple store.
 */
export default class SynaptixDatastoreRdfSession extends SynaptixDatastoreSession {
  /** @type {GraphControllerService} */
  _graphControllerService;

  /** @type {string} */
  _nodesPrefix;

  /**
   * Constructor
   *
   * @param {GraphQLContext} context GraphQL context object
   * @param {NetworkLayerAMQP} networkLayer
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {PubSubEngine} pubSubEngine
   * @param {object} schemaNamespaceMapping - Project namespaces mapping.
   * @param {string} nodesNamespaceURI  - The URI namespace used to create new node (aka: to generate new URIs)
   * @param {string} [nodesPrefix=ex]   - The prefix used for nodesNamespaceURI
   * @param {function} [nodesTypeFormatter] - This function tweaks a node type into a URI prefix-styled format
   * @param {GraphMiddleware[]} graphMiddlewares - A list of middlewares for GraphControllerService
   * @param {e.Response} [res] - Express response object
   * @param {SSOApiClient} [ssoApiClient]
   * @param {SSOMiddleware[]} [ssoMiddlewares] - A list of middlewares for SSO actions
   * @param {string} [adminUserGroupId] - Define UserGroup instance related to administrators.
   * @param {string} [insertingNamedGraphId] - The URI of the global named graph where data are inserted
   */
  constructor({context, networkLayer, modelDefinitionsRegister, pubSubEngine, schemaNamespaceMapping, nodesNamespaceURI, nodesPrefix,nodesTypeFormatter, graphMiddlewares, ssoApiClient, ssoMiddlewares, res, adminUserGroupId, insertingNamedGraphId}) {
    super({context, networkLayer, modelDefinitionsRegister, pubSubEngine,  ssoApiClient, ssoMiddlewares, res, adminUserGroupId});

    this._nodesTypeFormatter = nodesTypeFormatter;
    this._nodesPrefix = nodesPrefix;
    this._nodesNamespaceURI = nodesNamespaceURI;

    this._graphControllerService = new GraphControllerService({
      networkLayer,
      graphQLcontext: context,
      schemaNamespaceMapping,
      nodesNamespaceURI,
      nodesPrefix,
      middlewares: graphMiddlewares,
      nodesTypeFormatter: this.getNodesTypeFormatter(),
      insertingNamedGraphId
    });

    (graphMiddlewares || []).map(graphMiddleware => graphMiddleware.attachSession(this));
  }

  /**
   * @return {string}
   */
  getNodesNamespaceURI() {
    return this._nodesNamespaceURI;
  }

  /**
   * @return {string}
   */
  getNodesPrefix() {
    return this._nodesPrefix;
  }

  /**
   * This method returns that is called to take a type and get it's related form in an instance URI prefix.
   *
   * By default, and for example :
   *
   * A type `mnx:UserAccount` is tranformed into `useraccount`.
   * And a  `mnx:UserAccount` node URI could be `https://data.mnemotix.com/useraccount/36545`
   *
   * @return {function}
   */
  getNodesTypeFormatter(){
    return this._nodesTypeFormatter || ((type) => type.toLowerCase());
  }

  /**
   * @return {GraphControllerService}
   */
  getGraphControllerService() {
    return this._graphControllerService;
  }

  /**
   * Extract an id from a globalId object representation
   * @param {string} globalId
   * @return {string}
   */
  extractIdFromGlobalId(globalId) {
    //
    // If Relay is activated, use default method.
    //
    if (env.get("USE_GRAPHQL_RELAY", "false").asBool()) {
      return super.extractIdFromGlobalId(globalId);
      //
      // Otherwise, we assume that the datastore internal id IS the globalId
      //
    } else {
      let id;

      if (!!globalId.match(/https?:\/\//) || globalId.includes(':')) {
        id = globalId;
        // If ID is not a prefixed|absolture URI, we should reconstruct with the prefix.
      } else {
        id = `${this._nodesPrefix}:${globalId}`;
      }

      return id
    }
  }

  /**
   * Parse a graphQL global id string and return it's object representation.
   *
   * @param globalId
   * @return {{id: *, type: string}}
   */
  parseGlobalId(globalId) {
    //
    // If Relay is activated, use default method.
    //
    if (env.get("USE_GRAPHQL_RELAY", "false").asBool()) {
      return super.parseGlobalId(globalId);
    } else {
      let id, uri;

      // If globalId is an absolute URI. Nothing to do.
      if (!!globalId.match(/https?:\/\//)) {
        id = uri = globalId;
        // If it's a prefixed URI, we should reconstruct the absolute URI.
      } else if (globalId.includes(':')) {
        let delimiterPos = globalId.indexOf(':');
        id = globalId;
        uri = `${this._nodesNamespaceURI}${globalId.slice(delimiterPos + 1)}`
        // If there is no prefix, we should restruct the prefixed AND absolute URI
      } else {
        id = `${this._nodesPrefix}:${globalId}`;
        uri = `${this._nodesNamespaceURI}${globalId}`;
      }

      // For retrieved type, URI must be reconstructed is gived as prefixed.
      return {
        type: this.getModelDefinitionsRegister().getGraphQLTypeForURI({
          uri,
          nodesNamespaceURI: this._nodesNamespaceURI,
          nodesTypeFormatter: this.getNodesTypeFormatter()
        }),
        id
      };
    }
  }

  /**
   * Stringify a globalId object representation.
   *
   * @param {string} type - Object GraphQL type
   * @param {string} id   - Object id
   * @return {*}
   */
  stringifyGlobalId({type, id}) {
    if (!!parseInt(process.env.USE_GRAPHQL_RELAY)) {
      return super.parseGlobalId({type, id});
    } else {
      // If globalId is an absolute URI. Nothing to do.
      if (id.includes(this._nodesPrefix)) {
        id = id.replace(`${this._nodesPrefix}:`, '');
      } else if (id.includes(this._nodesNamespaceURI)) {
        id = id.replace(this._nodesNamespaceURI, '');
      }

      return id;
    }
  }

  /**
   * Generate a URI given a modelDefinition
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [idPrefix]  - Prefix before random id generation.
   * @param {string} [idValue]   - Set an manual id in place of random id generation.
   */
  generateUriForModelDefinition({modelDefinition, idPrefix, idValue}){
    return modelDefinition.generateURI({
      nodesNamespaceURI: this.getNodesNamespaceURI(),
      nodeTypeFormatter: this.getNodesTypeFormatter(),
      idPrefix,
      idValue
    })
  }

  /**
   * Returns a definition given an URI
   * @param {string} uri
   * @return {ModelDefinitionAbstract}
   */
  async getModelDefinitionForURI({uri}) {
    let type = await this._graphControllerService.getRdfTypeForURI({uri});

    if (Array.isArray(type)) {
      let candidatesModelDefinitions = type.reduce((acc, candidateType) => {
        let modelDefinition = this._modelDefinitionRegister.getModelDefinitionForRdfType(candidateType, false);

        if (modelDefinition) {
          acc.push(modelDefinition);
        }

        return acc;
      }, []);

      candidatesModelDefinitions.sort((modelDefinitionA, modelDefinitionB) => {
        let hierarchyDefinitions = gatherModelDefinitionHierarchy(modelDefinitionB);

        // Check is modelDefinitionB herits from modelDefinitionA
        if (hierarchyDefinitions.includes(modelDefinitionA)) {
          return 1;
        } else {
          return -1
        }
      });

      return candidatesModelDefinitions[0];
    } else {
      return this._modelDefinitionRegister.getModelDefinitionForRdfType(type);
    }
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param {string} lang
   * @param {boolean} [returnAsLocalizedLabel=false]
   * @param {boolean} [returnFirstOneIfNotExistForLang=true]
   * @return {string}
   */
  async getLocalizedLabelFor(object, labelDefinition, lang, returnAsLocalizedLabel = false, returnFirstOneIfNotExistForLang = true) {
    let label = await super.getLocalizedLabelFor(object, labelDefinition, lang, returnAsLocalizedLabel, returnFirstOneIfNotExistForLang);

    if (!label) {
      label = await this._graphControllerService.getLocalizedLabelForNode({
        labelDefinition,
        sourceNode: object,
        lang,
        returnAsLocalizedLabel,
        returnFirstOneIfNotExistForLang
      });
    }

    return label;
  }


  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} sourceNode
   * @param {string} lang
   * @return {boolean}
   */
  isLocalizedLabelTranslatedFor(sourceNode, labelDefinition, lang) {
    // TODO implement this.
    return this._graphControllerService.isLocalizedLabelTranslatedForNode({
      sourceNode,
      lang,
      labelDefinition
    });
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @param {boolean} bypassIndex
   * @param {boolean} [discardTypeAssertion=false] - Discard RDF type assertion. This this usefull when URI reference are stored in triple store but belongs to another triple store (Ex: Geonames)
   * @param {LinkFilter[]} [linkFilters] - A list of link filters to add.
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @return {Model|Model[]}
   */
  async getLinkedObjectFor({object, linkDefinition, args, bypassIndex, discardTypeAssertion, linkFilters, fragmentDefinitions}) {
    if (!this.isIndexDisabled() && !bypassIndex) {
      let result = await super.getLinkedObjectFor({object, linkDefinition, fragmentDefinitions, args});

      if (!!result) {
        return result;
      }
    }

    let modelDefinition = linkDefinition.getRelatedModelDefinition();

    let nodes = await this._graphControllerService.getNodes({
      modelDefinition,
      linkFilters: [{
        id: object.id,
        isReversed: true,
        linkDefinition
      }].concat(linkFilters || []),
      discardTypeAssertion,
      fragmentDefinitions,
      limit: this.getLimitFromArgs(args),
      offset: this.getOffsetFromArgs(args),
      sortings: this.getSortingsFromArgs({args, modelDefinition}),
    });

    if (linkDefinition.isPlural()) {
      return nodes;
    } else {
      return nodes[0];
    }
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @param {boolean} bypassIndex
   * @param {boolean} [discardTypeAssertion=false] - Discard RDF type assertion. This this usefull when URI reference are stored in triple store but belongs to another triple store (Ex: Geonames)
   * @param {LinkFilter[]} [linkFilters] - A list of link filters to add.
   *
   * @return {number}
   */
  async getLinkedObjectsCountFor({object, linkDefinition, args, bypassIndex, discardTypeAssertion, linkFilters}) {
    if (!this.isIndexDisabled() && !bypassIndex) {
      let result = await super.getLinkedObjectsCountFor({object, linkDefinition, args});

      if (!!result) {
        return result;
      }
    }

    return this._graphControllerService.getNodes({
      modelDefinition: linkDefinition.getRelatedModelDefinition(),
      linkFilters: [{
        id: object.id,
        isReversed: true,
        linkDefinition
      }].concat(linkFilters || []),
      discardTypeAssertion,
      justCount: true
    });
  }

  /**
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} [linkPath]
   * @param {DataQueryArguments} [args]
   * @param {boolean} discardTypeAssertion
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @return {Model[]}
   */
  async getObjectsFilteredByLinkPath({modelDefinition, linkPath, discardTypeAssertion, args, fragmentDefinitions}) {
    if (!this.isIndexDisabled() && !bypassIndex) {
      let result = await super.getObjectsFilteredByLinkPath({modelDefinition, linkPath, args, fragmentDefinitions});

      if (!!result) {
        return result;
      }
    }

    let nodes = await this._graphControllerService.getNodes({
      modelDefinition,
      linkPath,
      discardTypeAssertion,
      fragmentDefinitions,
      limit: this.getLimitFromArgs(args),
      offset: this.getOffsetFromArgs(args),
      sortings: this.getSortingsFromArgs({args, modelDefinition})
    });

    return nodes;
  }

  /**
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} [linkPath]
   * @param {DataQueryArguments} [args]
   * @param {boolean} discardTypeAssertion
   * @return {Model|null}
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   */
  async getObjectFilteredByLinkPath({modelDefinition, linkPath, discardTypeAssertion, args, fragmentDefinitions}) {
    let objects = await this.getObjectsFilteredByLinkPath({modelDefinition, linkPath, discardTypeAssertion, args, fragmentDefinitions});

    return objects?.[0];
  }

  /**
   * Is object has a link with another one defined by it's id ?
   *
   * @param {Model} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LinkDefinition|LinkDefinition[]} linkDefinitions
   * @param {string} targetId
   * @return {boolean}
   */
  async isObjectLinkedToTargetId({object, modelDefinition, linkDefinitions, targetId}) {
    return this._graphControllerService.isNodeLinkedToTargetId({
      id: object.id,
      modelDefinition,
      linkDefinitions,
      targetId
    });
  }

  /**
   * Is object exists
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @return {boolean}
   */
  async isObjectExists(modelDefinition, objectId) {
    let exists = await super.isObjectExists(modelDefinition, objectId);

    if (exists === null) {
      return this._graphControllerService.isNodeExists({id: objectId, modelDefinition});
    }
  }

  /**
   * Get object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {string} lang
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @return {Model}
   */
  async getObject(modelDefinition, objectId, lang, fragmentDefinitions = []) {
    let object = await super.getObject(modelDefinition, objectId, lang, fragmentDefinitions);

    if(!object){
      object = await this._graphControllerService.getNode({id: objectId, modelDefinition, lang, fragmentDefinitions});
    }

    return object;
  }

  /**
   * Creates an object
   *
   * @param {string} [graphQLType] - Type of object
   * @param {ModelDefinitionAbstract} [modelDefinition]
   * @param {Link[]} [links] -  List of links
   * @param {object} [objectInput] - Object properties
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {boolean} [waitForIndexSyncing] - Should wait indexation task.
   *
   * @return {Model}
   */
  async createObject({graphQLType, modelDefinition, links, objectInput, lang, uri, waitForIndexSyncing}) {
    if (!modelDefinition){
      modelDefinition = this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(graphQLType);
    }

    ({links, objectInput} = this._extractLinksFromObjectInput({modelDefinition, objectInput, links}));

    return this._graphControllerService.createNode({modelDefinition, links, objectInput, lang, uri, waitForIndexSyncing});
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @param {FragmentDefinition[]} fragmentDefinitions
   * @return {Model[]}
   */
  async getObjects(modelDefinition, args = {}, fragmentDefinitions) {
    let objects = await super.getObjects(modelDefinition, args, fragmentDefinitions);

    if (!objects) {
      let {qs, filters, linkFilters, labelFilters, literalFilters, linkPath} = args;

      if (Array.isArray(filters)){
        let parsedFilters = this.parseRawFilters(modelDefinition, filters);

        linkFilters = [].concat(linkFilters || [], parsedFilters.linkFilters);
        labelFilters = [].concat(labelFilters || [], parsedFilters.labelFilters);
        literalFilters = [].concat(literalFilters || [], parsedFilters.literalFilters);
      }

      objects = await this._graphControllerService.getNodes({
        modelDefinition,
        qs,
        limit: this.getLimitFromArgs(args),
        offset:  this.getOffsetFromArgs(args),
        sortings: this.getSortingsFromArgs({args, modelDefinition}),
        linkFilters,
        literalFilters,
        labelFilters,
        linkPath,
        fragmentDefinitions
      });
    }

    return objects;
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @return {Model[]}
   */
  async getObjectsCount(modelDefinition, args = {}) {
    let count = await super.getObjects(modelDefinition, args);

    if (count === null) {
      let {qs, filters, linkFilters, labelFilters, literalFilters, linkPath} = args;

      if (Array.isArray(filters)){
        let parsedFilters = this.parseRawFilters(modelDefinition, filters);

        linkFilters = [].concat(linkFilters || [], parsedFilters.linkFilters);
        labelFilters = [].concat(labelFilters || [], parsedFilters.labelFilters);
        literalFilters = [].concat(literalFilters || [], parsedFilters.literalFilters);
      }

      count = await this._graphControllerService.getNodes({
        modelDefinition,
        qs,
        linkFilters,
        literalFilters,
        labelFilters,
        linkPath,
        justCount: true
      });
    }

    return count;
  }

  /**
   * Update object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {object} [updatingProps]
   * @param {Link[]} [links] -  List of links
   * @param {string|null} [lang]
   */
  async updateObject({modelDefinition, objectId, updatingProps, links, lang}) {
    ({links, objectInput: updatingProps} = this._extractLinksFromObjectInput({modelDefinition, objectInput: updatingProps, links}));

    return this._graphControllerService.updateNode({
      modelDefinition,
      objectInput: {
        id: objectId,
        ...(updatingProps || {}),
      },
      lang,
      links
    });
  }

  /**
   * Remove object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param permanentRemoval
   */
  async removeObject(modelDefinition, objectId, permanentRemoval = false) {
    return this._graphControllerService.removeNode({
      modelDefinition,
      id: objectId,
      permanentRemoval
    });
  }

  /**
   * Create edge between two nodes and re-index them
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId) {
    await this._graphControllerService.createEdge(modelDefinition, objectId, linkDefinition, targetId);

    let object = await this.updateObject({
      modelDefinition,
      objectId
    });

    let target = await this.updateObject({
      modelDefinition: linkDefinition.getRelatedModelDefinition(),
      objectId: targetId
    });

    return {
      object,
      target
    };
  }

  /**
   * Create edges between a node and target nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string[]} targetIds
   *
   * @return {Model}
   */
  async createEdges(modelDefinition, objectId, linkDefinition, targetIds) {
    for (let targetId of targetIds) {
      await this._graphControllerService.createEdge(modelDefinition, objectId, linkDefinition, targetId);
      await this.updateObject({
        modelDefinition: linkDefinition.getRelatedModelDefinition(),
        targetId: objectId
      });
    }
    await this.updateObject({modelDefinition, objectId})
  }

  /**
   * Remove edge between nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async removeEdge(modelDefinition, objectId, linkDefinition, targetId) {
    await this._graphControllerService.deleteEdges({modelDefinition, objectId, linkDefinition, targetId});

    let object = await this.updateObject({modelDefinition, objectId});
    let target = await this.updateObject({modelDefinition: linkDefinition.getRelatedModelDefinition(), objectId: targetId});

    return {
      object,
      target
    };
  }

  //
  // Utilities
  //

  /**
   * Get an GraphQL objectInput and extract links from them.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]}  links
   */
  _extractLinksFromObjectInput({modelDefinition, objectInput, links}){
    objectInput = objectInput || {};
    links = links || [];
    return Object.entries(objectInput).reduce(({objectInput, links}, [property, value]) => {
      let linkDefinition = modelDefinition.getLinks().find(link => link.getRelatedInputName() === property);

      if(linkDefinition){

        let targetInputs = Array.isArray(value) ? value : [value];

        for(let targetInput of targetInputs){
          // If target input is filled as null or
          if(targetInput === null || (targetInput?.id === null)) {
            links.push(linkDefinition.generateDeletionLink());
          } else if(targetInput?.id) {
            let targetId = this.extractIdFromGlobalId(targetInput.id);

            // If related model is not instantiable, we use type in target id for gettting target model definition
            if (!linkDefinition.getRelatedModelDefinition().isInstantiable()){
              links.push(linkDefinition.generateLinkFromTargetId(
                targetId,
                this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(
                  this.extractTypeFromGlobalId(targetId)
                )
              ));
            } else {
              links.push(linkDefinition.generateLinkFromTargetId(targetId));
            }
          } else {
            /**
             * The following test ensures that input object related model definition is instantiable.
             * If not instantiable:
             *  - It's possible to pass an "inheritedTypename" parameter that refers to an instiable inherited model definition.
             *  - Otherwise throw an Error.
             */
            if(linkDefinition.getRelatedModelDefinition().isInstantiable() || targetInput.inheritedTypename) {
              let targetNestedLinks;
              let targetModelDefinition = targetInput.inheritedTypename ? this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(targetInput.inheritedTypename) : linkDefinition.getRelatedModelDefinition();
              ({links: targetNestedLinks, objectInput: targetInput} = this._extractLinksFromObjectInput({
                modelDefinition: targetModelDefinition,
                objectInput: targetInput,
              }));

              links.push(linkDefinition.generateLinkFromTargetProps({
                targetObjectInput: targetInput,
                targetNestedLinks,
                targetModelDefinition
              }));

            } else {
              let inheritedModelDefinitions = this.getModelDefinitionsRegister().getInheritedModelDefinitionsFor(linkDefinition.getRelatedModelDefinition());

              throw new I18nError(`Input object described in "${linkDefinition.getRelatedInputName()}" property refers to the model definition "${linkDefinition.getRelatedModelDefinition().name}"  that is defined as not instantiable. Please specify "inheritedTypename" property with a inherited type that refered to a model definition defined as instantiable. That is to say one of these : [${inheritedModelDefinitions.map(inheritedModelDefinition => inheritedModelDefinition.getGraphQLType()).join(", ")}]. 
  {
    ${linkDefinition.getRelatedInputName()}{
       inheritedTypename: "${inheritedModelDefinitions.map(inheritedModelDefinition => inheritedModelDefinition.getGraphQLType()).join(" | ")}"
       ...
    }
  }            
`, "NOT_INSTANTIABLE_OBJECT_INPUT")
            }
          }
        }
      } else {
        objectInput[property] = value;
      }

      return {objectInput, links};

    }, {objectInput: {}, links: links || []});
  }
}