/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import DatastoreSessionAbstract from "./DatastoreSessionAbstract";
import IndexControllerService from "../datamodules/services/index/IndexControllerService";
import {getModelDefinitionIndexMatcher} from "../datamodel/toolkit/definitions/helpers";
import {logWarning} from "../utilities/logger";
import Person from "../datamodel/ontologies/mnx-agent/models/Person";
import NetworkLayerAMQP from "../network/amqp/NetworkLayerAMQP";
import {
  connectionFromArray,
  cursorToOffset,
  fromGlobalId as fromGlobalRelayId,
  toGlobalId as toGlobalRelayId
} from "../datamodel/toolkit/graphql/helpers/connectionHelpers";

import {createModelFromNode} from "../datamodel/toolkit/models/helpers";
import {createNode} from "../datamodules/drivers/dse/models";
import {UseIndexMatcherOfDefinition} from "../datamodel/toolkit/definitions";
import SynchronizeGraphAfterRegisterSSOMiddleware from "../datamodules/middlewares/sso/MnxAgentGraphSyncSSOMiddleware";
import SSOControllerService from "../datamodules/services/sso/SSOControllerService";
import PersonDefinition from "../datamodel/ontologies/mnx-agent/definitions/PersonDefinition";
import UserAccountDefinition from "../datamodel/ontologies/mnx-agent/definitions/UserAccountDefinition";
import {I18nError} from "../utilities/I18nError";
import SSOUser from "../datamodules/drivers/sso/models/SSOUser";
import env from "env-var";
import {Sorting} from "../datamodel/toolkit/utils/Sorting";

/**
 * @typedef {Object} DataQueryArguments
 * @property {string} [qs]
 * @property {string} [sortBy]
 * @property {string} [sortDirection]
 * @property {string} [after]
 * @property {string[]} [filters] - A list of raw filters (may be links, labels or literals)
 * @property {LinkFilter[]} [linkFilters]
 * @property {LabelFilter[]} [labelFilters]
 * @property {LiteralFilter[]} [literalFilters]
 * @property {LinkPath} [linkPath]
 * @property {number} [first]
 */

/**
 * Abstract class to initiate a agnostic persistant database session on Synaptix middleware.
 */
export default class SynaptixDatastoreSession extends DatastoreSessionAbstract {
  /** @type {IndexControllerService} */
  _indexControllerService;
  /** @type {ModelDefinitionsRegister}*/
  _modelDefinitionRegister;
  /** @type {NetworkLayerAMQP} */
  _networkLayer;
  /** @type {PubSubEngine} */
  _pubSubEngine;
  /** @type {SSOControllerService} */
  _ssoControllerService;
  /** @type {e.Response}*/
  _res;
  /** @type {object} */
  _cache = {};

  /**
   * Constructor
   *
   * @param {GraphQLContext} context GraphQL context object
   * @param {NetworkLayerAMQP} networkLayer
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {PubSubEngine} pubSubEngine
   * @param {e.Response} [res] - Express response object
   * @param {SSOApiClient} [ssoApiClient]
   * @param {SSOMiddleware[]} [ssoMiddlewares]
   * @param {string} [adminUserGroupId] - Define UserGroup instance related to administrators.
   */
  constructor({context, networkLayer, modelDefinitionsRegister, pubSubEngine, ssoApiClient, ssoMiddlewares, res, adminUserGroupId}) {
    super(context);
    this._networkLayer = networkLayer;
    this._indexControllerService = new IndexControllerService(networkLayer, context);
    this._modelDefinitionRegister = modelDefinitionsRegister;
    this._pubSubEngine = pubSubEngine;
    this._res = res;
    this._adminUserGroupId = adminUserGroupId || env.get("ADMIN_USER_GROUP_ID").asString();

    if (ssoApiClient) {
      //
      // Add graph synchronization on account registering
      //
      ssoMiddlewares = (ssoMiddlewares || []).concat([new SynchronizeGraphAfterRegisterSSOMiddleware()]);

      this._ssoControllerService = new SSOControllerService({
        ssoApiClient,
        ssoMiddlewares,
        datastoreSession: this,
        ssoClientId: env.get("OAUTH_REALM_CLIENT_ID").required().asString(),
        ssoClientSecret: env.get("OAUTH_REALM_CLIENT_SECRET").required().asString(),
        ssoTokenEndpointUrl: env.get("OAUTH_TOKEN_URL").required().asString()
      })
    }
  }

  /**
   * @return {ModelDefinitionsRegister}
   */
  getModelDefinitionsRegister() {
    return this._modelDefinitionRegister;
  }

  /**
   * @return {PubSubEngine}
   */
  getPubSub() {
    return this._pubSubEngine;
  }

  /**
   * @return {NetworkLayerAMQP}
   */
  getNetworkLayer() {
    return this._networkLayer;
  }

  /**
   * @return {IndexControllerService}
   */
  getIndexService() {
    return this._indexControllerService;
  }

  /**
   * @return {e.Response}
   */
  getResponse() {
    return this._res;
  }

  /**
   * @return {SSOControllerService}
   */
  getSSOControllerService() {
    return this._ssoControllerService;
  }

  /**
   * @return {string}
   */
  getAdminUserGroupId() {
    return this._adminUserGroupId;
  }

  /**
   * Stringify a globalId object representation.
   *
   * @param {string} type - Object GraphQL type
   * @param {string} id   - Object id
   * @return {*}
   */
  stringifyGlobalId({type, id}) {
    if (env.get("USE_GRAPHQL_RELAY", "false").asBool()) {
      return toGlobalRelayId(type, id);
    } else {
      return id;
    }
  }

  /**
   * Parse a graphQL global id string and return it's object representation.
   *
   * @param globalId
   * @return {{id: *, type: string}}
   */
  parseGlobalId(globalId) {
    //
    // If Relay is activated, use default method.
    //
    if (env.get("USE_GRAPHQL_RELAY", "false").asBool()) {
      return fromGlobalRelayId(globalId);
      //
      // Otherwise, we assume that the id is constructed like so `[nodeType]:[dbInternalRepresentationalId]`
      //
    } else {
      const delimiterPos = globalId.indexOf(':');

      let type = globalId.substring(0, delimiterPos);

      return {
        type: this.getModelDefinitionsRegister().getGraphQLTypeForNodeType(type),
        id: globalId
      };
    }
  }

  /**
   * Extract an id from a globalId object representation
   * @param {string} globalId
   * @return {string}
   */
  extractIdFromGlobalId(globalId) {
    return (this.parseGlobalId(globalId) || {}).id;
  }

  /**
   * Extract a type from a globalId object representation
   * @param {string} globalId
   * @return {string}
   */
  extractTypeFromGlobalId(globalId) {
    return (this.parseGlobalId(globalId) || {}).type;
  }

  /**
   * Transform a "after" arg parameter into an offet.
   * @param {DataQueryArguments} args
   * @return {int}
   */
  getOffsetFromArgs(args = {}) {
    if (args.after) {
      return cursorToOffset(args.after) + 1;
    }

    return 0;
  }

  /**
   * Transform a "first" arg parameter into an offet.
   *
   * To make "hasNextPage" cursor property to work, add one element.
   *
   * @param {DataQueryArguments} args
   * @return {int}
   */
  getLimitFromArgs(args = {}) {
    if (args.first) {
      return args.first + 1;
    }

    return 0;
  }

  /**
   * @param {object} args
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   */
  getSortingsFromArgs({args, modelDefinition} = {}) {
    let sortings = args?.sortings;

    if (!sortings && args?.sortBy) {
      sortings = [{
        sortBy: args.sortBy,
        isSortDescending: args.isSortDescending
      }];
    }

    if (sortings) {
      return sortings.reduce((sortings, {sortBy, isSortDescending}) => {
        let propertyDefinition = modelDefinition.getProperty(sortBy);
        if (propertyDefinition) {
          sortings.push(new Sorting({
            propertyDefinition,
            descending: isSortDescending
          }));
        }
        return sortings;
      }, []);
    }
  }


  /**
   * @param {Model[]} objects
   * @param {DataQueryArguments} args
   * @return {object}
   */
  wrapObjectsIntoGraphQLConnection(objects, args) {
    return connectionFromArray(objects, args);
  }

  /**
   * Is index search disabled
   * @return {boolean}
   */
  isIndexDisabled() {
    return env.get("INDEX_DISABLED", "false").asBool();
  }

  /**
   * Get an instance of type index matcher.
   * @param nodeType
   * @return {DefaultIndexMatcher}
   */
  getIndexMatcherForType(nodeType) {
    return getModelDefinitionIndexMatcher(this._modelDefinitionRegister.getModelDefinitionForNodeType(nodeType), this._indexControllerService)
  }

  /**
   * Returns a model definition type given an URI
   * @param {string} uri
   * @return {typeof ModelDefinitionAbstract}
   */
  async getModelDefinitionForURI({uri}) {
  }

  /**
   * Check is a user is :
   * - Logged
   * - Not disabled
   * - Not unregistered
   *
   * @return {boolean}
   */
  async isLoggedUser() {
    // Check is there is a valid token session.
    if (!this.getContext().isAnonymous()) {
      // Ensure that current user session is not disabled or unregistered.
      let userAccount = await this.getLoggedUserAccount();

      if (userAccount) {
        let isDisabledOrUnregistered = userAccount.isUnregistered || userAccount.isDisabled;

        // If user session is disabled or unregistered, logout it !
        if (isDisabledOrUnregistered) {
          await this.getSSOControllerService().logout();
        }

        return !isDisabledOrUnregistered
      }
    }

    return false;
  }

  /**
   * Get my id.
   * @return {string}
   */
  getLoggedUserId() {
    let user = this.getContext().getUser();

    if (user) {
      return user.getId();
    }
  }

  /**
   * Get my username.
   * @return {string}
   */
  getLoggedUsername() {
    let user = this.getContext().getUser();

    if (user) {
      return user.getUsername();
    }
  }

  /**
   * This returns the logged user person related model definition
   *
   * This is usefull if
   *
   * @return {typeof ModelDefinitionAbstract}
   */
  getLoggedUserPersonModelDefinition() {
    return this._modelDefinitionRegister.getModelDefinitionForLoggedUserPerson() || PersonDefinition
  }

  /**
   * Get me as person
   * @return {Person}
   */
  async getLoggedUserPerson() {
    // Get the logged user person session cache if it exists..
    if (this._cache.loggedUserPerson) {
      return this._cache.loggedUserPerson;
    }

    let user = this.getContext().getUser();
    let person;

    if (user) {

      let personId = user.getAttribute("personId");

      // If personId information is stored in the SSO, use it to speedup request.
      if (personId) {
        person = await this.getObject(this.getLoggedUserPersonModelDefinition(), personId, this.getContext().getLang());
      }

      // If not, or personId corrupted, fallback to regular request.
      if (!person) {
        person = await this.getLinkedObjectFor({
          object: (await this.getLoggedUserAccount()),
          linkDefinition: UserAccountDefinition.getLink("hasPerson")
        })
      }

      if (person) {
        // Save the logged user person in the cache.
        this._cache.loggedUserPerson = person;

        return person;
      } else {
        // Clear the actual cookie that appears to be corrupted.
        if (this.getResponse()) {
          SSOUser.clearCookie(this.getResponse());
        }

        throw new I18nError(`Person related to UserAccount "${this.getContext().getUser().getId()}" not exists in graph...`, "USER_NOT_EXISTS_IN_GRAPH")
      }
    }
  }

  /**
   * Get me as person
   * @return {UserAccount}
   */
  async getLoggedUserAccount() {
    // Get the logged user userAccount in session cache if it exists..
    if (this._cache.loggedUserAccount) {
      return this._cache.loggedUserAccount;
    }

    let user = this.getContext().getUser();

    if (user) {
      let userAccount = await this.getUserAccountForUser(user);

      // Save the logged user userAccount in the cache.
      this._cache.loggedUserAccount = userAccount;

      return userAccount;
    }
  }

  /**
   *
   * @param {SSOUser} user
   * @return {UserAccount}
   */
  async getUserAccountForUser(user) {
    let userAccounts = await this.getObjects(UserAccountDefinition, {
      qs: `/^${user.getId()}$/i`
    });

    if (userAccounts.length > 0) {
      return userAccounts[0];
    } else {
      throw new I18nError(`UserAccount with userId = "${user.getId()}" not exists in graph...`, "USER_NOT_EXISTS_IN_GRAPH")
    }
  }

  /**
   * This method checks if a logged user is in UserGroup specified by a userGroupId
   * @param {string} userGroupId
   * @return {boolean}
   */
  async isLoggedUserInGroup({userGroupId}) {
    return false;
  }

  /**
   * This method checks if a logged user is allowed to update an object
   *
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {Model} object
   * @return {boolean}
   */
  async isLoggedUserAllowedToUpdateObject({modelDefinition, object}) {
    return true;
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param {string} lang
   * @param {boolean} [returnAsLocalizedLabel=false]
   * @param {boolean} [returnFirstOneIfNotExistForLang=true]
   * @return {string}
   */
  async getLocalizedLabelFor(object, labelDefinition, lang, returnAsLocalizedLabel = false, returnFirstOneIfNotExistForLang = true) {
    let pathInIndex = labelDefinition.getPathInIndex();

    if (!lang) {
      lang = 'fr';
    }

    // So if the node comes from index, get quick label values without requesting the graphstore
    if (object?.[pathInIndex] && Array.isArray(object[pathInIndex])) {
      let label = object[pathInIndex].find(label => label.lang === lang);

      if (!label && returnFirstOneIfNotExistForLang) {
        label = object[pathInIndex][0];
      }

      if (label) {
        let {value, lang, uri, id} = label;

        return returnAsLocalizedLabel ? createModelFromNode(labelDefinition.getModelClass(), createNode(labelDefinition.getNodeType(), id, {
          value,
          lang
        }, null, uri)) : value;
      }
    }
  }


  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param lang
   * @return {boolean}
   */
  isLocalizedLabelTranslatedFor(object, labelDefinition, lang) {
    // TODO implement this.
    return false;
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {LinkPath} [linkPath]
   * @param {DataQueryArguments} [args]
   * @return {Model|Model[]|null}
   */
  async getLinkedObjectFor({object, linkDefinition, linkPath, args}) {
    if (this.isIndexDisabled()) {
      return null;
    }

    let pathInIndex = linkDefinition.getPathInIndex();
    let ModelClass = linkDefinition.getRelatedModelDefinition().getModelClass();

    if (typeof pathInIndex === "string" && object[pathInIndex]) {
      return this._indexControllerService.getLinkedDocuments(object, pathInIndex, ModelClass);
    }

    if (pathInIndex instanceof UseIndexMatcherOfDefinition) {
      let indexMatcher = getModelDefinitionIndexMatcher(pathInIndex.getUseIndexMatcherOf(), this._indexControllerService);

      if (indexMatcher) {
        args.filters = (args.filters || []).concat([`${pathInIndex.getFilterName()}:${object.id}`]);
        return indexMatcher.search(args);
      }
    }
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @return {Model[]}
   */
  async getLinkedObjectsFor(object, linkDefinition, args = {}, fragmentDefinitions = []) {
    return this.getLinkedObjectFor({object, linkDefinition, args, fragmentDefinitions})
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @return {number}
   */
  async getLinkedObjectsCountFor({object, linkDefinition, args}) {
    if (!args) {
      args = {};
    }

    if (this.isIndexDisabled()) {
      return null;
    }

    let ModelClass = linkDefinition.getRelatedModelDefinition().getModelClass();
    let pathInIndex = linkDefinition.getPathInIndex();
    let pathInIndexForCount = linkDefinition.getPathInIndexForCount();

    if (pathInIndexForCount && typeof pathInIndexForCount === "string" && object[pathInIndexForCount] && (!args.filters || args.filters.length === 0)) {
      return object[pathInIndexForCount].length;
    }

    if (typeof pathInIndex === "string" && object[pathInIndex]) {
      return (this._indexControllerService.getLinkedDocuments(object, pathInIndex, ModelClass) || []).length;
    }

    if (pathInIndex instanceof UseIndexMatcherOfDefinition) {
      let indexMatcher = getModelDefinitionIndexMatcher(pathInIndex.getUseIndexMatcherOf(), this._indexControllerService);

      if (indexMatcher) {
        args.filters = (args.filters || []).concat([`${pathInIndex.getFilterName()}:${object.id}`]);
        return indexMatcher.search({...args, justCount: true});
      }
    }
  }

  /**
   * Is object exists
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @return {boolean}
   */
  async isObjectExists(modelDefinition, objectId) {
    if (this.isIndexDisabled()) {
      return null;
    }

    return !!(await this.getObject(modelDefinition, objectId));
  }

  /**
   * Is object has a link with another one defined by it's id ?
   *
   * @param {Model} object
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {LinkDefinition|LinkDefinition[]} [linkDefinitions]
   * @param {string} targetId
   * @return {boolean}
   */
  async isObjectLinkedToTargetId({object, modelDefinition, linkDefinitions, targetId}) {
    let linkDefinition = Array.isArray(linkDefinitions) ? linkDefinitions[0] : linkDefinitions;

    let linkedObjects = await this.getLinkedObjectFor({
      object, linkDefinition
    });

    return linkedObjects.find(({id}) => targetId === id);
  }

  /**
   * Get object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {string} [lang]
   * @param {FragmentDefinition[]} [fragmentDefinitions]
   * @return {Model}
   */
  async getObject(modelDefinition, objectId, lang, fragmentDefinitions) {
    if (this.isIndexDisabled()) {
      return null;
    }

    let object;

    if (modelDefinition.getIndexType()) {
      try {
        object = await this._indexControllerService.getNode(objectId, modelDefinition.getIndexType(), modelDefinition.getModelClass());
      } catch (e) {
        logWarning(`Index type is defined in ${typeof modelDefinition} but doesn't seem to be created.`)
      }
    }

    return object;
  }

  /**
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} [linkPath]
   * @param {DataQueryArguments} [args]
   * @param {FragmentDefinition[]} fragmentDefinitions
   * @return {Model|Model[]|null} The last LinkStep link definition isPlural property controls the return type.
   */
  async getObjectsFilteredByLinkPath({modelDefinition, linkPath, args, fragmentDefinitions}) {
    // TODO : Implement here the index LinkPath comprehension.
    return null;
  }

  /**
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} [linkPath]
   * @param {DataQueryArguments} [args]
   * @return {Model|Model[]|null} The last LinkStep link definition isPlural property controls the return type.
   */
  async getObjectsCountFilteredByLinkPath({modelDefinition, linkPath, args}) {
    // TODO : Implement here the index LinkPath comprehension.
    return null;
  }

  /**
   * @param {ModelDefinitionAbstract} modelDefinition
   * @param {LinkPath} [linkPath]
   * @param {DataQueryArguments} [args]
   * @param {FragmentDefinition[]} fragmentDefinitions
   * @return {Model|Model[]|null} The last LinkStep link definition isPlural property controls the return type.
   */
  async getObjectFilteredByLinkPath({modelDefinition, linkPath, fragmentDefinitions}) {
    // TODO : Implement here the index LinkPath comprehension.
    return null;
  }

  /**
   * Get object from its global ID
   * @param {string} globalId
   * @param {string} lang
   * @return {Model}
   */
  async getObjectFromGlobalId(globalId, lang) {
    let {id, type} = this.parseGlobalId(globalId);

    return this.getObject(this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(type), id, lang);
  }

  /**
   * Creates an object
   *
   * @param graphQLType
   * @param {string} graphQLType - Type of object
   * @param {Link[]} links -  List of links
   * @param {object} [objectInput] - Object properties
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {boolean} [waitForIndexSyncing] - Should wait indexation task.
   *
   * @return {Model}
   */
  async createObject({graphQLType, links, objectInput, lang, uri, waitForIndexSyncing}) {
    throw new Error(`You must implement ${this.constructor.name}::createObject()`);
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @param {FragmentDefinition[]} fragmentDefinitions
   * @return {Model[]}
   */
  async getObjects(modelDefinition, args, fragmentDefinitions) {
    if (this.isIndexDisabled()) {
      return null;
    }

    /** @type {typeof DefaultIndexMatcher} */
    let IndexMatcherClass = modelDefinition.getIndexMatcher();

    if (IndexMatcherClass) {
      let indexMatcher = new IndexMatcherClass({
        modelDefinition,
        indexService: this._indexControllerService
      });

      return indexMatcher.search(args);
    }
  }

  /**
   * Count objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @return {Model[]}
   */
  async getObjectsCount(modelDefinition, args) {
    if (this.isIndexDisabled()) {
      return null;
    }

    /** @type {typeof DefaultIndexMatcher} */
    let IndexMatcherClass = modelDefinition.getIndexMatcher();

    if (IndexMatcherClass) {
      let indexMatcher = new IndexMatcherClass({
        modelDefinition,
        indexService: this._indexControllerService
      });

      return indexMatcher.search(args).length;
    }
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @return {IndexSyncQueryResultAggregation}
   */
  async getObjectsBuckets(modelDefinition, args) {
    if (this.isIndexDisabled()) {
      return null;
    }

    let indexMatcher = modelDefinition.getIndexMatcher();

    if (indexMatcher) {
      return indexMatcher.aggregate(args);
    }
  }


  /**
   * Update object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {object} updatingProps
   * @param {Link[]} links -  List of links
   * @param {string|null} lang
   */
  async updateObject({modelDefinition, objectId, updatingProps, lang}) {
    throw new Error(`You must implement ${this.constructor.name}::updateObject()`);
  }

  /**
   * Remove object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param permanentRemoval
   */
  async removeObject(modelDefinition, objectId, permanentRemoval = false) {
    throw new Error(`You must implement ${this.constructor.name}::removeObject()`);
  }

  /**
   * Create edge between two nodes and re-index them
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId) {
    throw new Error(`You must implement ${this.constructor.name}::createEdge()`);
  }

  /**
   * Create edges between a node and target nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string[]} targetIds
   *
   * @return {Model}
   */
  async createEdges(modelDefinition, objectId, linkDefinition, targetIds) {
    throw new Error(`You must implement ${this.constructor.name}::createEdges()`);
  }

  /**
   * Remove edge between nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async removeEdge(modelDefinition, objectId, linkDefinition, targetId) {
    throw new Error(`You must implement ${this.constructor.name}::removeEdge()`);
  }

  /**
   * Get object attached value
   * @param {Model} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} valueName
   * @param {boolean} returnValueObject
   * @return {string|object}
   */
  async getObjectValue(object, modelDefinition, valueName, returnValueObject = false) {
    let values = await this.getLinkedObjectsFor(object, modelDefinition.getLink('values'), {first: 100});

    if (values) {
      let value = values.find(({name}) => name === valueName);

      if (value) {
        return returnValueObject ? value : value[`${value.valueType}Value`];
      }
    }
  }

  /**
   * Set object attached value
   * @param {Model} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} valueName
   * @param {string} valueType
   * @param {string} valueValue
   * @return {Value}
   */
  async setObjectValue(modelDefinition, object, valueName, valueType, valueValue) {
    throw new Error(`You must implement ${this.constructor.name}::setObjectValue()`);
  }

  /**
   * Parse DataQueryArguments.filters property into a list of LinkFilter, LabelFilter and LiteralFilter
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string[]} filters
   * @return {{linkFilters: LinkFilter[], labelFilters: LiteralFilter[], literalFilters: LabelFilter[]}}
   */
  parseRawFilters(modelDefinition, filters = []) {
    /** @type {LinkFilter[]} */
    let linkFilters = [];
    /** @type {LiteralFilter[]} */
    let literalFilters = [];
    /** @type {LabelFilter[]} */
    let labelFilters = [];

    filters.map(filter => {
      let splitAt = filter.indexOf(':');
      if (splitAt > 0) {
        let field = filter.slice(0, splitAt);
        let value = filter.slice(splitAt + 1);

        let literal = modelDefinition.getLiteral(field);

        if (literal) {
          literalFilters.push({
            literalDefinition: literal,
            value
          })
        }

        let label = modelDefinition.getLabel(field);

        if (label) {
          labelFilters.push({
            labelDefinition: label,
            value
          })
        }

        let link = modelDefinition.getLink(field);

        if (link) {
          // For now link filters are 1-step deep.
          // TODO: Improve this.
          return linkFilters.push({
            linkDefinition: link,
            id: value
          });
        }
      }
    });

    return {linkFilters, labelFilters, literalFilters};
  }
}