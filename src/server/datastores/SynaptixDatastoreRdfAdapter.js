/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SynaptixDatastoreAdapter from "./SynaptixDatastoreAdapter";
import SynaptixDatastoreRdfSession from "./SynaptixDatastoreRdfSession";
import env from "env-var";

/**
 * This is the default DatastoreAdpater used for a RDF storage system.
 */
export default class SynaptixDatastoreRdfAdapter extends SynaptixDatastoreAdapter {
  /**
   * @param {object} [schemaNamespaceMapping=JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING)] - Project namespaces mapping. If not passed, should be defined in `SCHEMA_NAMESPACE_MAPPING` env variable.
   * @param {string} [nodesNamespaceURI=process.env.NODES_NAMESPACE_URI]      - The URI namespace used to create new node (aka: to generate new URIs). If not passed, should be defined in `NODES_NAMESPACE_URI` env variable.
   * @param {string} [nodesPrefix=process.env.NODES_PREFIX]           - The prefix used for nodesNamespaceURI. If not passed, should be defined in `NODES_PREFIX` env variable.
   * @param {function} [nodesTypeFormatter] - This function tweaks a node type into a URI prefix-styled format
   * @param {string} [insertingNamedGraphId] - The URI of the global named graph where data are inserted
   * @param args
   */
  constructor({schemaNamespaceMapping, nodesNamespaceURI, nodesPrefix, nodesTypeFormatter, insertingNamedGraphId, ...args}) {
    super({
      SynaptixDatastoreSessionClass: SynaptixDatastoreRdfSession,
      ...args,
    });

    this._schemaNamespaceMapping = schemaNamespaceMapping || env.get("SCHEMA_NAMESPACE_MAPPING").required().asJson();
    this._nodesNamespaceURI = nodesNamespaceURI || env.get("NODES_NAMESPACE_URI").required().asString();
    this._nodesPrefix = nodesPrefix ||  env.get("NODES_PREFIX").required().asString();
    this._nodesTypeFormatter = nodesTypeFormatter;
    this._insertingNamedGraphId = insertingNamedGraphId || env.get("NODES_NAMED_GRAPH").asString() ;
  }

  /**
   * Returns a connection driver with context
   * @param {GraphQLContext} context GraphQL context object
   * @param {e.Response} [res] - Express response object
   * @returns {SynaptixDatastoreSession};
   */
  getSession({context, res} = {}) {
    return new (this._SynaptixDatastoreSessionClass)({
      context,
      networkLayer: this._networkLayer,
      modelDefinitionsRegister: this._modelDefinitionsRegister,
      pubSubEngine: this._pubSubEngine,
      ssoApiClient: this._ssoApiClient,
      ssoMiddlewares: this._ssoMiddlewares,
      graphMiddlewares: this._graphMiddlewares,
      adminUserGroupId: this._adminUserGroupId,
      schemaNamespaceMapping: this._schemaNamespaceMapping,
      nodesNamespaceURI: this._nodesNamespaceURI,
      nodesPrefix: this._nodesPrefix,
      nodesTypeFormatter: this._nodesTypeFormatter,
      insertingNamedGraphId: this._insertingNamedGraphId,
      res
    });
  }
}
