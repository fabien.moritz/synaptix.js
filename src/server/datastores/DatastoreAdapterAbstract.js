/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

export default class DatastoreAdapterAbstract {
  constructor() {
    if (new.target === DatastoreAdapterAbstract) {
      throw new TypeError("Cannot construct Abstract instances directly");
    }

    if (this.init === DatastoreAdapterAbstract.prototype.init) {
      throw new TypeError(`You must implement ${this.constructor.name}::init()`);
    }

    if (this.getSession === DatastoreAdapterAbstract.prototype.getSession) {
      throw new TypeError(`You must implement ${this.constructor.name}::getSession()`);
    }
  }

  /* istanbul ignore next */

  /**
   /**
   * Initialisation method called once on application start.
   */
  init() {
  }

  /* istanbul ignore next */

  /**
   * Returns a connection driver with context
   * @param {GraphQLContext} context - GraphQL context object
   * @param {e.Response} res - express response object
   *
   * @returns {DatastoreSessionAbstract};
   */
  getSession({context, res}) {
  }
}
