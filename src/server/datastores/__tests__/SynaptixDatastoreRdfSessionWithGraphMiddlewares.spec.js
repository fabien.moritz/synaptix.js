/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import SynaptixDatastoreRdfSession from '../SynaptixDatastoreRdfSession';
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";
import {PubSub} from "graphql-subscriptions";
import PersonDefinition from "../../datamodel/ontologies/mnx-agent/definitions/PersonDefinition";
import OrganizationDefinition from "../../datamodel/ontologies/mnx-agent/definitions/OrganizationDefinition";
import AgentDefinition from "../../datamodel/ontologies/mnx-agent/definitions/AgentDefinition";
import Person from "../../datamodel/ontologies-legacy/models/foaf/Person";
import generateId from "nanoid/generate";
import {MnxActionGraphMiddleware} from "../../datamodules/middlewares/graph/MnxActionGraphMiddleware";
import SSOUser from "../../datamodules/drivers/sso/models/SSOUser";

jest.mock("nanoid/generate");
jest.mock("dayjs", () => jest.fn((...args) => ({
  format: () => "now"
})));

let userAccountUri = "test:userAccount/1234";
let modelDefinitionsRegister = new ModelDefinitionsRegister([AgentDefinition, PersonDefinition, OrganizationDefinition]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();
let context = new GraphQLContext({
  user: new SSOUser({
    user: {
      id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
      username: 'test@domain.com',
      firstName: 'John',
      lastName: 'Doe'
    }
  }), lang: 'fr'
});

let session = new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine,
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  graphMiddlewares: [new MnxActionGraphMiddleware()]
});

let spyOnIndexServicefulltextSearch = jest.spyOn(session.getIndexService(), 'fulltextSearch');
let spyOnGraphControllerInsertTriples = jest.spyOn(session.getGraphControllerService().getGraphControllerPublisher(), 'insertTriples');
let spyOnGraphControllerUpdateTriples = jest.spyOn(session.getGraphControllerService().getGraphControllerPublisher(), 'updateTriples');
spyOnIndexServicefulltextSearch.mockImplementation(async () => {
  return {};
});
jest.spyOn(session, 'isIndexDisabled').mockImplementation(() => true);
jest.spyOn(session, 'getLoggedUserAccount').mockImplementation(() => ({id: userAccountUri}));

describe('SynaptixDatastoreRdfSessionWithGraphMiddlewares', () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it("should create a graph with actions attributed to a userAccount", async () => {
    spyOnGraphControllerInsertTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.createObject({
      graphQLType: "Person",
      objectInput: {
        firstName: "Derek",
        lastName: "Devel",
        phoneInputs: [{
          label: "Pro",
          number: "0600000000"
        }, {
          label: "Mobile",
          number: "0400000000"
        }, {
          id: "test:phone/1233456"
        }],
        userAccountInput: {
          userId: "123456",
          username: "derek@mnemotix.com"
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerInsertTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:person/1",
            type: "mnx:Person",
          },
        ],
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "Derek";
  foaf:lastName "Devel".
 <http://ns.mnemotix.com/instances/phone/2> rdf:type mnx:Phone.
 <http://ns.mnemotix.com/instances/creation/3> rdf:type mnx:Creation;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime "now"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/phone/2>.
 <http://ns.mnemotix.com/instances/phone/2> rdfs:label "Pro";
  mnx:number "0600000000".
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/2>.
 <http://ns.mnemotix.com/instances/phone/4> rdf:type mnx:Phone.
 <http://ns.mnemotix.com/instances/creation/3> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/phone/4>.
 <http://ns.mnemotix.com/instances/phone/4> rdfs:label "Mobile";
  mnx:number "0400000000".
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/4>.
 <http://ns.mnemotix.com/instances/update/5> rdf:type mnx:Update;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime "now"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/phone/1233456>.
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/1233456>.
 <http://ns.mnemotix.com/instances/useraccount/6> rdf:type mnx:UserAccount.
 <http://ns.mnemotix.com/instances/creation/3> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/useraccount/6>.
 <http://ns.mnemotix.com/instances/useraccount/6> mnx:userId "123456";
  mnx:username "derek@mnemotix.com".
 <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/useraccount/6>.
 <http://ns.mnemotix.com/instances/creation/3> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it("should update an object with actions attributed to a userAccount ", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: PersonDefinition,
      objectId: "test:person/1",
      updatingProps: {
        firstName: "Derek",
        lastName: "Devel",
        phoneInputs: [{
          label: "Pro",
          number: "0600000000"
        }, {
          label: "Mobile",
          number: "0400000000"
        }, {
          id: "test:phone/1233456"
        }],
        userAccountInput: {
          userId: "123456",
          username: "derek@mnemotix.com"
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: [
        ],
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
DELETE {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName;
  foaf:lastName ?lastName;
  foaf:account ?hasUserAccount.
}
INSERT {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName "Derek";
  foaf:lastName "Devel".
 <http://ns.mnemotix.com/instances/phone/1> rdf:type mnx:Phone.
 <http://ns.mnemotix.com/instances/creation/2> rdf:type mnx:Creation;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime "now"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/phone/1>.
 <http://ns.mnemotix.com/instances/phone/1> rdfs:label "Pro";
  mnx:number "0600000000".
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/1>.
 <http://ns.mnemotix.com/instances/phone/3> rdf:type mnx:Phone.
 <http://ns.mnemotix.com/instances/creation/2> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/phone/3>.
 <http://ns.mnemotix.com/instances/phone/3> rdfs:label "Mobile";
  mnx:number "0400000000".
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/3>.
 <http://ns.mnemotix.com/instances/update/4> rdf:type mnx:Update;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/userAccount/1234>;
  prov:startedAtTime "now"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/phone/1233456>.
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/1233456>.
 <http://ns.mnemotix.com/instances/useraccount/5> rdf:type mnx:UserAccount.
 <http://ns.mnemotix.com/instances/creation/2> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/useraccount/5>.
 <http://ns.mnemotix.com/instances/useraccount/5> mnx:userId "123456";
  mnx:username "derek@mnemotix.com".
 <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/useraccount/5>.
 <http://ns.mnemotix.com/instances/update/4> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:account ?hasUserAccount. }
}`
    });
  });
});




