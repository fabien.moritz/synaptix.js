/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import SynaptixDatastoreRdfSession from '../SynaptixDatastoreRdfSession';
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";
import {PubSub} from "graphql-subscriptions";
import PersonDefinition from "../../datamodel/ontologies/mnx-agent/definitions/PersonDefinition";
import OrganizationDefinition from "../../datamodel/ontologies/mnx-agent/definitions/OrganizationDefinition";
import EmailAccountDefinition from "../../datamodel/ontologies/mnx-agent/definitions/EmailAccountDefinition";
import AgentDefinition from "../../datamodel/ontologies/mnx-agent/definitions/AgentDefinition";
import Person from "../../datamodel/ontologies-legacy/models/foaf/Person";
import generateId from "nanoid/generate";
import AffiliationDefinition from "../../datamodel/ontologies/mnx-agent/definitions/AffiliationDefinition";
import UserAccountDefinition from "../../datamodel/ontologies/mnx-agent/definitions/UserAccountDefinition";
import {Model} from "../../datamodel/toolkit/models/Model";
import FooDefinitionMock from "../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";
import {LinkPath} from "../../datamodel/toolkit/utils/LinkPath";
import BazDefinitionMock from "../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
import BarDefinitionMock from "../../datamodel/__tests__/mocks/definitions/BarDefinitionMock";

jest.mock("nanoid/generate");


let modelDefinitionsRegister = new ModelDefinitionsRegister([AgentDefinition, PersonDefinition, OrganizationDefinition]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();
let context = new GraphQLContext({
  anonymous: true, lang: 'fr'
});

let session = new SynaptixDatastoreRdfSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine,
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  nodesTypeFormatter: (type) => `prefix-for-testing-${type}`
});

let spyOnIndexServicefulltextSearch = jest.spyOn(session.getIndexService(), 'fulltextSearch');
let spyOnGraphControllerConstruct = jest.spyOn(session.getGraphControllerService().getGraphControllerPublisher(), 'construct');
let spyOnGraphControllerInsertTriples = jest.spyOn(session.getGraphControllerService().getGraphControllerPublisher(), 'insertTriples');
let spyOnGraphControllerUpdateTriples = jest.spyOn(session.getGraphControllerService().getGraphControllerPublisher(), 'updateTriples');

jest.spyOn(session, 'isIndexDisabled').mockImplementation(() => true);

spyOnIndexServicefulltextSearch.mockImplementation(async () => {
  return {};
});

describe('SynaptixDatastoreRdfSession', () => {
  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });

  it('should play well with global id reprensentation extraction and stringification', async () => {
    expect(session.parseGlobalId("mnx:prefix-for-testing-Organization/43433432")).toEqual({
      id: "mnx:prefix-for-testing-Organization/43433432",
      type: "Organization"
    });
  });

  it("Should get non reversed linked nodes", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:email/1",
      "@context": {}
    }));

    // This is a rdfObjectProperty
    let ac = await session.getLinkedObjectFor({
      object: new Person('test:prefix-for-testing-Person/1', 'test:prefix-for-testing-Person/1', {}),
      linkDefinition: PersonDefinition.getLink('hasEmailAccount'),
      bypassIndex: true
    });


    expect(spyOnGraphControllerConstruct).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
 ?uri rdf:type mnx:EmailAccount.
 ?uri mnx:createdAt ?startedAtTime.
 ?uri mnx:accountName ?accountName.
 ?uri mnx:email ?email.
 ?uri mnx:isVerified ?isVerified.
 ?uri mnx:isMainEmail ?isMainEmail.
}
WHERE {
 ?uri rdf:type mnx:EmailAccount.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account ?uri.
 OPTIONAL {
  ?hasCreationAction_0 prov:wasGeneratedBy ?uri;
   rdf:type mnx:Creation;
   prov:startedAtTime ?startedAtTime.
 }
 OPTIONAL { ?uri mnx:accountName ?accountName. }
 OPTIONAL { ?uri mnx:email ?email. }
 OPTIONAL { ?uri mnx:isVerified ?isVerified. }
 OPTIONAL { ?uri mnx:isMainEmail ?isMainEmail. }
}`
    });
  });

  it("Should get reversed linked nodes", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:email/1",
      "@context": {}
    }));

    // This is a rdfReversedObjectProperty
    let ac = await session.getLinkedObjectFor({
      object: new Model('test:prefix-for-testing-UserAccount/1', 'test:prefix-for-testing-UserAccount/1', {}, "UserAccount"),
      linkDefinition: UserAccountDefinition.getLink('hasPerson'),
      bypassIndex: true
    });

    expect(spyOnGraphControllerConstruct).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
 ?uri rdf:type mnx:Person.
 ?uri mnx:createdAt ?startedAtTime.
 ?uri mnx:avatar ?avatar.
 ?uri foaf:firstName ?firstName.
 ?uri foaf:lastName ?lastName.
 ?uri foaf:nick ?nickName.
 ?uri mnx:maidenName ?maidenName.
 ?uri foaf:gender ?gender.
 ?uri foaf:birthday ?birthday.
 ?uri mnx:bio ?bio.
 ?uri mnx:shortBio ?shortBio.
}
WHERE {
 ?uri rdf:type mnx:Person.
 ?uri foaf:account <http://ns.mnemotix.com/instances/prefix-for-testing-UserAccount/1>.
 OPTIONAL {
  ?hasCreationAction_0 prov:wasGeneratedBy ?uri;
   rdf:type mnx:Creation;
   prov:startedAtTime ?startedAtTime.
 }
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri foaf:firstName ?firstName. }
 OPTIONAL { ?uri foaf:lastName ?lastName. }
 OPTIONAL { ?uri foaf:nick ?nickName. }
 OPTIONAL { ?uri mnx:maidenName ?maidenName. }
 OPTIONAL { ?uri foaf:gender ?gender. }
 OPTIONAL { ?uri foaf:birthday ?birthday. }
 OPTIONAL { ?uri mnx:bio ?bio. }
 OPTIONAL { ?uri mnx:shortBio ?shortBio. }
}`
    });
  });

  it("Should get linked nodes with link path", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:email/1",
      "@context": {}
    }));

    // This is a rdfObjectProperty
    await session.getObjectsFilteredByLinkPath({
      modelDefinition: BarDefinitionMock,
      bypassIndex: true,
      linkPath: new LinkPath()
        .step({linkDefinition: BarDefinitionMock.getLink("hasBaz")})
        .step({linkDefinition: BazDefinitionMock.getLink("hasFoo"), targetId: 'test:foo/1'})
    });


    expect(spyOnGraphControllerConstruct).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:barLiteral1 ?barLiteral1.
 ?uri mnx:barLiteral2 ?barLiteral2.
 ?uri mnx:barLiteral3 ?barLiteral3.
 ?uri mnx:barLabel1 ?barLabel1.
 ?uri mnx:barLabel2 ?barLabel2.
}
WHERE {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:barLiteral1 ?barLiteral1.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo <http://ns.mnemotix.com/instances/foo/1>.
 <http://ns.mnemotix.com/instances/foo/1> rdf:type mnx:Foo.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:barLiteral2 ?barLiteral2. }
 OPTIONAL { ?uri mnx:barLiteral3 ?barLiteral3. }
 OPTIONAL { ?uri mnx:barLabel1 ?barLabel1. }
 OPTIONAL { ?uri mnx:barLabel2 ?barLabel2. }
}`
    });
  });

  it("Should get linked nodes with filters", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:email/1",
      "@context": {}
    }));

    let ac = await session.getObjects(PersonDefinition, {
      linkFilters: [{
        rdfFilterFunction: {
          generateRdfFilterDefinition: ({literalVariable}) => ({
            rdfFunction: "http://ns.mnemotix.com/onto/isEqual",
            args: [
              literalVariable,
              EmailAccountDefinition.getLiteral("email").toSparqlValue("john.doe@mnemotix.com"),
            ]

          }),
          literalDefinition: EmailAccountDefinition.getLiteral("email"),
        },
        linkDefinition: PersonDefinition.getLink("hasEmailAccount")
      }]
    });


    expect(spyOnGraphControllerConstruct).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
 ?uri rdf:type mnx:Person.
 ?uri mnx:createdAt ?startedAtTime.
 ?uri mnx:avatar ?avatar.
 ?uri foaf:firstName ?firstName.
 ?uri foaf:lastName ?lastName.
 ?uri foaf:nick ?nickName.
 ?uri mnx:maidenName ?maidenName.
 ?uri foaf:gender ?gender.
 ?uri foaf:birthday ?birthday.
 ?uri mnx:bio ?bio.
 ?uri mnx:shortBio ?shortBio.
}
WHERE {
 ?uri rdf:type mnx:Person.
 ?uri foaf:account ?hasEmailAccount.
 ?hasEmailAccount mnx:email ?email.
 OPTIONAL {
  ?hasCreationAction_0 prov:wasGeneratedBy ?uri;
   rdf:type mnx:Creation;
   prov:startedAtTime ?startedAtTime.
 }
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri foaf:firstName ?firstName. }
 OPTIONAL { ?uri foaf:lastName ?lastName. }
 OPTIONAL { ?uri foaf:nick ?nickName. }
 OPTIONAL { ?uri mnx:maidenName ?maidenName. }
 OPTIONAL { ?uri foaf:gender ?gender. }
 OPTIONAL { ?uri foaf:birthday ?birthday. }
 OPTIONAL { ?uri mnx:bio ?bio. }
 OPTIONAL { ?uri mnx:shortBio ?shortBio. }
 FILTER(mnx:isEqual(?email, "john.doe@mnemotix.com"))
}`
    });
  });

  it("should get model definition given an URI", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:email/1",
      "@type": "mnx:Person",
      "@context": {}
    }));

    let personDefinition = await session.getModelDefinitionForURI({uri: "mnx:prefix-for-testing-Person/1"});

    expect(personDefinition).toBe(PersonDefinition);
  });

  it("should get the more specific model definition given an URI", async () => {
    spyOnGraphControllerConstruct.mockImplementation(() => ({
      "@id": "test:email/1",
      "@type": ["mnx:Person", "mnx:Agent", "owl:Thing"],
      "@context": {}
    }));

    let personDefinition = await session.getModelDefinitionForURI({uri: "mnx:prefix-for-testing-Person/1"});

    expect(personDefinition).toBe(PersonDefinition);
  });

  it("should extract links from object input", () => {
    expect(session._extractLinksFromObjectInput({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "Derek",
        lastName: "Devel",
        phoneInputs: [{
          label: "Pro",
          number: "0600000000"
        }, {
          label: "Mobile",
          number: "0400000000"
        }, {
          id: "mnxd:prefix-for-testing-Phone/1233456"
        }],
        userAccountInput: {
          userId: "123456",
          username: "derek@mnemotix.com"
        }
      },
      links: [
        PersonDefinition.getLink("hasAffiliation").generateLinkFromTargetId("mnxd:affiliation/1234")
      ]
    })).toEqual({
      links: [
        PersonDefinition.getLink("hasAffiliation").generateLinkFromTargetId("mnxd:affiliation/1234"),
        PersonDefinition.getLink("hasPhone").generateLinkFromTargetProps({
          targetObjectInput: {
            label: "Pro",
            number: "0600000000"
          }
        }),
        PersonDefinition.getLink("hasPhone").generateLinkFromTargetProps({
          targetObjectInput: {
            label: "Mobile",
            number: "0400000000"
          }
        }),
        PersonDefinition.getLink("hasPhone").generateLinkFromTargetId("mnxd:prefix-for-testing-Phone/1233456"),
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            userId: "123456",
            username: "derek@mnemotix.com"
          }
        })
      ],
      objectInput: {
        firstName: "Derek",
        lastName: "Devel"
      }
    });
  });

  it("should extract nested links from object input", () => {
    expect(session._extractLinksFromObjectInput({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "Derek",
        affiliationInputs: [{
          role: "The boss",
          startDate: "Now",
          orgInput:{
            name: "Mnx"
          }
        }]
      }
    })).toEqual({
      links: [
        PersonDefinition.getLink("hasAffiliation").generateLinkFromTargetProps({
          targetObjectInput: {
            role: "The boss",
            startDate: "Now"
          },
          targetNestedLinks: [
            AffiliationDefinition.getLink("hasOrg").generateLinkFromTargetProps({
              targetObjectInput: {
                name: "Mnx"
              }
            })
          ]
        })
      ],
      objectInput: {
        "firstName": "Derek"
      }
    });
  });

  it("should create a graph", async () => {
    spyOnGraphControllerInsertTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.createObject({
      graphQLType: "Person",
      objectInput: {
        firstName: "Derek",
        lastName: "Devel",
        phoneInputs: [{
          label: "Pro",
          number: "0600000000"
        }, {
          label: "Mobile",
          number: "0400000000"
        }, {
          id: "mnxd:prefix-for-testing-Phone/1233456"
        }],
        userAccountInput: {
          userId: "123456",
          username: "derek@mnemotix.com"
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerInsertTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: [
          {
            id: "test:prefix-for-testing-Person/1",
            type: "mnx:Person",
          },
        ],
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> rdf:type mnx:Person;
  foaf:firstName "Derek";
  foaf:lastName "Devel".
 <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/2> rdf:type mnx:Phone;
  rdfs:label "Pro";
  mnx:number "0600000000".
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/2>.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/3> rdf:type mnx:Phone;
  rdfs:label "Mobile";
  mnx:number "0400000000".
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/3>, <mnxd:prefix-for-testing-Phone/1233456>.
 <http://ns.mnemotix.com/instances/prefix-for-testing-UserAccount/4> rdf:type mnx:UserAccount;
  mnx:userId "123456";
  mnx:username "derek@mnemotix.com".
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account <http://ns.mnemotix.com/instances/prefix-for-testing-UserAccount/4>.
}`
    });
  });

  it("should update an object", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: PersonDefinition,
      objectId: "test:prefix-for-testing-Person/1",
      updatingProps: {
        firstName: "Derek",
        lastName: "Devel",
        bio: "Hello, I'm Derek !",
        phoneInputs: [{
          label: "Pro",
          number: "0600000000"
        }, {
          label: "Mobile",
          number: "0400000000"
        }, {
          id: "mnxd:prefix-for-testing-Phone/1233456"
        }],
        userAccountInput: {
          userId: "123456",
          username: "derek@mnemotix.com"
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: [
        ],
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
DELETE {
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:firstName ?firstName;
  foaf:lastName ?lastName;
  mnx:bio ?bio;
  foaf:account ?hasUserAccount.
}
INSERT {
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:firstName "Derek";
  foaf:lastName "Devel";
  mnx:bio "Hello, I'm Derek !"@fr.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/1> rdf:type mnx:Phone;
  rdfs:label "Pro";
  mnx:number "0600000000".
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/1>.
 <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/2> rdf:type mnx:Phone;
  rdfs:label "Mobile";
  mnx:number "0400000000".
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/prefix-for-testing-Phone/2>, <mnxd:prefix-for-testing-Phone/1233456>.
 <http://ns.mnemotix.com/instances/prefix-for-testing-UserAccount/3> rdf:type mnx:UserAccount;
  mnx:userId "123456";
  mnx:username "derek@mnemotix.com".
 <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account <http://ns.mnemotix.com/instances/prefix-for-testing-UserAccount/3>.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:firstName ?firstName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:lastName ?lastName. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> mnx:bio ?bio.
  FILTER((LANG(?bio)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account ?hasUserAccount. }
}`
    });
  });

  it("shoud remove a node link in case of 1:1 relationship for null value", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: PersonDefinition,
      objectId: "test:prefix-for-testing-Person/1",
      updatingProps: {
        userAccountInput: null
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: [
        ],
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE { <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account ?hasUserAccount. }
INSERT {  }
WHERE { OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account ?hasUserAccount. } }`
    });
  });

  it("shoud remove a node link in case of 1:1 relationship for id === null value", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: PersonDefinition,
      objectId: "test:prefix-for-testing-Person/1",
      updatingProps: {
        userAccountInput: {
          id: null
        }
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).toHaveBeenCalledWith({
      messageContext: {
        indexable: [
        ],
      },
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE { <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account ?hasUserAccount. }
INSERT {  }
WHERE { OPTIONAL { <http://ns.mnemotix.com/instances/prefix-for-testing-Person/1> foaf:account ?hasUserAccount. } }`
    });
  });

  it("shoud not remove a node link in case of 1:N relationship", async () => {
    spyOnGraphControllerUpdateTriples.mockImplementation(() => ({
      "@context": {}
    }));

    await session.updateObject({
      modelDefinition: PersonDefinition,
      objectId: "test:prefix-for-testing-Person/1",
      updatingProps: {
        phoneInputs: [null]
      },
      lang: "fr"
    });

    expect(spyOnGraphControllerUpdateTriples).not.toBeCalled();
  })
});




