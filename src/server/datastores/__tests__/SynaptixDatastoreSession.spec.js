/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import SynaptixDatastoreSession from '../SynaptixDatastoreSession';
import ModelDefinitionsRegister from "../../datamodel/toolkit/definitions/ModelDefinitionsRegister";
import NetworkLayerAMQP from "../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";
import {PubSub} from "graphql-subscriptions";
import PersonDefinition from "../../datamodel/ontologies-legacy/definitions/foaf/PersonDefinition";
import DefaultIndexMatcher from "../../datamodel/toolkit/matchers/DefaultIndexMatcher";
import IndexControllerService from "../../datamodules/services/index/IndexControllerService";
import DseControllerService from "../../datamodules/services/dse/DseControllerService";
import Person from "../../datamodel/ontologies-legacy/models/foaf/Person";

let modelDefinitionsRegister = new ModelDefinitionsRegister([PersonDefinition]);
let networkLayer = new NetworkLayerAMQP("amqp://", "topicExchange");
let pubSubEngine = new PubSub();
let context = new GraphQLContext({
  anonymous: true, lang: 'fr'
});

let session = new SynaptixDatastoreSession({
  modelDefinitionsRegister,
  networkLayer,
  context,
  pubSubEngine
});

let spyOnIndexServicefulltextSearch = jest.spyOn(session.getIndexService(), 'fulltextSearch');

spyOnIndexServicefulltextSearch.mockImplementation(async () => {
  return {};
});

describe('SynaptixDatastoreSession', () => {
  it('should return the right getters', () => {
    expect(session.getNetworkLayer()).toBeInstanceOf(NetworkLayerAMQP);
    expect(session.getPubSub()).toBeInstanceOf(PubSub);
    expect(session.getModelDefinitionsRegister()).toBeInstanceOf(ModelDefinitionsRegister);
    expect(session.getIndexService()).toBeInstanceOf(IndexControllerService);
  });

  it('should return an index matcher', () => {
    expect(session.getIndexMatcherForType('Person')).toBeInstanceOf(DefaultIndexMatcher);
  });

  it('should not return me in case of anomynous session', async () => {
    expect(await session.getLoggedUserPerson()).toBeUndefined();
  });

  it('should play well with global id reprensentation extraction and stringification', async () => {
    let getModelDefinitionsRegisterSpy = jest.spyOn(session, 'getModelDefinitionsRegister');
    let getGraphQLTypeForNodeTypeSpy = jest.fn();

    getModelDefinitionsRegisterSpy.mockImplementation(() => ({
      getGraphQLTypeForNodeType: getGraphQLTypeForNodeTypeSpy
    }));

    getGraphQLTypeForNodeTypeSpy.mockImplementation((type) => "Organisation");

    expect(session.extractIdFromGlobalId("Person:098787:43433432")).toBe("Person:098787:43433432");
    expect(session.extractTypeFromGlobalId("Org:098787:43433432")).toBe("Organisation");

    expect(getGraphQLTypeForNodeTypeSpy).toHaveBeenCalledWith("Org");

    expect(session.parseGlobalId("Org:098787:43433432")).toEqual({id: "Org:098787:43433432", type: "Organisation"});

    expect(getGraphQLTypeForNodeTypeSpy).toHaveBeenCalledWith("Org");

    expect(session.stringifyGlobalId({id: "Org:098787:43433432", type: "Organisation"})).toEqual("Org:098787:43433432");


    getModelDefinitionsRegisterSpy.mockRestore();
    getGraphQLTypeForNodeTypeSpy.mockRestore();
  });

  it('should play well with global id reprensentation extraction and stringification in case of Relay', async () => {
    process.env.USE_GRAPHQL_RELAY = 1;

    let getModelDefinitionsRegisterSpy = jest.spyOn(session, 'getModelDefinitionsRegister');

    expect(session.extractIdFromGlobalId("UGVyc29uOlBlcnNvbjowOTg3ODc6NDM0MzM0MzI=")).toBe("Person:098787:43433432");
    expect(session.extractTypeFromGlobalId("T3JnYW5pc2F0aW9uOk9yZzowOTg3ODc6NDM0MzM0MzI=")).toBe("Organisation");

    expect(getModelDefinitionsRegisterSpy).not.toHaveBeenCalledWith("Org");

    expect(session.parseGlobalId("T3JnYW5pc2F0aW9uOk9yZzowOTg3ODc6NDM0MzM0MzI=")).toEqual({
      id: "Org:098787:43433432",
      type: "Organisation"
    });
    expect(session.stringifyGlobalId({
      id: "Org:098787:43433432",
      type: "Organisation"
    })).toEqual("T3JnYW5pc2F0aW9uOk9yZzowOTg3ODc6NDM0MzM0MzI=");

    delete process.env.USE_GRAPHQL_RELAY;
  });

  it('should parse raw filters', async () => {
    let filters = session.parseRawFilters(PersonDefinition, ["firstName:Derek", "bio:/^Devel.*$/", "userAccount:mnxd:userAccount/12345"]);

    let bioLabelDefinition = PersonDefinition.getLabel("bio");
    let firstNameLiteralDefinition =  PersonDefinition.getLiteral("firstName");
    let userAccountDefinition = PersonDefinition.getLink("userAccount");

    expect(filters).toEqual({
      labelFilters: [
        {
          labelDefinition:  bioLabelDefinition,
          value: "/^Devel.*$/"
        }
      ],
      literalFilters: [
        {
          literalDefinition: firstNameLiteralDefinition,
          value: "Derek"
        }
      ],
      linkFilters: [
        {
          linkDefinition:  userAccountDefinition,
          id: "mnxd:userAccount/12345"
        }
      ]
    });
  })
});




