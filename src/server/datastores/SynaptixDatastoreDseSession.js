/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import DseControllerService from "../datamodules/services/dse/DseControllerService";
import GraphstorePublisher from "../datamodules/drivers/dse/DseControllerPublisher";
import {createModelFromNode} from "../datamodel/toolkit/models/helpers";
import ValueDefinition from "../datamodel/ontologies-legacy/definitions/common/ValueDefinition";
import NetworkLayerAMQP from "../network/amqp/NetworkLayerAMQP";
import SynaptixDatastoreSession from "./SynaptixDatastoreSession";

/**
 * This is the synaptix session designed to use DSE property graphstore.
 */
export default class SynaptixDatastoreDseSession extends SynaptixDatastoreSession {
  /** @type {DseControllerService} */
  _graphService;

  /**
   * Constructor
   *
   * @param {GraphQLContext} context GraphQL context object
   * @param {NetworkLayerAMQP} networkLayer
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {PubSubEngine} pubSubEngine
   * @param {e.Response} [res] - Express response object
   * @param {SSOApiClient} [ssoApiClient]
   * @param {SSOMiddleware[]} [ssoMiddlewares]
   */
  constructor({context, networkLayer, modelDefinitionsRegister, pubSubEngine, ssoApiClient, ssoMiddlewares, res}) {
    super({context, networkLayer, modelDefinitionsRegister, pubSubEngine,  ssoApiClient, ssoMiddlewares, res});
    this._graphService = new DseControllerService(networkLayer, context);
  }

  /**
   * @return {DseControllerService}
   */
  getGraphStoreService() {
    return this._graphService;
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param lang
   * @param returnAsLocalizedLabel
   * @return {Promise}
   */
  async getLocalizedLabelFor(object, labelDefinition, lang, returnAsLocalizedLabel = false) {
    let label = await super.getLocalizedLabelFor(object, labelDefinition, lang, returnAsLocalizedLabel);

    if (!label) {
      label = await this._graphService.getLocalizedLabelForNode({
        labelDefinition,
        sourceNode: object,
        lang, returnAsLocalizedLabel
      });
    }

    return label;
  }


  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param lang
   * @return {boolean}
   */
  isLocalizedLabelTranslatedFor(object, labelDefinition, lang) {
    // TODO implement this.
    return false;
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @param {boolean} bypassIndex
   * @return {Model|Model[]}
   */
  async getLinkedObjectFor({object, linkDefinition, args, bypassIndex}) {
    if (!bypassIndex) {
      let result = super.getLinkedObjectFor({object, linkDefinition, args});

      if (!!result) {
        return result;
      }
    }

    let pathInGraphstore = linkDefinition.getPathInGraphstore();
    let ModelClass = linkDefinition.getRelatedModelDefinition().getModelClass();

    let nodes = await this._graphService.queryGraphNodes(
      `g.V('${object.id}')
        .${pathInGraphstore}
        .${GraphstorePublisher.getEnabledNodeFilter()}
      `,
      {
        first: 1,
        ...args
      }
    );

    if (linkDefinition.isPlural() === true) {
      return nodes.map(node => createModelFromNode(ModelClass, node));
    } else if (nodes.length > 0) {
      return createModelFromNode(ModelClass, nodes[0]);
    }
  }

  /**
   * @param {Model} object
   * @param {LinkDefinition} linkDefinition
   * @param {DataQueryArguments} args
   * @param {boolean} bypassIndex
   * @return {number}
   */
  async getLinkedObjectsCountFor({object, linkDefinition, args, bypassIndex}) {
    let pathInGraphstore = linkDefinition.getPathInGraphstore();

    if (!bypassIndex) {
      let result = super.getLinkedObjectsCountFor({object, linkDefinition, args});

      if (!!result) {
        return result;
      }
    }

    return this._graphService.queryRawGraph(
      `g.V('${object.id}')
        .${pathInGraphstore}
        .${GraphstorePublisher.getEnabledNodeFilter()}
        .count()
      `
    );
  }

  /**
   * Get object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {string} lang
   * @return {Model}
   */
  async getObject(modelDefinition, objectId, lang) {
    let object = await super.getObject(modelDefinition, objectId, lang);

    if (!object) {
      object = await this._graphService.getNode(objectId, modelDefinition.getModelClass());
    }

    return object;
  }

  /**
   * Creates an object
   *
   * @param graphQLType
   * @param {string} graphQLType - Type of object
   * @param {Link[]} links -  List of links
   * @param {object} [objectInput] - Object properties
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {boolean} [waitForIndexSyncing] - Should wait indexation task.
   *
   * @return {Model}
   */
  async createObject({graphQLType, links, objectInput, lang, uri, waitForIndexSyncing}) {
    let modelDefinition = this.getModelDefinitionsRegister().getModelDefinitionForGraphQLType(graphQLType);

    return this._graphService.createNode({modelDefinition, links, objectInput, lang, uri, waitForIndexSyncing});
  }

  /**
   * Get objects
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {DataQueryArguments} args
   * @return {Model[]}
   */
  async getObjects(modelDefinition, args) {
    let objects = await super.getObjects(modelDefinition, args);

    if (!objects) {
      objects = this._graphService.queryObjects(modelDefinition, null, args)
    }

    return objects;
  }

  /**
   * Update object
   */
  async updateObject(...args) {
    let {modelDefinition, objectId, updatingProps, lang} = {};

    if(args.length === 1){
      ({modelDefinition, objectId, updatingProps, lang} = args)
    } else {
      modelDefinition = args[0];
      objectId = args[1];
      updatingProps = args[2];
      lang = args[3];
    }

    return this._graphService.updateNode({
      modelDefinition,
      objectId,
      updatingProps,
      lang
    });
  }

  /**
   * Remove object
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param permanentRemoval
   */
  async removeObject(modelDefinition, objectId, permanentRemoval = false) {
    return this._graphService.removeNode(modelDefinition, objectId, permanentRemoval);
  }

  /**
   * Create edge between two nodes and re-index them
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId) {
    await this._graphService.createEdge(modelDefinition, objectId, linkDefinition, targetId);

    let object = await this.updateObject(modelDefinition, objectId, {});
    let target = await this.updateObject(linkDefinition.getRelatedModelDefinition(), targetId, {});

    return {
      object,
      target
    };
  }

  /**
   * Create edges between a node and target nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string[]} targetIds
   *
   * @return {Model}
   */
  async createEdges(modelDefinition, objectId, linkDefinition, targetIds) {
    for (let targetId of targetIds) {
      await this._graphService.createEdge(modelDefinition, objectId, linkDefinition, targetId);
      await this.updateObject(linkDefinition.getRelatedModelDefinition(), targetId, {});
    }

    await this.updateObject(modelDefinition, objectId, {})
  }

  /**
   * Remove edge between nodes and re-index them.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   *
   * @return {{object: Model, target: Model}}
   */
  async removeEdge(modelDefinition, objectId, linkDefinition, targetId) {
    await this._graphService.deleteEdges(modelDefinition, objectId, linkDefinition, targetId);

    let object = await this.updateObject(modelDefinition, objectId, {});
    let target = await this.updateObject(linkDefinition.getRelatedModelDefinition(), targetId, {});

    return {
      object,
      target
    };
  }

  /**
   * Set object attached value
   * @param {Model} object
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} valueName
   * @param {string} valueType
   * @param {string} valueValue
   * @return {Value}
   */
  async setObjectValue(modelDefinition, object, valueName, valueType, valueValue) {
    let valueObject = await this.getObjectValue(object, modelDefinition, valueName, true);

    if (valueObject) {
      return await this.updateObject(ValueDefinition, valueObject.id, {
        valueType,
        [`${valueObject.valueType}Value`]: valueValue
      });
    } else {
      return this._graphService.createNode({
        modelDefinition: ValueDefinition,
        links: [{
          linkName: 'object',
          targetId: object.id
        }],
        objectInput: {
          name: valueName,
          valueType,
          [`${valueType}Value`]: valueValue
        }
      });
    }
  }
}