/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import IndexSyncV2Publisher from '../IndexControllerV6Publisher';
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";

let indexhControllerPublisher = new IndexSyncV2Publisher(new NetworkLayerAMQP("amqp://", "fake"), "fooSender");

describe('IndexControllerPublisherV6', () => {
  let publishSpyFn = jest.spyOn(indexhControllerPublisher, 'publish');

  publishSpyFn.mockImplementation(() => ({}));

  it('queries on type', () => {
    indexhControllerPublisher.query({
      types: 'person',
      query: {"match_all": {}},
    });

    expect(publishSpyFn).toHaveBeenCalledWith('index.search', {
      types: ['person'],
      source: {"match_all": {}},
    });
  });

  it('queries on types', () => {
    indexhControllerPublisher.query({
      types: ['person'],
      query: {"match_all": {}},
    });

    expect(publishSpyFn).toHaveBeenCalledWith('index.search', {
      types: ['person'],
      source: {"match_all": {}},
    });
  });

  it('queries on indices and types', () => {
    indexhControllerPublisher.query({
      indicesAndTypes: {
        indices: ["synaptix"],
        types: ["person"]
      },
      query: {"match_all": {}}
    });

    expect(publishSpyFn).toHaveBeenCalledWith('index.search', {
      indicesAndTypes: {"indices": ["synaptix"], "types": ["person"]},
      source: {"match_all": {}},
    });
  });

  it('throws exception on ES exception', async (done) => {
    publishSpyFn.mockImplementation(() => ({
      status: 400,
      error: {
        message: "There is an error"
      }
    }));

    await expect(
      indexhControllerPublisher.query({
        indicesAndTypes: {
          indices: [""],
          types: [""]
        },
        query: {"match_all": {}}
      })
    ).rejects.toThrow(JSON.stringify({
      message: "There is an error"
    }));

    done();
  });
});




