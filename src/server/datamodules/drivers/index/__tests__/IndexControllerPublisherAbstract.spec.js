/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import IndexControllerPublisherAbstract from '../IndexControllerPublisherAbstract';
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";

let indexhControllerPublisher = new IndexControllerPublisherAbstract(new NetworkLayerAMQP("amqp://", "fake"), "fooSender");

describe('IndexControllerPublisherAbstract', () => {
  it('throws an exception if query called', async (done) => {
    await expect(
      indexhControllerPublisher.query({
        indicesAndTypes: {
          indices: [""],
          types: [""]
        },
        query: {
          query: {
            match_all: {}
          }
        }
      })
    ).rejects.toThrow(Error);
    done()

  });

  it('throws an exception if percolate called', async (done) => {
    await expect(
      indexhControllerPublisher.percolate({})
    ).rejects.toThrow(Error);

    done()
  });
});




