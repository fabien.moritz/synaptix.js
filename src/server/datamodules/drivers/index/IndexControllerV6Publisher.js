/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import IndexControllerPublisherAbstract from "./IndexControllerPublisherAbstract";

export default class IndexControllerV6Publisher extends IndexControllerPublisherAbstract {
  /**
   * Query index
   *
   * @param {string|string[]} types - List of types.
   * @param {IndicesAndTypesMapping} indicesAndTypes - Indices and types mapping
   * @param {object} query - Raw ES query
   * @param {boolean} [fullResponse=false] - Returns full response and not just hits.
   * @return {IndexSyncQueryResult}
   */
  async query({types, indicesAndTypes, query, fullResponse}) {
    let body = {
      source: query
    };

    if (indicesAndTypes) {
      body.indicesAndTypes = indicesAndTypes;
    } else if (types) {
      if (typeof types === 'string') {
        types = [types.toLowerCase()];
      }
      body.types = types;
    }

    let result = await this.publish('index.search', body);

    if (result.status === 400) {
      throw new Error(JSON.stringify(result.error));
    } else {
      return fullResponse ? result : result.hits;
    }
  }

  /**
   * Percolate against document
   *
   * @param {string} docType Document index type to percolate.
   * @param {string} docId Document id against to percolate
   * @param {string} parentId Parent document if the relation is parent/child
   *
   * @returns {Promise.<Node[],Error>}
   */
  // percolate(docType, docId, parentId){
  //   return this.publish('index.percolate', {
  //     docType: docType.toLowerCase(),
  //     docId,
  //     parentId
  //   });
  // }
}