/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import IndexControllerPublisherAbstract from "./IndexControllerPublisherAbstract";

export default class IndexControllerV2Publisher extends IndexControllerPublisherAbstract {
  /* istanbul ignore next */

  /**
   * Search for nodes
   *
   * @param {string|string[]} types Document index types to match.
   * @param {object} body ES query
   * @param {int} from
   * @param {int} size
   * @param fullResponse
   *
   * @param sourcePropName
   * @deprecated Use this::query instead
   *
   * @returns {IndexSyncQueryResult}
   */
  search(types, body, from = 0, size = 20, fullResponse = false, sourcePropName = "source") {
    return this.query({
      types,
      query: body,
      from,
      size,
      fullResponse,
      sourcePropName
    })
  }

  /**
   * Query index
   *
   * @param {string|string[]} types - List of types.
   * @param {IndicesAndTypesMapping} indicesAndTypes - Indices and types mapping
   * @param {object} query - Raw ES query
   * @param {boolean} [fullResponse=false] - Returns full response and not just hits.
   * @return {IndexSyncQueryResult}
   */
  async query({types, query, from, size, fullResponse, sourcePropName}) {
    if (typeof types === 'string') {
      types = [types];
    }

    types = types.map(docType => docType.toLowerCase());

    let result = await this.publish('index.search', {
      docTypes: types,
      [sourcePropName || 'source']: query,
      per_page: size,
      offset: from
    });

    return fullResponse ? result : result.hits;
  }

  /**
   * Percolate against document
   *
   * @param {string} docType Document index type to percolate.
   * @param {string} docId Document id against to percolate
   * @param {string} parentId Parent document if the relation is parent/child
   *
   * @returns {Promise.<Node[],Error>}
   */
  percolate(docType, docId, parentId) {
    return this.publish('index.percolate', {
      docType: docType.toLowerCase(),
      docId,
      parentId
    });
  }
}