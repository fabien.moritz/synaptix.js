/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Abstract from './Abstract';

export default class Edge extends Abstract {
  /** @type {string} node subject id (@out direction) */
  subj;
  /** @type {string} edge label */
  pred;
  /** @type {string} node object id (@in direction) */
  obj;
  /** @type {string} node subject nodeType */
  subjNodeType;
  /** @type {string} node object nodeType */
  objNodeType;

  /**
   * @constructor
   *
   * @param id
   * @param subj
   * @param pred
   * @param obj
   * @param properties
   */
  constructor(id, subj, pred, obj, properties) {
    super(id, properties);

    this.subj = subj;
    this.pred = pred;
    this.obj = obj;
  }

  /**
   * Normalize the predicate name
   * @param edge
   */
  static normalizePredicate(edge) {
    return edge.toUpperCase();
  }

  /**
   * Get an edge type structure in command
   *
   * @param {string} subj Subject node id (edge OUT direction)
   * @param {string} pred Relation name
   * @param {string} obj  Object node id (edge IN direction)
   * @param properties
   *
   * @returns {{subj: *, pred: *, obj: *}}
   */
  static serializePartial(subj, pred, obj, properties = {}) {
    if (typeof properties._enabled === 'undefined') {
      properties._enabled = true;
    }

    return {
      subj,
      pred: Edge.normalizePredicate(pred),
      obj,
      _origin: Abstract.getOrigin(),
      _source: properties
    };
  }

  getSubj() {
    return this.subj;
  }

  getPred() {
    return this.pred;
  }

  getObj() {
    return this.obj;
  }

  getSubjNodeType() {
    return this.subjNodeType;
  }

  getObjNodeType() {
    return this.objNodeType;
  }

  getProperties() {
    return this.properties;
  }

  setSubjNodeType(value) {
    this.subjNodeType = value;
  }

  setObjNodeType(value) {
    this.objNodeType = value;
  }

  /**
   * Serialize Edge into a JSON compatible format.
   *
   * @returns {{subj: string, pred: string, obj: string, properties: list}}
   */
  serialize() {
    return {
      subj: this.subj,
      pred: this.pred,
      obj: this.obj,
      _origin: Abstract.getOrigin(),
      _source: this.properties
    };
  }
}