/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Node from './Node';
import Edge from './Edge';
import Filter from './Filter';


function createEdge(subj, pred, obj, properties) {
  return new Edge(subj, pred, obj, properties);
}

function createNode(nodeType, id, properties, meta, uri) {
  return new Node(nodeType, id, properties, meta, uri);
}

export {
  Node,
  Edge,
  Filter,
  createEdge,
  createNode
}