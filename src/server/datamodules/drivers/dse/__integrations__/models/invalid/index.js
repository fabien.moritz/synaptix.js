import edges from './edges.json';
import vertices from './vertices.json';

export {edges, vertices};