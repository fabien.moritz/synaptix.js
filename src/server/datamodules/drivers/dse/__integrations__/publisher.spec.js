/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Publisher from '../DseControllerPublisher';
import {Edge, Node} from '../models';

import {edges, vertices} from './models/valid';

var publisher = new Publisher("dlfkjfldksjfldkjdlsk");

fdescribe('GraphStorePublisher', () => {
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;

  /**
   * Helper method to create a node
   *
   * @param vertexIndex
   * @returns {{vertex, node: Node}}
   */
  async function createNode(vertexIndex = 0, forceUri = null) {
    let vertex = vertices[Object.keys(vertices)[vertexIndex]];
    let node = await publisher.createNode(vertex.nodeType, forceUri || vertex.uri, vertex.properties, vertex.meta);

    return {vertex, node};
  }

  /**
   * Helper method to create an edge
   * @param edgeIndex
   * @returns {{inVertex, outVertex, edge, inNode: Node, outNode: Node, createdEdge: Edge}}
   */
  async function createEdge(edgeIndex = 0) {
    let edge = edges[Object.keys(edges)[edgeIndex]],
      inVertex = vertices[edge.in],
      outVertex = vertices[edge.out];

    let inNode = await publisher.createNode(
      inVertex.nodeType,
      inVertex.uri,
      inVertex.properties
    );

    let outNode = await publisher.createNode(
      outVertex.nodeType,
      outVertex.uri,
      outVertex.properties
    );

    let createdEdge = await publisher.createEdge(inNode.id, edge.rel, outNode.id);

    return {
      inVertex,
      outVertex,
      edge,
      inNode,
      outNode,
      createdEdge
    };
  }

  /**
   * Helper method to create a graph
   *
   * @param extraEdges
   * @param countEdge
   * @returns {{nodeDefinitions: {}, edgeDefinitions: Array, graph: Graph}}
   */
  async function createGraph(extraEdges = [], countEdge = undefined) {

    let nodeDefinitions = {},
      edgeDefinitions = [];

    Object.keys(edges)
      .slice(0, countEdge)
      .map((edgekey) => {

        let edge = edges[edgekey],
          inVertex = vertices[edge.in],
          outVertex = vertices[edge.out];

        nodeDefinitions[edge.in] = Node.serializePartial(inVertex.nodeType, inVertex.uri, inVertex.properties);
        nodeDefinitions[edge.out] = Node.serializePartial(outVertex.nodeType, outVertex.uri, outVertex.properties);

        edgeDefinitions.push(
          Edge.serializePartial(edge.in, edge.rel, edge.out)
        )
      });

    edgeDefinitions = edgeDefinitions.concat(extraEdges);

    let graph = await publisher.createGraph(nodeDefinitions, edgeDefinitions);

    return {
      nodeDefinitions,
      edgeDefinitions,
      graph
    }
  }

  /**
   * Empty the graph before each test
   */
  beforeEach(async function (done) {
    try {
      await publisher.queryGraph("g.V().drop().iterate()");
      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('creates a node', async (done) => {
    try {
      let {node, vertex} = await createNode();

      expect(node instanceof Node).toBeTruthy();
      expect(node.id).toBeDefined();

      for (let property in node.getProperties()) {
        expect(node.getPropertyValue(property)).toEqual(vertex.properties[property]);
      }

      for (let meta in node.getMeta()) {
        expect(node.getMetaValue(meta)).toEqual(vertex.meta[meta]);
      }

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('gets a node by id', async (done) => {
    try {
      let {node, vertex} = await createNode();

      let reNode = await publisher.getNode(node.id);

      expect(reNode instanceof Node).toBeTruthy();
      expect(reNode.id).toEqual(node.id);

      for (let property in node.getProperties()) {
        expect(node.getPropertyValue(property)).toEqual(reNode.getPropertyValue(property));
      }

      done();

    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('gets a node by uri', async (done) => {
    try {
      let {node, vertex} = await createNode();

      let reNode = await publisher.getNodeByURI(vertex.uri);

      expect(reNode instanceof Node).toBeTruthy();
      expect(reNode.id).toEqual(node.id);
      done();

    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('updates a node', async (done) => {
    try {
      let {node, vertex} = await createNode();

      node.properties.historyNote = "Test les 'apostrophes' \"test\" et des dollars $ •ë“‘{¶«¡Çø}—€ô@Ù";

      let updatedNode = await publisher.updateNode(
        node.nodeType,
        node.id,
        {
          historyNote: node.properties.historyNote
        }
      );

      for (let property in node.getProperties()) {
        expect(node.getPropertyValue(property)).toEqual(updatedNode.getPropertyValue(property));
      }

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('delete a node', async (done) => {
    try {
      let {node} = await createNode();

      let success = await publisher.deleteNode(node.id);

      expect(success).toEqual(true);
      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('delete a node that does not exist', async (done) => {
    try {
      let {node} = await createNode();

      await publisher.deleteNode(node.id);
      let success = await publisher.deleteNode(node.id);

      expect(success).toBeTruthy();
      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('create an edge', async (done) => {
    try {
      let {inNode, outNode, edge, createdEdge} = await createEdge();

      expect(createdEdge instanceof Edge).toBeTruthy();

      expect(createdEdge.getSubj()).toEqual(inNode.id);
      expect(createdEdge.getPred()).toEqual(edge.rel);
      expect(createdEdge.getObj()).toEqual(outNode.id);

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  // Due to datastax weird edge ID definition, this test is not possible for the moment.
  xit('gets an edge by id', async (done) => {
    try {
      let {createdEdge: edge} = await createEdge();

      let reEdge = await publisher.getEdge(edge.id);

      expect(reEdge instanceof Edge).toBeTruthy();
      expect(reEdge.id).toEqual(edge.id);

      for (let property in edge.getProperties()) {
        expect(edge.getPropertyValue(property)).toEqual(reEdge.getPropertyValue(property));
      }

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('delete an edge', async (done) => {
    try {
      let {createdEdge} = await createEdge();

      let success = await publisher.deleteEdge(createdEdge.id);

      expect(success).toEqual(true);

      publisher.getEdge(createdEdge.id)
        .then(() => {
          fail('Not supposed to pass here...');
          done();
        })
        .catch(error => {
          expect(error.status).toEqual('ERROR');
          expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.RecordNotFoundException');
          done();
        });
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('create an edge and delete a node edged to test if edge is deleted too', async (done) => {
    try {
      let {inNode, createdEdge} = await createEdge();

      let success = await publisher.deleteNode(inNode.id);

      expect(success).toBeTruthy();

      publisher.getEdge(createdEdge.id)
        .then(() => {
          fail('Not supposed to pass here...');
          done();
        })
        .catch(error => {
          expect(error.status).toEqual('ERROR');
          expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.RecordNotFoundException');
          done();
        });
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('create a graph', async (done) => {
    try {
      let {nodeDefinitions, edgeDefinitions, graph} = await createGraph();

      expect(Object.keys(graph.nodes).length).toEqual(Object.keys(nodeDefinitions).length);
      expect(graph.edges.length).toEqual(edgeDefinitions.length);

      Object.keys(nodeDefinitions).map(nodeKey => {
        expect(graph.nodes[nodeKey].getId()).toBeDefined();
      });

      edgeDefinitions.map((edgeDefinition, index) => {
        expect(graph.edges[index].getSubj()).toEqual(graph.nodes[edgeDefinition.subj].getId());
        expect(graph.edges[index].getObj()).toEqual(graph.nodes[edgeDefinition.obj].getId());
      });

      done();

    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('create a graph with existing nodes', async (done) => {
    try {
      let {node} = await createNode(0, "http://ns.mnemotix.com/resource/concept#1bis");

      let {nodeDefinitions, edgeDefinitions, graph} = await createGraph([
        Edge.serializePartial(Object.keys(vertices)[0], 'TEST_RELATION', Object.keys(vertices)[1])
      ]);

      edgeDefinitions.map((edgeDefinition, index) => {
        expect(graph.edges[index].getSubj()).toEqual(graph.nodes[edgeDefinition.subj].getId());
        expect(graph.edges[index].getObj()).toEqual(graph.nodes[edgeDefinition.obj].getId());
      });

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('request nodes by edges', async (done) => {
    try {
      let {graph} = await createGraph();

      let outNodes = await publisher.getNodesWithOutRelationTo(graph.nodes['concept1'].id, 'ALT_LABEL');
      let inNodes = await publisher.getNodesWithInRelationTo(graph.nodes['label4'].id, 'ALT_LABEL');

      expect(outNodes.length).toEqual(3);
      expect(inNodes.length).toEqual(2);

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('request nodes by label', async (done) => {
    try {
      await createGraph();

      let nodes = await publisher.getNodesWithLabel('Concept');

      expect(nodes.length).toEqual(2);
      done();

    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('request edges', async (done) => {
    try {
      let {graph} = await createGraph();

      let edgees = await publisher.getEdges(graph.edges[0].getSubj(), graph.edges[0].getPred(), graph.edges[0].getObj());

      expect(edgees.length).toEqual(1);
      done();

    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('request edges by label', async (done) => {
    try {
      let {graph} = await createGraph();

      let edges = await publisher.getEdgesByLabel('ALT_LABEL');
      expect(edges.length).toEqual(4);
      done();

    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('delete edges', async (done) => {
    try {
      let {graph} = await createGraph();
      await publisher.deleteEdges(graph.edges[0].getSubj(), graph.edges[0].getPred(), graph.edges[0].getObj());
      let edges = await publisher.getEdges(graph.edges[0].getSubj(), graph.edges[0].getPred(), graph.edges[0].getObj());
      expect(edges.length).toEqual(0);
      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('queries graph nodes', async (done) => {
    try {
      await createGraph();

      let nodes = await publisher.queryGraphNodes(`g.V().hasLabel('${vertices['concept1'].nodeType}').limit(2)`);

      expect(nodes.length).toEqual(2);

      for (let node of nodes) {
        expect(node instanceof Node).toBeTruthy();
      }

      nodes = await publisher.queryGraphNodes(`g.V().hasLabel('${vertices['concept1'].nodeType}').limit(1)`);

      expect(nodes.length).toEqual(1);

      nodes = await publisher.queryGraphNodes(`g.V().hasLabel('NOT_EXISTING').limit(1)`);

      expect(nodes.length).toEqual(0);

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('queries graph edges', async (done) => {
    try {
      await createGraph();

      let edgees = await publisher.queryGraphEdges(`g.E().hasLabel('${edges['prefLabel1'].rel}').limit(2)`);

      expect(edgees.length).toBe(2);

      for (let edge of edgees) {
        expect(edge instanceof Edge).toBeTruthy();
      }

      edgees = await publisher.queryGraphEdges(`g.E().hasLabel('${edges['prefLabel1'].rel}').limit(1)`);

      expect(edgees.length).toBe(1);


      edgees = await publisher.queryGraphEdges(`g.E().hasLabel('NOT_EXISTING').limit(2)`);

      expect(edgees.length).toBe(0);


      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });

  it('queries graph count', async (done) => {
    try {
      await createGraph();

      let count = await publisher.queryGraph(`g.V().limit(5).count()`);

      expect(count).toEqual([5]);
      expect(typeof count).toBe('object');
      expect(Array.isArray(count)).toBeTruthy();

      count = await publisher.queryGraph(`g.V().limit(1).count()`);

      expect(count).toEqual([1]);
      expect(typeof count).toBe('object');
      expect(Array.isArray(count)).toBeTruthy();

      count = await publisher.queryGraph(`g.V().hasLabel('NOT_EXISTING').limit(1).count()`);

      expect(count).toEqual([0]);
      expect(typeof count).toBe('object');
      expect(Array.isArray(count)).toBeTruthy();

      done();
    } catch (e) {
      fail(typeof e == "string" ? e : JSON.stringify(e, null, ' '));
      done();
    }
  });
});

fdescribe('GraphStorePublisher error handling', () => {

  fit('send a empty message', (done) => {
    publisher.getNode()
      .then(() => {
        fail('Should not succeed...');
      })
      .catch((error) => {
        expect(error.status).toEqual('ERROR');
        expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.MessageParsingException');
        done();
      });
  });

  it('send a malformed message', (done) => {
    publisher.getNode({
      id: 'fdlkjldfkj'
    })
      .then(() => {
        fail('Should not succeed...');
      })
      .catch((error) => {
        expect(error.status).toEqual('ERROR');
        expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.MessageParsingException');
        done();
      });
  });

  it('get a node that does not exists', (done) => {
    publisher.getNode('#1:4444')
      .then(() => {
        fail('Should not succeed...');
      })
      .catch((error) => {
        expect(error.status).toEqual('ERROR');
        expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.RecordNotFoundException');
        done();
      });
  });

  it('gets a node whose uri not exists', (done) => {
    publisher.getNodeByURI('i_dont_exist')
      .then(() => {
        fail('Should not succeed...');
      })
      .catch((error) => {
        expect(error.status).toEqual('ERROR');
        expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.RecordNotFoundException');
        done();
      });
  });

  it('creates 2 nodes with the same URI', (done) => {
    publisher.createNode(
      'concept',
      `http://ns.mnemotix.com/resource/concept#test_ABCD`,
      {
        historyNote: "Lorem ipsum"
      }
    )
      .then((body) => {
        var id = body.id;

        publisher.createNode(
          'concept',
          `http://ns.mnemotix.com/resource/concept#test_ABCD`,
          {
            historyNote: "Lorem ipsum"
          }
        )
          .then(() => {
            fail('Should not succeed...');
            publisher.deleteNode(id).then(done);
          })
          .catch((error) => {
            expect(error.status).toEqual('ERROR');
            expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.DuplicateRecordException');
            publisher.deleteNode(id).then(done);
          });
      });
  });

  it('creates an edge between two nodes that does not exists', (done) => {
    publisher.createEdge('#1:4345534', `SKOS_RELATED`, '#1:455534533')
      .then(() => {
        fail('Should not succeed...');
      })
      .catch((error) => {
        expect(error.status).toEqual('ERROR');
        done();
      });
  });

  it('get a node for a malformed id', (done) => {
    publisher.getNode('ouch')
      .then(() => {
        fail('Should not succeed...');
      })
      .catch((error) => {
        expect(error.status).toEqual('ERROR');
        expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.RecordNotFoundException');

        publisher.getNode('#5435465665565:655465465465454645645')
          .then(() => {
            expect(false).toEqual(true);
          })
          .catch((error) => {
            expect(error.status).toEqual('ERROR');
            expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.RecordNotFoundException');

            publisher.getNode('')
              .then(() => {
                expect(false).toEqual(true);
              })
              .catch((error) => {
                expect(error.status).toEqual('ERROR');
                expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.RecordNotFoundException');
                done();
              })
          })
      });
  });

  it('create a graph with bad edge', (done) => {
    publisher.createGraph([
      Node.serializePartial('concept', 'http://ns.mnx/#cagwbe1'),
      Node.serializePartial('concept', 'http://ns.mnx/#cagwbe2')
    ], [
      Edge.serializePartial('_:0', 'Narrower', '_:1'),
      Edge.serializePartial('_:1', 'Narrower', '#45:67')
    ]).then(() => {
      fail("Should not pass here !!");
      done();
    }).catch(error => {
      expect(error.status).toEqual('ERROR');
      expect(error.error.errorType).toEqual('com.mnemotix.synaptix.core.exceptions.MessageParsingException');
      done();
    });
  });
});