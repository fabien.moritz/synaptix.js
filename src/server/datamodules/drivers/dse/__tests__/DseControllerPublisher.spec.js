/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import Publisher from '../DseControllerPublisher';

import {createNode, Edge, Node} from '../models';

describe('Dse pController Publishers', () => {
  let publisher = new Publisher("blabla");
  let publishSpyFn = jest.spyOn(publisher, 'publish');

  publishSpyFn.mockImplementation(() => Promise.resolve([
    createNode('Dumb', 2, {}, {}, '2'),
  ]));

  it('get a node by id', async () => {
    await publisher.getNode('foo');

    expect(publisher.publish).toHaveBeenCalledWith('nodes.select', {query: "g.V('foo').has('_enabled', true)"}, undefined);

  });

  it('get a node by URI', async () => {
    await publisher.getNodeByURI('http://blabla.com/#foo');

    expect(publisher.publish).toHaveBeenCalledWith('nodes.select', {query: "g.V().has('uri','http://blabla.com/#foo').has('_enabled', true)"}, undefined);
  });


  it('create a node', async () => {
    await publisher.createNode('concept', 'http://blabla.com/#foo', {foo: 'bar'});

    expect(publisher.publish).toHaveBeenCalledWith('node.create', {
      nodeType: 'Concept',
      uri: 'http://blabla.com/#foo',
      _origin: 'mnx:app:nodejs',
      _source: {
        foo: 'bar',
        _enabled: true
      }
    }, undefined);
  });

  it('create a node with metas', async () => {
    await publisher.createNode('concept', 'http://blabla.com/#foo', {foo: 'bar'}, {bar: 'faz'});

    expect(publisher.publish).toHaveBeenCalledWith('node.create', {
      nodeType: 'Concept',
      uri: 'http://blabla.com/#foo',
      _origin: 'mnx:app:nodejs',
      _source: {
        foo: 'bar',
        _enabled: true
      },
      _meta: {
        bar: 'faz'
      }
    }, undefined);
  });

  it('update a node', async () => {
    await publisher.updateNode('concept', '#1:2', {foo: 'baz'});

    expect(publisher.publish).toHaveBeenCalledWith('node.update', {
      _id: '#1:2',
      nodeType: 'Concept',
      _origin: 'mnx:app:nodejs',
      _source: {
        foo: 'baz'
      }
    }, undefined);
  });

  it('delete a node', async () => {
    await publisher.deleteNode('#1:2');

    expect(publisher.publish).toHaveBeenCalledWith('node.delete', '#1:2', undefined);
  });

  it('create a edge', async () => {
    await publisher.createEdge('http://blabla.com/#foo', 'foo:bar', 'http://blabla.com/#bar');

    expect(publisher.publish).toHaveBeenCalledWith('edge.create', {
      subj: 'http://blabla.com/#foo',
      pred: 'FOO:BAR',
      obj: 'http://blabla.com/#bar',
      _origin: 'mnx:app:nodejs',
      _source: {
        _enabled: true
      }
    }, undefined);
  });

  it('delete a edge', async () => {
    await publisher.deleteEdge('#2:3');

    expect(publisher.publish).toHaveBeenCalledWith('edge.delete', '#2:3', undefined);
  });

  it('delete edges', async () => {
    await publisher.deleteEdges('#2:3', 'foo:bar', '#2:4');

    expect(publisher.publish).toHaveBeenCalledWith('edges.delete', {
      subj: '#2:3',
      pred: 'FOO:BAR',
      obj: '#2:4',
      _origin: 'mnx:app:nodejs',
      _source: {_enabled: true}
    }, {});
  });

  it('create a graph', async () => {
    let nodes = [
      Node.serializePartial('concept', 'http://ns.mnx/#1'),
      Node.serializePartial('concept', 'http://ns.mnx/#2'),
      Node.serializePartial('concept', 'http://ns.mnx/#3')
    ];

    let edges = [
      Edge.serializePartial('_:0', 'Narrower', '_:1'),
      Edge.serializePartial('_:1', 'Narrower', '_:2')
    ];

    publishSpyFn.mockImplementation(() => Promise.resolve({
      nodes,
      edges
    }));

    await publisher.createGraph(nodes, edges);

    expect(publisher.publish).toHaveBeenCalledWith('graph.create', {
      nodes: [
        {
          nodeType: 'Concept',
          uri: 'http://ns.mnx/#1',
          _origin: 'mnx:app:nodejs',
          _source: {
            _enabled: true,
          }
        },
        {
          nodeType: 'Concept',
          uri: 'http://ns.mnx/#2',
          _origin: 'mnx:app:nodejs',
          _source: {
            _enabled: true,
          }
        },
        {
          nodeType: 'Concept',
          uri: 'http://ns.mnx/#3',
          _origin: 'mnx:app:nodejs',
          _source: {
            _enabled: true,
          }
        }
      ],
      edges: [
        {
          subj: '_:0',
          pred: 'NARROWER',
          obj: '_:1',
          _origin: 'mnx:app:nodejs',
          _source: {
            _enabled: true,
          }
        },
        {
          subj: '_:1',
          pred: 'NARROWER',
          obj: '_:2',
          _origin: 'mnx:app:nodejs',
          _source: {
            _enabled: true,
          }
        }
      ]
    }, undefined);
  });
});




