/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import yargs from "yargs";
import ora from "ora";
import SSOApiClient from "../SSOApiClient";
import dotenv from 'dotenv';

dotenv.config();

export let addUser = async () => {
  let spinner = ora().start();
  spinner.spinner = "clock";

  let {
    username, email, password, lastName, firstName, isTemporaryPassword, apiTokenEndpointUrl,
    apiEndpointUrl, apiClientId, apiLogin, apiPassword
  } = yargs
    .usage("yarn sso:user:add [options] -u [Username] -p [Password]")
    .example("yarn ontology:generate:owl -o mnx-agent.ttl", "")
    .option('u', {
      alias: 'username',
      describe: 'Username',
      demandOption: true,
      nargs: 1
    })
    .option('e', {
      alias: 'email',
      describe: 'email',
      nargs: 1,
      defaultDescription: "Same as username if not given"
    })
    .option('p', {
      alias: 'password',
      describe: 'Password',
      demandOption: true,
      nargs: 1
    })
    .option('o', {
      alias: 'isTemporaryPassword',
      describe: 'Is password temporary and must be reasked',
      default: false,
      nargs: 1
    })
    .option('f', {
      alias: 'firstName',
      describe: 'First name',
      demandOption: true,
      nargs: 1
    })
    .option('l', {
      alias: 'lastName',
      describe: 'Last name',
      demandOption: true,
      nargs: 1
    })
    .option('T', {
      alias: 'apiTokenEndpointUrl',
      describe: 'SSO API token endpoint URL',
      default: process.env.OAUTH_ADMIN_TOKEN_URL,
      nargs: 1
    })
    .option('A', {
      alias: 'apiEndpointUrl',
      describe: 'SSO API endpoint URL',
      default: process.env.OAUTH_ADMIN_API_URL,
      nargs: 1
    })
    .option('U', {
      alias: 'apiLogin',
      describe: 'SSO API user login',
      default: process.env.OAUTH_ADMIN_USERNAME,
      nargs: 1
    })
    .option('P', {
      alias: 'apiPassword',
      describe: 'SSO API user login',
      default: process.env.OAUTH_ADMIN_PASSWORD,
      nargs: 1
    })
    .help('h')
    .alias('h', 'help')
    .epilog('Copyright Mnemotix 2019')
    .help()
    .argv;


  let ssoAPIClient = new SSOApiClient({
    apiEndpointUrl, apiLogin, apiPassword, apiTokenEndpointUrl
  });

  if (!email){
    email = username;
  }

  try {
    console.log(apiEndpointUrl, apiLogin, apiPassword, apiTokenEndpointUrl);
    spinner.info(`Creating user ${firstName} ${lastName} (${username}:${password.replace(/./ig, "*")})`);
    let user = await ssoAPIClient.createUser({
      username, password, isTemporaryPassword, firstName, lastName, email
    });
    spinner.succeed(`User created with id : ${user.getId()}`);
  } catch (e) {
    console.log(e.message);
    spinner.fail(e.message);
  }
};

