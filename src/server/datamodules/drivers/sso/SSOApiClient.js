/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import got from "got";
import get from "lodash/get";
import SSOUser from "./models/SSOUser";
import {I18nError} from "../../../utilities/I18nError";

export default class SSOApiClient {
  /**
   * @param {string} apiTokenEndpointUrl
   * @param {string} apiEndpointUrl
   * @param {string} [apiClientId=admin-cli]
   * @param {string} [apiLogin]
   * @param {string} [apiPassword]
   */
  constructor({apiTokenEndpointUrl, apiEndpointUrl, apiClientId, apiLogin, apiPassword}) {
    this._apiTokenEndpointUrl = apiTokenEndpointUrl;
    this._apiEndpointUrl = apiEndpointUrl;
    this._apiClientId = apiClientId || "admin-cli";
    this._apiLogin = apiLogin;
    this._apiPassword = apiPassword;
  }

  /**
   * Login a user
   *
   * @param {string} tokenEndpointUrl
   * @param {string} username
   * @param {string} password
   * @param {string} clientId
   * @param {string} [clientSecret]
   *
   * @return {SSOUser}
   */
  async loginUser({tokenEndpointUrl, username, password, clientId, clientSecret}) {
    try{
      let response = await got.post(tokenEndpointUrl, {
        body: {
          username: username.toLowerCase(),
          password,
          client_id: clientId,
          client_secret: clientSecret,
          grant_type: "password"
        },
        form: true,
        responseType: 'json'
      });

      // When login succeeds, the returned stringified object in the response body looks like :
      // {
      //   access_token: '...',
      //   expires_in: 900,
      //   refresh_expires_in: 1800,
      //   refresh_token: '...',
      //   token_type: 'bearer',
      //   'not-before-policy': 0,
      //   session_state: '...',
      //   scope: '...'
      // }
      return new SSOUser({
        jwtSession: {
          ticket: JSON.parse(response.body)
        }
      });

    } catch (e) {
      console.log(e.response.statusCode, e.response.body);
      if(e.response?.statusCode === 401){
        throw new I18nError("Invalid credentials", "INVALID_CREDENTIALS", 400);
      } else if(e.response?.statusCode === 400){
        if (e.response.body.includes("Account disabled")) {
          throw new I18nError("This account has been disabled", "ACCOUNT_DISABLED", 400);
        }

        if (e.response.body.includes("Account is not fully set up")) {
          throw new I18nError("Account is not fully set up. This may be a problem of email not verified of password reset request.", "ACCOUNT_NOT_VALIDATED", 400);
        }
      }
      throw new I18nError(e.response.body)
    }
  }

  /**
   * @param {string} action
   * @param {object} [body]
   * @param {object} [method=GET]
   * @return {object}
   */
  async requestApi({uri, body, method}) {
    let adminUser = await this.loginUser({
      tokenEndpointUrl: this._apiTokenEndpointUrl,
      username: this._apiLogin,
      password: this._apiPassword,
      clientId: this._apiClientId
    });

    return got(uri, {
      method: method || "GET",
      json: true,
      body,
      headers: {
        'Authorization': `Bearer ${adminUser.getAccessToken()}`,
      },
    });
  }

  /**
   * @param userId
   * @return {object}
   */
  async getUserById(userId) {
    let {body: user} = await this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
    });

    if (user) {
      return new SSOUser({user});
    }

    throw new I18nError(`User ${userId} is not registered on Keycloak.`, "USER_NOT_IN_SSO");
  }

  /**
   * @param username
   * @return {SSOUser}
   * @throws Error if user is not registered for this username.
   */
  async getUserByUsername(username) {
    username = username.toLowerCase();
    let {body: users} = await this.requestApi({
      uri: `${this._apiEndpointUrl}/users?username=${username}`
    });

    if (users && Array.isArray(users)) {
      let user = users.find(user => user.username === username);

      if (user) {
        return new SSOUser({user});
      }
    }

    throw new I18nError(`User ${username} is not registered on Keycloak.`, "USER_NOT_IN_SSO");
  }

  /**
   * @param username
   * @return {boolean}
   */
  async isUsernameExists(username) {
    username = username.toLowerCase();
    let {body: users} = await this.requestApi({
      uri: `${this._apiEndpointUrl}/users?username=${username}`
    });

    return !!(users || []).find(user => user.username === username);
  }

  /**
   * Create a user.
   *
   * @param username
   * @param password
   * @param isTemporaryPassword
   * @param properties - A list of user properties
   * @return {SSOUser}
   */
  async createUser({username, password, isTemporaryPassword, ...properties}) {
    username = username.toLowerCase();
    try {
      await this.requestApi({
        uri: `${this._apiEndpointUrl}/users`,
        method: "POST",
        body: {
          username,
          ...properties,
          enabled: true
        }
      });
    } catch (error) {
      if (get(error, "response.body.errorMessage") === "User exists with same username") {
        throw new I18nError(`User ${username} already exists in SSO database and can't be recreated`, "USER_ALREADY_EXISTS", 400);
      } else {
        throw new I18nError(error);
      }
    }

    let user = await this.getUserByUsername(username);

    await this.resetUserPassword({
      userId: user.getId(),
      password,
      isTemporaryPassword
    });

    return user;
  }

  /**
   * @param userId
   * @param attributes
   */
  async setUserAttributes({userId, attributes}) {
    await this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: "PUT",
      body: {
        attributes
      }
    });
  }

  /**
   *
   * @param username
   * @param props
   * @param identityProvider
   * @param userId
   * @param userName
   * @param password
   * @return {boolean}
   */
  async createUserWithFederatedIdentity({username, props, identityProvider, userId, userName, password}) {
    let user = await this.createUser({
      username,
      props,
      password,
      isTemporaryPassword: false
    });

    await this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${user.getId()}/federated-identity/${identityProvider}`,
      body: {
        identityProvider,
        userId,
        userName
      }
    });

    return user;
  }

  /**
   * @param userId
   * @param password
   * @param isTemporaryPassword
   * @return {boolean}
   */
  async resetUserPassword({userId, password, isTemporaryPassword}) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}/reset-password`,
      method: 'PUT',
      body: {
        temporary: !!isTemporaryPassword,
        type: "password",
        value: password
      }
    });
  }

  /**
   * Reset password by sending an email.
   *
   * @param userId
   * @param redirectUri
   * @param clientId
   * @return {boolean}
   */
  async resetUserPasswordByMail({userId, redirectUri, clientId}) {
    let extraParams = "";

    if (redirectUri && clientId) {
      extraParams = `?redirect_uri=${redirectUri}&client_id=${clientId}`;
    }

    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}/execute-actions-email${extraParams}`,
      method: 'PUT',
      body: ["UPDATE_PASSWORD"]
    });
  }

  /**
   * Send an email verification to validate email address.
   * @param userId
   * @param redirectUri
   * @param clientId
   * @return {boolean}
   */
  async sendEmailValidation({userId, redirectUri, clientId}) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}/send-verify-email?redirect_uri=${redirectUri}&client_id=${clientId}`,
      method: 'PUT'
    });
  }

  /**
   * Remove a user
   *
   * @param userId
   * @return {boolean}
   */
  async removeUser(userId) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: 'DELETE'
    });
  }

  /**
   * Disable user
   *
   * @param userId
   * @return {boolean}
   */
  async disableUser(userId) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: 'PUT',
      body: {
        enabled: false
      }
    });
  }

  /**
   * Enable user
   *
   * @param userId
   * @return {boolean}
   */
  async enableUser(userId) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}`,
      method: 'PUT',
      body: {
        enabled: true
      }
    });
  }

  /**
   * Logout user
   *
   * @param userId
   * @return {boolean}
   */
  async logoutUser(userId) {
    return this.requestApi({
      uri: `${this._apiEndpointUrl}/users/${userId}/logout`,
      method: 'POST',
      body: {
        user: userId
      }
    });
  }
}
