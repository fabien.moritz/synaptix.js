/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import DataModulePublisher from '../DataModulePublisher';

/**
 * Class used to publish commands to the URIGen module on Synaptix V1 (non RDF version).
 *
 * @extends DataModulePublisher
 */
export default class UrigenPublisher extends DataModulePublisher {
  /* istanbul ignore next */
  getName() {
    return 'urigen';
  }

  /**
   * Get an URI for a given id and object type.
   *
   * @param {string} objectType object type (skos:concept, foaf:person...)
   * @param {string} id id of the object
   * @returns {Promise.<string, Error>} A promise that returns an URI
   */
  getUri(objectType, id) {
    return this.publish('uri.get', `${objectType}:${id}:uri`);
  }

  /**
   * Create an URI given an object type
   *
   * @param {string} objectType object type (skos:concept, foaf:person...)
   * @returns {Promise.<string, Error>} A promise that returns an URI
   */
  createUri(objectType) {
    return this.publish('uri.create', {
      objectType
    });
  }

  /**
   *
   * @param {string} objectType object type (skos:concept, foaf:person...)
   * @param {string} id id of the object
   * @returns {Promise.<bool, Error>} A promise that returns True if deleted, False otherwise
   */
  deleteUri(objectType, id) {
    return this.publish('uri.delete', `${objectType}:${id}:*`);
  }
}