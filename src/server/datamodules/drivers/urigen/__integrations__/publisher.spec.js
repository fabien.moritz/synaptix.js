/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import UriGetPublisher from '../UrigenPublisher';

var publisher = new UriGetPublisher("dlfkjfldksjfldkjdlsk");

describe("UriGetPublisher integration tests", () => {

  var createdURI;

  it('create an URI', (done) => {
    publisher.createUri('concept')
      .then((uri) => {
        // Just test that concept is created.
        expect(true).toEqual(true);
        createdURI = uri;
      })
      .catch((error) => {
        throw error;
      })
      .then(done);
  });

  it('get an existing URI', (done) => {
    if (!createdURI) {
      throw new Error('UriGetPublisher seems not to respond, check it is running before running tests');
    }

    publisher.getUri('concept', createdURI.replace(/[^#]*#/, ''))
      .then((uri) => {
        expect(uri).toEqual(createdURI);
      })
      .catch((error) => {
        throw error;
      })
      .then(done);
  });


  it('delete an URI', (done) => {
    if (!createdURI) {
      throw new Error('UriGetPublisher seems not to respond, check it is running before running tests');
    }

    publisher.deleteUri('concept', createdURI.replace(/[^#]*#/, ''))
      .then((success) => {
        expect(success).toBeTruthy();
        done();
      })
      .catch((error) => {
        throw error;
      });
  });

  it('delete an URI that does not exist', (done) => {
    if (!createdURI) {
      throw new Error('UriGetPublisher seems not to respond, check it is running before running tests');
    }

    publisher.deleteUri('concept', createdURI.replace(/[^#]*#/, ''))
      .catch((error) => {
        expect(error.status).toEqual('error');
      })
      .then((success) => {
        expect(success).toEqual(false);
        done();
      });
  });
});

xdescribe('UriGetPublisher stress test', () => {
  var maxCount = 100,
    createdUris = [];

  it(`Create ${maxCount} URIs`, (done) => {
    var d = Date.now(), ps = [];

    for (var i = 0; i < 100; i++) {
      ps.push(
        publisher.createUri('concept')
          .then((uri) => {
            createdUris.push(uri);
          })
      );
    }

    Promise.all(ps)
      .then(() => {
        console.log(`Done in ${(Date.now() - d) / 1000} seconds`);

        expect(createdUris.length).toEqual(maxCount);
      })
      .then(done);
  });

  it(`Get ${maxCount} URIs`, (done) => {
    var d = Date.now(), ps = [], count = 0;

    createdUris.map(uri => {
      ps.push(
        publisher.getUri('concept', uri.replace(/[^#]*#/, ''))
          .then((uri) => {
            count++;
          })
          .catch(err => {
            console.error(`Erreur : ${err}`);
          }));
    });

    Promise.all(ps)
      .then(() => {
        console.log(`Done in ${(Date.now() - d) / 1000} seconds`);

        expect(count).toEqual(maxCount);
      })
      .then(done);
  });

  it(`Delete ${maxCount} URIs`, (done) => {
    var d = Date.now(), ps = [], count = 0;

    createdUris.map(uri => {
      ps.push(
        publisher.deleteUri('concept', uri.replace(/[^#]*#/, ''))
          .then((uri) => {
            count++;
          })
          .catch(err => {
            console.error(`Erreur : ${err}`);
          }));
    });

    Promise.all(ps)
      .then(() => {
        console.log(`Done in ${(Date.now() - d) / 1000} seconds`);

        expect(count).toEqual(maxCount);
      })
      .then(done);
  });
});
