/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import request from 'request';
import XML from 'pixl-xml';
import dayjs from 'dayjs';
import get from 'lodash/get';
import {logDebug, logError} from "../../../utilities/logger";

//request.debug = true;

/**
 * Cloud client
 */
export default class OwncloudClient {
  getAuthorizationHeader(asRoot = false) {
    let login = asRoot ? process.env.CLOUD_ROOT_LOGIN : process.env.CLOUD_ARCHIVE_LOGIN;
    let password = asRoot ? process.env.CLOUD_ROOT_PASSWORD : process.env.CLOUD_ARCHIVE_PASSWORD;

    return `Basic ${new Buffer(`${login}:${password}`).toString('base64')}`;
  }

  getRootSession() {
    return new Session({
      'Authorization': this.getAuthorizationHeader(true)
    });
  }

  getArchiveSession() {
    return new Session({
      'Authorization': this.getAuthorizationHeader(false)
    });
  }

  getUserSessionWithPassword(login, password) {
    return new Session({
      'Authorization': `Basic ${new Buffer(`${login}:${password}`).toString('base64')}`
    });
  }

  getUserSession(accessToken) {
    return new Session({
      // @see https://bitbucket.org/mnemotix/owncloud-user_keycloak
      'Authorization': `Basic Og==`,
      'Delegated-Authorization': `Bearer: ${accessToken}`
    });
  }
}

/**
 * Defines a cloud session.
 */
class Session {
  SHARE_TYPE_USER = 0;
  SHARE_TYPE_GROUP = 1;
  SHARE_TYPE_PUBLIC = 3;
  SHARE_TYPE_FEDERATED_CLOUD_SHARE = 6;
  SHARE_PERMISSION_READ = 1;
  SHARE_PERMISSION_UPDATE = 2;
  SHARE_PERMISSION_CREATE = 4;
  SHARE_PERMISSION_DELETE = 8;
  SHARE_PERMISSION_SHARE = 16;

  constructor(authHeaders = {}) {
    let host = process.env.CLOUD_BASE_URL || 'http://192.168.99.100:8082';

    this.baseWebDAV = `${host}/remote.php/webdav`;
    this.baseShareAPI = `${host}/ocs/v1.php/apps/files_sharing/api/v1`;
    this.baseCloudAPI = `${host}/ocs/v1.php/cloud`;

    this.authHeaders = authHeaders;
  }

  requestWebDAV(uri, options = {}) {
    let extraHeaders = {};

    if (options.headers) {
      extraHeaders = options.headers;
    }

    delete options.headers;

    return new Promise((resolve, reject) => {
      request({
        uri: `${this.baseWebDAV}${this.trulyEncodeUri(uri)}`,
        headers: {
          ...this.authHeaders,
          ...extraHeaders
        },
        followRedirect: true,
        followAllRedirects: true,
        followOriginalHttpMethod: true,
        ...options
      }, (error, response, body) => {
        if (!error) {
          resolve(body, response);
        } else {
          reject(error);
        }
      })
    });
  }

  requestAPI(uri, options = {}) {
    return new Promise((resolve, reject) => {
      request({
        uri,
        headers: {
          ...this.authHeaders
        },
        followRedirect: true,
        followAllRedirects: true,
        followOriginalHttpMethod: true,
        json: true,
        ...options
      }, (error, response, body) => {
        if (!error) {
          resolve(body, response);
        } else {
          reject(error);
        }
      });
    });
  }

  trulyEncodeUri(path) {
    return path.split('/').map((fragment) => encodeURIComponent(fragment)).join('/');
  }

  requestShareAPI(uri, options = {}, asRoot = false) {
    return this.requestAPI(`${this.baseShareAPI}${uri}`, options, asRoot);
  }

  requestCloudAPI(uri, options = {}, asRoot = false) {
    return this.requestAPI(`${this.baseCloudAPI}${uri}`, options, asRoot);
  }

  ping(options = {}, asRoot) {
    return this.requestAPI(`${this.baseCloudAPI}`, options, asRoot);
  }

  async propFind(path) {
    try {
      let body = await this.requestWebDAV(path, {
        method: 'PROPFIND',
        body: `<?xml version="1.0"?>
<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns">
  <d:prop>
    <d:getlastmodified />
    <d:getetag />
    <d:getcontenttype />
    <d:resourcetype />
    <oc:fileid />
    <oc:permissions />
    <oc:size />
    <d:getcontentlength />
    <oc:share-types />
  </d:prop>
</d:propfind>`
      });
    } catch (e) {
      logError(e);
    }

    try {
      let parsing = XML.parse(body);

      if (parsing && parsing['d:response']) {
        return parsing['d:response'].map(file => {
          let propstat, props = {};

          let href = file['d:href'].replace('remote.php/webdav/', '');

          if (file['d:propstat']) {
            if (file['d:propstat'].length) {
              propstat = file['d:propstat'].find(stats => stats['d:status'] === "HTTP/1.1 200 OK");
            } else {
              propstat = file['d:propstat'];
            }

            if (propstat && propstat['d:prop']) {
              props = {
                size: propstat['d:prop']['oc:size'] || propstat['d:prop']['d:getcontentlength'],
                internalId: propstat['d:prop']['oc:fileid'],
                mtime: dayjs(propstat['d:prop']['d:getlastmodified']).valueOf(),
                mime: propstat['d:prop']['d:getcontenttype'],
                isDirectory: !!propstat['d:prop']['d:resourcetype'] && typeof propstat['d:prop']['d:resourcetype']['d:collection'] != "undefined"
              };
            }
          }

          let splittedHref = href.split("/"),
            name;

          if (props.isDirectory && splittedHref.length > 1) {
            name = splittedHref[splittedHref.length - 2];
          } else {
            name = splittedHref[splittedHref.length - 1];
          }

          return {
            href,
            name: decodeURIComponent(name),
            ...props
          }
        });
      }
    } catch (e) {
      logError(e);
    }
  }

  createFolder(path, raiseErrorIfAlreadyExists = false) {
    if (!path || path === "/") {
      return Promise.resolve();
    }

    let paths = path.split('/').splice(1);
    let promise;

    paths.map((fragment, index) => {
      let path = '/' + paths.slice(0, index + 1).join('/');

      if (!promise) {
        logDebug("WebDAV command MKCOL : " + path);

        promise = this.requestWebDAV(path, {
          method: 'MKCOL'
        }).catch(err => {
          // 405 == Resource already exists
          if (err.statusCode !== 405 || raiseErrorIfAlreadyExists) {
            throw err;
          }
        });
      } else {
        promise = promise.then(() => {

          logDebug("WebDAV command MKCOL : " + path);

          return this.requestWebDAV(path, {
            method: 'MKCOL'
          }).catch(err => {
            // 405 == Resource already exists
            if (err.statusCode !== 405 || raiseErrorIfAlreadyExists) {
              throw err;
            }
          })
        })
      }
    });

    return promise;
  }

  moveFolder(sourcePath, targetPath) {
    return this.moveFile(sourcePath, targetPath);
  }

  removeFolder(path) {
    return this.requestWebDAV(path, {
      method: 'DELETE'
    })
  }

  getFileDownloadUrl(path) {
    return `${this.baseWebDAV}${path}`;
  }

  getFile(path) {
    return this.requestWebDAV(this._internalizePath(path), {
      method: 'GET'
    });
  }

  putFile(path, data) {
    let dirname = path.replace(/\/[^\/]*$/, '');

    return this.createFolder(dirname)
      .then(() => {
        logDebug('WebDAV command PUT :' + path);
        return this.requestWebDAV(path, {
          method: 'PUT',
          body: data
        })
      });
  }

  moveFile(sourcePath, targetPath) {
    return this.requestWebDAV(sourcePath, {
      method: 'MOVE',
      headers: {
        'Destination': `${this.baseWebDAV}${this.trulyEncodeUri(targetPath)}`
      },
    });
  }

  async streamFile(path, stream) {
    let dirname = path.replace(/\/[^\/]*$/, '');

    await this.createFolder(dirname);

    logDebug('WebDAV command PUT (by streaming):' + path);

    return new Promise((resolve, reject) => {
      stream.pipe(request.put({
        uri: `${this.baseWebDAV}${this.trulyEncodeUri(path)}`,
        headers: {
          ...this.authHeaders
        },
        followRedirect: true,
        followAllRedirects: true,
        followOriginalHttpMethod: true
      }, (error, response, body) => {
        if (!error) {
          resolve(body, response);
        } else {
          reject(error);
        }
      }));
    });
  }

  sharePublicResource(path, options = {}, returnJustUrl = true) {
    return this.requestShareAPI('/shares?format=json', {
      method: 'POST',
      body: {
        path: this._internalizePath(path),
        shareType: this.SHARE_TYPE_PUBLIC,
        publicUpload: false,
        ...options
      }
    }).then(rsp => {
      if (get(rsp, 'ocs.meta.statuscode', 500) < 400) {
        if (returnJustUrl) {
          return rsp.ocs.data.url + '/download';
        } else {
          let file = rsp.ocs.data;
          file.url += '/download';
          return file;
        }
      } else {
        logError(`Something gone wrong with file sharing : ${JSON.stringify(rsp, null, ' ')}`);
        throw new Error(`Something gone wrong with file sharing\n${rsp.ocs}`);
      }
    });
  }

  shareResourceWithGroup(path, groupId, options = {}, throwErrorIfGroupAlreadyShared = false) {
    return this.requestShareAPI('/shares?format=json', {
      method: 'POST',
      form: {
        path: this._internalizePath(path),
        shareType: this.SHARE_TYPE_GROUP,
        shareWith: groupId,
        permissions: this.SHARE_PERMISSION_READ | this.SHARE_PERMISSION_CREATE | this.SHARE_PERMISSION_UPDATE | this.SHARE_PERMISSION_DELETE,
        ...options
      }
    }).then(rsp => {
      switch (get(rsp, 'ocs.meta.statuscode', 500)) {
        case 400:
          throw new Error("Something gone wrong with file sharing: 400 - Unknown share type");
        case 403:
          if (throwErrorIfGroupAlreadyShared) {
            throw new Error("Something gone wrong with file sharing: 403 - Path already shared with this group");
          }
          break;
        case 404:
          throw new Error("Something gone wrong with file sharing: 404 - Wrong path, file/folder doesn't exist");
      }

      return rsp.ocs;
    });
  }

  getResourcePublicUrl(path) {
    return this.requestShareAPI(`/shares?format=json&reshares=true&path=${this._internalizePath(path)}`, {
      method: 'GET'
    }).then(rsp => {
      if (get(rsp, 'ocs.meta.statuscode', 500) < 400 && get(rsp, 'ocs.data') && rsp.ocs.data.length > 0) {
        let share = rsp.ocs.data.find(share => share.share_type === 3);

        if (share) {
          return share.url + '/download';
        }
      }
    });
  }

  addArchiveUser() {
    return this.addUser(this.archiveLogin, this.archivePassword);
  }

  addUser(userId, password, throwErrorIfGroupExists = false) {
    return this.requestCloudAPI('/users?format=json', {
      method: 'POST',
      form: {
        userid: userId,
        password: password
      }
    }, true).then(rsp => {
      switch (get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with user creation: 101 - Invalid input data");
        case 103:
          throw new Error("Something gone wrong with user creation: 103 - Unknown error occurred whilst adding the user");
        case 102:
          if (throwErrorIfGroupExists) {
            throw new Error("Something gone wrong with group creation: 102 - Username already exists");
          }
      }

      return true;
    });
  }

  addGroup(groupId, throwErrorIfGroupExists = false) {
    return this.requestCloudAPI('/groups?format=json', {
      method: 'POST',
      form: {
        groupid: groupId
      }
    }, true).then(rsp => {
      switch (get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with group creation: 101 - Invalid input data");
        case 103:
          throw new Error("Something gone wrong with group creation: 103 - Failed to add the group");
        case 102:
          if (throwErrorIfGroupExists) {
            throw new Error("Something gone wrong with group creation: 102 - Group already exists");
          }
      }

      return true;
    });
  }

  removeGroup(groupId, throwErrorIfGroupNotExists = false) {
    return this.requestCloudAPI(`/groups/${groupId}?format=json`, {
      method: 'DELETE'
    }).then(rsp => {
      switch (get(rsp, 'ocs.meta.statuscode', 500)) {
        case 102:
          throw new Error("Something gone wrong with group deletion: 101 - Failed to delete group");
        case 101:
          if (throwErrorIfGroupNotExists) {
            throw new Error("Something gone wrong with group deletion: 102 - Group does not exist");
          }
      }
      return true;
    }, true);
  }

  addUserToAdminGroup(userId) {
    return this.addUserToGroup(userId, 'admin');
  }

  addUserToGroup(userId, groupId) {
    return this.requestCloudAPI(`/users/${userId}/groups?format=json`, {
      method: 'POST',
      form: {
        groupid: groupId
      }
    }, true).then(rsp => {
      switch (get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with user to group: 101 - No group specified");
        case 102:
          throw new Error("Something gone wrong with user to group: 102 - Group doesn't exists");
        case 103:
          throw new Error("Something gone wrong with user to group: 103 - User doesn't exists");
        case 104:
          throw new Error("Something gone wrong with user to group: 104 - Insufficient privileges");
        case 105:
          throw new Error("Something gone wrong with user to group: 104 - Failed to add user to group");
      }

      return true;
    });
  }

  removeUserFromGroup(userId, groupId) {
    return this.requestCloudAPI(`/users/${userId}/groups?format=json`, {
      method: 'DELETE',
      form: {
        groupid: groupId
      }
    }, true).then(rsp => {
      switch (get(rsp, 'ocs.meta.statuscode', 500)) {
        case 101:
          throw new Error("Something gone wrong with user to group: 101 - No group specified");
        case 102:
          throw new Error("Something gone wrong with user to group: 102 - Group doesn't exists");
        case 103:
          throw new Error("Something gone wrong with user to group: 103 - User doesn't exists");
        case 104:
          throw new Error("Something gone wrong with user to group: 104 - Insufficient privileges");
        case 105:
          throw new Error("Something gone wrong with user to group: 104 - Failed to add user to group");
      }

      return true;
    });
  }

  /**
   *
   * @param req
   * @param res
   */
  proxifyRequest(req, res) {
    let url = this._internalizePath(req.query.url);

    return req.pipe(request({
      url,
      headers: {
        'Authorization': `Basic Og==`,
        'Delegated-Authorization': `Bearer: ${this.accessToken}`
      }
    })).pipe(res);
  }

  _internalizePath(path) {
    return path.replace(/\/[^\/]*\/files/, '');
  }
}
