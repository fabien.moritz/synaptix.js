/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import DataModulePublisher from '../DataModulePublisher';
import get from 'lodash/get';

/**
 *  @typedef {object} JsonLdNode - A JSON-LD node
 *  @property {string|object} [@context] - The context
 *  @property {string} [@type] - The type
 *  @property {string} [@id] - The URI
 *  @property {object} [...props] - The object and data properties.
 */

/**
 * Class used to publish commands to the Graphsync module (based on RDF triple store) on Synaptix.
 *
 *  @extends DataModulePublisher
 */
export default class GraphControllerPublisher extends DataModulePublisher {
  /* istanbul ignore next */
  getName() {
    return 'graph_controller_publisher';
  }

  /**
   * Create triples
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {object} jsonLdContext - The list of RDF prefixes (See @context attribute)
   * @param {JsonLdNode[]}  jsonLdNodes - The list of triples to create (See @graph attribute)
   * @param {string} [graphId] - The id of the named graph (if wanted)
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns {JsonLdNode[]}
   */
  async createTriples({jsonLdContext, jsonLdNodes, graphId, messageContext}) {
    let body = {
      "@context": jsonLdContext,
    };

    if (graphId) {
      body["@id"] = graphId;
    }

    body["@graph"] = jsonLdNodes;

    return this.publish('graph.create.triples', body, {
      ...messageContext,
      format: "JSON-LD"
    });
  }

  /**
   * Create triple
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {JsonLdNode} jsonLdNode - The list of RDF prefixes (See @context attribute)
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns
   */
  async createTriple({jsonLdNode, messageContext}) {
    return this.publish('graph.create.triples', [jsonLdNode], {
      ...messageContext,
      format: "JSON-LD"
    });
  }

  /**
   * Delete triples
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {object} jsonLdContext - The list of RDF prefixes (See @context attribute)
   * @param {string} query - The SPARQL Query
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns
   */
  async deleteTriples({query, messageContext}) {
    return this.publish('graph.delete.triples', query, {
      ...messageContext,
      format: "SPARQL"
    });
  }

  /**
   * Insert triples
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {string} query - The SPARQL Query
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns {JsonLdNode[]}
   */
  async insertTriples({query, messageContext}) {
    return this.publish('graph.insert.triples', query, {
      ...messageContext,
      format: "SPARQL"
    });
  }

  /**
   * Update triples
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {string} query - The SPARQL Query
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns
   */
  async updateTriples({query, messageContext}) {
    return this.publish('graph.update.triples', query, {
      ...messageContext,
      format: "SPARQL"
    });
  }


  /**
   * Execute a "SELECT" query against the triple store.
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {string} query - The SPARQL Query
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns {object} - SPARQL query result in JSON format https://www.w3.org/TR/sparql11-results-json/
   * @example :
   */
  async select({query, messageContext}) {
    return this.publish('graph.select', query, {
      ...messageContext,
      format: "SPARQL"
    });
  }

  /**
   * Execute a "SELECT" query against the triple store and assume that the result is a count.
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {string} query - The SPARQL Query
   * @param {string} [countVariable=count] - The SPARQL variable where count is stored.
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns {object} - SPARQL query result in JSON format https://www.w3.org/TR/sparql11-results-json/
   * @example :
   */
  async count({query, countVariable, messageContext}) {
    let sparqlResult = await this.publish('graph.select', query, {
      ...messageContext,
      format: "SPARQL"
    });

    return parseInt(get(sparqlResult, `results.bindings.0.${countVariable || 'count'}.value`, 0));
  }

  /**
   * Execute a "CONSTRUCT" query against the triple store.
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-sobjects
   *
   * @param {string} query - The SPARQL Query
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns {JsonLdNode}
   */
  async construct({query, messageContext}) {
    return this.publish('graph.construct', query, {
      ...messageContext,
      format: "SPARQL"
    });
  }

  /**
   * Execute a "ASK" query against the triple store.
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {string} query - The SPARQL Query
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns {boolean}
   */
  async ask({query, messageContext}) {
    return this.publish('graph.ask', query, {
      ...messageContext,
      format: "SPARQL"
    });
  }

  /**
   * Execute a "DESCRIBE" query against the triple store.
   *
   * @see https://json-ld.org/spec/latest/json-ld/#graph-objects
   *
   * @param {string} query - The SPARQL Query
   * @param {object} [messageContext]  - Context information to add as AMQP headers.
   *
   * @returns {boolean}
   */
  async describe({query, messageContext}) {
    return this.publish('graph.describe', query, {
      ...messageContext,
      format: "SPARQL"
    });
  }
}