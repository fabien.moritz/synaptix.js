/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import uuid from 'uuid/v4';
import get from "lodash/get";
import {SynaptixError} from "./SynaptixError";
import {logError} from "../../utilities/logger";

/**
 * @typedef {object} SynaptixResponse
 * @property {string} command
 * @property {string} sender
 * @property {number} date
 * @property {*} body
 */

export default class DataModulePublisher {
  /**
   * @param {string} senderId
   * @param {NetworkLayerAMQP} networkLayer
   * @param {boolean} [useLegacySynaptixFormat=false] - Use Synaptix V1 AMQP message format
   */
  constructor(networkLayer, senderId, useLegacySynaptixFormat) {
    this._networkLayer = networkLayer;
    this._senderId = senderId;
    this._useLegacySynaptixFormat = useLegacySynaptixFormat || !!parseInt(process.env.USE_LEGACY_SYNAPTIX_FORMAT);
  }

  /* istanbul ignore next */
  getName() {
    return uuid();
  }

  /**
   * Publish an RPC AMQP message.
   *
   * @param {string} command RPC command
   * @param {*} body RPC body
   * @param {object} context extra AMQP headers
   * @param {boolean} [expectingResponse=true]
   * @returns {*}
   */
  async publish(command, body, context = {}, expectingResponse = true) {
    let payload = this._useLegacySynaptixFormat ? {
      command,
      sender: this._senderId || process.env.UUID || 'mnx:app:nodejs',
      date: Date.now(),
      context,
      body
    } : {
      headers: {
        command,
        sender: this._senderId || process.env.UUID || 'mnx:app:nodejs',
        timestamp: Date.now(),
        ...context
      },
      body
    };

    if (expectingResponse) {
      let response = await this._networkLayer.request(command, payload);

      if (response) {
        let data = typeof response === "object" ? response : JSON.parse(response);

        if (this._useLegacySynaptixFormat) {
          let status = get(data, 'status');

          if (!!status) {
            // Is the response successful ?
            if (['ok', 'empty'].indexOf(status.toLowerCase()) !== -1) {
              return data.body;
            } else {
              logError(`Something gone wrong with the RPC command ${data.command} : ${JSON.stringify({
                command: data.command,
                requestPayload: payload.body,
                status: data.status,
                error: data.body
              }, null, " ")}`);

              throw new SynaptixError(data.body, data.command, data.status, payload.body);
            }
          }
        } else {
          let status = get(data, 'headers.status');

          if (!!status) {
            if ('OK' === status) {
              return data.body;
            } else {
              logError(`Something gone wrong with the RPC command ${JSON.stringify(payload)}. ${status} : ${JSON.stringify(data.body)}`);
              throw new SynaptixError(data.body, data.command, status, payload.body);
            }
          }
        }
      }

      logError(`Callback timeout has been raised with the RPC command ${payload.headers ? payload.headers.command : payload.command} ${JSON.stringify(payload)}.`);
      throw  new SynaptixError("Callback timeout has been raised with the Synaptix command.", payload.command, "TIMEOUT", payload);
    } else {
      await this._networkLayer.publish(command, payload);
    }
  }
}