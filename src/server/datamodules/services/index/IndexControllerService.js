/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */
import get from 'lodash/get';
import {cursorToOffset} from "../../../datamodel/toolkit/graphql/helpers/connectionHelpers";

import {createModelFromDocument, createModelFromNode} from '../../../datamodel/toolkit/models/helpers';

import {IndexControllerV2Publisher, IndexSyncV6Publisher} from "../../drivers";
import {createNode} from "../../drivers/dse/models";
import NetworkLayerAbstract from "../../../network/NetworkLayerAbstract";

export default class IndexControllerService {
  /**
   * Constructor
   *
   * @param {NetworkLayerAbstract} networkLayer
   * @param {GraphQLContext} context
   */
  constructor(networkLayer, context) {
    if (!context) {
      throw new Error(`You must provide a GrapQLContext to ${this.constructor.name}::constructor()`);
    }

    this.context = context.getUser() ? context.getUser().toJSON() : {};

    switch (process.env.USE_INDEX_VERSION) {
      case "V2":
      case "LEGACY":
        this.indexPublisher = new IndexControllerV2Publisher(networkLayer, context.getUser() ? context.getUser().getId() : process.env.UUID);
        break;
      default:
        this.indexPublisher = new IndexSyncV6Publisher(networkLayer, context.getUser() ? context.getUser().getId() : process.env.UUID);
    }

  }

  static isDSLQuery(qs) {
    return !!qs.match(/\+|-|=|&&|\|\|>|<\\!|\(|\)|{|}|\[|\\]|\^|"|\\~|\*|\?|:|\\|\/| OR | AND /);
  }

  useLegacyFormat() {
    return !!parseInt(process.env.USE_LEGACY_INDEX_FORMAT);
  }

  /**
   * Generic method to retrieve percolated tags for a node
   *
   * @param nodeId
   * @param parentId
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   */
  async getPercolatedConceptsFor(modelDefinition, nodeId, parentId) {
    try {
      let results = await this.indexPublisher.percolate(modelDefinition.getNodeType(), nodeId, parentId);

      return results.map((perco) => {
        const {target: conceptId, label, src: thesaurusId} = perco;

        return createModelFromNode(modelDefinition.getModelClass(), createNode(modelDefinition.getNodeType(), conceptId, {
          prefLabels: [label]
        }))
      });
    } catch (e) {
      return [];
    }
  }

  /**
   * Make a fulltext search on index type
   *
   * @param types
   * @param ModelClasses
   * @param args
   * @param query
   * @param fields
   * @param extraFilters
   * @param justCount
   * @param returnFirstOne
   */
  async fulltextSearch({types, ModelClasses, args, query, extraFilters, fields, justCount, returnFirstOne}) {
    let {first: size, after} = args || {};
    let from = after ? cursorToOffset(after) : 0;

    if (justCount === true) {
      size = 0;
      fields = {excludes: '*'};
    } else {
      if (!size) {
        size = 1;
      } else {
        // This is for Relay weird cursor connection https://facebook.github.io/relay/docs/graphql-connections.html
        size += from + 2;
      }
    }


    let {sortBy, sortDirection, sortings} = args;

    let sort = [],
      extra = {
        from: 0,
        size
      };

    let applySorting = (sortBy, sortDirection, sortFilters) => {
      if (sortBy.match(/^_count:/)) {
        let key = sortBy.slice(7);

        sort.push({
          _script: {
            type: "number",
            script: {
              inline: `
              if(_source.${key}) {
                return _source.${key}.values.size()
              } else {
                return 0;
              }
            `
            },
            order: sortDirection ? sortDirection : 'asc'
          }
        });
      } else {
        let extraSortProperties = {};

        if (sortBy.match(/\./)) {
          let sortFragments = sortBy.split(".");

          if (sortFragments.length > 1) {
            let offset = !!sortBy.match(/\.raw/) ? 2 : 1;

            extraSortProperties["nested_path"] = sortFragments.slice(0, sortFragments.length - offset).join('.');
          }
        }

        if (sortFilters) {
          let filters = [];

          sortFilters.map(sortFilter => {
            let filterFragments = sortFilter.split(":");
            if (filterFragments[1] === "_true_") {
              filterFragments[1] = true;
            } else if (filterFragments[1] === "_false_") {
              filterFragments[1] = false;
            }

            filters.push({
              term: {[filterFragments[0]]: filterFragments[1]}
            });
          });

          extraSortProperties["nested_filter"] = {
            "and": filters
          }
        }

        sort.push({
          [sortBy]: {
            order: sortDirection ? sortDirection : 'asc',
            ...extraSortProperties
          }
        });
      }
    };

    if (sortings) {
      sortings.map(({sortBy, sortDirection, sortFilters}) => applySorting(sortBy, sortDirection, sortFilters))
    }

    if (sortBy) {
      applySorting(sortBy, sortDirection);
    }

    if (fields) {
      extra._source = fields;
    }

    let result = await this.indexPublisher.query({
      types,
      query: {
        "query": {
          "filtered": {
            "query": query,
            "filter": {
              "and": [
                {
                  "term": {"_enabled": true}
                },
                ...(extraFilters || [])
              ]
            }
          }
        },
        sort,
        ...extra
      },
      from,
      size
    });


    let hitToNode = (hit) => {
      let ModelClass = typeof ModelClasses === "object" ? ModelClasses[hit._type] : ModelClasses;

      return createModelFromNode(ModelClass, createNode(types, hit._id, hit._source))
    };

    if (justCount === true) {
      return result.total;
    } else {
      return returnFirstOne ? hitToNode(result.hits[0]) : result.hits.map(hit => hitToNode(hit));
    }
  }


  /**
   * Make a fast search on index type and return facets
   *
   * @param types
   * @param aggs
   * @param args
   * @param extraFilters
   * @returns {*}
   */
  async aggregate({types, aggs, query, extraFilters = []}) {
    let result = await this.indexPublisher.query({
      types,
      query: {
        "query": {
          "filtered": {
            "query": query,
            "filter": {
              "and": [
                {
                  "term": {"_enabled": true}
                },
                ...(extraFilters || [])
              ]
            }
          }
        }
      },
      size: 0,
      from: 0,
      fullResponse: true
    });

    return result.aggregations;
  }

  /**
   * Get a node
   *
   * @param documentId
   * @param indexType
   * @param documentModel
   * @param restrictFields
   * @returns {Promise.<*>}
   */
  async getNode(documentId, indexType, documentModel, restrictFields = "*") {
    let result = await this.indexPublisher.query({
      types: indexType,
      query: {
        "query": {
          "bool": {
            "must": [
              {
                "ids": {"values": [documentId]}
              },
              {
                "term": {"_enabled": true}
              }
            ]
          }
        },
        "_source": restrictFields
      }
    });

    if (result.hits.length === 1) {
      let hit = result.hits[0];

      if (typeof documentModel === "object") {
        documentModel = documentModel[hit._type];
      }

      if (Array.isArray(indexType)) {
        indexType = indexType.find(type => hit._type === type.toLowerCase());
      }

      return createModelFromNode(documentModel, createNode(indexType, hit._id, hit._source || {}));
    }
  }

  /**
   * Get documents linked to another document
   * @param document
   * @param property
   * @param nodeModel
   * @param includeDisabledLinks
   *
   * @return Instance (or array of instances) of nodeModel
   */
  getLinkedDocuments(document, property, nodeModel, includeDisabledLinks = false) {
    let returnValues;

    let targetValue = get(document, property, false);

    if (targetValue) {
      if (Array.isArray(targetValue)) {
        returnValues = targetValue
          .filter(link => includeDisabledLinks || link._enabled === true)
          .map(link => createModelFromDocument(nodeModel, link));
        returnValues.sort((a, b) => a.creationDate - b.creationDate);
      } else {
        if (includeDisabledLinks || targetValue._enabled === true) {
          returnValues = createModelFromDocument(nodeModel, targetValue);
        }
      }
    }

    return returnValues;
  }

  /**
   * Search related documents using the More Like This query
   *
   * @see https://www.elastic.co/guide/en/elasticsearch/reference/2.4/query-dsl-mlt-query.html
   *
   * @param types
   * @param documentIds
   * @param lookupFields
   * @param ModelClasses
   * @param args
   * @returns {Promise.<void>}
   */
  async searchMoreLikeThisDocuments(types, ModelClasses, documentIds, lookupFields, args = {}) {
    let {first: size, after} = args;
    let offset = after ? cursorToOffset(after) : 0;

    if (typeof documentIds === "string") {
      documentIds = [documentIds];
    }

    let query = {
      "_source": {"excludes": "*.*"},
      "query": {
        "filtered": {
          "query": {
            "more_like_this": {
              "fields": lookupFields,
              "like": documentIds.map(documentId => {
                if (typeof documentId === "string") {
                  return {
                    "_id": documentId
                  };
                } else {
                  return documentId;
                }
              }),
              "min_term_freq": 1,
              "max_query_terms": 12
            }
          },
          "filter": [
            {
              "term": {"_enabled": true}
            }
          ]
        }
      }
    };

    let result = await this.indexPublisher.query({
      types,
      query,
      from: 0,
      size: offset + size + 2
    });

    return result.hits.map(hit => {
      let ModelClass = typeof ModelClasses === "object" ? ModelClasses[hit._type] : ModelClasses;

      return createModelFromNode(ModelClass, createNode(types, hit._id, hit._source))
    })
  }
}
