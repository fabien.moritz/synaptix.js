/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import {createModelFromNode} from '../../../datamodel/toolkit/models/helpers';
import uuid from 'uuid';
import {
  getLabelDefinitionEdge,
  getLinkDefinitionFromGroovy,
  getModelDefinitionCascadingRemoveLinks,
  getModelDefinitionCascadingUpdateLinks
} from "../../../datamodel/toolkit/definitions/helpers";
import Edge from "../../drivers/dse/models/Edge";
import Node from "../../drivers/dse/models/Node";
import LocalizedLabelDefinition from "../../../datamodel/ontologies-legacy/definitions/common/LocalizedLabelDefinition";
import HTMLContentDefinition from "../../../datamodel/ontologies-legacy/definitions/common/HTMLContentDefinition";
import DseControllerPublisher from "../../drivers/dse/DseControllerPublisher";
import UrigenPublisher from "../../drivers/urigen/UrigenPublisher";
import {logDebug} from "../../../utilities/logger";
import {Link} from "../../../datamodel/toolkit/definitions/LinkDefinition";
import {cursorToOffset} from "../../../datamodel/toolkit/graphql/helpers/connectionHelpers";

/**
 * Service used to perform CRUD on a property graph store.
 */
export default class DseControllerService {
  _networkLayer;

  /**
   * @param {NetworkLayerAMQP} networkLayer
   * @param {GraphQLContext} context
   */
  constructor(networkLayer, context) {
    if (!context) {
      throw new Error(`You must provide a GrapQLContext to ${this.constructor.name}::constructor()`);
    }

    this.context = context.getUser() ? context.getUser().toJSON() : {};
    this._dseControllerPublisher = new DseControllerPublisher(networkLayer, context.getUser() ? context.getUser().getId() : process.env.UUID);
    this._uriGenPublisher = new UrigenPublisher(networkLayer, context.getUser() ? context.getUser().getId() : process.env.UUID);
    this._networkLayer = networkLayer;
  }

  /**
   * @param {NodeSerialization[]} nodes
   */
  async waitForIndexSyncingOf(nodes) {
    return new Promise((resolve) => {
      if (nodes.length === 0) {
        return resolve();
      }

      logDebug(`Wait for ${nodes.length} node(s) to be synced !`);

      let updatingNodes = [];
      let creatingNodes = [];

      for (let node of nodes) {
        if (node._id) {
          updatingNodes.push(node._id)
        } else {
          creatingNodes.push(node.uri);
        }
      }

      let listeningQueue;

      let timeout = setTimeout(() => {
        logDebug("Index sync timeout !");
        if (listeningQueue) {
          listeningQueue.close();
        }
        resolve();
      }, 5000);

      this._networkLayer.listen('sync.index.node.*', (response) => {
        /** @type {SynaptixResponse} */
        let synaptixResponse = response;

        switch (synaptixResponse.command) {
          case 'sync.index.node.updated':
            let index = updatingNodes.indexOf(synaptixResponse.body);

            if (index >= 0) {
              updatingNodes.splice(index, 1);
            }
            break;
          case 'sync.index.node.created':
            creatingNodes.pop();
            break;
        }

        if (updatingNodes.length === 0 && creatingNodes.length === 0) {
          clearTimeout(timeout);
          logDebug("Index synced !");
          if (listeningQueue) {
            listeningQueue.close();
          }
          resolve();
        }

      }).then(queue => listeningQueue = queue);
    });
  }

  /**
   * @return {DseControllerPublisher}
   */
  getGraphSyncPublisher() {
    return this._dseControllerPublisher;
  }

  /**
   * Generic method to remove a node
   *
   * @param id
   * @param ModelClass
   * @returns {*}
   */
  async getNode(id, ModelClass) {
    let node = await this._dseControllerPublisher.getNode(id, this.context);
    return createModelFromNode(ModelClass, node);
  }

  /**
   * Create a node
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition -
   * @param {Link[]} links -  List of links
   * @param {object} [objectInput] - Object input
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {boolean} [waitForIndexSyncing] - Should wait indexation task.
   */
  async createNode({modelDefinition, links, objectInput, lang, uri, waitForIndexSyncing}) {
    let waitIndexationOfNodes = [];
    let extraNodes = {}, extraEdges = [];

    objectInput = objectInput || {};
    lang = lang || 'fr';

    (links || []).map(async (link) => {
      let targetId = link.getTargetId();
      let targetInput = link.getTargetObjectInput();
      let linkDefinition = link.getLinkDefinition();
      let targetModelDefinition = linkDefinition.getRelatedModelDefinition();
      let {direction, edgeName} = getLinkDefinitionFromGroovy(linkDefinition.getPathInGraphstore(), false, link.isReversed());

      // If linked node does not exit, create it.
      if (!targetId) {
        targetId = uuid.v4();

        let targetUri = await this._uriGenPublisher.createUri(targetModelDefinition.getNodeType());

        extraNodes[targetId] = Node.serializePartial(targetModelDefinition.getNodeType(), targetUri, {
          ...targetInput,
          creationDate: Date.now()
        });

        if (targetModelDefinition.isIndexed()) {
          waitIndexationOfNodes.push(extraNodes[targetId]);
        }
        // Otherwise, update it
      } else {
        extraNodes[`Update-${targetId}`] = Node.serializeNode(targetModelDefinition.getNodeType(), targetId, "", {
          lastUpdate: Date.now()
        });

        if (targetModelDefinition.isIndexed()) {
          waitIndexationOfNodes.push(extraNodes[`Update-${targetId}`]);
        }
      }

      extraEdges.push(
        Edge.serializePartial(direction === 'out' ? "_:0" : targetId, edgeName, direction === 'out' ? targetId : "_:0"),
      )
    });

    let localizedLabelsProps = [],
      straightProps = {
        creationDate: Date.now(),
        lastUpdate: Date.now()
      };

    for (let propName in objectInput) {
      if (!objectInput.hasOwnProperty || objectInput.hasOwnProperty(propName)) {
        let value = objectInput[propName];

        if (value) {
          let label = modelDefinition.getLabel(propName);

          if (label) {
            localizedLabelsProps.push({
              value,
              labelDefinition: label
            });
          } else {
            straightProps[propName] = value;
          }
        }
      }
    }

    let {graphNodes: labelsNodes, graphEdges: labelsEdges} = await this.updateNodeLocalizedLabels({
      localizedLabelsProps,
      lang,
      sourceNodeId: "_:0",
      returnAsGraphObject: true
    });

    let objectUri = uri ? uri : await this._uriGenPublisher.createUri(modelDefinition.getNodeType());
    let objectSerialization = Node.serializePartial(modelDefinition.getNodeType(), objectUri, {
      ...straightProps
    });

    let {nodes} = await this._dseControllerPublisher.createGraph({
      "_:0": objectSerialization,
      ...extraNodes,
      ...labelsNodes
    }, [
      ...extraEdges,
      ...labelsEdges
    ]);

    if (modelDefinition.isIndexed()) {
      waitIndexationOfNodes.push(objectSerialization);
    }

    if (waitForIndexSyncing) {
      await this.waitForIndexSyncingOf(waitIndexationOfNodes);
    }

    return createModelFromNode(modelDefinition.getModelClass(), nodes["_:0"]);
  }

  /**
   * Create an edge.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param objectId
   * @param {LinkDefinition} linkDefinition
   * @param targetId
   * @param {bool} [waitForIndexSyncing] : Should wait indexation task.
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId, waitForIndexSyncing) {
    let waitIndexationOfNodes = [];

    let {direction, edgeName} = getLinkDefinitionFromGroovy(linkDefinition.getPathInGraphstore());
    let targetDefinition = linkDefinition.getRelatedModelDefinition();

    let objectNodeSerialization = Node.serializeNode(modelDefinition.getNodeType(), objectId, "", {lastUpdate: Date.now()});
    let targetNodeSerialization = Node.serializeNode(targetDefinition.getNodeType(), targetId, "", {lastUpdate: Date.now()});

    if (modelDefinition.isIndexed()) {
      waitIndexationOfNodes.push(objectNodeSerialization);
    }

    if (targetDefinition.isIndexed()) {
      waitIndexationOfNodes.push(targetNodeSerialization);
    }

    let {nodes} = await this._dseControllerPublisher.createGraph({
      [objectId]: objectNodeSerialization,
      [targetId]: targetNodeSerialization
    }, [
      Edge.serializePartial(direction === 'out' ? objectId : targetId, edgeName, direction === 'out' ? targetId : objectId),
    ]);

    if (waitForIndexSyncing) {
      await this.waitForIndexSyncingOf(waitIndexationOfNodes);
    }

    return {
      target: createModelFromNode(targetDefinition.getModelClass(), nodes[targetId]),
      object: createModelFromNode(modelDefinition.getModelClass(), nodes[objectId])
    }
  }

  /**
   * Delete all edges.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   */
  async deleteEdges(modelDefinition, objectId, linkDefinition, targetId) {
    let {direction, edgeName} = getLinkDefinitionFromGroovy(linkDefinition.getPathInGraphstore());

    return this._dseControllerPublisher.deleteEdges(direction === 'out' ? objectId : targetId, edgeName, direction === 'out' ? targetId : objectId, {}, this.context);
  }

  /**
   * Generic method to update a node.
   * @param {string} objectId
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string|null} lang
   * @param {object} updatingProps
   * @param {array|null} extraUpdatingEdges
   * @param {object|null} extraUpdatingNodes
   * @param {bool} [waitForIndexSyncing] : Should wait indexation task.
   */
  async updateNode({modelDefinition, objectId, updatingProps, lang, extraUpdatingEdges, extraUpdatingNodes, waitForIndexSyncing}) {
    let waitIndexationOfNodes = [];

    if (!extraUpdatingNodes) {
      extraUpdatingNodes = {};
    }

    if (!lang) {
      lang = 'fr'
    }

    let localizedLabelsProps = [],
      straightProps = {
        lastUpdate: Date.now()
      };

    for (let prop in updatingProps) {
      if (!updatingProps.hasOwnProperty || updatingProps.hasOwnProperty(prop)) {
        let value = updatingProps[prop];

        if (value !== null) {
          let labelDefinition = modelDefinition.getLabel(prop);

          if (labelDefinition) {
            localizedLabelsProps.push({
              value,
              labelDefinition
            });
          } else {
            straightProps[prop] = value;
          }
        }
      }
    }

    let cascadingUpdatingLinkDefinitions = getModelDefinitionCascadingUpdateLinks(modelDefinition);

    for (let cascadingUpdatingLinkDefinition of cascadingUpdatingLinkDefinitions) {
      let pathInGraphStore = cascadingUpdatingLinkDefinition.getPathInGraphstore();

      if (pathInGraphStore) {
        let modelDefinition = cascadingUpdatingLinkDefinition.getRelatedModelDefinition();

        let nodes = await this.queryGraphNodes(`
          g.V('${objectId}')
          .${pathInGraphStore}
          .${DseControllerPublisher.getEnabledNodeFilter()}
        `);

        for (let node of nodes) {
          logDebug(`Cascading update node ${modelDefinition.getNodeType()}:${node.id}`);
          extraUpdatingNodes[`${node.id}-Update`] = Node.serializeNode(modelDefinition.getNodeType(), node.id, "", {
            lastUpdate: Date.now()
          });

          if (modelDefinition.isIndexed()) {
            waitIndexationOfNodes.push(extraUpdatingNodes[`${node.id}-Update`]);
          }
        }
      }
    }

    let {graphNodes, graphEdges} = await this.updateNodeLocalizedLabels({
      localizedLabelsProps,
      lang,
      sourceNodeId: objectId,
      returnAsGraphObject: true
    });

    let updatingNode = Node.serializeNode(modelDefinition.getNodeType(), objectId, "", straightProps);

    if (modelDefinition.isIndexed()) {
      waitIndexationOfNodes.push(updatingNode)
    }

    let graph = await this._dseControllerPublisher.createGraph({
      [objectId]: updatingNode,
      ...graphNodes,
      ...(extraUpdatingNodes || {})
    }, graphEdges.concat(extraUpdatingEdges || []));

    if (waitForIndexSyncing) {
      await this.waitForIndexSyncingOf(waitIndexationOfNodes);
    }

    return createModelFromNode(modelDefinition.getModelClass(), graph.nodes[objectId]);
  }

  /**
   * Generic method to remove a node
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param id
   * @param permanentRemoval
   */
  async removeNode(modelDefinition, id, permanentRemoval = false) {
    let result = await this._dseControllerPublisher.disableNode(modelDefinition.getNodeType(), id, this.context);

    let localizedLabelsNodes = await this.queryGraphNodes(`
      g.V('${id}')
      .both()
      .hasLabel('${LocalizedLabelDefinition.getNodeType()}', '${HTMLContentDefinition.getNodeType()}')
      .${DseControllerPublisher.getEnabledNodeFilter()}
    `);

    for (let localizedLabelNode of localizedLabelsNodes) {
      logDebug(`Removing localized label ${localizedLabelNode.id} (${localizedLabelNode.getPropertyValue('value')})`);
      await this._dseControllerPublisher.disableNode(localizedLabelNode.getNodeType(), localizedLabelNode.id, this.context);
    }

    let cascadingRemovingLinkDefinitions = getModelDefinitionCascadingRemoveLinks(modelDefinition);

    for (let cascadingRemovingLinkDefinition of cascadingRemovingLinkDefinitions) {
      let pathInGraphStore = cascadingRemovingLinkDefinition.getPathInGraphstore();

      if (pathInGraphStore) {
        let modelDefinition = cascadingRemovingLinkDefinition.getRelatedModelDefinition();

        let nodes = await this.queryGraphNodes(`
          g.V('${id}')
          .${pathInGraphStore}
          .${DseControllerPublisher.getEnabledNodeFilter()}
        `);

        for (let node of nodes) {
          logDebug(`Cascading remove node ${modelDefinition.getNodeType()}:${node.id}`);
          await this.removeNode(modelDefinition, node.id, permanentRemoval)
        }
      }
    }

    let cascadingUpdatingLinkDefinitions = getModelDefinitionCascadingUpdateLinks(modelDefinition);

    for (let cascadingUpdatingLinkDefinition of cascadingUpdatingLinkDefinitions) {
      let pathInGraphStore = cascadingUpdatingLinkDefinition.getPathInGraphstore();

      if (pathInGraphStore) {
        let modelDefinition = cascadingUpdatingLinkDefinition.getRelatedModelDefinition();

        let nodes = await this.queryGraphNodes(`
          g.V('${id}')
          .${pathInGraphStore}
          .${DseControllerPublisher.getEnabledNodeFilter()}
        `);

        for (let node of nodes) {
          logDebug(`Cascading update node ${modelDefinition.getNodeType()}:${node.id}`);

          await this.updateNode({
            modelDefinition,
            objectId: node.id,
            updatingProps: {}
          });
        }
      }
    }

    if (permanentRemoval) {
      result = await this._dseControllerPublisher.deleteNode(id, this.context);
    }

    return result;
  }

  /**
   * Generic method to update localized labels of a node
   *
   * @param {string} lang
   * @param {string} sourceNodeId
   * @param {object[]} localizedLabelsProps
   * @param {LabelDefinition} localizedLabelsProps.labelDefinition
   * @param {string} localizedLabelsProps.value
   * @param returnAsGraphObject
   */
  async updateNodeLocalizedLabels({lang, sourceNodeId, localizedLabelsProps, returnAsGraphObject}) {
    let graphNodes = {}, graphEdges = [];

    for (let localizedLabelsProp of localizedLabelsProps) {
      /** @var {LabelDefinition} */
      let labelDefinition = localizedLabelsProp.labelDefinition;
      let value = localizedLabelsProp.value;
      let labelType = labelDefinition.getNodeType();

      let localizedLabel = await this.getLocalizedLabelForNode({
        labelDefinition,
        sourceNodeId,
        lang,
        returnAsLocalizedLabel: true,
        dontReturnFallbackLabel: true
      });

      if (localizedLabel) {
        if (returnAsGraphObject) {
          graphNodes[localizedLabel.id] = Node.serializeNode(labelType, localizedLabel.id, localizedLabel.uri, {
            value,
            _enabled: true
          });
        } else {
          await this._dseControllerPublisher.updateNode(labelType, localizedLabel.id, {
            value,
            _enabled: true
          }, this.context);
        }
      } else {
        let updateGraph = await this.addLocalizedLabel({
          sourceNodeId,
          lang,
          value,
          labelDefinition,
          returnAsGraphObject,
          labelType
        });

        if (returnAsGraphObject) {
          Object.assign(graphNodes, updateGraph.graphNodes);
          graphEdges = graphEdges.concat(updateGraph.graphEdges);
        }
      }
    }

    if (returnAsGraphObject) {
      return {graphNodes, graphEdges};
    }
  }

  /**
   * Add a localized label
   * @param sourceNodeId Source node id
   * @param lang Lang
   * @param value
   * @param {LabelDefinition} labelDefinition
   * @param returnAsGraphObject
   * @param returnAsModelClass
   */
  async addLocalizedLabel({sourceNodeId, lang, value, labelDefinition, returnAsGraphObject, returnAsModelClass}) {
    let labelType = labelDefinition.getNodeType();

    if (!lang) {
      lang = 'fr';
    }

    let graphNodes = {}, graphEdges = [], labeNodeId = uuid.v4();

    let labelUri = await this._uriGenPublisher.createUri(labelType);

    graphNodes[labeNodeId] = Node.serializePartial(labelType, labelUri, {
      creationDate: (new Date()).getTime(),
      lang,
      value
    });

    graphEdges.push(Edge.serializePartial(sourceNodeId, getLabelDefinitionEdge(labelDefinition), labeNodeId));

    if (returnAsGraphObject) {
      return {graphNodes, graphEdges};
    } else {
      let {nodes} = await this._dseControllerPublisher.createGraph(graphNodes, graphEdges);

      if (returnAsModelClass) {
        return createModelFromNode(returnAsModelClass, nodes[labeNodeId]);
      }
    }
  }

  /**
   * Get a localized label.
   * @param {LabelDefinition} labelDefinition
   * @param sourceNode Source node id
   * @param lang Lang
   * @param relationName Relation name
   * @param returnAsLocalizedLabel Return
   *
   */
  async getLocalizedLabelForNode({labelDefinition, sourceNode, sourceNodeId, lang, returnAsLocalizedLabel, dontReturnFallbackLabel}) {
    if (!sourceNodeId) {
      sourceNodeId = sourceNode.id;
    }

    let pathInGraphstore = labelDefinition.getPathInGraphstore();

    if (pathInGraphstore) {
      let nodes = await this.queryGraphNodes(
        `g.V('${sourceNodeId}')
        .${pathInGraphstore}
        ${dontReturnFallbackLabel === true ? `.has('lang', '${lang}')` : ""}
      `
      );


      if (nodes.length > 0) {
        let targetNode = nodes.find(node => node.getPropertyValue('lang') === lang);

        if (!targetNode) {
          targetNode = nodes[0];
        }

        return returnAsLocalizedLabel ? createModelFromNode(labelDefinition.getModelClass(), targetNode) : targetNode.getPropertyValue('value');
      }
    }
  }

  /**
   * Is label translated for lang.
   * @param sourceNode Source node
   * @param lang Lang
   * @param relationName Relation name
   *
   * @return {boolean}
   */
  async isLocalizedLabelTranslatedForNode({sourceNode, lang, relationName}) {
    return false; // TODO: Fix this !
  }

  /**
   * Generic method to get nodes from graph query
   *
   * @param query
   * @param args
   * @returns {Node[]}
   */
  async queryGraphNodes(query, args = {}) {
    if (args.sortBy) {
      query += `.order().by('${args.sortBy}', ${args.sortDirection === "desc" ? "decr" : "incr"})`;
    }

    if (args.first) {
      if (args.after) {
        query += `.range(0, ${cursorToOffset(args.after) + args.first + 2})`;
      } else {
        query += `.limit(local, ${args.first + 2})`;
      }
    }

    return this._dseControllerPublisher.queryGraphNodes(query, this.context);
  }

  /**
   * Query objects
   *
   * @param modelDefinition
   * @param query
   * @param args
   * @return {Promise.<void>}
   */
  async queryObjects(modelDefinition, query, args = {}) {
    let nodes = await this.queryGraphNodes(query || `
      g.V()
       .hasLabel('${modelDefinition.getNodeType()}')
       .${DseControllerPublisher.getEnabledNodeFilter()}
     `, args);

    return nodes.map(node => createModelFromNode(modelDefinition.getModelClass(), node));
  }

  /**
   * Query raw graph
   * @param groovy
   * @return {*}
   */
  async queryRawGraph(groovy) {
    return this._dseControllerPublisher.queryRawGraph(groovy, this.context);
  }
}
