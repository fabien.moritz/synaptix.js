
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import omit from "lodash/omit";
import get from "lodash/get";
import has from "lodash/has";
import dayjs from "dayjs";
import {replaceAbsoluteNamespaceToPrefix, replacePrefixToAbsoluteNamespace} from "./sparqlPatternHelpers";

/**
 * @param {JsonLdNode} jsonLdNode - the node
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {object} rdfPrefixesMappings
 * @return {Model}
 */
export function parseObjectFromJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings}) {
  rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, modelDefinition.getRdfPrefixesMapping());
  let ModelClass = modelDefinition.getModelClass();
  let id = replaceAbsoluteNamespaceToPrefix(jsonLdNode["@id"], rdfPrefixesMappings);

  if (id) {
    let uri = replacePrefixToAbsoluteNamespace(id, rdfPrefixesMappings);
    let context = jsonLdNode["@context"];

    let props = Object.entries(omit(jsonLdNode, ["@id", "@type", "@context"])).reduce((acc, [prop, value]) => {
      // There is a weird bug with graph-controller where sometime, prefix is not stripped.
      // It seems to appear if data property is a boolean.
      // Ie : mnx:isUserDisabled
      prop = prop.replace(/^\w+:/, '');

      let rdfProp = get(context, `${prop}.@id`);
      rdfProp = replaceAbsoluteNamespaceToPrefix(rdfProp, rdfPrefixesMappings);

      let normalizedProp;
      let datatype;

      let property = modelDefinition.getPropertyFromRdfDataProperty(rdfProp);

      if (property) {
        normalizedProp = property.getPropertyName();
        datatype       = property.getRdfDataType() || property.getRdfAliasDataProperty();
      }

      // If the value is a localized label with several languages
      if (has(value, '0.@language')) {
        value = value.reduce((acc, localizedValue) => {
          acc[localizedValue["@language"]] = localizedValue["@value"];
          return acc;
        }, {});
      }

      // If the value is a localized label with a single language
      if (has(value, '@language')) {
        value = {
          [get(value, '@language')]: get(value, '@value')
        }
      }

      switch (datatype) {
        case "http://www.w3.org/2001/XMLSchema#float":
        case "http://www.w3.org/2001/XMLSchema#double":
        case "http://www.w3.org/2001/XMLSchema#timestamp":
          value = parseFloat(value);
          break;
        case "http://www.w3.org/2001/XMLSchema#int":
        case "http://www.w3.org/2001/XMLSchema#integer":
          value = parseInt(value);
          break;
        case "http://www.w3.org/2001/XMLSchema#boolean":
          value = typeof value === "boolean" ? value : value === "true";
          break;
        case "http://www.w3.org/2001/XMLSchema#date":
        case "http://www.w3.org/2001/XMLSchema#dateTime":
        case "http://www.w3.org/2001/XMLSchema#dateTimestamp":
          value = dayjs(value).toISOString();
          break;
      }

      if (normalizedProp || prop) {
        acc[normalizedProp || prop] = value;
      }
      return acc;
    }, {});

    return new ModelClass(id, uri, props, modelDefinition.getNodeType());
  }
}