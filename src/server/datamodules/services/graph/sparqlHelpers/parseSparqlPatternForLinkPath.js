/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {LinkStep, PropertyFilterStep, PropertyStep} from "../../../..";
import {parseSparqlPatternForFilteredPropertyDefinitions} from "./parseSparqlPatternForFilteredPropertyDefinitions";
import {parseSparqlPatternForFilters} from "./parseSparqlPatternForFilters";

/**
 * @param {string}   sourceId
 * @param {LinkPath} linkPath
 * @param {object} rdfPrefixesMappings
 * @param {function} requestNodeFiltersMiddleware
 * @param {boolean} isOptional
 * @return {{whereTriples: array, optionalWhereTriples, templateTriples: array, filters: array, rdfPrefixesMappings: object}}
 */
export async function parseSparqlPatternForLinkPath({sourceId, linkPath, rdfPrefixesMappings, requestNodeFiltersMiddleware, isOptional}) {
  let filters = [];
  let whereTriples = [];
  let optionalWhereTriples = [];
  let templateTriples = [];

  let lastTargetId = sourceId;

  for (let [index, step] of linkPath.getSteps().entries()) {
    if (step instanceof LinkStep) {
      let linkDefinition = step.getLinkDefinition();
      let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
      let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();
      let reversePredicates = !!rdfObjectProperty && step.isReversed() || rdfReversedObjectProperty && !step.isReversed();

      let stepSourceId = lastTargetId;
      let stepTargetId = step.getTargetId() || `${linkDefinition.toSparqlVariable({})}_${index}`;

      rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping());

      whereTriples.push({
        subject: !reversePredicates ? stepSourceId : stepTargetId,
        predicate: linkDefinition.getRdfObjectProperty() || linkDefinition.getRdfReversedObjectProperty(),
        object: !reversePredicates ? stepTargetId : stepSourceId
      });

      if (!step.isTypeAssertionDiscarded()){
        whereTriples.push({
          subject: stepTargetId,
          predicate: "rdf:type",
          object: linkDefinition.getRelatedModelDefinition().getRdfType()
        })
      }

      let {mustNotExistLinkFilters} = await requestNodeFiltersMiddleware({modelDefinition: linkDefinition.getRelatedModelDefinition()});

      filters = filters.concat(parseSparqlPatternForFilters({
        mustNotExistLinkFilters,
        rdfPrefixesMappings,
        sourceId: stepTargetId,
        isStrict: true
      }));

      lastTargetId = stepTargetId;
    }

    if (step instanceof PropertyFilterStep) {
      whereTriples.push({
        subject: lastTargetId,
        predicate: step.getPropertyDefinition().getRdfDataProperty(),
        object: step.getPropertyDefinition().toSparqlVariable()
      });

      let searchableFilters = parseSparqlPatternForFilteredPropertyDefinitions({
        value: step.getValue(),
        propertyDefinitions: [step.getPropertyDefinition()]
      });

      filters = filters.concat(searchableFilters);
    }

    if(step instanceof PropertyStep){
      let propertyDefinition = step.getPropertyDefinition();

      let triple = {
        subject: lastTargetId,
        predicate: propertyDefinition.getRdfDataProperty(),
        object: propertyDefinition.toSparqlVariable()
      };

      whereTriples.push(triple);
      templateTriples.push({
        subject: sourceId,
        predicate: step.getRdfDataPropertyAlias(),
        object: propertyDefinition.toSparqlVariable()
      });
    }
  }

  if (isOptional){
    optionalWhereTriples = [whereTriples];
    whereTriples = [];
  }

  return {whereTriples, optionalWhereTriples, filters, templateTriples, rdfPrefixesMappings};
}