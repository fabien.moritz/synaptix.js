/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {parseSparqlPatternFromInputObject} from "./parseSparqlPatternFromInputObject";

/**
 * @callback NodeCrudMiddleware
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {object} [objectInput] - Object input
 * @param {Link[]} [links] -  List of links
 * @param {Link[]} [globalLinks] -  List of global links
 * @return {{links: *, objectInput: *}}
 */

/**
 * @param {typeof ModelDefinitionAbstract} modelDefinition
 * @param {string} objectId
 * @param {Link[]} links       -  List of links to update
 * @param {Link[]} globalLinks -  List of other links used in transaction
 * @param {string} lang
 * @param {string} nodesPrefix
 * @param {function} nodeTypeFormatter
 * @param {NodeCrudMiddleware} nodeCreationMiddleware
 * @param {NodeCrudMiddleware} nodeUpdateMiddleware
 * @param {NodeCrudMiddleware} nodeDeletionMiddleware
 * @return {{prefixes: object, insertTriples: [object], deleteTriples: [object], whereTriples: [object], indexable: [object]}}
 */
export async function parseSparqlPatternFromLinks({modelDefinition, objectId, links, globalLinks, lang, nodesPrefix, nodeTypeFormatter,nodeCreationMiddleware, nodeUpdateMiddleware, nodeDeletionMiddleware}) {
  let insertTriples = [];
  let deleteTriples = [];
  let whereTriples = [];
  let prefixes = {};
  let indexable = [];

  for (let link of links || []) {
    let targetId = link.getTargetId();
    let targetObjectInput = link.getTargetObjectInput();
    let linkDefinition = link.getLinkDefinition();
    let targetModelDefinition = link.getTargetModelDefinition();
    let targetLinks = link.getTargetNestedLinks() || [];

    /* istanbul ignore next */
    if (linkDefinition.isReadOnly()) {
      throw new Error(`The link "${linkDefinition.getLinkName()}" between "${modelDefinition.name}" and "${targetModelDefinition.name}" is read only. This behaviour is deprecated, use the not instantiable property of a model definition instead`)
    }

    if (!targetId && !targetModelDefinition.isInstantiable()) {
      throw new Error(`The model definition "${targetModelDefinition.name}" is not instantiable. Please specify a "targetModelDefinition" property in the link "${modelDefinition.name}" ~> "${linkDefinition.getLinkName()}"`);
    }

    let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
    let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();

    // If link is tagged as deletion, don't insert anything.
    if (!link.isDeletion()) {
      // If linked node does not exit, create it.
      if (!targetId) {
        targetId = !!targetObjectInput.uri ? targetObjectInput.uri : targetModelDefinition.generateURI({
          nodesNamespaceURI: `${nodesPrefix}:`,
          nodeTypeFormatter
        });

        delete targetObjectInput.uri;

        insertTriples.push({
          subject: targetId,
          predicate: "rdf:type",
          object: targetModelDefinition.getRdfType()
        });

        if (nodeCreationMiddleware) {
          ({
            objectInput: targetObjectInput,
            links: targetLinks
          } = await nodeCreationMiddleware({
            modelDefinition: targetModelDefinition,
            objectInput: targetObjectInput,
            links: targetLinks,
            globalLinks: [].concat(targetLinks, links, globalLinks)
          }));
        }


        // Save target Id and discard target objectInput
        // This is usefull if a link reference is used multiple times.
        link.setTargetId(targetId);
        link.setTargetObjectInput({});
        link.setTargetNestedLinks([]);
      } else {
        if (nodeUpdateMiddleware){
          ({
            objectInput: targetObjectInput,
            links: targetLinks
          } = await nodeUpdateMiddleware({
            modelDefinition: targetModelDefinition,
            objectInput: targetObjectInput,
            links: targetLinks,
            globalLinks: [].concat(targetLinks, links, globalLinks)
          }));
        }
      }

      if (targetLinks.length > 0) {
        let {insertTriples: linksInsertTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await parseSparqlPatternFromLinks({
          modelDefinition: targetModelDefinition,
          objectId: targetId,
          links: targetLinks,
          globalLinks: [].concat(targetLinks, links, globalLinks),
          lang,
          nodesPrefix,
          nodeTypeFormatter,
          nodeCreationMiddleware,
          nodeUpdateMiddleware,
          nodeDeletionMiddleware
        });

        insertTriples = insertTriples.concat(linksInsertTriples);
        prefixes = Object.assign(prefixes, linksPrefixes);
        indexable = indexable.concat(linksIndexable);

        // Save new links in globalLinks.
        globalLinks = [].concat(globalLinks, targetLinks);
      }

      if (targetModelDefinition.isIndexed()) {
        indexable.push({
          id: targetId,
          type: targetModelDefinition.getRdfType()
        })
      }

      let {insertTriples: linkPropertiesTriples} = parseSparqlPatternFromInputObject({
        modelDefinition: targetModelDefinition,
        objectInput: {
          id: targetId,
          ...targetObjectInput
        },
        lang
      });

      insertTriples = insertTriples.concat(linkPropertiesTriples);
      prefixes = Object.assign(prefixes, targetModelDefinition.getRdfPrefixesMapping());

      insertTriples.push({
        // If LinkDefinition::rdfObjectProperty is defined but Link::reversed property is true, swap the direction.
        subject: !!rdfObjectProperty && !link.isReversed() ? objectId : targetId,
        predicate: rdfObjectProperty || rdfReversedObjectProperty,
        object: !!rdfObjectProperty && !link.isReversed() ? targetId : objectId,
      });
    }

    //
    // If the related link definition is not plural, hypothetical existing edge must be removed.
    //
    if (!link.getLinkDefinition().isPlural()) {
      let deleteTriple = {
        subject: !!rdfObjectProperty && !link.isReversed() ? objectId : linkDefinition.toSparqlVariable(),
        predicate: rdfObjectProperty || rdfReversedObjectProperty,
        object: !!rdfObjectProperty && !link.isReversed() ? linkDefinition.toSparqlVariable() : objectId,
      };

      deleteTriples.push(deleteTriple);
      whereTriples.push(deleteTriple);

      ////
      //// If the related link definition is strongly linked, hypothetical existing linked node must be removed.
      //// TODO: Make this running. Otherwise zombie triples might live in the store.
      //
      // if (link.getLinkDefinition().isCascadingRemoved()){
      //   let {links: deletionLinks} = await nodeDeletionMiddleware({modelDefinition: targetModelDefinition});
      //
      //   let {insertTriples: deletionInsertTriples, deleteTriples: deletionDeleteTriples} = await parseSparqlPatternFromLinks({
      //     modelDefinition: targetModelDefinition,
      //     links: deletionLinks,
      //     objectId: linkDefinition.toSparqlVariable()
      //   });
      //
      //   console.log(deletionInsertTriples, deletionDeleteTriples);
      // }
    }
  }

  return {prefixes, insertTriples, deleteTriples, whereTriples, indexable};
}
