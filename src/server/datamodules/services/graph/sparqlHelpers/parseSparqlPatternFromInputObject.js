
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Extract object input and return a SPARQL AST representation.
 *
 * @param modelDefinition
 * @param objectInput
 * @param lang
 * @return {insertTriples, deleteTriples, deleteTriples, filters}
 */
export function parseSparqlPatternFromInputObject({modelDefinition, objectInput, lang}) {
  const {id, ...objectProps} = objectInput;

  let insertTriples = [];
  let deleteTriples = [];
  let whereTriples = [];
  let filters = [];

  Object.entries(objectProps).map(([prop, value]) => {
    let literalDefinition = modelDefinition.getLiteral(prop);
    if (literalDefinition) {
      let deleteTriple = {
        subject: id,
        predicate: literalDefinition.getRdfDataProperty(),
        object: literalDefinition.toSparqlVariable({})
      };

      deleteTriples.push(deleteTriple);
      whereTriples.push(deleteTriple);

      if (!!value && value !== "") {
        insertTriples.push({
          subject: id,
          predicate: literalDefinition.getRdfDataProperty(),
          object: literalDefinition.toSparqlValue(value)
        });
      }
    }

    let labelDefinition = modelDefinition.getLabel(prop);

    // See https://json-ld.org/spec/latest/json-ld/#language-indexing
    if (labelDefinition) {
      deleteTriples.push({
        subject: id,
        predicate: labelDefinition.getRdfDataProperty(),
        object: labelDefinition.toSparqlVariable({})
      });

      whereTriples.push({
        type: "optional",
        patterns: [
          {
            type: "bgp",
            triples: [{
              subject: id,
              predicate: labelDefinition.getRdfDataProperty(),
              object: labelDefinition.toSparqlVariable({})
            }]
          },
          {
            type: "filter",
            expression: {
              type: "operation",
              operator: "=",
              args: [
                {
                  type: "operation",
                  operator: "lang",
                  args: [
                    labelDefinition.toSparqlVariable({})
                  ]
                },
                `"${lang}"`
              ]
            }
          }
        ]
      });

      if (!!value && value !== "") {
        insertTriples.push({
          subject: id,
          predicate: labelDefinition.getRdfDataProperty(),
          object: labelDefinition.toSparqlLocalizedValue(value, lang)
        });
      }
    }
  });

  return {insertTriples, deleteTriples, whereTriples, filters};
}