
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Generator as SPARQL} from "sparqljs";
import {normalizePatterns, replacePrefixToAbsoluteNamespaceInTriples} from "./sparqlPatternHelpers";

/**
 * Create a SPARQL query (INSERT, DELETE).
 *
 * @param updateType
 * @param prefixes
 * @param insertTriples
 * @param whereTriples
 * @param deleteTriples
 * @param graphId
 * @param filters
 * @param options
 * @return {*}
 */
export function stringifyUpdateFromSparqlPattern({updateType, prefixes, insertTriples, whereTriples, graphId, deleteTriples, filters, options, stripNewlineCharInSparqlQuery}) {
  let newline = "\n";

  /* istanbul ignore next */
  if (stripNewlineCharInSparqlQuery) {
    newline = " "
  }

  if (updateType === "insert" && graphId){
    insertTriples = {
      type: "graph",
      name: graphId,
      triples: [
        {
          type: "bgp",
          triples: replacePrefixToAbsoluteNamespaceInTriples(insertTriples, prefixes)
        }
      ]
    }
  }
   else {
      insertTriples = insertTriples ? [
        {
          type: "bgp",
          triples: replacePrefixToAbsoluteNamespaceInTriples(insertTriples, prefixes)
        }
      ] : null;
    }

  return (new SPARQL({
    indent: ' ',
    newline,
    allPrefixes: true,
    ...options
  })).stringify({
    type: "update",
    prefixes,
    updates: [{
      updateType,
      insert: insertTriples ,
      delete: deleteTriples ? [
        {
          type: "bgp",
          triples: replacePrefixToAbsoluteNamespaceInTriples(deleteTriples, prefixes)
        }
      ] : null,
      where: [].concat(whereTriples ? whereTriples.map(whereTriple => {
        return {
          type: "optional",
          patterns: whereTriple.type === "optional" ? normalizePatterns(whereTriple.patterns, prefixes) : [
            {
              type: "bgp",
              triples: replacePrefixToAbsoluteNamespaceInTriples([whereTriple], prefixes)
            }
          ]
        }
      }) : null, filters || [])
    }]
  });
}