/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import {Generator as SPARQL} from "sparqljs";
import {replacePrefixToAbsoluteNamespaceInTriples} from "./sparqlPatternHelpers";

/**
 * Create a SPARQL query (SELECT, CONSTRUCT, ASK).
 *
 * @see documentation here : https://github.com/RubenVerborgh/SPARQL.js
 *
 * @param queryType
 * @param template
 * @param prefixes
 * @param variables
 * @param triples
 * @param options
 * @param filters
 * @param order
 * @return {*}
 */
export function stringifyQueryFromSparqlPattern({queryType, template, prefixes, variables, whereTriples, optionalWhereTriples, options, limit, offset, filters, order, stripNewlineCharInSparqlQuery, distinct}) {
  let newline = "\n";

  /* istanbul ignore next */
  if (stripNewlineCharInSparqlQuery) {
    newline = " "
  }

  whereTriples = [].concat(
    replacePrefixToAbsoluteNamespaceInTriples(whereTriples || [], prefixes),
    (optionalWhereTriples || []).map((optionalWhereTriple) => ({
      type: "optional",
      patterns: [
        {
          type: "bgp",
          triples: replacePrefixToAbsoluteNamespaceInTriples(Array.isArray(optionalWhereTriple) ? optionalWhereTriple : [optionalWhereTriple], prefixes)
        }
      ]
    })));

  return (new SPARQL({
    indent: ' ',
    newline,
    allPrefixes: true,
    ...options
  })).stringify({
    type: "query",
    prefixes,
    queryType,
    template: template ? replacePrefixToAbsoluteNamespaceInTriples(template, prefixes) : null,
    variables,
    where: [].concat(whereTriples, filters || []),
    limit,
    offset,
    order,
    distinct
  });
}