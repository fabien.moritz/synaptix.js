/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerService from "../GraphControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";
import PersonDefinition from "../../../../datamodel/ontologies-legacy/definitions/foaf/PersonDefinition";
import Person from "../../../../datamodel/ontologies-legacy/models/foaf/Person";

jest.unmock('amqplib');
jest.unmock("../../../../networkLayers/amqp/NetworkLayerAMQP");
jest.setTimeout(60000);

process.env.UUID = "integration-tests";

// let networkLayer = new NetworkLayerAMQP("amqp://admin:Pa55w0rd@192.168.1.11:5672", "default");
let networkLayer = new NetworkLayerAMQP("amqp://guest:mnxpowa!@localhost:5672", "local-cortex");


let graphControllerService = new GraphControllerService({
  networkLayer,
  graphQLcontext: new GraphQLContext({anonymous: true, lang: "fr"}),
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/",
  nodesPrefix: "test",
});

let now = Date.now();
Date.now = jest.fn().mockImplementation(() => now);

describe('GraphControllerService', async () => {
  beforeEach(async () => {
    await networkLayer.connect();
  });

  it('shoud create a simple node in JSON-LD', async () => {
    await graphControllerService.getGraphControllerPublisher().createTriples({
      jsonLdContext: PersonDefinition.getRdfPrefixesMapping(),
      jsonLdNodes: [{
        "@id": "test:Person/3cga1588eeo4bseee",
        "@type": "foaf:Person",
        "bio": {"@language": "fr", "@value": "Blabla en français"},
        "creationDate": "1555085200911",
        "lastUpdate": "1555085200911",
        "firstName": "John",
        "lastName": "Doe",
        "@context": {
          "bio": {"@id": "http://ns.mnemotix.com/onto/bio"},
          "lastName": {"@id": "http://xmlns.com/foaf/0.1/lastName"},
          "firstName": {"@id": "http://xmlns.com/foaf/0.1/firstName"},
          "lastUpdate": {
            "@id": "http://ns.mnemotix.com/onto/lastUpdate",
            "@type": "http://www.w3.org/2001/XMLSchema#float"
          },
          "creationDate": {
            "@id": "http://ns.mnemotix.com/onto/creationDate",
            "@type": "http://www.w3.org/2001/XMLSchema#float"
          },
          "test": "http://ns.mnemotix.com/",
          "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
          "owl": "http://www.w3.org/2002/07/owl#",
          "xsd": "http://www.w3.org/2001/XMLSchema#",
          "fn": "http://www.w3.org/2005/xpath-functions#",
          "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
          "sesame": "http://www.openrdf.org/schema/sesame#",
          "mnx": "http://ns.mnemotix.com/onto/",
          "foaf": "http://xmlns.com/foaf/0.1/"
        }
      }]
    });

    let person = await graphControllerService.getNode({
      id: "test:Person/3cga1588eeo4bseee",
      modelDefinition: PersonDefinition
    });

    expect(person.id).toEqual("test:Person/3cga1588eeo4bseee");
  });

  it('shoud create a simple node', async () => {
    let person = await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr',
      // graphId: 'http://ns.mnemotix.com/testGraph'
    });

    expect(person.firstName).toEqual('John');
    expect(person.lastName).toEqual('Doe');
    expect(person.bio).toEqual({"fr": 'Blabla en français'});
  });

  it('shoud create simple nodes with stress', async () => {
    for (let i of [...Array(200)]) {
      await graphControllerService.createNode({
        modelDefinition: PersonDefinition,
        objectInput: {
          firstName: `John-${i}`,
          lastName: `Doe-${i}`
        },
        lang: 'fr',
        // graphId: 'http://ns.mnemotix.com/testGraph'
      })
    }
  });

  it('should select a basic query', async () => {
    let person = await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr',
      // graphId: 'http://ns.mnemotix.com/testGraph'
    });

    let result = await graphControllerService.getGraphControllerPublisher().select({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT ?creationDate ?lastUpdate ?_enabled ?avatar ?firstName ?lastName ?maidenName ?gender ?birthday ?bio ?shortBio
WHERE {
 OPTIONAL { <${person.uri}> mnx:avatar ?avatar. }
 OPTIONAL { <${person.uri}> foaf:firstName ?firstName. }
 OPTIONAL { <${person.uri}> foaf:lastName ?lastName. }
 OPTIONAL { <${person.uri}> mnx:maidenName ?maidenName. }
 OPTIONAL { <${person.uri}> foaf:gender ?gender. }
 OPTIONAL { <${person.uri}> mnx:birthday ?birthday. }
 OPTIONAL { <${person.uri}> mnx:bio ?bio. }
 OPTIONAL { <${person.uri}> mnx:shortBio ?shortBio. }
}`
    });

    expect(result).toEqual({
      "head": {"vars": ["creationDate", "lastUpdate", "_enabled", "avatar", "firstName", "lastName", "maidenName", "gender", "birthday", "bio", "shortBio"]},
      "results": {
        "bindings": [{
          "lastUpdate": {
            "type": "literal",
            "datatype": "http://www.w3.org/2001/XMLSchema#float",
            "value": now.toString()
          },
          "creationDate": {
            "type": "literal",
            "datatype": "http://www.w3.org/2001/XMLSchema#float",
            "value": now.toString()
          },
          "firstName": {"type": "literal", "value": "John"},
          "lastName": {"type": "literal", "value": "Doe"},
          "bio": {"type": "literal", "xml:lang": "fr", "value": "Blabla en français"}
        }]
      }
    })
  });

  it('should construct a basic query', async () => {
    await graphControllerService.getGraphControllerPublisher().construct({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
 ?person mnx:avatar ?avatar.
 ?person foaf:firstName ?firstName.
 ?person foaf:lastName ?lastName.
 ?person mnx:maidenName ?maidenName.
 ?person foaf:gender ?gender.
 ?person mnx:birthday ?birthday.
 ?person mnx:bio ?bio.
 ?person mnx:shortBio ?shortBio.
}
WHERE {
 ?person a foaf:Person.
 OPTIONAL { ?person mnx:avatar ?avatar. }
 OPTIONAL { ?person foaf:firstName ?firstName. }
 OPTIONAL { ?person foaf:lastName ?lastName. }
 OPTIONAL { ?person mnx:maidenName ?maidenName. }
 OPTIONAL { ?person foaf:gender ?gender. }
 OPTIONAL { ?person mnx:birthday ?birthday. }
 OPTIONAL { ?person mnx:bio ?bio. }
 OPTIONAL { ?person mnx:shortBio ?shortBio. }
}`
    });
  });

  it('should ask a basic query', async () => {
    let person = await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr',
      // graphId: 'http://ns.mnemotix.com/testGraph'
    });

    let result = await graphControllerService.getGraphControllerPublisher().ask({
      query: `
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      ASK WHERE { 
        <${person.uri}> a foaf:Person
      }
      `
    });

    expect(result).toEqual(true)
  });

  it('should describe a basic query', async () => {
    let result = await graphControllerService.getGraphControllerPublisher().describe({
      query: `
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      DESCRIBE ?class WHERE { 
        ?class rdf:type rdfs:Class.
        FILTER(?class=<http://xmlns.com/foaf/0.1/Person>)
      } LIMIT 1`
    });

    console.log(JSON.stringify(result, null, ' '));
  });

  it('should get a node', async () => {
    let person = await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr',
      // graphId: 'http://ns.mnemotix.com/testGraph'
    });

    expect(await graphControllerService.getNode({
      id: person.id,
      modelDefinition: PersonDefinition
    }))
      .toEqual(new Person(person.id, person.uri, {
        firstName: person.firstName,
        lastName: person.lastName,
        creationDate: now,
        lastUpdate: now,
        bio: person.bio,
      }));
  });

  it('should update a node', async () => {
    let person = await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr',
      // graphId: 'http://ns.mnemotix.com/testGraph'
    });

    expect(await graphControllerService.updateNode({
      objectInput: {
        id: person.id,
        firstName: "Mathieu"
      },
      modelDefinition: PersonDefinition
    }))
      .toEqual(new Person(person.id, person.uri, {
        firstName: "Mathieu",
        lastUpdate: now
      }));
  });

  it.only('should receive an error on mal formed query', async () => {
    await expect(graphControllerService.getGraphControllerPublisher().ask({
      query: `
      PREFIX foaf: <http://xmlns.com/foaf/0.1/>
      AS WHERE { 
        <http://ns.com/Person/012334> a foaf:Person
      }
      `
    })).rejects.toThrow()
  });

  // it('shoud delete a simple node', async () => {
  //   expect(await graphControllerService.removeNode({
  //     modelDefinition: PersonDefinition,
  //     id: 'test:person:1'
  //   })).toEqual(true);
  // });
});