/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  
 */

import isObject from 'lodash/isObject';

import LinkDefinition, {Link} from "../../../datamodel/toolkit/definitions/LinkDefinition";
import GraphControllerPublisher from "../../drivers/graph/GraphControllerPublisher";
import {
  LinkPath
} from "../../../datamodel/toolkit/utils/LinkPath";
import {stringifyQueryFromSparqlPattern} from "./sparqlHelpers/stringifyQueryFromSparqlPattern";
import {stringifyUpdateFromSparqlPattern} from "./sparqlHelpers/stringifyUpdateFromSparqlPattern";
import {parseObjectFromJsonLdNode} from "./sparqlHelpers/parseObjectFromJsonLdNode";
import {parseObjectFromObjectInput} from "./sparqlHelpers/parseObjectFromObjectInput";
import {parseSparqlPatternFromInputObject} from "./sparqlHelpers/parseSparqlPatternFromInputObject";
import {parseSparqlPatternFromLinks} from "./sparqlHelpers/parseSparqlPatternFromLinks";
import {parseSparqlPatternForFilters} from "./sparqlHelpers/parseSparqlPatternForFilters";
import {parseSparqlPatternForFilteredPropertyDefinitions} from "./sparqlHelpers/parseSparqlPatternForFilteredPropertyDefinitions";
import {parseSparqlPatternForLinkPath} from "./sparqlHelpers/parseSparqlPatternForLinkPath";
import {parseSparqlPatternForPropertyDefinition} from "./sparqlHelpers/parseSparqlPatternForPropertyDefinition";

/**
 * Service used to perform CRUD on a RDF triple store.
 */
export default class GraphControllerService {
  /**
   * @param {NetworkLayerAMQP} networkLayer - The network layer.
   * @param {GraphQLContext} graphQLcontext - The graphQL session context .
   * @param {object} schemaNamespaceMapping - The namespace URI used to describe "mnx:" prefix.
   * @param {string} nodesNamespaceURI  - The URI namespace used to create new node (aka: to generate new URIs)
   * @param {string} [nodesPrefix=ex]   - The prefix used for nodesNamespaceURI
   * @param {function} [nodesTypeFormatter] - This function tweaks a node type into a urified version
   * @param {boolean} [stripNewlineCharInSparqlQuery=false] - Remove \n char in SPARQL query (usefull for testing)
   * @param {string} [insertingNamedGraphId] - The URI of the global named graph where data are inserted
   * @param {GraphMiddleware[]} [middlewares}
   */
  constructor({networkLayer, graphQLcontext, schemaNamespaceMapping, nodesNamespaceURI, nodesPrefix, stripNewlineCharInSparqlQuery, middlewares, nodesTypeFormatter, insertingNamedGraphId}) {
    if (!graphQLcontext) {
      throw new Error(`You must provide a GrapQLContext to ${this.constructor.name}::constructor()`);
    }

    if (!schemaNamespaceMapping) {
      throw new Error(`You must provide a schemaNamespaceMapping to ${this.constructor.name}::constructor()`);

    }

    if (!nodesNamespaceURI) {
      throw new Error(`You must provide a nodesNamespaceURI to ${this.constructor.name}::constructor()`);
    }

    let senderId = graphQLcontext.getUser() ? graphQLcontext.getUser().getId() : process.env.UUID;

    this._graphControllerPublisher = new GraphControllerPublisher(networkLayer, senderId);
    this._networkLayer = networkLayer;
    this._nodesNamespaceURI = nodesNamespaceURI;
    this._schemaNamespaceMapping = schemaNamespaceMapping;
    this._nodesPrefix = nodesPrefix || "ex";
    this._stripNewlineCharInSparqlQuery = stripNewlineCharInSparqlQuery || false;
    this._middlewares = middlewares;
    this._nodesTypeFormatter = nodesTypeFormatter;
    this._insertingNamedGraphId = insertingNamedGraphId;
  }

  /**
   * Apply middlewares on node creation.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} [objectInput] - Object input
   * @param {Link[]} [links] -  List of links
   * @param {Link[]} [globalLinks] -  List of global links
   * @return {{links: *, objectInput: *}}
   */
  async applyNodeCreationMiddlewares({modelDefinition, objectInput, links, globalLinks}) {
    objectInput = objectInput || {};
    links = links || [];

    for (let middleware of this._middlewares || []) {
      ({objectInput, links} = await middleware.processNodeCreationParameters({
        modelDefinition,
        objectInput,
        links,
        globalLinks
      }));
    }

    return {objectInput, links};
  }

  /**
   * Apply middlewares on node update.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} [objectInput] - Object input
   * @param {Link[]} [links] -  List of links
   * @param {Link[]} [globalLinks] -  List of global links
   * @return {{links: *, objectInput: *}}
   */
  async applyNodeUpdateMiddlewares({modelDefinition, objectInput, links, globalLinks}) {
    objectInput = objectInput || {};
    links = links || [];

    for (let middleware of this._middlewares || []) {
      ({objectInput, links} = await middleware.processNodeUpdateParameters({
        modelDefinition,
        objectInput,
        links,
        globalLinks
      }));
    }

    return {objectInput, links};
  }

  /**
   * Apply middlewares on node deletion.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} [objectInput] - Object input
   * @param {Link[]} [links] -  List of links used in deletion
   * @param {Link[]} [globalLinks] -  List of global links
   * @return {{links: *, objectInput: *}}
   */
  async applyNodeDeletionMiddlewares({modelDefinition, objectInput, links, globalLinks}) {
    objectInput = objectInput || {};
    links = links || [];

    for (let middleware of this._middlewares || []) {
      ({objectInput, links} = await middleware.processNodeDeletionParameters({
        modelDefinition,
        objectInput,
        links,
        globalLinks
      }));
    }

    return {objectInput, links};
  }

  /**
   * Apply middlewares on node deletion.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LiteralFilter[]} [literalFilters] -  List of literal filters
   * @param {LabelFilter[]} [labelFilters] -  List of label filters
   * @param {LinkFilter[]} [mustExistLinkFilters] -  List of link filters
   * @param {LinkFilter[]} [mustNotExistLinkFilters] -  List of link filters
   * @returns {{literalFilters: LiteralFilter[], labelFilters: LabelFilter[], mustExistLinkFilters: LinkFilter[], mustNotExistLinkFilters: LinkFilter[]}}
   */
  async applyRequestNodeFiltersMiddlewares({modelDefinition, literalFilters, labelFilters, mustExistLinkFilters, mustNotExistLinkFilters}) {
    literalFilters = literalFilters || [];
    labelFilters = labelFilters || [];
    mustExistLinkFilters = mustExistLinkFilters || [];
    mustNotExistLinkFilters = mustNotExistLinkFilters || [];

    for (let middleware of this._middlewares || []) {
      ({
        literalFilters,
        labelFilters,
        mustExistLinkFilters,
        mustNotExistLinkFilters
      } = await middleware.processNodeRequestFilters({
        modelDefinition,
        literalFilters,
        labelFilters,
        mustExistLinkFilters,
        mustNotExistLinkFilters
      }));
    }

    return {literalFilters, labelFilters, mustExistLinkFilters, mustNotExistLinkFilters};
  }

  /**
   * @param {NodeSerialization[]} nodes
   */
  async waitForIndexSyncingOf(nodes) {
  }

  /* istanbul ignore next */

  /**
   * @return {GraphControllerPublisher}
   */
  getGraphControllerPublisher() {
    return this._graphControllerPublisher;
  }

  /**
   *
   * @return {object}
   */
  getDefaultRdfMappings() {
    return {
      ...this._schemaNamespaceMapping,
      [this._nodesPrefix]: this._nodesNamespaceURI,
    }
  }

  /**
   * Is node exists
   * @param id
   * @param modelDefinition
   * @return {boolean}
   */
  async isNodeExists({id, modelDefinition}) {
    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    let {mustNotExistLinkFilters} = await this.applyRequestNodeFiltersMiddlewares({modelDefinition});

    let filters = this._parseSparqlPatternForFilters({
      mustNotExistLinkFilters,
      rdfPrefixesMappings,
      isStrict: true,
      sourceId: id
    });

    return this._graphControllerPublisher.ask({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "ASK",
        prefixes: rdfPrefixesMappings,
        whereTriples: [{
          subject: id,
          predicate: "rdf:type",
          object: modelDefinition.getRdfType()
        }],
        filters
      })
    });
  }

  /**
   * Is object has a link with another one defined by it's id ?
   *
   * @param {string} id
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LinkDefinition|LinkDefinition[]} linkDefinitions
   * @param {string} targetId
   * @return {boolean}
   */
  async isNodeLinkedToTargetId({id, modelDefinition, linkDefinitions, targetId}) {
    let whereTriples = [];
    let filters = [];
    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    if (linkDefinitions instanceof LinkDefinition) {
      linkDefinitions = [linkDefinitions];
    }

    let lastTargetId = id;

    for (let [index, linkDefinition] of linkDefinitions.entries()) {
      let stepSourceId = lastTargetId;
      let stepTargetId = linkDefinition.toSparqlVariable();

      if (index === linkDefinitions.length - 1) {
        stepTargetId = targetId;
      } else {
        whereTriples.push({
          subject: stepTargetId,
          predicate: "rdf:type",
          object: linkDefinition.getRelatedModelDefinition().getRdfType()
        });
      }
      rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping());

      whereTriples.push({
        subject: !!linkDefinition.getRdfObjectProperty() ? stepSourceId : stepTargetId,
        predicate: linkDefinition.getRdfObjectProperty() || linkDefinition.getRdfReversedObjectProperty(),
        object: !!linkDefinition.getRdfObjectProperty() ? stepTargetId : stepSourceId
      });

      let {mustNotExistLinkFilters} = await this.applyRequestNodeFiltersMiddlewares({modelDefinition: linkDefinition.getRelatedModelDefinition()});

      filters = filters.concat(this._parseSparqlPatternForFilters({
        mustNotExistLinkFilters,
        rdfPrefixesMappings,
        sourceId: stepTargetId
      }));

      lastTargetId = stepTargetId;
    }

    return this._graphControllerPublisher.ask({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "ASK",
        prefixes: rdfPrefixesMappings,
        whereTriples,
        filters
      })
    });
  }

  /**
   * Returns a RDF type given an URI
   * @param {string} uri
   * @return {string|string[]}
   */
  async getRdfTypeForURI({uri}) {
    let node = await this._graphControllerPublisher.construct({
      query: `
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
${Object.entries(this.getDefaultRdfMappings()).map(([prefix, namespace]) => `PREFIX ${prefix}: <${namespace}>`).join("\n")}
CONSTRUCT { <${uri}> a $type } WHERE{
  <${uri}> a $type
  FILTER (!isBlank($type))
}      
`
    });

    return node["@type"];
  }

  /**
   * Generic method to get a node
   *
   * @param id
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [lang]
   * @param {FragmentDefinition[]} fragmentDefinitions
   * @returns {*}
   */
  async getNode({id, modelDefinition, lang, fragmentDefinitions}) {
    let typeTriple = {
      subject: id,
      predicate: "rdf:type",
      object: modelDefinition.getRdfType()
    };

    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());
    let templateTriples = [typeTriple];
    let whereTriples = [typeTriple];
    let optionalWhereTriples = [];
    let filters = [];
    let properties = modelDefinition.getProperties();

    // If fragments, we add their literals to properties to return
    if (fragmentDefinitions) {
      for(let fragmentDefinition of fragmentDefinitions){
        properties = properties.concat(fragmentDefinition.getProperties());
      }
    }

    for(let propertyDefinition of properties){
      let patterns = await this._parseSparqlPatternForPropertyDefinition({
        propertyDefinition,
        sourceId: id,
        rdfPrefixesMappings
      });

      whereTriples = whereTriples.concat(patterns.whereTriples);
      optionalWhereTriples = optionalWhereTriples.concat(patterns.optionalWhereTriples);
      filters = filters.concat(patterns.filters);
      templateTriples = templateTriples.concat(patterns.templateTriples);
      rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, patterns.rdfPrefixesMappings);
    }

    let {mustNotExistLinkFilters} = await this.applyRequestNodeFiltersMiddlewares({modelDefinition});

    filters = filters.concat(this._parseSparqlPatternForFilters({
      mustNotExistLinkFilters,
      rdfPrefixesMappings,
      sourceId: id,
      isStrict: true
    }));

    let jsonLdNode = await this._graphControllerPublisher.construct({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "CONSTRUCT",
        prefixes: rdfPrefixesMappings,
        template: templateTriples,
        whereTriples: whereTriples,
        optionalWhereTriples: optionalWhereTriples,
        filters
      })
    });

    return this._parseObjectFromJsonLdNode({jsonLdNode, modelDefinition});
  }


  /**
   * @typedef {object} LinkFilter
   * @property {string} [id]
   * @property {boolean} [isReversed]
   * @property {object} [rdfFilterFunction]
   * @property {LinkDefinition} linkDefinition
   */

  /**
   * @typedef {object} LiteralFilter
   * @property {string} value
   * @property {LiteralDefinition} literalDefinition
   */

  /**
   * @typedef {object} LabelFilter
   * @property {string} value
   * @property {LabelDefinition} labelDefinition
   * @property {string} [lang]
   */

  /**
   * Generic method to get a list of nodes
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} [qs] - A query string
   * @param {number} [limit=10]
   * @param {number} [offset]
   * @param {Sorting[]} [sortings] - A list of sorting
   * @param {LinkFilter[]} [linkFilters] - A list of link filters.
   * @param {LiteralFilter[]} [literalFilters] - A list of literal filters.
   * @param {LabelFilter[]} [labelFilters] - A list of label filters.
   * @param {LinkPath} [linkPath] - A link path
   * @param {boolean} [discardTypeAssertion=false] - Discard RDF type assertion. This this usefull when URI reference are stored in triple store but belongs to another triple store (Ex: Geonames)
   * @param {FragmentDefinition[]} fragmentDefinitions
   * @param {boolean} [justCount]
   * @returns {*}
   */
  async getNodes({modelDefinition, qs, limit, offset, sortings, linkFilters, literalFilters, labelFilters, linkPath, discardTypeAssertion, justCount, fragmentDefinitions}) {
    let typeAssertionTriples = [{
      subject: '?uri',
      predicate: "rdf:type",
      object: modelDefinition.getRdfType()
    }];

    let rdfPrefixesMappings = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());
    let whereTriples = discardTypeAssertion ? [] : [].concat(typeAssertionTriples);
    let optionalWhereTriples = [];
    let templateTriples = [];
    let filters = [];

    let properties = modelDefinition.getProperties();

    // If fragments, we add their literals to properties to return
    if (fragmentDefinitions) {
      for(let fragmentDefinition of fragmentDefinitions){
        properties = properties.concat(fragmentDefinition.getProperties());
      }
    }

    for (let propertyDefinition of properties){
      let patterns = await this._parseSparqlPatternForPropertyDefinition({
        propertyDefinition,
        sourceId: "?uri",
        rdfPrefixesMappings
      });

      whereTriples = whereTriples.concat(patterns.whereTriples);
      optionalWhereTriples = optionalWhereTriples.concat(patterns.optionalWhereTriples);
      filters = filters.concat(patterns.filters);
      templateTriples = templateTriples.concat(patterns.templateTriples);
      rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, patterns.rdfPrefixesMappings);
    }

    if (qs && qs !== "") {
      let searchableFilters = this._parseSparqlPatternForFilteredPropertyDefinitions({
        value: qs,
        propertyDefinitions: modelDefinition.getSearchableProperties()
      });

      filters = filters.concat(searchableFilters);
    }

    if (linkPath && linkPath instanceof LinkPath) {
      let {whereTriples: linkPathWhereTriples, filters: linkPathFilters, rdfPrefixesMappings: linkPathRdfPrefixesMappings} = await this._parseSparqlPatternForLinkPath({
        sourceId: "?uri",
        linkPath,
        rdfPrefixesMappings
      });

      rdfPrefixesMappings = Object.assign(rdfPrefixesMappings, linkPathRdfPrefixesMappings);
      whereTriples = whereTriples.concat(linkPathWhereTriples);
      filters = filters.concat(linkPathFilters);
    }

    if (linkFilters) {
      for (let linkFilter of linkFilters) {
        let rdfObjectProperty = linkFilter.linkDefinition.getRdfObjectProperty();
        let rdfReversedObjectProperty = linkFilter.linkDefinition.getRdfReversedObjectProperty();
        let reversePredicates =
          !!rdfObjectProperty && linkFilter.isReversed || rdfReversedObjectProperty && !linkFilter.isReversed;

        if (linkFilter.id) {
          whereTriples.push({
            subject: !reversePredicates ? '?uri' : linkFilter.id,
            predicate: rdfObjectProperty || rdfReversedObjectProperty,
            object: !reversePredicates ? linkFilter.id : '?uri'
          });
        }

        if (linkFilter.rdfFilterFunction) {
          if (linkFilter.rdfFilterFunction?.literalDefinition) {
            let linkVariable = `?${linkFilter.linkDefinition.getLinkName()}`;

            whereTriples.push({
              subject: !reversePredicates ? "?uri" : linkVariable,
              predicate: rdfObjectProperty || rdfReversedObjectProperty,
              object: !reversePredicates ? linkVariable : "?uri"
            });

            whereTriples.push({
              subject: linkVariable,
              predicate: linkFilter.rdfFilterFunction.literalDefinition.getRdfDataProperty(),
              object: linkFilter.rdfFilterFunction.literalDefinition.toSparqlVariable({})
            });

            if (linkFilter.rdfFilterFunction?.generateRdfFilterDefinition) {
              let {args, rdfFunction, rdfOperator} = linkFilter.rdfFilterFunction.generateRdfFilterDefinition({
                literalVariable: linkFilter.rdfFilterFunction.literalDefinition.toSparqlVariable({})
              });

              filters.push({
                type: "filter",
                expression: rdfFunction ? {
                  type: "functioncall",
                  function: rdfFunction,
                  args
                } : {
                  type: "operation",
                  operator: rdfOperator,
                  args
                }
              });
            }
          }
        }

        rdfPrefixesMappings = Object.assign(
          rdfPrefixesMappings,
          linkFilter.linkDefinition.getRdfPrefixesMapping(),
          linkFilter.linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping()
        );
      }
    }

    if (literalFilters) {
      for (let literalFilter of literalFilters) {
        let rdfDataProperty = literalFilter.literalDefinition.getRdfDataProperty();

        if (literalFilter.generateRdfFilterDefinition) {
          let {args, rdfFunction, rdfOperator} = literalFilter.generateRdfFilterDefinition({
            literalVariable: literalFilter.literalDefinition.toSparqlVariable({}),
          });

          filters.push({
            type: "filter",
            expression: rdfFunction ? {
              type: "functioncall",
              function: rdfFunction,
              args
            } : {
              type: "operation",
              operator: rdfOperator,
              args
            }
          });
        } else {
          whereTriples.push({
            subject: '?uri',
            predicate: rdfDataProperty,
            object: literalFilter.literalDefinition.toSparqlValue(literalFilter.value)
          });
        }
      }
    }

    if (labelFilters) {
      for (let {value, lang, labelDefinition} of labelFilters) {
        let rdfDataProperty = labelDefinition.getRdfDataProperty();

        whereTriples.push({
          subject: '?uri',
          predicate: rdfDataProperty,
          object: lang ? labelDefinition.toSparqlLocalizedValue(value, lang) : labelDefinition.toSparqlValue(value)
        });
      }
    }

    let {mustNotExistLinkFilters} = await this.applyRequestNodeFiltersMiddlewares({modelDefinition});

    filters = filters.concat(this._parseSparqlPatternForFilters({mustNotExistLinkFilters, rdfPrefixesMappings}));

    if (justCount) {
      return this._graphControllerPublisher.count({
        query: this._stringifyQueryFromSparqlPattern({
          queryType: "SELECT",
          prefixes: rdfPrefixesMappings,
          variables: [{
            expression: {
              expression: "?uri",
              type: "aggregate",
              aggregation: "count",
              distinct: true
            },
            variable: "?count"
          }],
          distinct: true,
          whereTriples,
          filters
        })
      });
    } else {
      let uris;

      if (limit || (sortings || []).length > 0) {
        let order;
        let variables = ["?uri"];
        if ((sortings || []).length > 0) {
          order = [];

          for(let sorting of sortings){
            order.push({
              expression: sorting.getPropertyDefinition().toSparqlVariable(),
              descending: sorting.isDescending()
            });
            variables.push(sorting.getPropertyDefinition().toSparqlVariable());
          }
        }

        let {results: {bindings}} = await this._graphControllerPublisher.select({
          query: this._stringifyQueryFromSparqlPattern({
            queryType: "SELECT",
            prefixes: rdfPrefixesMappings,
            variables,
            whereTriples,
            optionalWhereTriples: order?.length > 0 || filters?.length > 0 ? optionalWhereTriples : [], // Add optional filters only if filters or sorting is applied.
            filters,
            limit: limit,
            offset: offset || 0,
            order,
            distinct: true,
          })
        });

        uris = bindings.map(binding => binding.uri?.value);
        filters = [{
          type: "filter",
          expression: {
            type: "operation",
            operator: "in",
            args: [ "?uri", uris ]
          }
        }];
      }

      let jsonLdNodes = await this._graphControllerPublisher.construct({
        query: this._stringifyQueryFromSparqlPattern({
          queryType: "CONSTRUCT",
          prefixes: rdfPrefixesMappings,
          template: [].concat(typeAssertionTriples, templateTriples),
          optionalWhereTriples,
          whereTriples,
          filters
        })
      });

      if (Object.keys(jsonLdNodes).length <= 1) {
        return [];
      } else if (jsonLdNodes["@graph"]) {
        let objects = jsonLdNodes["@graph"].map((jsonLdNode) => this._parseObjectFromJsonLdNode({
          jsonLdNode: {
            "@context": jsonLdNodes["@context"],
            ...jsonLdNode
          },
          modelDefinition
        }));

        if ((sortings || []).length > 0) {
          objects = objects.sort(({uri: uri1}, {uri: uri2}) => {
            return uris.indexOf(uri1) - uris.indexOf(uri2);
          })
        }

        return objects;
      } else {
        return [this._parseObjectFromJsonLdNode({
          jsonLdNode: jsonLdNodes,
          modelDefinition
        })];
      }
    }
  }

  /**
   * Get a count of nodes.
   *
   * @param modelDefinition
   * @return {number}
   */
  async countNodes({modelDefinition}) {
    let rdfPrefixesMapping = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    let whereTriples = [
      {
        subject: '?uri',
        predicate: "rdf:type",
        object: modelDefinition.getRdfType()
      }
    ];

    return this._graphControllerPublisher.count({
      query: this._stringifyQueryFromSparqlPattern({
        queryType: "SELECT",
        prefixes: rdfPrefixesMapping,
        variables: [{
          expression: {
            expression: "?uri",
            type: "aggregate",
            aggregation: "count",
            distinct: true
          },
          variable: "?count"
        }],
        optionalWhereTriples: whereTriples
      })
    });

  }

  /**
   * Create a node
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition -
   * @param {Link[]} links -  List of links
   * @param {object} [objectInput] - Object input
   * @param {string} [lang] -  Force a language
   * @param {string} [uri] - Force an URI
   * @param {string} [graphId] - Named graph id.
   */
  async createNode({modelDefinition, links, objectInput, lang, uri, graphId}) {
    links = links || [];

    ({objectInput, links} = await this.applyNodeCreationMiddlewares({modelDefinition, objectInput, links}));

    let indexable = [];
    let prefixes = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    // Create URI if not provided.
    let objectId = !!uri ? uri : modelDefinition.generateURI({
      nodesNamespaceURI: `${this._nodesPrefix}:`,
      nodeTypeFormatter: this._nodesTypeFormatter
    });

    if (modelDefinition.isIndexed()) {
      indexable.push({
        id: objectId,
        type: modelDefinition.getRdfType()
      })
    }

    let insertTriples = [{
      subject: objectId,
      predicate: "rdf:type",
      object: modelDefinition.getRdfType()
    }];

    objectInput = {
      id: objectId,
      ...objectInput
    };

    // Get the object JSON-LD node based on object input
    let {insertTriples: propertiesTriples} = this._parseSparqlPatternFromInputObject({
      modelDefinition,
      objectInput,
      lang
    });

    insertTriples = insertTriples.concat(propertiesTriples);

    let {insertTriples: linksInsertTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await this._parseSparqlPatternFromLinks({
      modelDefinition,
      objectId,
      links,
      globalLinks: links,
      lang
    });

    insertTriples = insertTriples.concat(linksInsertTriples);
    prefixes = Object.assign(prefixes, linksPrefixes);
    indexable = indexable.concat(linksIndexable);

    await this._graphControllerPublisher.insertTriples({
      query: this._stringifyUpdateFromSparqlPattern({
        updateType: "insert",
        prefixes,
        insertTriples,
        graphId
      }),
      messageContext: {
        indexable
      }
    });

    return this._parseObjectFromObjectInput({
      objectInput,
      modelDefinition,
      lang
    });
  }

  /**
   * Create an edge.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   */
  async createEdge(modelDefinition, objectId, linkDefinition, targetId) {
    let targetModelDefinition = linkDefinition.getRelatedModelDefinition();
    let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
    let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();
    let indexable = [];
    let prefixes = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping(), linkDefinition.getRelatedModelDefinition().getRdfPrefixesMapping());

    if (!rdfObjectProperty && !rdfReversedObjectProperty) {
      throw new Error(`A rdfObjectProperty or rdfReversedObjectProperty must be set in the "${linkDefinition.getLinkName()}" link definition from "${modelDefinition.name}"`)
    }

    if (modelDefinition.isIndexed()) {
      indexable.push({
        type: modelDefinition.getRdfType(),
        id: objectId
      })
    }

    if (targetModelDefinition.isIndexed()) {
      indexable.push({
        type: targetModelDefinition.getRdfType(),
        id: targetId
      })
    }

    return this._graphControllerPublisher.insertTriples({
      query: this._stringifyUpdateFromSparqlPattern({
        updateType: "insert",
        prefixes,
        insertTriples: [{
          subject: rdfObjectProperty ? objectId : targetId,
          predicate: rdfObjectProperty || rdfReversedObjectProperty,
          object: rdfObjectProperty ? targetId : objectId,
        }],
      }),
      messageContext: {
        indexable
      }
    });
  }

  /**
   * Delete all edges.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {LinkDefinition} linkDefinition
   * @param {string} targetId
   */
  async deleteEdges({modelDefinition, objectId, linkDefinition, targetId}) {
    let targetModelDefinition = linkDefinition.getRelatedModelDefinition();
    let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
    let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();

    if (!rdfObjectProperty && !rdfReversedObjectProperty) {
      throw new Error(`A rdfObjectProperty or rdfReversedObjectProperty must be set in the "${linkDefinition.getLinkName()}" link definition from "${modelDefinition.name}"`)
    }

    const deleteTriple = {
      subject: rdfObjectProperty ? objectId : targetId,
      predicate: rdfObjectProperty || rdfReversedObjectProperty,
      object: rdfObjectProperty ? targetId : objectId
    };

    await this._graphControllerPublisher.deleteTriples({
      query: this._stringifyUpdateFromSparqlPattern({
        updateType: "delete",
        prefixes: Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping(), targetModelDefinition.getRdfPrefixesMapping()),
        deleteTriples: [deleteTriple],
        whereTriples: [deleteTriple],
        insertTriples: []
      }),
      messageContext: {}
    });
  }

  /**
   * Generic method to update a node.
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {Link[]} links -  List of links
   * @param {object} objectInput
   * @param {string|null} [lang]
   */
  async updateNode({modelDefinition, objectInput, links, lang}) {
    if (!objectInput.id) {
      throw new Error(`Impossible to update node, "id" property is not defined in "objectInput". Got ${objectInput}`)
    }

    let indexable = [];

    ({objectInput, links} = await this.applyNodeUpdateMiddlewares({
      modelDefinition,
      objectInput,
      links,
      globalLinks: links
    }));

    // Get the object JSON-LD node based on object input
    let {insertTriples, deleteTriples, whereTriples, filters} = this._parseSparqlPatternFromInputObject({
      modelDefinition,
      objectInput,
      lang
    });

    let prefixes = Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping());

    let {insertTriples: linksInsertTriples, deleteTriples: linksDeleteTriples, whereTriples: linksWhereTriples, prefixes: linksPrefixes, indexable: linksIndexable} = await this._parseSparqlPatternFromLinks({
      modelDefinition,
      objectId: objectInput.id,
      links,
      globalLinks: links,
      lang
    });

    insertTriples = insertTriples.concat(linksInsertTriples);
    deleteTriples = deleteTriples.concat(linksDeleteTriples);
    whereTriples = whereTriples.concat(linksWhereTriples);
    prefixes = Object.assign(prefixes, linksPrefixes);
    indexable = indexable.concat(linksIndexable);

    let cascadingUpdatingLinkDefinitions = modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingUpdated());

    for (let cascadingUpdatingLinkDefinition of cascadingUpdatingLinkDefinitions) {
      // TODO: Set indexation infos here.
    }

    if (insertTriples.length !== 0 || deleteTriples.length !== 0) {
      await this._graphControllerPublisher.updateTriples({
        query: this._stringifyUpdateFromSparqlPattern({
          updateType: "insertdelete",
          prefixes,
          insertTriples,
          deleteTriples,
          whereTriples,
          filters
        }),
        messageContext: {
          indexable
        }
      });
    }

    return this._parseObjectFromObjectInput({modelDefinition, objectInput, lang});
  }

  /**
   * Generic method to remove a node
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} id
   * @param {boolean} permanentRemoval
   */
  async removeNode({modelDefinition, id, permanentRemoval}) {
    if (!permanentRemoval) {
      // If no middleware apply do nothing.
      let objectInput = {id};
      let links = [];

      ({objectInput, links} = await this.applyNodeDeletionMiddlewares({modelDefinition, objectInput, links}));

      return this.updateNode({modelDefinition, objectInput, links});
    } else {
      // Get the object JSON-LD node based on object input
      let deleteTriples = [{
        subject: id,
        predicate: '?p',
        object: '?v'
      }];

      let indexable = [];

      let cascadingRemovingLinkDefinitions = modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingRemoved());

      for (let [index, linkDefinition] of Object.entries(cascadingRemovingLinkDefinitions)) {
        let targetModelDefinition = linkDefinition.getRelatedModelDefinition();
        let targetId = targetModelDefinition.toSparqlVariable();

        let rdfObjectProperty = linkDefinition.getRdfObjectProperty();
        let rdfReversedObjectProperty = linkDefinition.getRdfReversedObjectProperty();

        deleteTriples.push({
          subject: rdfObjectProperty ? id : targetId,
          predicate: rdfObjectProperty || rdfReversedObjectProperty,
          object: rdfObjectProperty ? targetId : id
        });

        // deleteTriples.push({
        //   subject: targetId,
        //   predicate: `?p${index}`,
        //   object: `?v${index}`,
        // });
      }

      let cascadingUpdatingLinkDefinitions = modelDefinition.getLinks().filter(linkDefinition => linkDefinition.isCascadingUpdated());

      for (let cascadingUpdatingLinkDefinition of cascadingUpdatingLinkDefinitions) {
        if (cascadingUpdatingLinkDefinition.getRelatedModelDefinition().isIndexed()) {
          // indexable.push({
          //   type: cascadingUpdatingLinkDefinition.getRelatedModelDefinition().getRdfType(),
          //   id: ""
          // })
        }
      }


      await this._graphControllerPublisher.deleteTriples({
        query: this._stringifyUpdateFromSparqlPattern({
          updateType: "insertdelete",
          prefixes: Object.assign({}, this.getDefaultRdfMappings(), modelDefinition.getRdfPrefixesMapping()),
          deleteTriples,
          whereTriples: deleteTriples,
          insertTriples: []
        }),
        messageContext: {
          // TODO: Set indexation infos here.
        }
      });
    }
  }


  /**
   * Is label translated for lang.
   * @param {Model} sourceNode Source node
   * @param {LabelDefinition} labelDefinition
   * @param {string} lang Lang
   *
   * @return {boolean}
   */
  async isLocalizedLabelTranslatedForNode({sourceNode, lang, labelDefinition}) {
    let labels = sourceNode[labelDefinition.getLabelName()] || {};

    return !!labels[lang];
  }

  /**
   * @param {LabelDefinition} labelDefinition
   * @param {Model} object
   * @param {string} lang
   * @param {boolean} [returnFirstOneIfNotExistForLang]
   * @return {string}
   */
  async getLocalizedLabelForNode({sourceNode, labelDefinition, lang, returnFirstOneIfNotExistForLang}) {
    let labels = sourceNode[labelDefinition.getLabelName()] || {};

    if (isObject(labels)){
      if (labels[lang]){
        return labels[lang];
      }

      if(returnFirstOneIfNotExistForLang){
        return Object.values(labels)[0]
      }
    } else {
      return labels;
    }
  }

  /////////
  // UTILS
  /////////

  /**
   * Create a SPARQL query (SELECT, CONSTRUCT, ASK).
   *
   * @see documentation here : https://github.com/RubenVerborgh/SPARQL.js
   *
   * @param queryType
   * @param template
   * @param prefixes
   * @param variables
   * @param triples
   * @param options
   * @param filters
   * @param order
   * @param distinct
   * @return {*}
   */
  _stringifyQueryFromSparqlPattern({queryType, template, prefixes, variables, whereTriples, optionalWhereTriples, options, limit, offset, filters, order, distinct}) {
    return stringifyQueryFromSparqlPattern({
      queryType, template, prefixes, variables, whereTriples, optionalWhereTriples, options, limit, offset, filters, order, distinct, stripNewlineCharInSparqlQuery: this._stripNewlineCharInSparqlQuery
    });
  }

  /**
   * Create a SPARQL query (INSERT, DELETE).
   *
   * @param updateType
   * @param prefixes
   * @param insertTriples
   * @param whereTriples
   * @param deleteTriples
   * @param graphId
   * @param filters
   * @param options
   * @return {*}
   */
  _stringifyUpdateFromSparqlPattern({updateType, prefixes, insertTriples, whereTriples, deleteTriples, filters, options, graphId}) {
    if(!graphId){
      graphId = this._insertingNamedGraphId;
    }

    return stringifyUpdateFromSparqlPattern({
      updateType, prefixes, insertTriples, whereTriples, deleteTriples, filters, graphId, options, stripNewlineCharInSparqlQuery: this._stripNewlineCharInSparqlQuery
    });
  }

  /**
   * @param {JsonLdNode} jsonLdNode - the node
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @return {Model}
   */
  _parseObjectFromJsonLdNode({jsonLdNode, modelDefinition}) {
    return parseObjectFromJsonLdNode({jsonLdNode, modelDefinition, rdfPrefixesMappings: this.getDefaultRdfMappings()})
  }

  /**
   *
   * @param objectInput
   * @param modelDefinition
   * @param lang
   * @private
   */
  _parseObjectFromObjectInput({objectInput, modelDefinition, lang}) {
    return parseObjectFromObjectInput({objectInput, modelDefinition, lang, rdfPrefixesMapping: this.getDefaultRdfMappings()});
  }

  /**
   * Extract object input and return a SPARQL AST representation.
   *
   * @param modelDefinition
   * @param objectInput
   * @param lang
   * @return {insertTriples, deleteTriples, deleteTriples, filters}
   */
  _parseSparqlPatternFromInputObject({modelDefinition, objectInput, lang}) {
    return parseSparqlPatternFromInputObject({modelDefinition, objectInput, lang});
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {string} objectId
   * @param {Link[]} links       -  List of links to update
   * @param {Link[]} globalLinks -  List of other links used in transaction
   * @param {string} lang
   * @return {{prefixes: object, insertTriples: [object], deleteTriples: [object], whereTriples: [object], indexable: [object]}}
   */
  async _parseSparqlPatternFromLinks({modelDefinition, objectId, links, globalLinks, lang}) {
    return parseSparqlPatternFromLinks({
      modelDefinition, objectId, links, globalLinks, lang,
      nodesPrefix: this._nodesPrefix,
      nodeTypeFormatter: this._nodesTypeFormatter,
      nodeCreationMiddleware: this.applyNodeCreationMiddlewares.bind(this),
      nodeUpdateMiddleware: this.applyNodeUpdateMiddlewares.bind(this),
      nodeDeletionMiddleware: this.applyNodeDeletionMiddlewares.bind(this)
    });
  }

  /**
   * @param {LinkFilter[]} mustNotExistLinkFilters
   * @param {object} rdfPrefixesMappings
   * @param {string} [sourceId]
   * @param {boolean} [isStrict]
   * @return {array}
   */
  _parseSparqlPatternForFilters({mustNotExistLinkFilters, rdfPrefixesMappings, sourceId, isStrict}) {
    return parseSparqlPatternForFilters({mustNotExistLinkFilters, rdfPrefixesMappings, sourceId, isStrict});
  }

  /**
   * @param {PropertyDefinitionAbstract[]} propertyDefinitions
   * @param {string} value
   * @return {array}
   */
  _parseSparqlPatternForFilteredPropertyDefinitions({propertyDefinitions, value}) {
    return parseSparqlPatternForFilteredPropertyDefinitions({propertyDefinitions, value});
  }

  /**
   * @param {boolean} isOptional
   * @param {LinkPath} linkPath
   * @param {string}   sourceId
   * @param {object} rdfPrefixesMappings
   * @return {{whereTriples: array, optionalWhereTriples, templateTriples: array, filters: array, rdfPrefixesMappings: object}}
   * @private
   */
  async _parseSparqlPatternForLinkPath({sourceId, linkPath, rdfPrefixesMappings}) {
    return parseSparqlPatternForLinkPath({
      sourceId,
      linkPath,
      rdfPrefixesMappings,
      isOptional: false,
      requestNodeFiltersMiddleware: this.applyRequestNodeFiltersMiddlewares.bind(this)
    })
  }

  /**
   * @param {PropertyDefinitionAbstract} propertyDefinition
   * @param {string}   sourceId
   * @param {object} rdfPrefixesMappings
   * @return {{whereTriples: array, optionalWhereTriples, templateTriples: array, filters: array, rdfPrefixesMappings: object}}
   * @private
   */
  async _parseSparqlPatternForPropertyDefinition({propertyDefinition, sourceId, rdfPrefixesMappings}) {
    return parseSparqlPatternForPropertyDefinition({
      propertyDefinition,
      sourceId,
      rdfPrefixesMappings,
      requestNodeFiltersMiddleware: this.applyRequestNodeFiltersMiddlewares.bind(this)
    });
  }
}
