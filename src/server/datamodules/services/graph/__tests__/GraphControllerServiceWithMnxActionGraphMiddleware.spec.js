/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerService from "../GraphControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";
import Person from "../../../../datamodel/ontologies/mnx-agent/models/Person";
import generateId from "nanoid/generate";
import PersonDefinition from "../../../../datamodel/ontologies/mnx-agent/definitions/PersonDefinition";
import AffiliationDefinition from "../../../../datamodel/ontologies/mnx-agent/definitions/AffiliationDefinition";
import LinkDefinition, {Link} from "../../../../datamodel/toolkit/definitions/LinkDefinition";
import {MnxActionGraphMiddleware} from "../../../middlewares/graph/MnxActionGraphMiddleware";
import CreationDefinition from "../../../../datamodel/ontologies/mnx-contribution/definitions/CreationDefinition";
import EntityDefinition from "../../../../datamodel/ontologies/mnx-common/definitions/EntityDefinition";

import dayjs from "dayjs";
import SSOUser from "../../../drivers/sso/models/SSOUser";
import ModelDefinitionAbstract from "../../../../datamodel/toolkit/definitions/ModelDefinitionAbstract";
import LiteralDefinition from "../../../../datamodel/toolkit/definitions/LiteralDefinition";
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";

jest.mock("nanoid/generate");
jest.mock("dayjs", () => jest.fn((...args) => ({
  format: () => "now"
})));

let datastoreSession = new (jest.fn().mockImplementation(() => ({
  getLoggedUserAccount: async () => ({
    id: "test:user-account/12345"
  }),
})));

let mnxActionGraphMiddleware = new MnxActionGraphMiddleware();
mnxActionGraphMiddleware.attachSession(datastoreSession);

let graphControllerService = new GraphControllerService({
  networkLayer: new NetworkLayerAMQP("amqp://", "fake"),
  graphQLcontext: new GraphQLContext({
    user: new SSOUser({
      user: {
        id: '94f82fe2-21ce-4ea3-b48e-3fadbe4444ca',
        username: 'test@domain.com',
        firstName: 'John',
        lastName: 'Doe'
      }
    }), lang: "fr"
  }),
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  stripNewlineCharInSparqlQuery: false,
  middlewares: [mnxActionGraphMiddleware]
});

class MockDefinition extends ModelDefinitionAbstract{
  static getRdfType() {
    return "mnx:Mock"
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'fooLink',
        pathInIndex: 'fooLink',
        rdfObjectProperty: "mnx:fooLink",
        relatedModelDefinition: MockDefinition
      }),
      new LinkDefinition({
        linkName: 'barLink',
        pathInIndex: 'barLink',
        rdfObjectProperty: "mnx:barLink",
        relatedModelDefinition: MockDefinition
      })
    ];
  }
  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: 'fooLabel',
        rdfDataProperty: "mnx:fooLabel"
      })
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'fooLiteral',
        rdfDataProperty: 'mnx:fooLiteral',
        isSearchable: true,
        isRequired: true
      }),
    ];
  }
}

describe('GraphControllerServiceWithMnxActionCrudMiddleware', () => {
  let selectSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'select');
  let countSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'count');
  let constructSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'construct');
  let createTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'insertTriples');
  let updateTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'updateTriples');
  let askTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'ask');
  let deleteTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'deleteTriples');


  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });


  let now = "now";

  it('should create an edge', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.createEdge(PersonDefinition, "test:person/1", PersonDefinition.getLink("hasEmailAccount"), "test:email/1");

    expect(createTriplesSpyFn).toHaveBeenCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA { <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/email/1>. }`
    });
  });

  it('shoud create a simple node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr'
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        "fr": "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/creation/2> rdf:type mnx:Creation;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud create a node and connect it to an existing target node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        new Link({
          linkDefinition: PersonDefinition.getLink("hasUserAccount"),
          targetId: 'test:useraccount:1'
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/update/2> rdf:type mnx:Update;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/useraccount:1>.
 <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/useraccount:1>.
 <http://ns.mnemotix.com/instances/creation/3> rdf:type mnx:Creation;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud update a node', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        id: "test:person/1",
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        // This is a plural link with subgraph creation
        PersonDefinition.getLink("hasPhone").generateLinkFromTargetProps({
          targetObjectInput: {
            number: "06070809010",
            label: "Pro"
          }
        }),
        // This is a single link with subgraph creation
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            userId: "derek@mnemotix.com",
            username: "derekd"
          }
        }),
        PersonDefinition.getLink("hasAffiliation").generateLinkFromTargetId(
          "test:affiliation/065544333"
        ),
      ],
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
DELETE {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName;
  foaf:lastName ?lastName;
  mnx:bio ?bio;
  foaf:account ?hasUserAccount.
}
INSERT {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/phone/1> rdf:type mnx:Phone.
 <http://ns.mnemotix.com/instances/creation/2> rdf:type mnx:Creation;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/phone/1>.
 <http://ns.mnemotix.com/instances/phone/1> mnx:number "06070809010";
  rdfs:label "Pro".
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/1>.
 <http://ns.mnemotix.com/instances/useraccount/3> rdf:type mnx:UserAccount.
 <http://ns.mnemotix.com/instances/creation/2> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/useraccount/3>.
 <http://ns.mnemotix.com/instances/useraccount/3> mnx:userId "derek@mnemotix.com";
  mnx:username "derekd".
 <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/useraccount/3>.
 <http://ns.mnemotix.com/instances/update/4> rdf:type mnx:Update;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/affiliation/065544333>.
 <http://ns.mnemotix.com/instances/affiliation/065544333> mnx:hasPerson <http://ns.mnemotix.com/instances/person/1>.
 <http://ns.mnemotix.com/instances/update/4> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/person/1> mnx:bio ?bio.
  FILTER((LANG(?bio)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:account ?hasUserAccount. }
}`,
      messageContext: {
        indexable: [{
          id: "test:affiliation/065544333",
          type: "mnx:Affiliation",
        }]
      },
    });
  });

  it('shoud create a node and connect it to an existing target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: AffiliationDefinition.getLink("hasPerson"),
          targetId: 'test:affiliation/1'
        })).reverse({
          targetModelDefinition: AffiliationDefinition
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }, {
          "id": "test:affiliation/1",
          "type": "mnx:Affiliation"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/update/2> rdf:type mnx:Update;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/affiliation/1>.
 <http://ns.mnemotix.com/instances/affiliation/1> mnx:hasPerson <http://ns.mnemotix.com/instances/person/1>.
 <http://ns.mnemotix.com/instances/creation/3> rdf:type mnx:Creation;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud create a node and connect it to an new target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: AffiliationDefinition.getLink("hasPerson"),
          targetObjectInput: {
            startDate: 123456,
          }
        })).reverse({
          targetModelDefinition: AffiliationDefinition
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }, {
          "id": "test:affiliation/2",
          "type": "mnx:Affiliation"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/affiliation/2> rdf:type mnx:Affiliation.
 <http://ns.mnemotix.com/instances/creation/3> rdf:type mnx:Creation;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/affiliation/2>.
 <http://ns.mnemotix.com/instances/affiliation/2> mnx:startDate "123456"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  mnx:hasPerson <http://ns.mnemotix.com/instances/person/1>.
 <http://ns.mnemotix.com/instances/creation/3> prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud delete edges', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.deleteEdges({
      modelDefinition: PersonDefinition,
      objectId: "test:person/1",
      targetId: "test:emailAccount/1",
      linkDefinition: PersonDefinition.getLink("hasEmailAccount")
    });

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE DATA { <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/emailAccount/1>. }`,
      messageContext: {},
    });
  });

  it('shoud remove a node', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.removeNode({
      modelDefinition: PersonDefinition,
      id: "test:person/1"
    });

    expect(deleteTriplesSpyFn).not.toBeCalled();
    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
DELETE {  }
INSERT {
 <http://ns.mnemotix.com/instances/deletion/1> rdf:type mnx:Deletion;
  prov:wasAttributedTo <http://ns.mnemotix.com/instances/user-account/12345>;
  prov:startedAtTime "${now}"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>.
}
WHERE {  }`,
      messageContext: {
        indexable: []
      },
    });
  });

  it("should get a node and check it's not deleted", async () => {
    constructSpyFn.mockImplementation(() => ([]));

    await graphControllerService.getNode({
      id: "test:mock/134",
      modelDefinition: MockDefinition,
    });

    expect(constructSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/mock/134> rdf:type mnx:Mock.
 <http://ns.mnemotix.com/instances/mock/134> mnx:fooLiteral ?fooLiteral.
 <http://ns.mnemotix.com/instances/mock/134> mnx:fooLabel ?fooLabel.
}
WHERE {
 <http://ns.mnemotix.com/instances/mock/134> rdf:type mnx:Mock.
 <http://ns.mnemotix.com/instances/mock/134> mnx:fooLiteral ?fooLiteral.
 OPTIONAL { <http://ns.mnemotix.com/instances/mock/134> mnx:fooLabel ?fooLabel. }
 FILTER(NOT EXISTS {
  ?hasDeletionAction prov:wasGeneratedBy <http://ns.mnemotix.com/instances/mock/134>;
   rdf:type mnx:Deletion.
 })
}`
    });
  });

  it("should get list of nodes minus deleted", async () => {
    selectSpyFn.mockImplementation(() => ([]));
    constructSpyFn.mockImplementation(() => ([]));

    await graphControllerService.getNodes({
      modelDefinition: MockDefinition,
    });

    expect(constructSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
CONSTRUCT {
 ?uri rdf:type mnx:Mock.
 ?uri mnx:fooLiteral ?fooLiteral.
 ?uri mnx:fooLabel ?fooLabel.
}
WHERE {
 ?uri rdf:type mnx:Mock.
 ?uri mnx:fooLiteral ?fooLiteral.
 OPTIONAL { ?uri mnx:fooLabel ?fooLabel. }
 MINUS {
  ?hasDeletionAction prov:wasGeneratedBy ?uri;
   rdf:type mnx:Deletion.
 }
}`
    });
  });

  it("should count a list of nodes minus deleted", async () => {
    countSpyFn.mockImplementation(() => ([]));

    await graphControllerService.getNodes({
      modelDefinition: MockDefinition,
      justCount: true
    });

    expect(countSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
SELECT DISTINCT (COUNT(DISTINCT ?uri) AS ?count) WHERE {
 ?uri rdf:type mnx:Mock.
 ?uri mnx:fooLiteral ?fooLiteral.
 MINUS {
  ?hasDeletionAction prov:wasGeneratedBy ?uri;
   rdf:type mnx:Deletion.
 }
}`
    });
  });

  it("should gather creation nodes.", async () => {
    let links = [
      EntityDefinition.getLink("hasAction").generateLinkFromTargetProps({
        targetModelDefinition: CreationDefinition,
        targetObjectInput: {
          startedAtTime: "now"
        }
      }),
      PersonDefinition.getLink("hasPhone").generateLinkFromTargetProps({
        targetObjectInput: {
          number: "06070809010",
          label: "Pro"
        }
      }),
    ];

    let {insertTriples, deleteTriples, indexable, prefixes} = await graphControllerService._parseSparqlPatternFromLinks({
      modelDefinition: PersonDefinition,
      objectId: "test:person/12345",
      links,
      globalLinks: links,
      lang: "fr"
    });

    expect(insertTriples).toEqual([{
        subject: "test:creation/1",
        predicate: "rdf:type",
        object: "mnx:Creation"
      }, {
        subject: "test:creation/1",
        predicate: "prov:startedAtTime",
        object: "\"now\"^^http://www.w3.org/2001/XMLSchema#dateTimeStamp"
      }, {
        subject: "test:creation/1",
        predicate: "prov:wasGeneratedBy",
        object: "test:person/12345"
      }, {
        subject: "test:phone/2",
        predicate: "rdf:type",
        object: "mnx:Phone"
      }, {
        subject: "test:creation/1",
        predicate: "prov:wasGeneratedBy",
        object: "test:phone/2"
      }, {
        subject: "test:phone/2",
        predicate: "mnx:number",
        object: "\"06070809010\""
      }, {
        subject: "test:phone/2",
        predicate: "rdfs:label",
        object: "\"Pro\""
      }, {
        subject: "test:person/12345",
        predicate: "mnx:hasPhone",
        object: "test:phone/2"
      }]
    );
  });

  it("should test if node deleted", async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeExists({
      modelDefinition: MockDefinition,
      id: "test:mock/1234"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
ASK WHERE {
 <http://ns.mnemotix.com/instances/mock/1234> rdf:type mnx:Mock.
 FILTER(NOT EXISTS {
  ?hasDeletionAction prov:wasGeneratedBy <http://ns.mnemotix.com/instances/mock/1234>;
   rdf:type mnx:Deletion.
 })
}`
    });
  });

  it("should test if node connected to another and is not deleted", async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: MockDefinition,
      id: "test:mock/1234",
      linkDefinitions: [
        MockDefinition.getLink("fooLink"),
      ],
      targetId: "test:whatever/4567"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
ASK WHERE {
 <http://ns.mnemotix.com/instances/mock/1234> mnx:fooLink <http://ns.mnemotix.com/instances/whatever/4567>.
 MINUS {
  ?hasDeletionAction prov:wasGeneratedBy <http://ns.mnemotix.com/instances/whatever/4567>;
   rdf:type mnx:Deletion.
 }
}`
    });
  });

  it("should test if node connected to another and no one is deleted between them", async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: MockDefinition,
      id: "test:mock/1234",
      linkDefinitions: [
        MockDefinition.getLink("fooLink"),
        MockDefinition.getLink("barLink"),
      ],
      targetId: "test:whatever/4567"
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
ASK WHERE {
 ?fooLink rdf:type mnx:Mock.
 <http://ns.mnemotix.com/instances/mock/1234> mnx:fooLink ?fooLink.
 ?fooLink mnx:barLink <http://ns.mnemotix.com/instances/whatever/4567>.
 MINUS {
  ?hasDeletionAction prov:wasGeneratedBy ?fooLink;
   rdf:type mnx:Deletion.
 }
 MINUS {
  ?hasDeletionAction prov:wasGeneratedBy <http://ns.mnemotix.com/instances/whatever/4567>;
   rdf:type mnx:Deletion.
 }
}`
    });
  });
});