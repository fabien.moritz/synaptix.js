/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerService from "../GraphControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";
import PersonDefinition from "../../../../datamodel/ontologies/mnx-agent/definitions/PersonDefinition";
import AffiliationDefinition from "../../../../datamodel/ontologies/mnx-agent/definitions/AffiliationDefinition";
import ModelDefinitionAbstract from "../../../../datamodel/toolkit/definitions/ModelDefinitionAbstract";
import LinkDefinition, {Link} from "../../../../datamodel/toolkit/definitions/LinkDefinition";
import Person from "../../../../datamodel/ontologies/mnx-agent/models/Person";
import InvolvementDefinition from "../../../../datamodel/ontologies/mnx-project/definitions/InvolvementDefinition";
import Involvement from "../../../../datamodel/ontologies/mnx-project/models/Involvement";
import generateId from "nanoid/generate";
import UserAccountDefinition from "../../../../datamodel/ontologies/mnx-agent/definitions/UserAccountDefinition";
import UserGroup from "../../../../datamodel/ontologies/mnx-agent/models/UserGroup";
import ConceptDefinition from "../../../../datamodel/ontologies/mnx-skos/definitions/ConceptDefinition";
import LabelDefinition from "../../../../datamodel/toolkit/definitions/LabelDefinition";
import LiteralDefinition from "../../../../datamodel/toolkit/definitions/LiteralDefinition";
import OrganizationDefinition from "../../../../datamodel/ontologies/mnx-agent/definitions/OrganizationDefinition";
import {LinkPath} from "../../../../datamodel/toolkit/utils/LinkPath";
import FooDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/FooDefinitionMock";
import BazDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/BazDefinitionMock";
import BarDefinitionMock from "../../../../datamodel/__tests__/mocks/definitions/BarDefinitionMock";
import {FragmentDefinition} from "../../../../datamodel/toolkit/definitions/FragmentDefinition";
import NotInstantiableDefinitionMock
  from "../../../../datamodel/__tests__/mocks/definitions/NotInstantiableDefinitionMock";
import LinkPathPropertyDefinitionMock
  from "../../../../datamodel/__tests__/mocks/definitions/LinkPathPropertyDefinitionMock";
import {Sorting} from "../../../../datamodel/toolkit/utils/Sorting";

jest.mock("nanoid/generate");

let graphControllerService = new GraphControllerService({
  networkLayer: new NetworkLayerAMQP("amqp://", "fake"),
  graphQLcontext: new GraphQLContext({anonymous: true, lang: "fr"}),
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  stripNewlineCharInSparqlQuery: false
});


describe('GraphControllerService', () => {
  let selectSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'select');
  let countSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'count');
  let constructSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'construct');
  let createTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'insertTriples');
  let updateTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'updateTriples');
  let askTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'ask');
  let deleteTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'deleteTriples');


  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
      return ++uriCounter;
    });
  });


  let now = Date.now();
  Date.now = jest.fn().mockImplementation(() => now);

  it('should get an object from a json-ld node', async () => {
    expect(graphControllerService._parseObjectFromJsonLdNode({
      modelDefinition: PersonDefinition,
      jsonLdNode: {
        "@context": {
          "firstName": {
            "@id": "http://xmlns.com/foaf/0.1/firstName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "lastName": {
            "@id": "http://xmlns.com/foaf/0.1/lastName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "bio": {
            "@id": "http://ns.mnemotix.com/onto/bio",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "mnx": "http://ns.mnemotix.com/onto/",
          "test": "http://ns.mnemotix.com/instances/"
        },
        "@type": "mnx:Person",
        "@id": "test:person/1",
        "firstName": "John",
        "lastName": "Doe",
        "bio": [
          {
            "@language": "en",
            "@value": "Blabla in english"
          },
          {
            "@language": "fr",
            "@value": "Blabla en français"
          }
        ]
      },
      lang: 'en'
    })).toEqual(new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "John",
      lastName: "Doe",
      bio: {
        "en": "Blabla in english",
        "fr": "Blabla en français"
      }
    }));

    expect(graphControllerService._parseObjectFromJsonLdNode({
      modelDefinition: PersonDefinition,
      jsonLdNode: {
        "@context": {
          "firstName": {
            "@id": "http://xmlns.com/foaf/0.1/firstName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "lastName": {
            "@id": "http://xmlns.com/foaf/0.1/lastName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "bio": {
            "@id": "http://ns.mnemotix.com/onto/bio",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "mnx": "http://ns.mnemotix.com/onto/",
          "test": "http://ns.mnemotix.com/instances/"
        },
        "@type": "mnx:Person",
        "@id": "test:person/1",
        "firstName": "John",
        "lastName": "Doe",
        "bio": [
          {
            "@language": "en",
            "@value": "Blabla in english"
          },
          {
            "@language": "fr",
            "@value": "Blabla en français"
          }
        ]
      }
    })).toEqual(new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "John",
      lastName: "Doe",
      bio: {
        "en": "Blabla in english",
        "fr": "Blabla en français"
      }
    }));
  });

  it('should get an void from a bad json-ld node', async () => {
    expect(graphControllerService._parseObjectFromJsonLdNode({
      modelDefinition: PersonDefinition,
      jsonLdNode: {
        "@context": {
          "firstName": {
            "@id": "http://xmlns.com/foaf/0.1/firstName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "lastName": {
            "@id": "http://xmlns.com/foaf/0.1/lastName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "bio": {
            "@id": "http://ns.mnemotix.com/onto/bio",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "mnx": "http://ns.mnemotix.com/onto/",
          "test": "http://ns.mnemotix.com/instances/"
        }
      },
      lang: 'en'
    })).toBeUndefined();

    expect(graphControllerService._parseObjectFromJsonLdNode({
      modelDefinition: PersonDefinition,
      jsonLdNode: {
        "@context": {
          "firstName": {
            "@id": "http://xmlns.com/foaf/0.1/firstName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "lastName": {
            "@id": "http://xmlns.com/foaf/0.1/lastName",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "bio": {
            "@id": "http://ns.mnemotix.com/onto/bio",
            "@type": "http://www.w3.org/2001/XMLSchema#string"
          },
          "mnx": "http://ns.mnemotix.com/onto/",
          "test": "http://ns.mnemotix.com/instances/"
        },
        "@type": "mnx:Person",
        "@id": "test:person/1",
        "firstName": "John",
        "lastName": "Doe",
        "bio": [
          {
            "@language": "en",
            "@value": "Blabla in english"
          },
          {
            "@language": "fr",
            "@value": "Blabla en français"
          }
        ]
      }
    })).toEqual(new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "John",
      lastName: "Doe",
      bio: {
        "en": "Blabla in english",
        "fr": "Blabla en français"
      }
    }));
  });

  it('should test if a node exists', async () => {
    askTriplesSpyFn.mockImplementation(() => true);

    let node = await graphControllerService.isNodeExists({id: "test:person/1", modelDefinition: PersonDefinition});

    expect(askTriplesSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
ASK WHERE { <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person. }`
    });
  });

  it('should get a node', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@id": "test:person/1",
      "bio": [
        {
          "@language": "en",
          "@value": "Blabla in english"
        },
        {
          "@language": "fr",
          "@value": "Blabla en français"
        }
      ],
      "firstName": "John",
      "lastName": "Doe",
      "@context": {
        "bio": {
          "@id": "http://ns.mnemotix.com/onto/bio"
        },
        "lastName": {
          "@id": "http://xmlns.com/foaf/0.1/lastName"
        },
        "firstName": {
          "@id": "http://xmlns.com/foaf/0.1/firstName"
        },
        "test": "http://ns.mnemotix.com/instances/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "fn": "http://www.w3.org/2005/xpath-functions#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "sesame": "http://www.openrdf.org/schema/sesame#",
        "mnx": "http://ns.mnemotix.com/onto/",
        "foaf": "http://xmlns.com/foaf/0.1/"
      }
    }));

    let node = await graphControllerService.getNode({id: "test:person/1", modelDefinition: PersonDefinition});

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person.
 <http://ns.mnemotix.com/instances/person/1> mnx:createdAt ?startedAtTime.
 <http://ns.mnemotix.com/instances/person/1> mnx:avatar ?avatar.
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName.
 <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName.
 <http://ns.mnemotix.com/instances/person/1> foaf:nick ?nickName.
 <http://ns.mnemotix.com/instances/person/1> mnx:maidenName ?maidenName.
 <http://ns.mnemotix.com/instances/person/1> foaf:gender ?gender.
 <http://ns.mnemotix.com/instances/person/1> foaf:birthday ?birthday.
 <http://ns.mnemotix.com/instances/person/1> mnx:bio ?bio.
 <http://ns.mnemotix.com/instances/person/1> mnx:shortBio ?shortBio.
}
WHERE {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person.
 OPTIONAL {
  ?hasCreationAction_0 prov:wasGeneratedBy <http://ns.mnemotix.com/instances/person/1>;
   rdf:type mnx:Creation;
   prov:startedAtTime ?startedAtTime.
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> mnx:avatar ?avatar. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:nick ?nickName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> mnx:maidenName ?maidenName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:gender ?gender. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:birthday ?birthday. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> mnx:bio ?bio. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> mnx:shortBio ?shortBio. }
}`
    });

    expect(node).toEqual(new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français",
        en: "Blabla in english"
      },
    }))
  });

  it('should get a list of nodes', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [
        {
          "@id": "test:person/1",
          "bio": [
            {
              "@language": "en",
              "@value": "Blabla in english"
            },
            {
              "@language": "fr",
              "@value": "Blabla en français"
            }
          ],
          "firstName": "John",
          "lastName": "Doe"
        },
        {
          "@id": "test:person/1",
          "firstName": "Mathieu"
        },
      ],
      "@context": {
        "bio": {
          "@id": "http://ns.mnemotix.com/onto/bio"
        },
        "lastName": {
          "@id": "http://xmlns.com/foaf/0.1/lastName"
        },
        "firstName": {
          "@id": "http://xmlns.com/foaf/0.1/firstName"
        },
        "test": "http://ns.mnemotix.com/instances/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "fn": "http://www.w3.org/2005/xpath-functions#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "sesame": "http://www.openrdf.org/schema/sesame#",
        "mnx": "http://ns.mnemotix.com/onto/",
        "foaf": "http://xmlns.com/foaf/0.1/"
      }
    }));

    let node = await graphControllerService.getNodes({modelDefinition: PersonDefinition});

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
 ?uri rdf:type mnx:Person.
 ?uri mnx:createdAt ?startedAtTime.
 ?uri mnx:avatar ?avatar.
 ?uri foaf:firstName ?firstName.
 ?uri foaf:lastName ?lastName.
 ?uri foaf:nick ?nickName.
 ?uri mnx:maidenName ?maidenName.
 ?uri foaf:gender ?gender.
 ?uri foaf:birthday ?birthday.
 ?uri mnx:bio ?bio.
 ?uri mnx:shortBio ?shortBio.
}
WHERE {
 ?uri rdf:type mnx:Person.
 OPTIONAL {
  ?hasCreationAction_0 prov:wasGeneratedBy ?uri;
   rdf:type mnx:Creation;
   prov:startedAtTime ?startedAtTime.
 }
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri foaf:firstName ?firstName. }
 OPTIONAL { ?uri foaf:lastName ?lastName. }
 OPTIONAL { ?uri foaf:nick ?nickName. }
 OPTIONAL { ?uri mnx:maidenName ?maidenName. }
 OPTIONAL { ?uri foaf:gender ?gender. }
 OPTIONAL { ?uri foaf:birthday ?birthday. }
 OPTIONAL { ?uri mnx:bio ?bio. }
 OPTIONAL { ?uri mnx:shortBio ?shortBio. }
}`
    });

    expect(node).toEqual([new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français",
        en: "Blabla in english"
      }
    }), new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "Mathieu",
    })]);
  });

  it('should get a list of node with pagination', async () => {
    selectSpyFn.mockImplementation(() => ({
      "results": {
        "bindings": [
          { uri: { value: "http://ns.mnemotix.com/instances/foo/1" }},
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" }},
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" }}
        ]
      }
    }));
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: FooDefinitionMock,
      limit: 3,
      offset: 2
    });

    expect(selectSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT DISTINCT ?uri WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:bazLabel1 ?bazLabel1.
}
OFFSET 2
LIMIT 3`
    });
    
    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:bazLabel1 ?bazLabel1.
 ?uri mnx:bazLabel2 ?bazLabel2.
}
WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:bazLabel1 ?bazLabel1.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:bazLabel2 ?bazLabel2. }
 FILTER(?uri IN(<http://ns.mnemotix.com/instances/foo/1>, <http://ns.mnemotix.com/instances/foo/2>, <http://ns.mnemotix.com/instances/foo/2>))
}`
    });
  });

  it('should get a list of nodes with required properties', async () => {
    constructSpyFn.mockImplementation(() => ({}));

    class RequiredPropModelDef  extends ModelDefinitionAbstract{
      static getRdfType(){
        return "mnx:Bar"
      }
      static getLiterals() {
        return [
          ...super.getLiterals(),
          new LiteralDefinition({
            literalName: 'foo1',
            rdfDataProperty: "mnx:foo1",
            isRequired: true
          })
        ];
      }
      static getLabels() {
        return [
          ...super.getLabels(),
          new LabelDefinition({
            labelName: 'foo2',
            rdfDataProperty: "mnx:foo2",
            isRequired: true
          })
        ];
      }
    }

    let node = await graphControllerService.getNodes({
      modelDefinition: RequiredPropModelDef
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:foo1 ?foo1.
 ?uri mnx:foo2 ?foo2.
}
WHERE {
 ?uri rdf:type mnx:Bar.
 ?uri mnx:foo1 ?foo1.
 ?uri mnx:foo2 ?foo2.
}`
    });
  });

  it('should get a list of nodes without type assertion', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [
        {
          "@id": "test:person/1",
          "bio": [
            {
              "@language": "en",
              "@value": "Blabla in english"
            },
            {
              "@language": "fr",
              "@value": "Blabla en français"
            }
          ],
          "firstName": "John",
          "lastName": "Doe"
        },
        {
          "@id": "test:person/1",
          "firstName": "Mathieu"
        },
      ],
      "@context": {
        "bio": {
          "@id": "http://ns.mnemotix.com/onto/bio"
        },
        "lastName": {
          "@id": "http://xmlns.com/foaf/0.1/lastName"
        },
        "firstName": {
          "@id": "http://xmlns.com/foaf/0.1/firstName"
        },
        "test": "http://ns.mnemotix.com/instances/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "fn": "http://www.w3.org/2005/xpath-functions#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "sesame": "http://www.openrdf.org/schema/sesame#",
        "mnx": "http://ns.mnemotix.com/onto/",
        "foaf": "http://xmlns.com/foaf/0.1/"
      }
    }));

    let node = await graphControllerService.getNodes({
      modelDefinition: PersonDefinition,
      discardTypeAssertion: true
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
CONSTRUCT {
 ?uri rdf:type mnx:Person.
 ?uri mnx:createdAt ?startedAtTime.
 ?uri mnx:avatar ?avatar.
 ?uri foaf:firstName ?firstName.
 ?uri foaf:lastName ?lastName.
 ?uri foaf:nick ?nickName.
 ?uri mnx:maidenName ?maidenName.
 ?uri foaf:gender ?gender.
 ?uri foaf:birthday ?birthday.
 ?uri mnx:bio ?bio.
 ?uri mnx:shortBio ?shortBio.
}
WHERE {
 OPTIONAL {
  ?hasCreationAction_0 prov:wasGeneratedBy ?uri;
   rdf:type mnx:Creation;
   prov:startedAtTime ?startedAtTime.
 }
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri foaf:firstName ?firstName. }
 OPTIONAL { ?uri foaf:lastName ?lastName. }
 OPTIONAL { ?uri foaf:nick ?nickName. }
 OPTIONAL { ?uri mnx:maidenName ?maidenName. }
 OPTIONAL { ?uri foaf:gender ?gender. }
 OPTIONAL { ?uri foaf:birthday ?birthday. }
 OPTIONAL { ?uri mnx:bio ?bio. }
 OPTIONAL { ?uri mnx:shortBio ?shortBio. }
}`
    });

    expect(node).toEqual([new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français",
        en: "Blabla in english"
      }
    }), new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "Mathieu",
    })]);
  });

  it('should get a void list of 1 node', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@id": "test:person/1",
      "bio": [
        {
          "@language": "en",
          "@value": "Blabla in english"
        },
        {
          "@language": "fr",
          "@value": "Blabla en français"
        }
      ],
      "firstName": "John",
      "lastName": "Doe",
      "@context": {
        "bio": {
          "@id": "http://ns.mnemotix.com/onto/bio"
        },
        "lastName": {
          "@id": "http://xmlns.com/foaf/0.1/lastName"
        },
        "firstName": {
          "@id": "http://xmlns.com/foaf/0.1/firstName"
        },
        "test": "http://ns.mnemotix.com/instances/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "fn": "http://www.w3.org/2005/xpath-functions#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "sesame": "http://www.openrdf.org/schema/sesame#",
        "mnx": "http://ns.mnemotix.com/onto/",
        "foaf": "http://xmlns.com/foaf/0.1/"
      }
    }));

    let nodes = await graphControllerService.getNodes({modelDefinition: PersonDefinition});

    expect(nodes).toEqual([new Person("test:person/1", "http://ns.mnemotix.com/instances/person/1", {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français",
        en: "Blabla in english"
      }
    })])
  });

  it('should get a searched list of node ', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: PersonDefinition,
      qs: "Derek",
     
    });

    expect(constructSpyFn.mock.calls[0][0].query).toMatch(`FILTER((REGEX(STR(?nickName), "Derek")) || ((REGEX(STR(?lastName), "Derek")) || (REGEX(STR(?firstName), "Derek"))))`);

    await graphControllerService.getNodes({
      modelDefinition: PersonDefinition,
      qs: "/^Derek$/",
     
    });

    expect(constructSpyFn.mock.calls[1][0].query).toMatch(`(REGEX(STR(?nickName), "^Derek$")) || ((REGEX(STR(?lastName), "^Derek$")) || (REGEX(STR(?firstName), "^Derek$")))`);

    await graphControllerService.getNodes({
      modelDefinition: PersonDefinition,
      qs: "/^Derek$/im",
     
    });

    expect(constructSpyFn.mock.calls[2][0].query).toMatch(`FILTER((REGEX(STR(?nickName), "^Derek$", "im")) || ((REGEX(STR(?lastName), "^Derek$", "im")) || (REGEX(STR(?firstName), "^Derek$", "im"))))`);
  });

  it('should get a filtered list of node ', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: PersonDefinition,
      literalFilters: [{
        literalDefinition: PersonDefinition.getLiteral("firstName"),
        value: "Derek"
      }, {
        literalDefinition: PersonDefinition.getLiteral("lastName"),
        value: "Devel"
      }],
      labelFilters: [
        {
          labelDefinition: PersonDefinition.getLabel("bio"),
          lang: "fr",
          value: "Devel"
        },
        {
          labelDefinition: PersonDefinition.getLabel("bio"),
          value: "Not localized Devel"
        }
      ],
     
    });

    expect(constructSpyFn.mock.calls[0][0].query).toMatch(`?uri foaf:firstName "Derek".`);
    expect(constructSpyFn.mock.calls[0][0].query).toMatch(`?uri foaf:lastName "Devel".`);
    expect(constructSpyFn.mock.calls[0][0].query).toMatch(`?uri mnx:bio "Devel"@fr.`);
    expect(constructSpyFn.mock.calls[0][0].query).toMatch(`?uri mnx:bio "Not localized Devel".`);
  });

  it('should get a list of node filtered by linkPath', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: FooDefinitionMock,
      linkPath: new LinkPath()
        .step({linkDefinition: FooDefinitionMock.getLink("hasBaz")})
        .step({linkDefinition: BazDefinitionMock.getLink("hasBar"), targetId: "test:bar/1234"})
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:bazLabel1 ?bazLabel1.
 ?uri mnx:bazLabel2 ?bazLabel2.
}
WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:bazLabel1 ?bazLabel1.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasBar <http://ns.mnemotix.com/instances/bar/1234>.
 <http://ns.mnemotix.com/instances/bar/1234> rdf:type mnx:Bar.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:bazLabel2 ?bazLabel2. }
}`
    });
  });

  it('should get a list of node linked to another one defined by linkPath with filterOnProperty', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: FooDefinitionMock,
      linkPath: new LinkPath()
        .step({linkDefinition: FooDefinitionMock.getLink("hasBaz")})
        .step({linkDefinition: BazDefinitionMock.getLink("hasBar")})
        .filterOnProperty({propertyDefinition: BarDefinitionMock.getLabel("barLabel1"), value: "^mnx$"})
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:bazLabel1 ?bazLabel1.
 ?uri mnx:bazLabel2 ?bazLabel2.
}
WHERE {
 ?uri rdf:type mnx:Foo.
 ?uri mnx:bazLabel1 ?bazLabel1.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasBar ?hasBar_1.
 ?hasBar_1 rdf:type mnx:Bar.
 ?hasBar_1 mnx:barLabel1 ?barLabel1.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
 OPTIONAL { ?uri mnx:bazLabel2 ?bazLabel2. }
 FILTER(REGEX(STR(?barLabel1), "^mnx$"))
}`
    });
  });

  it('should get a list of node enhanced by fragments', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: NotInstantiableDefinitionMock,
      fragmentDefinitions: [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("bazLabel1")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        })
      ]
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:NotInstantiableMock.
 ?uri mnx:avatar ?avatar.
 ?uri mnx:bazLabel1 ?bazLabel1.
 ?uri mnx:barLiteral1 ?barLiteral1.
}
WHERE {
 ?uri rdf:type mnx:NotInstantiableMock.
 ?uri mnx:bazLabel1 ?bazLabel1.
 ?uri mnx:barLiteral1 ?barLiteral1.
 OPTIONAL { ?uri mnx:avatar ?avatar. }
}`
    });
  });


  it('should get a node enhanced by fragments', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNode({
      id: "test:foo/123",
      modelDefinition: NotInstantiableDefinitionMock,
      fragmentDefinitions: [
        new FragmentDefinition({
          modelDefinition: FooDefinitionMock,
          properties: [FooDefinitionMock.getProperty("bazLabel1")]
        }),
        new FragmentDefinition({
          modelDefinition: BarDefinitionMock,
          properties: [BarDefinitionMock.getProperty("barLiteral1")]
        })
      ]
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:NotInstantiableMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:avatar ?avatar.
 <http://ns.mnemotix.com/instances/foo/123> mnx:bazLabel1 ?bazLabel1.
 <http://ns.mnemotix.com/instances/foo/123> mnx:barLiteral1 ?barLiteral1.
}
WHERE {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:NotInstantiableMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:bazLabel1 ?bazLabel1.
 <http://ns.mnemotix.com/instances/foo/123> mnx:barLiteral1 ?barLiteral1.
 OPTIONAL { <http://ns.mnemotix.com/instances/foo/123> mnx:avatar ?avatar. }
}`
    });
  });

  it('should get a node with link path property', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNode({
      id: "test:foo/123",
      modelDefinition: LinkPathPropertyDefinitionMock,
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:aliasLabel1 ?bazLabel1.
}
WHERE {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz <http://ns.mnemotix.com/instances/foo/123>.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:bazLabel1 ?bazLabel1.
}`
    });
  });

  it('should get a list of node with link path property', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: LinkPathPropertyDefinitionMock,
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?uri mnx:aliasLabel1 ?bazLabel1.
}
WHERE {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:bazLabel1 ?bazLabel1.
}`
    });
  });

  it('should get a list of node with link path property with sorting', async () => {
    selectSpyFn.mockImplementation(() => ({
      "results": {
        "bindings": [
          { uri: { value: "http://ns.mnemotix.com/instances/foo/1" }},
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" }},
          { uri: { value: "http://ns.mnemotix.com/instances/foo/2" }}
        ]
      }
    }));

    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNodes({
      modelDefinition: LinkPathPropertyDefinitionMock,
      sortings: [new Sorting({
        propertyDefinition: LinkPathPropertyDefinitionMock.getProperty("linkPathLabel1"),
        descending: true
      })]
    });

    expect(selectSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT DISTINCT ?uri ?bazLabel1 WHERE {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:bazLabel1 ?bazLabel1.
}
ORDER BY DESC (?bazLabel1)`
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?uri mnx:aliasLabel1 ?bazLabel1.
}
WHERE {
 ?uri rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz ?uri.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:bazLabel1 ?bazLabel1.
 FILTER(?uri IN(<http://ns.mnemotix.com/instances/foo/1>, <http://ns.mnemotix.com/instances/foo/2>, <http://ns.mnemotix.com/instances/foo/2>))
}`
    });
  });

  it('should get a node with link path property', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@graph": [],
      "@context": {}
    }));

    await graphControllerService.getNode({
      id: "test:foo/123",
      modelDefinition: LinkPathPropertyDefinitionMock,
    });

    expect(constructSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 <http://ns.mnemotix.com/instances/foo/123> mnx:aliasLabel1 ?bazLabel1.
}
WHERE {
 <http://ns.mnemotix.com/instances/foo/123> rdf:type mnx:LinkPathPropertyMock.
 ?hasBaz_0 mnx:hasBaz <http://ns.mnemotix.com/instances/foo/123>.
 ?hasBaz_0 rdf:type mnx:Baz.
 ?hasBaz_0 mnx:hasFoo ?hasFoo_1.
 ?hasFoo_1 rdf:type mnx:Foo.
 ?hasFoo_1 mnx:bazLabel1 ?bazLabel1.
}`
    });
  });

  it('should get a void list of node', async () => {
    constructSpyFn.mockImplementation(() => ({
      "@context": {
        "bio": {
          "@id": "http://ns.mnemotix.com/onto/bio"
        },
        "lastName": {
          "@id": "http://xmlns.com/foaf/0.1/lastName"
        },
        "firstName": {
          "@id": "http://xmlns.com/foaf/0.1/firstName"
        },
        "test": "http://ns.mnemotix.com/instances/",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "fn": "http://www.w3.org/2005/xpath-functions#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "sesame": "http://www.openrdf.org/schema/sesame#",
        "mnx": "http://ns.mnemotix.com/onto/",
        "foaf": "http://xmlns.com/foaf/0.1/"
      }
    }));

    let nodes = await graphControllerService.getNodes({modelDefinition: PersonDefinition});

    expect(nodes).toEqual([])
  });

  it('should get a void list of node if response empty', async () => {
    constructSpyFn.mockImplementation(() => ({}));

    let nodes = await graphControllerService.getNodes({modelDefinition: PersonDefinition});

    expect(nodes).toEqual([])
  });

  it('should count a list of node', async () => {
    countSpyFn.mockImplementation(() => 2);

    let count = await graphControllerService.countNodes({modelDefinition: PersonDefinition});

    expect(countSpyFn).toHaveBeenCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT (COUNT(DISTINCT ?uri) AS ?count) WHERE { OPTIONAL { ?uri rdf:type mnx:Person. } }`
    });

    expect(count).toEqual(2)
  });

  it('should create an edge', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.createEdge(PersonDefinition, "test:person/1", PersonDefinition.getLink("hasEmailAccount"), "test:email/1");

    expect(createTriplesSpyFn).toHaveBeenCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA { <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/email/1>. }`
    });
  });

  it('raise an error on edge creation', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    class BlablaDefinition extends ModelDefinitionAbstract {
      static getLinks() {
        return [
          new LinkDefinition({
            linkName: 'person',
            pathInIndex: 'person',
            relatedModelDefinition: PersonDefinition,
          }),
        ];
      }
    }

    await expect(graphControllerService.createEdge(BlablaDefinition, "blabla:123", BlablaDefinition.getLink("person"), "person:123")).rejects.toThrow("A rdfObjectProperty or rdfReversedObjectProperty must be set in the \"person\" link definition from \"BlablaDefinition\"");
  });

  it('shoud create a simple node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr'
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        "fr": "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
}`
    });
  });

  it('shoud create a node and connect it to an existing target node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        new Link({
          linkDefinition: PersonDefinition.getLink('hasUserAccount'),
          targetId: 'test:useraccount:1'
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr;
  foaf:account <http://ns.mnemotix.com/instances/useraccount:1>.
}`
    });
  });

  it('shoud throw an exception if link points to a non instantiable model definition', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await expect(graphControllerService.createNode({
      modelDefinition: InvolvementDefinition,
      objectInput: {
        role: "Boss",
        startDate: now,
      },
      links: [
        new Link({
          linkDefinition: InvolvementDefinition.getLink('hasAgent'),
          targetObjectInput: {
            firstName: "John",
            lastName: "Doe",
            bio: "Blabla en français"
          }
        }),
      ],
      lang: 'fr',
    })).rejects.toThrow("The model definition \"AgentDefinition\" is not instantiable. Please specify a \"targetModelDefinition\" property in the link \"InvolvementDefinition\" ~> \"hasAgent\"");
  });

  it('shoud create a node and connect it to a new target node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: InvolvementDefinition,
      objectInput: {
        role: "Boss",
        startDate: now,
      },
      links: [
        new Link({
          linkDefinition: InvolvementDefinition.getLink('hasAgent'),
          targetModelDefinition: PersonDefinition,
          targetObjectInput: {
            firstName: "John",
            lastName: "Doe",
            bio: "Blabla en français"
          }
        }),
      ],
      lang: 'fr',
    })).toEqual(new Involvement("test:involvement/1", "http://ns.mnemotix.com/instances/involvement/1", {
      role: {
        fr: "Boss"
      },
      startDate: now,
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:involvement/1",
          "type": "mnx:Involvement"
        }, {
          "id": "test:person/2",
          "type": "mnx:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/involvement/1> rdf:type mnx:Involvement;
  mnx:role "Boss"@fr.
 <http://ns.mnemotix.com/instances/person/2> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/involvement/1> mnx:hasAgent <http://ns.mnemotix.com/instances/person/2>.
}`
    });
  });

  it("should return a type", async () => {
    constructSpyFn.mockImplementation(() => ({
      '@id': 'dinv:0/en/0',
      '@type': 'ddf:Verb',
      '@context': {
        dinv: 'http://data.dictionnairedesfrancophones.org/dict/inv/entry/',
        owl: 'http://www.w3.org/2002/07/owl#',
        rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        ddf: 'http://data.dictionnairedesfrancophones.org/ontology/ddf#',
        lexicog: 'http://www.w3.org/ns/lemon/lexicog#',
        xsd: 'http://www.w3.org/2001/XMLSchema#',
        fn: 'http://www.w3.org/2005/xpath-functions#',
        rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
        ontolex: 'http://www.w3.org/ns/lemon/ontolex#',
        sesame: 'http://www.openrdf.org/schema/sesame#',
        lexinfo: 'http://www.lexinfo.net/ontology/2.0/lexinfo#',
        mnx: 'http://ns.mnemotix.com/ontologies/crossdomain/'
      }
    }));

    let type = await graphControllerService.getRdfTypeForURI("http://data.dictionnairedesfrancophones.org/dict/inv/entry/0/en/0");

    expect(type).toBe("ddf:Verb");
  });

  it('shoud create a node and connect it to an existing target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: AffiliationDefinition.getLink('hasPerson'),
          targetId: 'test:affiliation/1'
        })).reverse({
          targetModelDefinition: AffiliationDefinition
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }, {
          "id": "test:affiliation/1",
          "type": "mnx:Affiliation"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/affiliation/1> mnx:hasPerson <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud create a node and connect it to an new target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: AffiliationDefinition.getLink('hasPerson'),
          targetObjectInput: {
            startDate: 123456,
          }
        })).reverse({
          targetModelDefinition: AffiliationDefinition
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "mnx:Person"
        }, {
          "id": "test:affiliation/2",
          "type": "mnx:Affiliation"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type mnx:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/affiliation/2> rdf:type mnx:Affiliation;
  mnx:startDate "123456"^^<http://www.w3.org/2001/XMLSchema#dateTimeStamp>;
  mnx:hasPerson <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud delete edges', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.deleteEdges({
      modelDefinition: PersonDefinition,
      objectId: "test:person/1",
      targetId: "test:emailAccount/1",
      linkDefinition: PersonDefinition.getLink("hasEmailAccount")
    });

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE DATA { <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/emailAccount/1>. }`,
      messageContext: {},
    });
  });

  it('shoud not remove a node is not permanentRemoval selected', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));
    updateTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.removeNode({
      modelDefinition: PersonDefinition,
      id: "test:person/1"
    });

    expect(updateTriplesSpyFn).not.toBeCalled();
    expect(deleteTriplesSpyFn).not.toBeCalled();
  });

  it("should parse links into triples", async () => {
    let {insertTriples, deleteTriples, indexable, prefixes} = await graphControllerService._parseSparqlPatternFromLinks({
      modelDefinition: PersonDefinition,
      objectId: "test:person/12345",
      links: [
        // This is a plural link with subgraph creation
        PersonDefinition.getLink("hasPhone").generateLinkFromTargetProps({
          targetObjectInput: {
            number: "06070809010",
            label: "Pro"
          }
        }),
        // This is a plural link with simple edge creation
        PersonDefinition.getLink("hasAffiliation").generateLinkFromTargetId(
          "test:affiliation/aff2"
        ),
        // This is a single link with subgraph creation
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            userId: "derek@mnemotix.com",
            username: "derekd"
          }
        }),
        // This is a plural link with subgraph creation and nested links
        PersonDefinition.getLink("hasAffiliation").generateLinkFromTargetProps({
          targetObjectInput: {
            role: "Boss",
          },
          targetNestedLinks: [
            AffiliationDefinition.getLink("hasOrg").generateLinkFromTargetProps({
              targetObjectInput: {
                name: "mnx"
              }
            })
          ]
        }),
      ],
      lang: "fr"
    });

    expect(insertTriples).toEqual([
      {
        subject: "test:phone/1",
        predicate: "rdf:type",
        object: "mnx:Phone"
      },
      {
        subject: "test:phone/1",
        predicate: "mnx:number",
        object: "\"06070809010\""
      },
      {
        subject: "test:phone/1",
        predicate: "rdfs:label",
        object: "\"Pro\""
      },
      {
        subject: "test:person/12345",
        predicate: "mnx:hasPhone",
        object: "test:phone/1"
      },
      {
        subject: "test:affiliation/aff2",
        predicate: "mnx:hasPerson",
        object: "test:person/12345"
      },
      {
        subject: "test:useraccount/2",
        predicate: "rdf:type",
        object: "mnx:UserAccount"
      },
      {
        subject: "test:useraccount/2",
        predicate: "mnx:userId",
        object: "\"derek@mnemotix.com\""
      },
      {
        subject: "test:useraccount/2",
        predicate: "mnx:username",
        object: "\"derekd\""
      },
      {
        subject: "test:person/12345",
        predicate: "foaf:account",
        object: "test:useraccount/2"
      }, {
        subject: "test:affiliation/3",
        predicate: "rdf:type",
        object: "mnx:Affiliation"
      },
      {
        subject: "test:organization/4",
        predicate: "rdf:type",
        object: "mnx:Organization"
      },
      {
        subject: "test:organization/4",
        predicate: "mnx:name",
        "object": "\"mnx\"@fr"
      },
      {
        subject: "test:affiliation/3",
        predicate: "mnx:hasOrg",
        object: "test:organization/4"
      },
      {
        subject: "test:affiliation/3",
        predicate: "mnx:role",
        "object": "\"Boss\"@fr"
      },
      {
        subject: "test:affiliation/3",
        predicate: "mnx:hasPerson",
        object: "test:person/12345"
      },
    ]);

    expect(deleteTriples).toEqual([
      {
        subject: "test:person/12345",
        predicate: "foaf:account",
        object: "?hasUserAccount"
      }
    ]);
  });

  it('shoud update a node', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        id: "test:person/1",
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        // This is a plural link with subgraph creation
        PersonDefinition.getLink("hasPhone").generateLinkFromTargetProps({
          targetObjectInput: {
            number: "06070809010",
            label: "Pro"
          }
        }),
        // This is a single link with subgraph creation
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            userId: "derek@mnemotix.com",
            username: "derekd"
          }
        }),
        // This is a plural link with subgraph creation and nested links
        PersonDefinition.getLink("hasAffiliation").generateLinkFromTargetProps({
          targetObjectInput: {
            role: "Boss",
          },
          targetNestedLinks: [
            AffiliationDefinition.getLink("hasOrg").generateLinkFromTargetProps({
              targetObjectInput: {
                name: "mnx"
              }
            })
          ]
        })
      ],
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
DELETE {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName;
  foaf:lastName ?lastName;
  mnx:bio ?bio;
  foaf:account ?hasUserAccount.
}
INSERT {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/phone/1> rdf:type mnx:Phone;
  mnx:number "06070809010";
  rdfs:label "Pro".
 <http://ns.mnemotix.com/instances/person/1> mnx:hasPhone <http://ns.mnemotix.com/instances/phone/1>.
 <http://ns.mnemotix.com/instances/useraccount/2> rdf:type mnx:UserAccount;
  mnx:userId "derek@mnemotix.com";
  mnx:username "derekd".
 <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/useraccount/2>.
 <http://ns.mnemotix.com/instances/affiliation/3> rdf:type mnx:Affiliation.
 <http://ns.mnemotix.com/instances/organization/4> rdf:type mnx:Organization;
  mnx:name "mnx"@fr.
 <http://ns.mnemotix.com/instances/affiliation/3> mnx:hasOrg <http://ns.mnemotix.com/instances/organization/4>;
  mnx:role "Boss"@fr;
  mnx:hasPerson <http://ns.mnemotix.com/instances/person/1>.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/person/1> mnx:bio ?bio.
  FILTER((LANG(?bio)) = "fr")
 }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:account ?hasUserAccount. }
}`,
      messageContext: {
        indexable: [{
          "id": "test:organization/4",
          "type": "mnx:Organization",
        }, {
          "id": "test:affiliation/3",
          "type": "mnx:Affiliation"
        }]
      },
    });
  });

  it('shoud remove data properties of a node', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        id: "test:person/1",
        lastName: null,
        bio: null
      },
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE {
 <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName;
  mnx:bio ?bio.
}
INSERT {  }
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/person/1> mnx:bio ?bio.
  FILTER((LANG(?bio)) = "fr")
 }
}`,
      messageContext: {
        indexable: []
      },
    });
  });

  it('shoud remove a node link in case of 1:1 relationship', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        id: "test:person/1",
      },
      links: [
        PersonDefinition.getLink("hasUserAccount").generateDeletionLink()
      ],
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE { <http://ns.mnemotix.com/instances/person/1> foaf:account ?hasUserAccount. }
INSERT {  }
WHERE { OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:account ?hasUserAccount. } }`,
      messageContext: {
        indexable: []
      },
    });
  });

  it('shoud not remove a node link in case of 1:N relationship', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        id: "test:person/1",
      },
      links: [
        PersonDefinition.getLink("hasPhone").generateDeletionLink()
      ],
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).not.toBeCalled();
  });

  it('shoud ask if two reversed nodes are connected', async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: UserAccountDefinition,
      id: "test:useraccount/1",
      linkDefinitions: UserAccountDefinition.getLink("hasUserGroup"),
      targetId: "test:usergroup/Admin",
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
ASK WHERE { <http://ns.mnemotix.com/instances/usergroup/Admin> sioc:has_member <http://ns.mnemotix.com/instances/useraccount/1>. }`
    });
  });

  it('shoud ask if two nodes are connected', async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: PersonDefinition,
      id: "test:person/1",
      linkDefinitions: PersonDefinition.getLink("hasUserAccount"),
      targetId: "test:useraccount/123",
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX sioc: <http://rdfs.org/sioc/ns#>
ASK WHERE { <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/useraccount/123>. }`
    });
  });

  it('shoud ask if two nodes are connected between a path', async () => {
    askTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.isNodeLinkedToTargetId({
      modelDefinition: PersonDefinition,
      id: "test:person/1",
      linkDefinitions: [
        PersonDefinition.getLink("hasAffiliation"),
        AffiliationDefinition.getLink("hasOrg")
      ],
      targetId: "test:organization/123",
    });

    expect(askTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
ASK WHERE {
 ?hasAffiliation rdf:type mnx:Affiliation.
 ?hasAffiliation mnx:hasPerson <http://ns.mnemotix.com/instances/person/1>.
 ?hasAffiliation mnx:hasOrg <http://ns.mnemotix.com/instances/organization/123>.
}`
    });
  });
});