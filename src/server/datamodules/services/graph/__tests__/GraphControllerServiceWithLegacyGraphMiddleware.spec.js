/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
import GraphControllerService from "../GraphControllerService";
import NetworkLayerAMQP from "../../../../network/amqp/NetworkLayerAMQP";
import GraphQLContext from "../../../../datamodel/toolkit/graphql/GraphQLContext";
import Person from "../../../../datamodel/ontologies/mnx-agent/models/Person";
import generateId from "nanoid/generate";
import {LegacyGraphMiddleware} from "../../../middlewares/graph/LegacyGraphMiddleware";
import PersonDefinition from "../../../../datamodel/ontologies-legacy/definitions/foaf/PersonDefinition";
import AffiliationDefinition from "../../../../datamodel/ontologies-legacy/definitions/foaf/AffiliationDefinition";
jest.mock("nanoid/generate");
import {Link} from "../../../../datamodel/toolkit/definitions/LinkDefinition";


let graphControllerService = new GraphControllerService({
  networkLayer: new NetworkLayerAMQP("amqp://", "fake"),
  graphQLcontext: new GraphQLContext({anonymous: true, lang: "fr"}),
  schemaNamespaceMapping: {
    "mnx": "http://ns.mnemotix.com/onto/"
  },
  nodesNamespaceURI: "http://ns.mnemotix.com/instances/",
  nodesPrefix: "test",
  stripNewlineCharInSparqlQuery: false,
  middlewares: [new LegacyGraphMiddleware()]
});

describe('GraphControllerServiceWithLegacyCrudMiddleware', () => {
  let selectSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'select');
  let countSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'count');
  let constructSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'construct');
  let createTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'insertTriples');
  let updateTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'updateTriples');
  let askTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'ask');
  let deleteTriplesSpyFn = jest.spyOn(graphControllerService.getGraphControllerPublisher(), 'deleteTriples');


  beforeEach(() => {
    let uriCounter = 0;
    generateId.mockImplementation(() => {
        return ++uriCounter;
      });
  });


  let now = Date.now();
  Date.now = jest.fn().mockImplementation(() => now);

  it('should create an edge', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.createEdge(PersonDefinition, "test:person/1", PersonDefinition.getLink("emails"), "test:email/1");

    expect(createTriplesSpyFn).toHaveBeenCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "foaf:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA { <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/email/1>. }`
    });
  });

  it('shoud create a simple node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr'
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        "fr": "Blabla en français"
      },
      creationDate: now,
      lastUpdate: now
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "foaf:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type foaf:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
}`
    });
  });

  it('shoud create a node and connect it to an existing target node', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        new Link({
          linkDefinition: PersonDefinition.getLink('userAccount'),
          targetId: 'test:useraccount:1'
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
      creationDate: now,
      lastUpdate: now
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "foaf:Person"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type foaf:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr;
  foaf:account <http://ns.mnemotix.com/instances/useraccount:1>.
}`
    });
  });

  it('shoud update a node', async () => {
    updateTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.updateNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        id: "test:person/1",
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      lang: 'fr'
    });

    expect(updateTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName;
  foaf:lastName ?lastName;
  mnx:bio ?bio.
}
INSERT {
 <http://ns.mnemotix.com/instances/person/1> foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
}
WHERE {
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:firstName ?firstName. }
 OPTIONAL { <http://ns.mnemotix.com/instances/person/1> foaf:lastName ?lastName. }
 OPTIONAL {
  <http://ns.mnemotix.com/instances/person/1> mnx:bio ?bio.
  FILTER((LANG(?bio)) = "fr")
 }
}`,
      messageContext: {indexable : []},
    });
  });

  it('shoud create a node and connect it to an existing target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: AffiliationDefinition.getLink('person'),
          targetId: 'test:affiliation/1'
        })).reverse({
          targetModelDefinition: AffiliationDefinition
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
      creationDate: now,
      lastUpdate: now
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "foaf:Person"
        }, {
          "id": "test:affiliation/1",
          "type": "mnx:Affiliation"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type foaf:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/affiliation/1> mnx:affiliate <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud create a node and connect it to an new target node with reversed property on link', async () => {
    createTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    expect(await graphControllerService.createNode({
      modelDefinition: PersonDefinition,
      objectInput: {
        firstName: "John",
        lastName: "Doe",
        bio: "Blabla en français"
      },
      links: [
        (new Link({
          linkDefinition: AffiliationDefinition.getLink('person'),
          targetObjectInput: {
            startDate: 123456,
          }
        })).reverse({
          targetModelDefinition: AffiliationDefinition
        })
      ],
      lang: 'fr',
    })).toEqual(new Person('test:person/1', 'http://ns.mnemotix.com/instances/person/1', {
      firstName: "John",
      lastName: "Doe",
      bio: {
        fr: "Blabla en français"
      },
      creationDate: now,
      lastUpdate: now
    }));

    expect(createTriplesSpyFn).toBeCalledWith({
      "messageContext": {
        "indexable": [{
          "id": "test:person/1",
          "type": "foaf:Person"
        }, {
          "id": "test:affiliation/2",
          "type": "mnx:Affiliation"
        }]
      }, "query": `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
INSERT DATA {
 <http://ns.mnemotix.com/instances/person/1> rdf:type foaf:Person;
  foaf:firstName "John";
  foaf:lastName "Doe";
  mnx:bio "Blabla en français"@fr.
 <http://ns.mnemotix.com/instances/affiliation/2> rdf:type mnx:Affiliation;
  mnx:startDate "123456"^^<http://www.w3.org/2001/XMLSchema#float>;
  mnx:affiliate <http://ns.mnemotix.com/instances/person/1>.
}`
    });
  });

  it('shoud delete edges', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));

    await graphControllerService.deleteEdges({
      modelDefinition: PersonDefinition,
      objectId: "test:person/1",
      targetId: "test:emailAccount/1",
      linkDefinition: PersonDefinition.getLink("emails")
    });

    expect(deleteTriplesSpyFn).toBeCalledWith({
      query: `PREFIX mnx: <http://ns.mnemotix.com/onto/>
PREFIX test: <http://ns.mnemotix.com/instances/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
DELETE DATA { <http://ns.mnemotix.com/instances/person/1> foaf:account <http://ns.mnemotix.com/instances/emailAccount/1>. }`,
      messageContext: {},
    });
  });

  it('shoud remove a node', async () => {
    deleteTriplesSpyFn.mockImplementation(() => ([]));
    selectSpyFn.mockImplementation(() => ([]));
    updateTriplesSpyFn.mockImplementation(() => ([]));

    await graphControllerService.removeNode({
      modelDefinition: PersonDefinition,
      id: "test:person/1"
    });

    expect(updateTriplesSpyFn).not.toBeCalled();
    expect(deleteTriplesSpyFn).not.toBeCalled();
  });
});