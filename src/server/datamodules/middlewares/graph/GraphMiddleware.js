/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @typedef {object} NodeCrudMiddlewareProcessedParameters
 * @property {object} [objectInput]
 * @property {Link[]} [links]
 */

export class GraphMiddleware {
  /** @type {SynaptixDatastoreSession} */
  _datastoreSession;

  /**
   *
   * @param {SynaptixDatastoreSession} datastoreSession
   */
  attachSession(datastoreSession){
    this._datastoreSession = datastoreSession;
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]} [links]
   * @returns {NodeCrudMiddlewareProcessedParameters}
   */
  async processNodeCreationParameters({modelDefinition, objectInput, links}){
    return {objectInput, links};
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]} [links] -  List of links
   * @returns {NodeCrudMiddlewareProcessedParameters}
   */
  async processNodeUpdateParameters({modelDefinition, objectInput, links}){
    return {objectInput, links};
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]} [links] -  List of links
   * @returns {NodeCrudMiddlewareProcessedParameters}
   */
  async processNodeDeletionParameters({modelDefinition, objectInput, links}){
    return {objectInput, links};
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {LiteralFilter[]} [literalFilters] -  List of literal filters
   * @param {LabelFilter[]} [labelFilters] -  List of label filters
   * @param {LinkFilter[]} [mustExistLinkFilters] -  List of link filters
   * @param {LinkFilter[]} [mustNotExistLinkFilters] -  List of link filters
   * @returns {{literalFilters: LiteralFilter[], labelFilters: LabelFilter[], mustExistLinkFilters: LinkFilter[], mustNotExistLinkFilters: LinkFilter[]}}
*/
  async processNodeRequestFilters({modelDefinition, literalFilters, labelFilters, mustExistsLinkFilters,mustNotExistLinkFilters}) {
    return {literalFilters, labelFilters, mustExistLinkFilters, mustNotExistLinkFilters};
  }
}