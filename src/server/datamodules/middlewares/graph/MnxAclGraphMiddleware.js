/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {GraphMiddleware} from "./GraphMiddleware";
import EntityDefinition from "../../../datamodel/ontologies/mnx-common/definitions/EntityDefinition";
import CreationDefinition from "../../../datamodel/ontologies/mnx-contribution/definitions/CreationDefinition";
import UpdateDefinition from "../../../datamodel/ontologies/mnx-contribution/definitions/UpdateDefinition";
import DeletionDefinition from "../../../datamodel/ontologies/mnx-contribution/definitions/DeletionDefinition";

export class MnxAclGraphMiddleware extends GraphMiddleware {
  /**
   * @param getCreator
   */
  constructor({getCreator}) {
    super();
  }

  /**
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @private
   */
  _isModelDefinitionAnAction(modelDefinition){
    return modelDefinition.isEqualOrDescendantOf(EntityDefinition.getLink("hasAction").getRelatedModelDefinition());
  }

  /**
   * @inheritDoc
   */
  processNodeCreationParameters({modelDefinition, objectInput, links}) {
    if (!this._isModelDefinitionAnAction(modelDefinition)) {
      links = (links || []).concat([
        EntityDefinition.getLink("hasAction").generateLinkFromTargetProps({
          targetModelDefinition: CreationDefinition,
          targetObjectInput: {
            occursAt: Date.now()
          }
        })
      ]);
    }

    return {objectInput, links};
  }

  /**
   * @inheritDoc
   */
  processNodeUpdateParameters({modelDefinition, objectInput, links}) {
    let isDeletion = !!links.find(link => link.getTargetModelDefinition().isEqualOrDescendantOf(DeletionDefinition));

    if (!isDeletion && !this._isModelDefinitionAnAction(modelDefinition)) {
      links = (links || []).concat([
        EntityDefinition.getLink("hasAction").generateLinkFromTargetProps({
          targetModelDefinition: UpdateDefinition,
          targetObjectInput: {
            occursAt: Date.now()
          }
        })
      ]);
    }

    return {objectInput, links};
  }

  /**
   * @inheritDoc
   */
  processNodeDeletionParameters({modelDefinition, objectInput, links}) {
    if (!this._isModelDefinitionAnAction(modelDefinition)) {
      links = (links || []).concat([
        EntityDefinition.getLink("hasAction").generateLinkFromTargetProps({
          targetModelDefinition: DeletionDefinition,
          targetObjectInput: {
            occursAt: Date.now()
          }
        })
      ]);
    }

    return {objectInput, links};
  }
}