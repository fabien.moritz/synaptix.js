/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {GraphMiddleware} from "./GraphMiddleware";
import EntityDefinition from "../../../datamodel/ontologies/mnx-common/definitions/EntityDefinition";
import CreationDefinition from "../../../datamodel/ontologies/mnx-contribution/definitions/CreationDefinition";
import UpdateDefinition from "../../../datamodel/ontologies/mnx-contribution/definitions/UpdateDefinition";
import DeletionDefinition from "../../../datamodel/ontologies/mnx-contribution/definitions/DeletionDefinition";
import dayjs from "dayjs";
import ActionDefinition from "../../../datamodel/ontologies/mnx-contribution/definitions/ActionDefinition";

/**
 * This middleware is used to plug PROV-O ontology (embedded in MnxAction model).
 * The goal is to track data manipulation and know :
 *  - Who has done something
 *  - When did it occur
 *  - Which nodes are targeted
 *
 *  TODO: Since the 4.5.1 version, Creation/Update/Deletion actions are mutualised to save triples for each transaction.
 *        The only thing that still remain to implement is a link between Creation/Update/Deletion actions generated in a transaction.
 *        Some investigations are done with `prov:wasInformedBy` property.
 */
export class MnxActionGraphMiddleware extends GraphMiddleware {
  /**
   * Check if Action node should be applied to modelDefinition.
   * Two conditions :
   *
   * - modelDefinition MUST NOT be an ActionDefinition (or descendant)
   * - modelDefinition MUST NOT be the target of nested link of ActionDefinition, so :
   *    - MUST NOT be a UserAccountDefinition with a link existing and pointing on logged UserAccount id.
   *
   * @param {typeof ModelDefinitionAbstract} modelDefinition
   * @param {object} objectInput
   * @param {Link[]} [globalLinks]
   * @param {Link[]} [links] -  List of links
   * @private
   */
  async _isActionAppliedTo({modelDefinition, objectInput, links, globalLinks}) {
    let userId = await this._getLoggedUserAccountId();

    let isModelDefinitionAnAction = modelDefinition.isEqualOrDescendantOf(ActionDefinition);
    let isModelDefinitionRelatedActionUserAccount = userId && modelDefinition === ActionDefinition.getLink("hasUserAccount").getRelatedModelDefinition() && !!globalLinks.find(link => link.getTargetId() === userId);

    return !isModelDefinitionAnAction && !isModelDefinitionRelatedActionUserAccount ;
  }

  async _getLoggedUserAccountId(){
    if (this._datastoreSession){
      let userAccount = await this._datastoreSession.getLoggedUserAccount();
      if (userAccount) {
        return userAccount.id;
      }
    }
  }

  async _getActionTargetProps() {
    let userId = await this._getLoggedUserAccountId();

    return {
      targetObjectInput: {
        startedAtTime: dayjs().format(),
      },
      ...(userId ? {
        targetNestedLinks: [
          ActionDefinition.getLink("hasUserAccount").generateLinkFromTargetId(userId)
        ]
      } : {})
    }
  };

  /**
   * @inheritDoc
   */
  async processNodeCreationParameters({modelDefinition, objectInput, links, globalLinks}) {
    if (await this._isActionAppliedTo({modelDefinition, objectInput, links, globalLinks})) {
      let existingActionLink = (globalLinks || []).find((link) => {
        return link.getTargetModelDefinition() === CreationDefinition
      });

      links = (links || []).concat([
        existingActionLink || EntityDefinition.getLink("hasAction").generateLinkFromTargetProps({
          targetModelDefinition: CreationDefinition,
          ...(await this._getActionTargetProps())
        })
      ]);
    }

    return {objectInput, links};
  }

  /**
   * @inheritDoc
   */
  async processNodeUpdateParameters({modelDefinition, objectInput, links, globalLinks}) {
    let isDeletion = !!links.find(link => link.getTargetModelDefinition().isEqualOrDescendantOf(DeletionDefinition));

    if (!isDeletion && await this._isActionAppliedTo({modelDefinition, objectInput, links, globalLinks})) {
      let existingActionLink = (globalLinks || []).find((link) => link.getTargetModelDefinition() === UpdateDefinition);

      links = (links || []).concat([
        existingActionLink || EntityDefinition.getLink("hasAction").generateLinkFromTargetProps({
          targetModelDefinition: UpdateDefinition,
          ...(await this._getActionTargetProps())
        })
      ]);
    }

    return {objectInput, links};
  }

  /**
   * @inheritDoc
   */
  async processNodeDeletionParameters({modelDefinition, objectInput, links, globalLinks}) {
    if (await this._isActionAppliedTo({modelDefinition, objectInput, links, globalLinks})) {
      let existingActionLink = (globalLinks || []).find((link) => link.getTargetModelDefinition() === DeletionDefinition);

      links = (links || []).concat([
        existingActionLink || EntityDefinition.getLink("hasAction").generateLinkFromTargetProps({
          targetModelDefinition: DeletionDefinition,
          ...(await this._getActionTargetProps())
        })
      ]);
    }

    return {objectInput, links};
  }

  /**
   * @inheritDoc
   */
  async processNodeRequestFilters({modelDefinition, literalFilters, labelFilters, mustExistsLinkFilters,mustNotExistLinkFilters}) {
    let isModelDefinitionAnAction = modelDefinition.isEqualOrDescendantOf(ActionDefinition);

    if(!isModelDefinitionAnAction){
      mustNotExistLinkFilters = (mustNotExistLinkFilters || []).concat([{
        linkDefinition: EntityDefinition.getLink("hasDeletionAction")
      }]);
    }

    return {literalFilters, labelFilters, mustExistsLinkFilters, mustNotExistLinkFilters};
  }
}