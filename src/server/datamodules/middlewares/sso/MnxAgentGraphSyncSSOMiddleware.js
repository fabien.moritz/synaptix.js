/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import PersonDefinition from "../../../datamodel/ontologies/mnx-agent/definitions/PersonDefinition";
import {logDebug} from "../../../utilities/logger";
import SSOMiddleware from "./SSOMiddleware";
import UserAccountDefinition from "../../../datamodel/ontologies/mnx-agent/definitions/UserAccountDefinition";

/**
 * This middleware aim at creating a tuple mnx:Person / mnx:UserAccount / mnx:EmailAccount
 * after registration.
 */
export default class MnxAgentGraphSyncSSOMiddleware extends SSOMiddleware{
  /**
   * @param {SSOUser} user - Registered SSOUser
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreRdfSession} datastoreSession - Express app
   * @param {object} requestParams - Parameters passed in registration request body.
   */
  async afterRegister({user, ssoApiClient, datastoreSession, requestParams}) {
    logDebug(`Creating mnx:Person for user ${user.getEmail()}`);

    let personInput = requestParams?.personInput || {};

    if(requestParams?.nickName){
      personInput.nickName = requestParams.nickName;
    }

    // This is the case where registration form let the choice to enter a nickname
    // instead of firstName/lastName
    if(!!user.getFirstName() && user.getFirstName() !== requestParams?.nickName){
      personInput.firstName = user.getFirstName();
    }

    if(!!user.getLastName()){
      personInput.lastName = user.getLastName();
    }

    // If personId is set in user attributes, force a created person id to this one.
    let personId = user.getAttribute("personId");

    if (personInput.id) {
      personId = personInput.id;
      delete personInput.id;
    }

    // If userAccountId is set in user attributes, force a created userAccount id to this one.
    let userAccountId = user.getAttribute("userAccountId") || datastoreSession.generateUriForModelDefinition({
      modelDefinition: UserAccountDefinition,
      idValue: user.getId(),
    });

    // if userGroupIds is set in user attributes, force a created userAccount to link to this ones.
    let userGroupIds = !!user.getAttribute("userGroupIds") ? JSON.parse(user.getAttribute("userGroupIds")) : [];

    let personModelDefinition = datastoreSession.getLoggedUserPersonModelDefinition();

    let person = await datastoreSession.createObject({
      graphQLType: personModelDefinition.getGraphQLType(),
      ...(!!personId ? {uri: personId} : {}),
      links: [
        personModelDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            userId: user.getId(),
            username: user.getEmail(),
            ...(!!userAccountId ? {uri: userAccountId} : {})
          },
          targetNestedLinks: userGroupIds.map(userGroupId => UserAccountDefinition.getLink("hasUserGroup").generateLinkFromTargetId(userGroupId))
        }),
        personModelDefinition.getLink("hasEmailAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            email: user.getEmail(),
            isMainEmail: true,
            accountName: 'Main'
          }
        }),
      ],
      objectInput: personInput
    });

    let userAttributes = {
      personId: person.uri,
      userAccountId
    };

    logDebug(`Person created with id ${person.id}`);

    user.addAttributes(userAttributes);

    logDebug(`Updating SSO to add person attributes to user ${user.getId()}`);

    await ssoApiClient.setUserAttributes({userId: user.getId(), attributes: user.getAttributes()});

    logDebug(`SSO updated`);
  }

  /**
   * @param {SSOUser} user - User id in SSO
   * @param {SSOApiClient} ssoApiClient
   * @param {SynaptixDatastoreSession} datastoreSession - Express app
   */
  async afterUnregister({user, ssoApiClient, datastoreSession}) {
    let userAccountId = user.getAttribute("userAccountId");

    if (!userAccountId) {
      userAccountId = (await datastoreSession.getUserAccountForUser(user)).id;
    }

    logDebug(`Updating user ${user.getEmail()} related mnx:UserAccount ${userAccountId}`);

    await datastoreSession.updateObject({
      objectId: userAccountId,
      modelDefinition: UserAccountDefinition,
      updatingProps: {
        isUnregistered: true
      }
    });
  }

  /**
   * @param user
   * @param ssoApiClient
   * @param datastoreSession
   */
  async afterAccountDisabling({user, ssoApiClient, datastoreSession}) {
    let userAccountId = user.getAttribute("userAccountId");

    if (!userAccountId) {
      userAccountId = (await datastoreSession.getUserAccountForUser(user)).id;
    }

    logDebug(`Updating user ${user.getEmail()} related mnx:UserAccount ${userAccountId} with isDisabled: true`);

    await datastoreSession.updateObject({
      objectId: userAccountId,
      modelDefinition: UserAccountDefinition,
      updatingProps: {
        isDisabled: true
      }
    });
  }

  /**
   * @param user
   * @param ssoApiClient
   * @param datastoreSession
   */
  async afterAccountEnabling({user, ssoApiClient, datastoreSession}) {
    let userAccountId = user.getAttribute("userAccountId");

    if (!userAccountId) {
      userAccountId = (await datastoreSession.getUserAccountForUser(user)).id;
    }

    logDebug(`Updating user ${user.getEmail()} related mnx:UserAccount ${userAccountId} with isDisabled: false`);

    await datastoreSession.updateObject({
      objectId: userAccountId,
      modelDefinition: UserAccountDefinition,
      updatingProps: {
        isDisabled: false
      }
    });
  }
}