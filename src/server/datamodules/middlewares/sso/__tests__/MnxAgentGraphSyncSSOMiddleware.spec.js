/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import MnxAgentGraphSyncSSOMiddleware from "../MnxAgentGraphSyncSSOMiddleware";
import SSOUser from "../../../drivers/sso/models/SSOUser";
import PersonDefinition from "../../../../datamodel/ontologies/mnx-agent/definitions/PersonDefinition";

let middleware = new MnxAgentGraphSyncSSOMiddleware();

let createObjectSpyFn = jest.fn().mockImplementation(() => ({
  id: "mnxd:person/1234"
}));
let createSetUserAttributesSpyFn = jest.fn().mockImplementation(() => Promise.resolve(true));

let datastoreSession = new (jest.fn().mockImplementation(() => ({
  createObject: createObjectSpyFn,
  getNodesNamespaceURI: () => "http://data.mnx.com/",
  generateUriForModelDefinition: ({modelDefinition, idPrefix, idValue}) => {
    return modelDefinition.generateURI({
      nodesNamespaceURI:  "http://data.mnx.com/",
      idPrefix,
      idValue,
      nodeTypeFormatter: (type) =>  type.toLowerCase()
    })
  },
  getLoggedUserPersonModelDefinition: () => PersonDefinition
})));


let ssoApiClient = new (jest.fn().mockImplementation(() => ({
  setUserAttributes: createSetUserAttributesSpyFn
})));

describe('MnxAgentGraphSyncSSOMiddleware', () => {
  beforeEach(() => {
  });

  it('handles a firstName/lastName registering', () => {
    middleware.afterRegister({
      user: new SSOUser({
        user: {
          id: "123456789",
          firstName: "Derek",
          lastName: "Devel",
          username: "derek@mnemotix.com"
        }
      }),
      datastoreSession,
      ssoApiClient
    });

    expect(createObjectSpyFn).toBeCalledWith({
      graphQLType: "Person",
      links: [
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            uri: "http://data.mnx.com/useraccount/123456789",
            userId: "123456789",
            username: "derek@mnemotix.com"
          }
        }),
        PersonDefinition.getLink("hasEmailAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            email: "derek@mnemotix.com",
            isMainEmail: true,
            accountName: 'Main'
          }
        })
      ],
      objectInput: {
        firstName: "Derek",
        lastName: "Devel"
      }
    });
  });

  it('handles a firstName/lastName registering with manual URIs registered in SSO', () => {
    middleware.afterRegister({
      user: new SSOUser({
        user: {
          id: "123456789",
          firstName: "Derek",
          lastName: "Devel",
          username: "derek@mnemotix.com",
          attributes: {
            "personId": "mnxd:person/1234",
            "userAccountId": "mnxd:useraccount/1234"
          }
        }
      }),
      datastoreSession,
      ssoApiClient
    });

    expect(createObjectSpyFn).toBeCalledWith({
      graphQLType: "Person",
      links: [
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            uri: "mnxd:useraccount/1234",
            userId: "123456789",
            username: "derek@mnemotix.com"
          }
        }),
        PersonDefinition.getLink("hasEmailAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            email: "derek@mnemotix.com",
            isMainEmail: true,
            accountName: 'Main'
          }
        })
      ],
      objectInput: {
        firstName: "Derek",
        lastName: "Devel"
      },
      uri: "mnxd:person/1234"
    });
  });

  it('handles a nickname registering', () => {
    middleware.afterRegister({
      user: new SSOUser({
        user: {
          id: "123456789",
          firstName: "derekd",
          lastName: null,
          username: "derek@mnemotix.com"
        }
      }),
      datastoreSession,
      ssoApiClient,
      requestParams: {
        nickName: "derekd"
      }
    });

    expect(createObjectSpyFn).toBeCalledWith({
      graphQLType: "Person",
      links: [
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            uri: "http://data.mnx.com/useraccount/123456789",
            userId: "123456789",
            username: "derek@mnemotix.com"
          }
        }),
        PersonDefinition.getLink("hasEmailAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            email: "derek@mnemotix.com",
            isMainEmail: true,
            accountName: 'Main'
          }
        })
      ],
      objectInput: {
        nickName: "derekd"
      }
    });
    // console.log(createSetUserAttributesSpyFn);
  });

  it('handles a nickname registering with a personInput', () => {
    middleware.afterRegister({
      user: new SSOUser({
        user: {
          id: "123456789",
          firstName: "derekd",
          lastName: null,
          username: "derek@mnemotix.com"
        }
      }),
      datastoreSession,
      ssoApiClient,
      requestParams: {
        nickName: "derekd",
        personInput: {
          affiliationInputs: [{
            id: "test:affiliation/12345"
          }]
        }
      }
    });

    expect(createObjectSpyFn).toBeCalledWith({
      graphQLType: "Person",
      links: [
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            uri: "http://data.mnx.com/useraccount/123456789",
            userId: "123456789",
            username: "derek@mnemotix.com"
          }
        }),
        PersonDefinition.getLink("hasEmailAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            email: "derek@mnemotix.com",
            isMainEmail: true,
            accountName: 'Main'
          }
        })
      ],
      objectInput: {
        nickName: "derekd",
        affiliationInputs: [{
          id: "test:affiliation/12345"
        }]
      }
    });
    // console.log(createSetUserAttributesSpyFn);
  });

  it('handles a nickname registering with manual personId set in personInput.id', () => {
    middleware.afterRegister({
      user: new SSOUser({
        user: {
          id: "123456789",
          firstName: "derekd",
          lastName: null,
          username: "derek@mnemotix.com"
        }
      }),
      datastoreSession,
      ssoApiClient,
      requestParams: {
        nickName: "derekd",
        personInput: {
          id: "mnxd:person/1234",
          affiliationInputs: [{
            id: "test:affiliation/12345"
          }]
        }
      }
    });

    expect(createObjectSpyFn).toBeCalledWith({
      graphQLType: "Person",
      links: [
        PersonDefinition.getLink("hasUserAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            uri: "http://data.mnx.com/useraccount/123456789",
            userId: "123456789",
            username: "derek@mnemotix.com"
          }
        }),
        PersonDefinition.getLink("hasEmailAccount").generateLinkFromTargetProps({
          targetObjectInput: {
            email: "derek@mnemotix.com",
            isMainEmail: true,
            accountName: 'Main'
          }
        })
      ],
      objectInput: {
        nickName: "derekd",
        affiliationInputs: [{
          id: "test:affiliation/12345"
        }]
      },
      uri: "mnxd:person/1234"
    });
    // console.log(createSetUserAttributesSpyFn);
  });
});