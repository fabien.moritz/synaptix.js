/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import express from "express";
import http from 'http';

/**
 * Proxy of express app redefined for better handling in Synaptix.js based applications.
 */
export default class ExpressApp {
  /** @type {e.Application} */
  _app;

  constructor() {
    this._app = express();
    this._server = http.createServer(this._app);
    this._networkLayers = {};
    this._datastoreAdapters = {};
  }

  /**
   * @return {Server | Http2Server | *}
   */
  getServer() {
    return this._server;
  }

  /**
   * @return {e.Application}
   */
  getOriginalApp(){
    return this._app;
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.use
   */
  use(...params) {
    return this._app.use(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.all
   */
  all(...params) {
    return this._app.all(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.get
   */
  get(...params) {
    return this._app.get(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.post
   */
  post(...params) {
    return this._app.post(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.puts
   */
  put(...params) {
    return this._app.put(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.patch
   */
  patch(...params) {
    return this._app.patch(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.delete
   */
  delete(...params) {
    return this._app.delete(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.update
   */
  update(...params) {
    return this._app.update(...params);
  }

  /**
   * @see https://expressjs.com/en/4x/api.html#app.head
   */
  head(...params) {
    return this._app.head(...params);
  }

  /**
   * Add a network layer.
   * @param networkLayer
   * @param key
   */
  addNetworkLayer({networkLayer, key}) {
    this._networkLayers[key || 'default'] = networkLayer;
  }

  /**
   * Get default network layer
   */
  getDefaultNetworkLayer() {
    return this._networkLayers['default'] || Object.values(this._networkLayers)[0];
  }

  /**
   * Add a network layer.
   * @param {SynaptixDatastoreAdapter} datastoreAdapter
   * @param key
   */
  addDatastoreAdapter({datastoreAdapter, key}) {
    this._datastoreAdapters[key || 'default'] = datastoreAdapter;
  }

  /**
   * Get default network layer
   * @return {SynaptixDatastoreAdapter}
   */
  getDefaultDatastoreAdapter() {
    return this._datastoreAdapters['default'] || Object.values(this._datastoreAdapters)[0];
  }
}