/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import graphQLHTTP from 'express-graphql';
import {attachDatastoreSession} from "../express-middlewares/attachDatastoreSession";
import {SynaptixError} from "../../datamodules/drivers/SynaptixError";
import {logError, logException} from "../logger";
import {FormValidationError} from "../../datamodel/toolkit/graphql/schema/middlewares/formValidation/FormValidationError";
import {I18nError} from "../I18nError";

/**
 * Generate an express GraphQL endpoint
 * @param {object} app Express application
 * @param {string} endpointURI
 * @param {GraphQLSchema} schema
 * @param {SynaptixDatastoreAdapter} datastoreAdapter
 * @param {boolean} [acceptAnonymousRequest]
 * @param {function} [isAuthorizedRequest]
 * @param {object} extraOptions
 * @param {object} extraContext
 * @param {boolean} [graphiql=true] - Setup a GraphiQL endpoint.
 */
export function generateGraphQLEndpoint(app, {endpointURI, schema, acceptAnonymousRequest, datastoreAdapter, extraOptions, extraContext, isAuthorizedRequest, graphiql}) {

  app[graphiql === false ? 'post' : 'use'](endpointURI, attachDatastoreSession({
    acceptAnonymousRequest,
    isAuthorizedRequest,
    datastoreAdapter
  }), graphQLHTTP((req, res) => {
    return {
      schema,
      graphiql: graphiql || true,
      pretty: true,
      context: req.datastoreSession,
      customFormatErrorFn: formatGraphQLError,
      ...extraOptions
    };
  }));
}

/**
 * @param {GraphQLError} error
 */
export function formatGraphQLError(error) {
  let {message, stack, originalError} = error;

  if(originalError) {
    logException(originalError);
  } else {
    logError(message);
  }

  let statusCode = originalError?.statusCode || 400;

  let errorPayload = {
    message: originalError?.i18nKey || "MALFORMED_REQUEST",
    statusCode
  };

  // In case of form validation error, add extra infos to help UI to matching failing inputs.
  if (originalError instanceof FormValidationError){
    errorPayload.formInputErrors = originalError.formInputErrors
  }

  // Trace maximum information in development.
  if (process.env.NODE_ENV !== "production") {
    if (!originalError){
      errorPayload = {
        ...errorPayload,
        extraInfos: message,
      };
    }

    if (originalError instanceof I18nError){
      errorPayload = {
        ...errorPayload,
        extraInfos: message,
        statusCode
      }
    }

    if(originalError instanceof SynaptixError){
      errorPayload.synaptixError = {
        command: originalError.command,
          status: originalError.status,
          requestPayload: originalError.requestPayload,
          statusCode
      };
    }

    errorPayload.stack = stack ? stack.split('\n') : [];
  }

  return errorPayload;
}
