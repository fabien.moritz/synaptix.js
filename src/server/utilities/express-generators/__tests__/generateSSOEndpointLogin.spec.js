import request from 'supertest';
import express from 'express';
import bodyParser from 'body-parser';
import {generateSSOEndpoint} from '../generateSSOEndpoint';
import {SSOEndpointMessageExamples} from '../../../../testing';
import {SSOEndpointLoginJSONSchemas} from '../__jsonSchemas__/SSOEndpointLoginJSONSchemas';
import ExpressApp from "../../ExpressApp";

/* Mocking 'passport' */
jest.mock('passport', () => {
  const {Authenticator} = jest.requireActual('passport');
  const LocalStrategy = jest.requireActual('passport-local');

  class PassportAuthenticatorMock extends Authenticator {
    constructor() {
      super();
      super.use('local', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
      }, (username, password, done) => {
        console.log(`passport mock local strategy called, with username "${username}" and password "${password}"`);
        done(null, {'authenticatorMock': 'works'});
      }));
    }

    use() {
    }
  }

  return {Authenticator: PassportAuthenticatorMock}
});

let app;

beforeEach(() => {
  app = new ExpressApp();
  app.use(bodyParser.json());

  generateSSOEndpoint(app, {
    authorizationURL: '/auth',
    tokenURL: '/token',
    logoutURL: '/logout',
    clientID: '123',
    clientSecret: '123',
    baseURL: '/',
    adminUsername: '123',
    adminPassword: '123',
    adminTokenUrl: '/admin/token',
    adminApiUrl: '/api',
    registrationEnabled: true
  });
});

describe('/login endpoint', () => {
  describe('successful request', () => {
    it.skip('returns the correct JSON payload and cookies', async () => {
      /* TODO : 
       *  - ensure result is send with Content-Type application/json (not the case currently)
       *  - find a way to mock the OAuth2 server, in order to have a meaningful test asserting the format of the returned JSON object
       *  - same thing for the presence of the SNXID cookie
       */
      let requestJSON = JSON.parse(SSOEndpointMessageExamples['POST /auth/login'].request);

      let response = await request(app.getOriginalApp())
        .post('/auth/login')
        .send(requestJSON);
      expect(response.status).toEqual(200);
      expect(response.body).toMatchSchema(SSOEndpointLoginJSONSchemas.successResponse);
    });
  });

  describe('input validation', () => {
    it('checks username is not empty', async () => {
      let response = await request(app.getOriginalApp())
        .post('/auth/login')
        .send({
          password: 'pass'
        });

      expect(response.status).toEqual(400);
      expect(response.body).toEqual({
        errors: {
          username: 'Username is empty'
        }
      });
      expect(response.body).toMatchSchema(SSOEndpointLoginJSONSchemas.errorResponse);
    });

    it('checks password is not empty', async () => {
      let response = await request(app.getOriginalApp())
        .post('/auth/login')
        .send({
          username: 'test@domain.com',
        });
      expect(response.status).toEqual(400);
      expect(response.body).toEqual({
        errors: {
          password: 'Password is empty'
        }
      });
      expect(response.body).toMatchSchema(SSOEndpointLoginJSONSchemas.errorResponse);
    });
  });

  describe('invalid credentials', () => {
    it.skip('returns an error response', async () => {
      /* TODO : 
       *  - find a way to mock the OAuth2 server, in order to have a meaningful test asserting the format of the returned JSON object
       */
      let requestJSON = {
        username: 'myuser',
        password: 'badpassword'
      };

      let response = await request(app.getOriginalApp())
        .post('/auth/login')
        .send(requestJSON);
      expect(response.status).toEqual(400);
      expect(response.body).toMatchSchema(SSOEndpointLoginJSONSchemas.errorResponse);
      expect(response.body).toEqual(SSOEndpointMessageExamples['POST /auth/login'].errorResponse.badCredentials);
    });
  });

});
