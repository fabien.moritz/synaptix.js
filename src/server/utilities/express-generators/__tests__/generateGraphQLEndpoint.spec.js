import {formatGraphQLError} from '../generateGraphQLEndpoint';
import {I18nError} from "../../I18nError";
import {SynaptixError} from "../../../datamodules/drivers/SynaptixError";
import {FormValidationError} from "../../../datamodel/toolkit/graphql/schema/middlewares/formValidation/FormValidationError";

let malformedRequestError = {
  "locations": [{
    "line": 2,
    "column": 41
  }, {
    "line": 1,
    "column": 1
  }],
  "message": "Variable \"$username\" is not defined by operation \"RegisterUserAccount\".",
  "positions": [
    87,
    0
  ],
  "source": {
    "body": "mutation RegisterUserAccount($type: String!) { registerUserAccount(input: {username: $username, password: $password, email: $email}) [...]",
    "name": "GraphQL request",
    "locationOffset": {
      "line": 1,
      "column": 1
    }
  },
  "stack": "GraphQLError: Variable \"$username\" is not defined by operation \"RegisterUserAccount\". at Object.leave [...]"
};

let i18nError = {
  message: "User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
  locations: [
    {
      "line": 2,
      "column": 3
    }
  ],
  path: ["me"],
  originalError: new I18nError("User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)", "USER_MUST_BE_AUTHENTICATED", 401)
};

let synatixError = {
  message: "An error occured on Synaptix side.",
  locations: [
    {
      "line": 2,
      "column": 3
    }
  ],
  path: ["forms"],
  originalError: new SynaptixError("Encountered ...", "graph.create", "ERROR", "PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>\nPREFIX skos: <http://www.w3.org/2004/02/skos/core#>\nPREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>\nPREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>\nPREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\nPREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>\nPREFIX geonames: <http://www.geonames.org/>\nPREFIX ddfv: <http://data.dictionnairedesfrancophones.org/dict/>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX prov: <http://www.w3.org/ns/prov#>\nCONSTRUCT {\n ?uri <<>rdf:type> ontolex:Form.\n ?uri ontolex:writtenRep ?writtenRep.\n}\nWHERE {\n ?uri <<>rdf:type> ontolex:Form.\n OPTIONAL { ?uri ontolex:writtenRep ?writtenRep. }\n}")
};

let formValidationError = {
  message: "2 errors occurred during form validation",
  locations: [
    {
      "line": 2,
      "column": 3
    }
  ],
  path: ["forms"],
  originalError: new FormValidationError("2 errors occurred during form validation", [
    {
      "field": "input.nickname",
      "errors": [
        "IS_REQUIRED"
      ]
    },
    {
      "field": "input.password",
      "errors": [
        "IS_REQUIRED"
      ]
    }
  ])
};

describe('formatGraphQLError', () => {
  beforeEach(() => {
    process.env.NODE_ENV = "dev";
  });

  it('handles a graphQLError from an malformed request in development', () => {
    expect(() => formatGraphQLError(malformedRequestError)).not.toThrow();
    expect(formatGraphQLError(malformedRequestError)).toEqual({
      "message": "MALFORMED_REQUEST",
      "extraInfos": "Variable \"$username\" is not defined by operation \"RegisterUserAccount\".",
      "statusCode": 400,
      "stack": [
        "GraphQLError: Variable \"$username\" is not defined by operation \"RegisterUserAccount\". at Object.leave [...]"
      ]
    });
  });

  it('handles a graphQLError from an malformed request in production', () => {
    process.env.NODE_ENV = "production";

    expect(formatGraphQLError(malformedRequestError)).toEqual({
      "message": "MALFORMED_REQUEST",
      "statusCode": 400
    });
  });

  it('handles a I18nError error in development', () => {
    expect(formatGraphQLError(i18nError)).toEqual({
      "message": "USER_MUST_BE_AUTHENTICATED",
      "extraInfos": "User must be authenticated ! (Blocked by `isAuthenticatedRule` 🛡 rule)",
      "statusCode": 401,
      "stack": []
    });
  });

  it('handles a I18nError error in production', () => {
    process.env.NODE_ENV = "production";

    expect(formatGraphQLError(i18nError)).toEqual({
      "message": "USER_MUST_BE_AUTHENTICATED",
      "statusCode": 401,
    });
  });

  it('handles a SynapixError in dev', () => {
    expect(formatGraphQLError(synatixError)).toEqual({
      "message": "SERVER_ERROR",
      "extraInfos": "An error occured on Synaptix side.",
      "statusCode": 500,
      "synaptixError": {
        "status": "ERROR",
        "command": "graph.create",
        "requestPayload": "PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>\nPREFIX skos: <http://www.w3.org/2004/02/skos/core#>\nPREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>\nPREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>\nPREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\nPREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>\nPREFIX geonames: <http://www.geonames.org/>\nPREFIX ddfv: <http://data.dictionnairedesfrancophones.org/dict/>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX prov: <http://www.w3.org/ns/prov#>\nCONSTRUCT {\n ?uri <<>rdf:type> ontolex:Form.\n ?uri ontolex:writtenRep ?writtenRep.\n}\nWHERE {\n ?uri <<>rdf:type> ontolex:Form.\n OPTIONAL { ?uri ontolex:writtenRep ?writtenRep. }\n}",
        "statusCode": 500
      },
      "stack": []
    });
  });

  it('handles a SynapixError in production', () => {
    process.env.NODE_ENV = "production";

    expect(formatGraphQLError(synatixError)).toEqual({
      "message": "SERVER_ERROR",
      "statusCode": 500
    });
  });

  it('handles a FormValidationError in dev', () => {
    expect(formatGraphQLError(formValidationError)).toEqual({
      "message": "FORM_VALIDATION_ERROR",
      "statusCode": 400,
      "formInputErrors": [
        {
          "field": "input.nickname",
          "errors": [
            "IS_REQUIRED"
          ]
        },
        {
          "field": "input.password",
          "errors": [
            "IS_REQUIRED"
          ]
        }
      ],
      "extraInfos": "2 errors occurred during form validation",
      "stack": []
    });
  });

  it('handles a FormValidationError in production', () => {
    process.env.NODE_ENV = "production";

    expect(formatGraphQLError(formValidationError)).toEqual({
      "message": "FORM_VALIDATION_ERROR",
      "statusCode": 400,
      "formInputErrors": [
        {
          "field": "input.nickname",
          "errors": [
            "IS_REQUIRED"
          ]
        },
        {
          "field": "input.password",
          "errors": [
            "IS_REQUIRED"
          ]
        }
      ],
    });
  });
});