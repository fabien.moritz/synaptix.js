/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export let SynaptixErrorResponse = {
  errors: [{
    "message": "SERVER_ERROR",
    "extraInfos": "An error occured on Synaptix side.",
    "statusCode": 500,
    "synaptixError": {
      "status": "ERROR",
      "command": "graph.create",
      "requestPayload": "PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>\nPREFIX skos: <http://www.w3.org/2004/02/skos/core#>\nPREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>\nPREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>\nPREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\nPREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>\nPREFIX geonames: <http://www.geonames.org/>\nPREFIX ddfv: <http://data.dictionnairedesfrancophones.org/dict/>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX prov: <http://www.w3.org/ns/prov#>\nCONSTRUCT {\n ?uri <<>rdf:type> ontolex:Form.\n ?uri ontolex:writtenRep ?writtenRep.\n}\nWHERE {\n ?uri <<>rdf:type> ontolex:Form.\n OPTIONAL { ?uri ontolex:writtenRep ?writtenRep. }\n}",
      "statusCode": 500
    },
    "stack": [

    ]
  }],
  data: {
    foo: null
  }
};