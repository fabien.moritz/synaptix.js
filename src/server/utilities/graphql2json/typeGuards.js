"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
const lodash_1 = require("lodash");
///////////////////
/// Type guards ///
///////////////////
exports.isIntrospectionField = (type) => lodash_1.has(type, 'args');
exports.isIntrospectionInputValue = (type) => lodash_1.has(type, 'defaultValue');
// @ts-ignore
exports.isIntrospectionListTypeRef = (type) => (type.kind === 'LIST');
exports.isIntrospectionObjectType = (type) => (type.kind === 'OBJECT');
exports.isIntrospectionInputObjectType = (type) => (type.kind === 'INPUT_OBJECT');
exports.isIntrospectionEnumType = (type) => (type.kind === 'ENUM');
exports.isNonNullIntrospectionType = (type) => (type.kind === 'NON_NULL');
exports.filterDefinitionsTypes = (types, opts) => {
  const ignoreInternals = opts && opts.ignoreInternals;
  return lodash_1.filter(types, type => ((exports.isIntrospectionObjectType(type) && !!type.fields) ||
    (exports.isIntrospectionInputObjectType(type) && !!type.inputFields)) &&
    (!ignoreInternals || (ignoreInternals && !lodash_1.startsWith(type.name, '__'))));
};
