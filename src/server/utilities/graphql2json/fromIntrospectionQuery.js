"use strict";
Object.defineProperty(exports, "__esModule", {value: true});
const lodash_1 = require("lodash");
const reducer_1 = require("./reducer");
const typeGuards_1 = require("./typeGuards");
/* istanbul ignore next */
exports.fromIntrospectionQuery = (introspection, opts) => {
  const options = opts || {ignoreInternals: true};
  const {queryType, mutationType} = introspection.__schema;
  const propertiesTypes = [queryType ? queryType.name : 'Query', mutationType ? mutationType.name : 'Mutation'];
  //////////////////////////////////////////////////////////////////////
  //// Query and Mutation are properties, custom Types are definitions
  //////////////////////////////////////////////////////////////////////
  const [properties, definitions] = lodash_1.partition(introspection.__schema.types, type => typeGuards_1.isIntrospectionObjectType(type) && lodash_1.includes(propertiesTypes, type.name));
  return {
    $schema: 'http://json-schema.org/draft-06/schema#',
    properties: lodash_1.reduce(properties, reducer_1.introspectionTypeReducer('properties'), {}),
    definitions: lodash_1.reduce(typeGuards_1.filterDefinitionsTypes(definitions, options), reducer_1.introspectionTypeReducer('definitions'), {})
  };
};
