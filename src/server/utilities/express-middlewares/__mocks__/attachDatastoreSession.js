/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SSOControllerService from "../../../datamodules/services/sso/SSOControllerService";
import SSOApiClient from "../../../datamodules/drivers/sso/SSOApiClient";
import GraphQLContext from "../../../datamodel/toolkit/graphql/GraphQLContext";
import SSOUser from "../../../datamodules/drivers/sso/models/SSOUser";

export let attachDatastoreSession = () => {
  return (req, res, next) => {
    req.datastoreSession = {
      getSSOControllerService: () => new SSOControllerService({
        ssoApiClient: new SSOApiClient({
          tokenEndpointUrl: '/api/token',
          apiEndpointUrl: '/api',
          login: "123",
          password: "123"
        }),
        ssoMiddlewares: [],
      }),
      getContext: () => new GraphQLContext({
        user: new SSOUser({
          user: {
            id: "1234",
            username: "jcduss@gmail.com",
            email: "jcduss@gmail.com",
            firstName: "Jean-Claude",
            lastName: "Duss"
          }
        })
      })
    };
    next();
  };
};