/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import GraphQLContext from "../../datamodel/toolkit/graphql/GraphQLContext";

/**
 * A middleware that init a Synaptix DatastoreSession on every request and check the user identity.
 *
 * @param {SynaptixDatastoreAdapter} datastoreAdapter
 * @param {boolean} [acceptAnonymousRequest]
 * @param {function} [isAuthorizedRequest]
 * @return {Function}
 */
export let attachDatastoreSession = ({datastoreAdapter, acceptAnonymousRequest, isAuthorizedRequest}) => {
  return (req, res, next) => {
    if (!isAuthorizedRequest) {
      isAuthorizedRequest = () => true;
    }

    if (!isAuthorizedRequest(req, res)) {
      return res.sendStatus(401);
    }

    let contextOptions = {
      request: req,
      lang: req.get('Lang') || 'fr'
    };

    if (!req.user) {
      if (!acceptAnonymousRequest) {
        return res.status(401).json({
          errors: [{
            message: "No authenticated session found for this request. Please log in and retry."
          }]
        });
      }

      contextOptions.anonymous = true;
    } else {
      contextOptions.user = req.user;
    }

    let context = new GraphQLContext(contextOptions);

    if (datastoreAdapter){
      req.datastoreSession = datastoreAdapter.getSession({
        context,
        res
      });
    }

    next();
  };
};