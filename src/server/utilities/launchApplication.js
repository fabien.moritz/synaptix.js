/*
 *  Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import chalk from 'chalk';
import figlet from 'figlet';
import cookie from 'cookie';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import http from 'http';
import {execute, subscribe} from 'graphql';
import {SubscriptionServer} from 'subscriptions-transport-ws';
import expressPlayground from 'graphql-playground-middleware-express';
import _ from 'lodash';
import {initEnvironment} from "./initEnvironment";
import {
  generateSSOEndpoint as generateDefaultSSOEndpoint
} from "./express-generators/generateSSOEndpoint";
import GraphQLContext from "../datamodel/toolkit/graphql/GraphQLContext";
import {logError, logInfo} from "./logger";
import {generateGraphQLEndpoint} from "./express-generators/generateGraphQLEndpoint";
import ExpressApp from './ExpressApp';
import SSOApiClient from "../datamodules/drivers/sso/SSOApiClient";
import SSOUser from "../datamodules/drivers/sso/models/SSOUser";
/**
 * @typedef {object} GraphQLEndpointDefinition
 * @property {string} endpointURI - URI of endpoint
 * @property {string} [datastoreKey=default]
 * @property {string} [endpointSubscriptionsURI] - Enable subscriptions providing an URI of the endpoint
 * @property {Schema} graphQLSchema - Executable GraphQL Schema
 * @property {DatastoreAdapterAbstract} datastoreAdapter - Datastore adapater to resolve data
 * @property {boolean} [acceptAnonymousRequest] - Disable authentication
 * @property {boolean} [disableAuthRedirection] - Disable auth redirection and send 401 response.
 */

/**
 * @callback GraphQLEndpointsCallback
 * @param {Object} options - options
 * @param {ExpressApp} options.app - Express app
 * @param {SSOApiClient} [options.ssoApiClient] ssoApiClient
 * @param {SSOMiddleware[]} options.ssoMiddlewares
 * @returns {[GraphQLEndpointDefinition]}
 */


/**
 * Launch an application.
 *
 * @param {object} Package - the npm package.json object
 * @param {object} environment - List of environment variables definition
 * @param {GraphQLEndpointsCallback} generateGraphQLEndpoints - A function returning a list of GraphQL endpoints definitions
 * @param {Array.<function>} launchMiddlewares - Apply middleware before launching express.
 * @param {function} addUserAttributesOnRegister
 * @param {SSOEndpointCallback} [generateSSOEndpoint]
 * @param {SSOMiddleware[]} [ssoMiddlewares]
 * @param {function[]} [initMiddlewares] - Apply middleware after creating express application
 * @param {SSOApiClient} [ssoApiClient]
 * @return {{app: ExpressApp}}
 */
export let launchApplication = async ({Package, environment, generateGraphQLEndpoints, launchMiddlewares, generateSSOEndpoint, ssoApiClient, ssoMiddlewares, initMiddlewares}) => {
  if (!generateGraphQLEndpoints) {
    throw new Error("You must provide a generateGraphQLEndpoints callback to provide GraphQL endpoints");
  }

  /* istanbul ignore next */
  console.info(`${chalk.yellow(figlet.textSync(Package.name, {
    font: 'Ghost',
    horizontalLayout: 'default',
    verticalLayout: 'default'
  }))}

Version ${chalk.yellow(Package.version)}
Coded with ${chalk.red('♥')} by ${chalk.underline.yellow('Mnemotix')}
`);

  initEnvironment(environment);

  /** @type {ExpressApp} */
  let app = new ExpressApp();
  const entrypoint = app.getServer();

  if (initMiddlewares) {
    for (let initMiddleware of initMiddlewares) {
      await initMiddleware({app});
    }
  }

  /** @type {object} */
  const io = require('socket.io')(entrypoint);

  io.on('connection', function (socket) {
    let cookies = cookie.parse(_.get(socket, 'request.headers.cookie', ''));

    if (cookies && cookies.SNXID) {
      /** @namespace cookies.UID */
      socket.request.user = cookies.SNXID;
    }
  });


  if (!parseInt(process.env.CORS_DISABLED)) {
    app.use(cors({
      origin: "*",
      methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
      preflightContinue: false,
      optionsSuccessStatus: 204,
      credentials: true
    }));
  }

  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));

  if (!ssoApiClient){
    ssoApiClient = new SSOApiClient({
      apiTokenEndpointUrl:  process.env.OAUTH_ADMIN_TOKEN_URL,
      apiEndpointUrl: process.env.OAUTH_ADMIN_API_URL,
      apiLogin: process.env.OAUTH_ADMIN_USERNAME,
      apiPassword: process.env.OAUTH_ADMIN_PASSWORD
    });
  }

  let graphQLEndpoints = await generateGraphQLEndpoints({app, ssoApiClient, ssoMiddlewares});

  //
  // Add datastores to ExpressApp
  //
  for (let {datastoreAdapter, datastoreKey} of  graphQLEndpoints) {
    app.addDatastoreAdapter({
      datastoreAdapter,
      key: datastoreKey
    });
  }

  //
  // Init authentication middleware
  //
  logInfo(`Generating SSO endpoint on 
  - Authentication   : ${process.env.OAUTH_AUTH_URL}
  - Token validation : ${process.env.OAUTH_TOKEN_URL}
  - Logout           : ${process.env.OAUTH_LOGOUT_URL}
  - Admin token      : ${process.env.OAUTH_ADMIN_TOKEN_URL}
  - Admin API        : ${process.env.OAUTH_ADMIN_API_URL}
  `);

  /** @type {SSOEndpointDefinition} */
  let {authenticate} = generateSSOEndpoint
    ? generateSSOEndpoint({app})
    : generateDefaultSSOEndpoint(app, {
      authorizationURL: process.env.OAUTH_AUTH_URL,
      tokenURL: process.env.OAUTH_TOKEN_URL,
      logoutURL: process.env.OAUTH_LOGOUT_URL,
      clientID: process.env.OAUTH_REALM_CLIENT_ID,
      clientSecret: process.env.OAUTH_REALM_CLIENT_SECRET,
      baseURL: process.env.APP_URL,
      registrationEnabled: !parseInt(process.env.OAUTH_REGISTRATION_DISABLED),
      ssoApiClient
    });


  // Securing "/heartbeat" URL is usefull to regenerate token if outdated.
  if (process.env.NODE_ENV !== 'test') {
    app.use('/heartbeat', authenticate({acceptAnonymousRequest: true}));
  }

  app.get('/heartbeat', (req, res) => {
    return res.status(200).send("︎💔🎈🏓🎉🕶");
  });

  for (let {endpointURI, graphQLSchema, datastoreAdapter, acceptAnonymousRequest, disableAuthRedirection, endpointSubscriptionsURI} of  graphQLEndpoints) {
    let endpointAbsoluteURI = `${process.env.APP_URL}${endpointURI}`.replace('//', '/');
    let playgroundURI = (endpointURI + '/playground').replace('//', '/');
    let subscriptionsURI = (endpointURI + endpointSubscriptionsURI).replace('//', '/');

    if (process.env.NODE_ENV !== 'test') {
      app.use(endpointURI, authenticate({acceptAnonymousRequest, disableAuthRedirection}));
      app.use(playgroundURI, authenticate({acceptAnonymousRequest, disableAuthRedirection}));
    }

    app.get(endpointURI, (req, res) => {
      res.redirect(playgroundURI);
    });

    generateGraphQLEndpoint(app, {
      schema: graphQLSchema,
      endpointURI,
      datastoreAdapter,
      acceptAnonymousRequest,
      graphiql: false
    });

    logInfo(`GraphQL endpoint listening on ${chalk.blue.underline(endpointAbsoluteURI)} ${chalk.bold(acceptAnonymousRequest ? `without` : 'with')} authentication`);

    if (endpointSubscriptionsURI) {
      new SubscriptionServer({
        execute,
        subscribe,
        schema: graphQLSchema,
        onConnect: async (connectionParams, webSocket, context) => {
          let lang = _.get(context, 'request.headers.lang', 'fr');

          let graphQLContextOptions = {
            lang
          };

          if (acceptAnonymousRequest) {
            graphQLContextOptions.anonymous = true;
          } else {
            let user = SSOUser.fromCookie(cookie.parse(_.get(context, 'request.headers.cookie', '')));
            if (!user) {
              throw new Error("UserId not found. Websocket must pass JWT in cookie. Please check that CORS credentials options are set to \"include\"");
            }

            graphQLContextOptions.user = user;
          }

          return graphQLContextOptions;
        },
        onOperation: (message, {context: graphQLContextOptions, ...params}) => {
          let datastoreSession = datastoreAdapter.getSession({
            context: new GraphQLContext(graphQLContextOptions)
          });

          return {context: datastoreSession, ...params};
        }
      }, {
        server: entrypoint,
        path: subscriptionsURI,
      });

      let uri = `${process.env.APP_URL.replace('http', 'ws')}${subscriptionsURI}`;

      logInfo(`GraphQL subscription endpoint listening on ${chalk.blue.underline(uri)}`);
    }

    app.get(playgroundURI, expressPlayground({
      endpoint: endpointURI,
      subscriptionEndpoint: subscriptionsURI,
      settings: {
        "request.credentials": "include"
      },
      useGraphQLConfig: false,
      shareEnabled: true
    }));

    logInfo(`GraphQL playground started at ${process.env.APP_URL}${playgroundURI}`);
  }

  //
  // Call extra middlewares
  //
  if (launchMiddlewares) {
    for (let launchMiddleware of launchMiddlewares) {
      await launchMiddleware({app, authenticate, ssoApiClient, ssoMiddlewares});
    }
  }

  try {
    for (let {datastoreAdapter} of graphQLEndpoints) {
      await datastoreAdapter.init({});
    }

    entrypoint.listen(process.env.APP_PORT, '0.0.0.0', (err) => {
      if (err) {
        logError(err);
      }

      logInfo(`ExpressJS server started and listening on port ${chalk.red.bold(process.env.APP_PORT)}...`);
    });
  } catch (err) {
    logError(err);
    process.exit(1);
  }

  return {app, authenticate};
};

