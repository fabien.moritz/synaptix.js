/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import fs from "fs";
import * as winston from 'winston';
import {I18nError} from "../I18nError";

export let logger = winston.createLogger({
  transports: /* istanbul ignore next */ process.env.NODE_ENV === 'test' ? [
    new winston.transports.Stream({
      stream: fs.createWriteStream('/dev/null')
    })
  ] : [
    new winston.transports.Console({
      level: "debug",
    })
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.simple()
  )
});

export function logInfo(...message) {
  logger.info(...message);
}

export function logError(...message) {
  logger.error(...message);
}

export function logWarning(...message) {
  logger.warn(...message);
}

export function logDebug(...message) {
  logger.debug(...message);
}

/**
 * @param {Error} e
 */
export function logException(e) {
  logger.log("error", e.stack);
}