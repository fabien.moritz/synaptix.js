/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import chalk from "chalk";
import amqplib from 'amqplib';
import {logDebug, logError, logInfo} from "../../utilities/logger/index";
import cbStore from "callback-store";
import generateId from "nanoid/generate";
import NetworkLayerAbstract from "../NetworkLayerAbstract";

/**
 * This callback is called when message is received in a AMQP callback queue.
 * @see https://www.squaremobius.net/amqp.node/channel_api.html#channel_consume
 *
 * @callback NetworkLayerAMQP~queueCallback
 * @param {string|object} content - Message content (format depending on properties.contentType)
 * @param {AMQPMessageFields} fields -  Message fields
 * @param {AMQPMessageProperties} properties - Message properties
 *
 * Message properties
 * @see https://www.squaremobius.net/amqp.node/channel_api.html#channel_publish
 */
/** @typedef {object} AMQPMessageProperties
 * @property {string} correlationId - Message correlationId parameter
 * @property {string} priority - Message priority parameter
 * @property {string} persistent - Message persistent parameter
 * @property {string} deliveryMode - Message deliveryMode parameter
 * @property {string} mandatory - Message mandatory parameter
 * @property {string} correlationId - Message mandatory parameter
 * @property {string} replyTo - Message replyTo parameter
 * @property {object} headers - Message headers parameter
 * @property {string} contentType - Message content type parameter
 * @property {string} contentEncoding - Message content encoding parameter
 */
/** Message fields
 * @see https://www.squaremobius.net/amqp.node/channel_api.html#channel_consume
 *
 * @typedef {object} AMQPMessageFields
 * @property {string} deliveryTag - Message deliveryTag
 * @property {string} consumerTag - Message consumerTag
 * @property {string} exchange - Message exchange name
 * @property {string} routingKey - Message routingKey
 * @property {boolean} redelivered - Message redelivered flag
 */

/**
 *
 */
export default class NetworkLayerAMQP extends NetworkLayerAbstract {
  /** @type {string} */
  _url;
  /** @type {string} */
  _exchangeName;
  /** @type {object} */
  _connection;
  /** @type {object} */
  _channel;
  /** @type {object} */
  _connectionOptions;
  /** @type {object} */
  _exchangeOptions;
  /** @type {boolean} */
  _connected = false;
  /** @type {string} */
  _rpcDefaultCallbackQueueName;
  /** @type {object} */
  _rpcDefaultCallbackQueue;
  /** @type {object} */
  _rpcDefaultCallbacks = cbStore();

  _listeningQueueId = 0;

  /**
   *
   * @param {string} url - AMQP broker URL
   * @param {string} exchangeName - Default topic exchange name used to route messages.
   * @param {object} [connectionOptions] - Connection options. See socketOption in https://www.squaremobius.net/amqp.node/channel_api.html#connect
   * @param {object} [exchangeOptions] - Exchange options. See options in https://www.squaremobius.net/amqp.node/channel_api.html#channel_assertExchange
   */
  constructor(url, exchangeName, connectionOptions, exchangeOptions) {
    super();

    if (!url) {
      throw new Error("You must pass the AMQP broker URL");
    }

    if (!exchangeName) {
      throw new Error("You must pass a default Topic exchange name");
    }

    this._url = url;
    this._exchangeName = exchangeName;
    this._connectionOptions = connectionOptions;
    this._exchangeOptions = exchangeOptions;
  }

  /**
   * Is layer connected ?
   * @return {boolean}
   */
  isConnected() {
    return this._connected;
  }

  /**
   * Connect to the broker.
   */
  async connect(attempts = 0) {
    if (!this._connected) {
      /* istanbul ignore next */
      if (attempts === 0) {
        logInfo(`Connecting to broker with uri [${this._url}]`);
      }

      try {
        this._connection = await amqplib.connect(this._url, this._connectionOptions);
      } catch (e) {
        /* istanbul ignore next */
        logError(e);
        /* istanbul ignore next */
        return await this.reconnect(attempts);
      }

      this._connection.on("error", async (e) => {
        logError("AMQP broker error:", e);
        this._connected = false;
        return await this.reconnect(1);
      });

      logInfo(`AMQP broker started and listening on ${chalk.red.bold(this._url)}. Default exchange is ${chalk.red.bold(this._exchangeName)}.`);

      this._channel = await this._connection.createChannel();

      await this._channel.assertExchange(this._exchangeName, 'topic', {
        durable: false,
        autoDelete: false,
        ...this._exchangeOptions
      });

      this._connected = true;

      this._rpcDefaultCallbackQueueName = `${this._exchangeName}_${process.env.UUID || generateId("0-9a-z", 5)}_callback_queue`;
      this._rpcDefaultCallbackQueue = await this.createCallbackQueue(this._rpcDefaultCallbackQueueName, this._onDefaultRPCCallback);
    }
  }

  /**
   * Reconnect closed connection.
   *
   * @param attempts
   */
  async reconnect(attempts) {
    logDebug(`Attempt reconnection in ${attempts} seconds...`);

    return new Promise((resolve) => {
      /* istanbul ignore next */
      setTimeout(() => {
        attempts++;
        resolve(this.connect(attempts + 1));
      }, 1000 * attempts)
    });
  }

  /**
   * Create a RPC callback queue (non exclusive and durable).
   *
   * @param {string}   callbackQueueName - AMQP queue name used to listen callback messages
   * @param {NetworkLayerAMQP~queueCallback} callbackFunction  - Function called for each callback message.
   * @param {string} [routingKey] - Connect callback queue to the topic exchange.
   * @param {boolean} [autoDelete=false] - Is queue durable
   */
  async createCallbackQueue(callbackQueueName, callbackFunction, routingKey, autoDelete = false) {
    if (!this.isConnected()) {
      throw new Error(`AMQP layer is not connected. Please call ${this.constructor.name}::connect()`);
    }

    let q = await this._channel.assertQueue(callbackQueueName, {
      exclusive: false,
      durable: true,
      autoDelete
    });

    if (routingKey) {
      await this._channel.bindQueue(callbackQueueName, this._exchangeName, routingKey);
    }

    this._channel.consume(callbackQueueName, ({content, fields, properties}) => {
      callbackFunction.apply(this, [
        properties.contentType === "application/json" ? JSON.parse(content.toString()) : content.toString(),
        fields,
        properties
      ]);

      // Ack only if correlation callback is found.
      this._channel.ack({content, fields, properties});
    });

    return q;
  }

  /**
   * Publish a message without waiting a response.
   *
   * @async
   * @param {string} command - RPC message topic
   * @param {object} payload - RPC message payload
   * @param {AMQPMessageProperties} [options={}] - RPC message options (see RabbitMQ options).
   */
  async publish(command, payload, options = {}) {
    if (!this.isConnected()) {
      throw new Error(`AMQP layer is not connected. Please call ${this.constructor.name}::connect()`);
    }

    await this._channel.publish(
      this._exchangeName,
      command,
      Buffer.from(JSON.stringify(payload)),
      {
        mandatory: true,
        ...options
      }
    );
  }

  /**
   * Send a message to a queue.
   *
   * @async
   * @param {string} queueName - Queue name
   * @param {object} payload   - RPC message payload
   * @param {AMQPMessageProperties} [options={}] - RPC message options (see RabbitMQ options).
   */
  async sendToQueue(queueName, payload, options) {
    if (!this.isConnected()) {
      throw new Error(`AMQP layer is not connected. Please call ${this.constructor.name}::connect()`);
    }

    /* istanbul ignore next */
    if (process.env.RABBITMQ_LOG_LEVEL === "DEBUG") {
      logDebug(`[AMQP send to queue] "${queueName}" ${JSON.stringify(payload)}`);
    }

    await this._channel.sendToQueue(
      queueName,
      Buffer.from(JSON.stringify(payload)),
      {
        mandatory: true,
        ...options
      }
    );
  }

  /**
   * Emit a rpc message and wait for a response.
   *
   * @async
   * @param {string} command - RPC message topic
   * @param {object} payload - RPC message payload
   * @param {AMQPMessageProperties} [options={}] - RPC message options (see RabbitMQ options).
   *
   * @return {string|object}
   */
  async request(command, payload, options = {}) {
    if (!this.isConnected()) {
      throw new Error(`AMQP layer is not connected. Please call ${this.constructor.name}::connect()`);
    }

    let {ttl} = options;
    let correlationId = generateId("A-Z0-9", 10);

    let promise = new Promise((resolve) => {
      this._rpcDefaultCallbacks.add(correlationId, (response) => {
        /* istanbul ignore next */
        if (process.env.RABBITMQ_LOG_LEVEL === "DEBUG") {
          logDebug(`[AMQP publishing] "${command}" ${JSON.stringify(payload)}`);
          logDebug(`[AMQP response] ${typeof response === "object" ? JSON.stringify(response) : response}`);
        }

        resolve(response)
      }, ttl || parseInt(process.env.RABBITMQ_RPC_TIMEOUT || 5e3));
    });

    await this.publish(command, payload, {
      contentType: 'application/json',
      mandatory: true,
      replyTo: this._rpcDefaultCallbackQueueName,
      correlationId,
      ...options
    });

    return promise;
  }

  /**
   * Listen on broadcasted messages filtered by a routingKey.
   *
   * @param {string} routingKey - Filter message with a routingKey. Default to #, don't filter.
   * @param {function|function[]} callbacks - A callback function (or an array).
   * @param {string} [queueName] - Options parameters
   */
  async listen(routingKey, callbacks, queueName) {
    if (!this.isConnected()) {
      throw new Error(`AMQP layer is not connected. Please call ${this.constructor.name}::connect()`);
    }

    if (!Array.isArray(callbacks)) {
      callbacks = [callbacks];
    }

    if (!routingKey) {
      routingKey = "#";
    }

    const resultsQueueName = queueName || `${process.env.RABBITMQ_EXCHANGE_NAME}_${process.env.UUID || generateId("0-9a-z", 5)}_listening_${++this._listeningQueueId}:${routingKey}`;

    return this.createCallbackQueue(resultsQueueName, this._onDefaultListenCallback.bind(this, callbacks), routingKey, true);
  }

  /* istanbul ignore next */

  /**
   * Callback function call on RPC responses.
   * @param {string|object} content - Message content
   * @param {AMQPMessageFields} fields -  Message fields
   * @param {AMQPMessageProperties} properties - RMessage properties
   * @private
   */
  _onDefaultRPCCallback(content, fields, {correlationId}) {
    let cb = this._rpcDefaultCallbacks.get(correlationId);
    if (!cb) return;

    try {
      cb(content, fields);
    } catch (e) {
      logError(e);
    }
  }

  /* istanbul ignore next */

  /**
   * @param {function|function[]} callbacks
   * @param {string|object} content - Message content
   * @param {AMQPMessageFields} fields -  Message fields
   * @param {AMQPMessageProperties} properties - RMessage properties
   * @private
   */
  _onDefaultListenCallback(callbacks, content, fields, properties) {
    try {
      callbacks.map(callback => callback(content, fields, properties));
    } catch (e) {
      logError(e);
    }
  }
}