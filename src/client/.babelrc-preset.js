module.exports = function() {
  return {
    presets: [
      ["@babel/env", {
        targets: "defaults",
        modules: process.env.ES6_MODULES ? false : "cjs",
      }]
    ]
  }
};
