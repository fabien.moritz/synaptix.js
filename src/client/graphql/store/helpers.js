/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import {ConnectionHandler} from 'relay-runtime';

let createNodeInConnection = ({recordEdge, parentId, connectionKey, connectionRangeBehavior, connectionCursor, updateCounters}, store) => {
  if (!store) {
    return;
  }

  let connection = ConnectionHandler.getConnection(store.get(parentId), connectionKey);

  if (connection) {
    ConnectionHandler[connectionRangeBehavior === "prepend" ? "insertEdgeBefore" : "insertEdgeAfter"](
      connection,
      recordEdge,
      connectionCursor
    );
  }

  if (updateCounters) {
    updateCounters.map(({id, propName}) => {
      let record = store.get(id);
      record.setValue(record.getValue(propName) + 1, propName);
    });
  }
};

export let recordTempIdPrefix = "client:new";

export let isOptimisticCreatedRecord = (record) => {
  return (record.id || "").match(recordTempIdPrefix) !== null;
};

export let createNodeInConnectionOptimisticUpdater = ({recordType, recordProps, parentId, connectionKey, connectionRangeBehavior, connectionCursor, updateCounters}, store) => {
  let tempID = Date.now();
  let record;

  if (!store) {
    return;
  }

  const recordId = `${recordTempIdPrefix}${recordType}:${tempID++}`;
  record = store.create(recordId, recordType);

  record.setValue(recordId, 'id');

  for (let recordProp in recordProps) {
    record.setValue(recordProps[recordProp], recordProp);
  }

  const recordEdge = store.create(`${recordTempIdPrefix}${recordType}Edge:${tempID++}`, `${recordType}Edge`);

  recordEdge.setLinkedRecord(record, 'node');

  createNodeInConnection({
    recordEdge,
    parentId,
    connectionKey,
    connectionRangeBehavior,
    connectionCursor,
    updateCounters
  }, store);
};

export let createNodeInConnectionUpdater = ({rootField, parentId, connectionKey, connectionRangeBehavior, connectionCursor, updateCounters}, store) => {
  const payload = store.getRootField(rootField);
  const recordEdge = payload.getLinkedRecord('createdEdge');

  if (!store) {
    return;
  }

  createNodeInConnection({
    recordEdge,
    parentId,
    connectionKey,
    connectionRangeBehavior,
    connectionCursor,
    updateCounters
  }, store);
};

export let addNodeInConnectionOptimisticUpdater = ({objectId, parentId, connectionKey, connectionRangeBehavior, connectionCursor, updateCounters}, store) => {
  let tempID = Date.now();

  if (!store) {
    return;
  }

  const record = store.get(objectId);

  const recordEdge = store.create(`${recordTempIdPrefix}${record.getType()}Edge:${tempID + 1}`, `${record.getType()}Edge`);

  recordEdge.setLinkedRecord(record, 'node');

  let connection = ConnectionHandler.getConnection(store.get(parentId), connectionKey);

  if (connection) {
    ConnectionHandler[connectionRangeBehavior === "prepend" ? "insertEdgeBefore" : "insertEdgeAfter"](
      connection,
      recordEdge,
      connectionCursor
    );
  }

  if (updateCounters) {
    updateCounters.map(({id, propName}) => {
      let record = store.get(id);
      record.setValue(record.getValue(propName) + 1, propName);
    });
  }
};

export function removeNodeFromConnection({connectionKey, connectionKeys, parentRecord, parentRecordID, deletedID, updateCounters}, store) {
  const parentNode = store.get(parentRecordID || parentRecord.id);

  if (!store) {
    return;
  }

  if (parentNode) {
    if (connectionKey) {
      connectionKeys = [connectionKey];
    }

    for (let connectionKey of connectionKeys) {
      const connection = ConnectionHandler.getConnection(
        parentNode,
        connectionKey
      );

      // console.log('removeNodeFromConnection', atob(parentRecordID || parentRecord.id), parentNode, connection, atob(deletedID));

      if (connection) {
        ConnectionHandler.deleteNode(
          connection,
          deletedID,
        );
      }
    }
  }

  if (updateCounters) {
    updateCounters.map(({id, propName}) => {
      let record = store.get(id);

      if (record) {
        record.setValue(Math.max(0, record.getValue(propName) - 1), propName);
      }
    });
  }
}


export function updateCounters(updateCounters, store) {
  updateCounters.map(({id, propName, value}) => {
    let record = store.get(id);

    if (record) {
      record.setValue(Math.max(0, record.getValue(propName) + (value || 0)), propName);
    }
  });
}