/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import MutationAbstract from "../MutationAbstract";
import get from 'lodash/get';
import set from 'lodash/set';

/**
 * @typedef {object} CacheQuery
 * @property {object} query
 * @property {object} variables
 */

/**
 * @typedef {object} ConnectionDefinition
 * @property {CacheQuery} cacheQuery
 * @property {object} connectionPath
 */

export default class CreateObjectMutation extends MutationAbstract {
  /**
   * @param {string} mutationName
   * @param {ConnectionDefinition[]} connectionDefinitions
   * @param {object} args
   */
  constructor({mutationName, connectionDefinitions, ...args}) {
    super({...args});

    if (!mutationName) {
      throw new Error("You must provide a mutationName");
    }

    this.mutationName = mutationName;
    this.connectionDefinitions = connectionDefinitions || [];
  }

  /**
   * @inheritDoc
   */
  transformOnCompletedPayload(payload) {
    return get(payload, `${this.mutationName}.createdEdge.node`)
  }

  /**
   * @param {function} mutateFn
   * @param objectInput
   * @param extraInputs
   * @return {Promise}
   */
  apply({mutateFn, objectInput, extraInputs}) {
    return super.apply({
      mutateFn,
      variables: {
        input: {
          objectInput,
          ...extraInputs
        }
      },
    })
  }

  /**
   * @inheritDoc
   */
  updateCache(cache, {data}) {
    for (let connectionDefinition of this.connectionDefinitions) {
      let cachedData = cache.readQuery(connectionDefinition.cacheQuery);
      let previousEdges = get(cachedData, `${connectionDefinition.connectionPath}.edges`);
      let createdEdge = get(data, `${this.mutationName}.createdEdge`);

      let newData = set(cachedData, `${connectionDefinition.connectionPath}.edges`, (previousEdges || []).concat([createdEdge]));

      cache.writeQuery({
        ...connectionDefinition.cacheQuery,
        data: newData
      })
    }
  }
}
