/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

import MutationAbstract from "../MutationAbstract";
import gql from 'graphql-tag';
import get from "lodash/get";
import filter from "lodash/filter";
import set from "lodash/set";

export default class RemoveObjectMutation extends MutationAbstract {
  /**
   * @param {string} objectId
   * @param {ConnectionDefinition[]} connectionDefinitions
   * @param {object} [mutation]
   * @param {object} args
   */
  constructor({mutation, objectId, connectionDefinitions, ...args}) {
    super({
      mutation: mutation || gql`
        mutation RemoveObjectMutation($input: RemoveObjectInput!) {
          removeObject(input: $input) {
            deletedId
          }
        }
      `,
      ...args
    });

    this.connectionDefinitions = connectionDefinitions;
    this.objectId = objectId;
  }

  /**
   * @param {function} mutateFn
   * @return {Promise}
   */
  apply({mutateFn}) {
    return super.apply({
      mutateFn,
      variables: {
        input: {
          objectId: this.objectId
        },
      },
      optimisticResponse: {
        removeObject: {
          deletedId: this.objectId
        }
      }
    });
  }

  /**
   * @inheritDoc
   */
  updateCache(cache, {data}) {
    for (let connectionDefinition of this.connectionDefinitions) {
      let cachedData = cache.readQuery(connectionDefinition.cacheQuery);
      let previousEdges = get(cachedData, `${connectionDefinition.connectionPath}.edges`);
      let deletedId = get(data, `removeObject.deletedId`);

      let edges = filter(previousEdges || [], ({node: {id}}) => id !== deletedId);

      let newData = set(cachedData, `${connectionDefinition.connectionPath}.edges`, edges);

      cache.writeQuery({
        ...connectionDefinition.cacheQuery,
        data: newData
      })
    }
  }
}