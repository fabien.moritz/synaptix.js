/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

/**
 * Abstract mutation
 */
export default class MutationAbstract {
  onCompletedCallbacks = [];
  onErrorCallbacks = [];
  onAfterCallbacks = [];

  /**
   * @param environment
   * @param mutation
   * @param onCompleted
   * @param onError
   * @param onAfter
   */
  constructor({mutation, onCompleted, onError, onAfter}) {
    this.mutation = mutation;

    this.addCallbacks({
      onCompleted, onError, onAfter
    });
  }

  /**
   * Transform the mutation into GraphQL AST object
   * @return {object}
   */
  toGql() {
    return this.mutation;
  }

  /**
   * @param onCompleted
   * @param onError
   * @param onAfter
   */
  addCallbacks({onCompleted, onError, onAfter}) {
    if (onCompleted) {
      this.onCompletedCallbacks.push(onCompleted);
    }

    if (onError) {
      this.onErrorCallbacks.push(onError);
    }

    if (onAfter) {
      this.onAfterCallbacks.push(onAfter);
    }
  }


  /**
   * @param {function} mutateFn
   * @param {function} updaterFn
   * @param variables
   * @param optimisticResponse
   * @param updater
   * @param optimisticUpdater
   *
   * @return {Promise}
   */
  async apply({mutateFn, updaterFn, variables, optimisticResponse, updater, optimisticUpdater}) {
    const {mutation, onCompleted, onError, onAfter} = this;

    let response = await mutateFn({
      variables,
      optimisticResponse,
      update: updaterFn || this.updateCache.bind(this)
    });

    if (response) {
      return this.transformOnCompletedPayload(response.data);
    }
  }

  /**
   * Apply a transformation on success payload
   * @param {object} payload
   * @return {*}
   */
  transformOnCompletedPayload(payload) {
    return payload;
  }

  /**
   * @param {array} errors
   * @return {*}
   */
  transformOnErrorResponse(errors) {
    return errors;
  }


  /**
   * Update cache callback.
   * @param {object} cache
   * @param {object} data - payload.
   */
  updateCache(cache, {data}) {
  }
}
