/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */


import RemoveObjectMutation from "./graphql/mutations/common/RemoveObjectMutation";
import RemoveEdgeMutation from "./graphql/mutations/common/RemoveEdgeMutation";
import UpdateObjectMutation from "./graphql/mutations/common/UpdateObjectMutation";
import CreateObjectMutation from "./graphql/mutations/common/CreateObjectMutation";
import UpdateExternalLinkMutation from "./graphql/mutations/common/UpdateExternalLinkMutation";

import * as foafMutations from "./graphql/mutations/foaf";
import * as geonamesMutations from "./graphql/mutations/geonames";
import * as resourcesMutations from "./graphql/mutations/resources";
import * as projectsMutations from "./graphql/mutations/projects";
import * as commonFragments from "./graphql/fragments/common";
import * as foafFragments from "./graphql/fragments/foaf";
import * as geonamesFragments from "./graphql/fragments/geonames";
import * as resourcesFragments from "./graphql/fragments/resources";
import * as projectsFragments from "./graphql/fragments/projects";

export const GraphQLMutations = {
  RemoveObjectMutation,
  RemoveEdgeMutation,
  UpdateObjectMutation,
  CreateObjectMutation,
  UpdateExternalLinkMutation,
  foaf: foafMutations,
  geonames: geonamesMutations,
  resources: resourcesMutations,
  projects: projectsMutations
};

export const GraphQLFragments = {
  common: commonFragments,
  foaf: foafFragments,
  geonames: geonamesFragments,
  resources: resourcesFragments,
  projects: projectsFragments
};

export {default as MutationAbstract} from "./graphql/mutations/MutationAbstract";

export {
  addNodeInConnectionOptimisticUpdater,
  createNodeInConnectionOptimisticUpdater,
  isOptimisticCreatedRecord,
  createNodeInConnectionUpdater,
  removeNodeFromConnection,
  updateCounters
} from "./graphql/store/helpers";

